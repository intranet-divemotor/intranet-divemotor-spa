var path = require('path')
module.exports = {
  NODE_ENV: '"development"',
  BASE_URL: '"http://localhost:8000"',
  API_URL: '"http://localhost:8000/api"',
  STYLES: '"DV"',
  //MURA CONFIG
  MURA: '"https://cms.intranet.divemotor.com.pe/intranet/index.cfm"',
  MURA_SITE: '"default"',
  communiqueId: '"BAF90EC2-91A1-4D35-A4F868C84E1630B0"',
  noticeContentId: '"718F2DD2-3925-4691-BB366528B837AB0F"',
  infoContentId: '"70C4FEAB-D290-4003-9F42350B9879E4E4"',
  healthId: '"1A352C0A-55B1-49BA-BD6002A517C4F2FF"',
  educationId: '"16E6821F-C1B9-433A-B283D2314D1DA50A"',
  exclusiveId: '"4D114F74-2A0C-4D5C-9C37C77ECFB22C55"',
  lawId: '"7E89EA2B-F2CF-4E92-B0C19EEE68218D8E"',
  docsId: '"6FF200FE-121D-4D09-8BEC2C9F99E06F6B"',
  docsDesaId: '"253E06D9-8156-42BD-81091540543165C0"',
  docsAutosId: '"554A0035-6CC7-44A9-A721D04156E2E416"',
  docsViajesId: '"5E69B7F8-6854-4B93-9B01DB3C23EC653A"',
  docsSegurosId: '"320DE532-AA2A-4B9A-BDCF3FD1CC11ACBE"',
  docsLegalId: '"4CE3234E-C2E2-4521-A990B8D49233B61C"',
  indicators: '"3F1EC49D-71A2-497C-AE688454BA094C2E"',
  docsSaludId: '"D108D117-948A-45ED-B9BE14359A8CB502"',
  manualGuiaId: '"E61B927F-521D-4596-9FB8CB34D87FD4FE"',

  // FIREBASE Credential
  FIREBASE: '"DEV"',
  apiKey: '"AIzaSyBLX5rvVGKC0tcEsoeFqgWKAyAWQtA4Fyk"',
  authDomain: '"orusjobs.firebaseapp.com"',
  databaseURL: '"https://orusjobs.firebaseio.com"',
  projectId: '"orusjobs"',
  storageBucket: '"orusjobs.appspot.com"',
  messagingSenderId: '"690907869500"'
}