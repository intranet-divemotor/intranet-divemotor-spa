var resolve = require('path').resolve
module.exports = {
  NODE_ENV: '"production"',
  BASE_URL: '"http://intranet.divemotor.gimalca.com"',
  API_URL: '"http://intranet.divemotor.gimalca.com/api"',
  FULL_PATH: '"'+ resolve("./") + '"',
  STYLES: '"DV"',
  //MURA CONFIG
  MURA: '"http://cms.intranet.divemotor.com.pe:8888/intranet/index.cfm"',
  MURA_SITE: '"desarrollo"',
  communiqueId: '"CB5EFBBD-5722-4E2F-86BF465280A94E6E"',
  noticeContentId: '"F94E358B-4454-4E1C-8C8F38D6DA88C7D9"',
  infoContentId: '"B007C58D-855D-4B9B-AFA896421255E4A0"',
  healthId: '"48C6A56F-EFC1-4865-836E89C8E98EB6AF"',
  educationId: '"ED25A159-7E35-4DE5-BB072C01145631B5"',
  exclusiveId: '"152C4594-6CF4-44DC-90196C25C6BDE083"',
  lawId: '"7E89EA2B-F2CF-4E92-B0C19EEE68218D8E"',
  indicators: '"3F1EC49D-71A2-497C-AE688454BA094C2E"',
  // FIREBASE Credential
  FIREBASE: '"PROD"',
  apiKey: '"AIzaSyBLX5rvVGKC0tcEsoeFqgWKAyAWQtA4Fyk"',
  authDomain: '"orusjobs.firebaseapp.com"',
  databaseURL: '"https://orusjobs.firebaseio.com"',
  projectId: '"orusjobs"',
  storageBucket: '"orusjobs.appspot.com"',
  messagingSenderId: '"690907869500"'
}
