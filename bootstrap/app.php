<?php

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

require_once __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Load your environment file
|--------------------------------------------------------------------------
|
| You know, to load your environment file.
*/

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();

$app->withEloquent();
//require_once __DIR__.'/../APP/Providers/phpexcel/Classes/PHPExcel.php';
/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(Illuminate\Session\SessionManager::class, function () use ($app) {
  return new Illuminate\Session\SessionManager($app);
});

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    App\Http\Middleware\CORSMiddleware::class,
    Illuminate\Session\Middleware\StartSession::class,
]);

$app->routeMiddleware([
     'auth' => App\Http\Middleware\Authenticate::class,
     'jwt.refres' => \Tymon\JWTAuth\Middleware\RefreshToken::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

 $app->register(App\Providers\AppServiceProvider::class);


// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\GuardServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

// Dingo Adapter for Lumen
$app->register(Zeek\LumenDingoAdapter\Providers\LumenDingoAdapterServiceProvider::class);
$app->register(GrahamCampbell\Markdown\MarkdownServiceProvider::class);
$app->register(Illuminate\Session\SessionServiceProvider::class);

// Lumen Generator disabled it on production if you want
$app->register(Flipbox\LumenGenerator\LumenGeneratorServiceProvider::class);
$app->register(Intervention\Image\ImageServiceProviderLumen::class);
$app->configure('services');


$app->configure('session');

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../routes/auth.php';
    require __DIR__.'/../routes/web.php';
    require __DIR__.'/../routes/event.php';
    require __DIR__.'/../routes/birthday.php';
    require __DIR__.'/../routes/headquarter.php';
    require __DIR__.'/../routes/postulation.php';
    require __DIR__.'/../routes/menu.php';
    require __DIR__.'/../routes/boletas.php';
    require __DIR__.'/../routes/ticket.php';
    require __DIR__.'/../routes/directoryContact.php';
    require __DIR__.'/../routes/region.php';
    require __DIR__.'/../routes/area.php';
    require __DIR__.'/../routes/request.php';
    require __DIR__.'/../routes/profile.php';
    require __DIR__.'/../routes/gallery.php';
    require __DIR__.'/../routes/suggest.php';
    require __DIR__.'/../routes/googleSuite.php';
    require __DIR__.'/../routes/link.php';
    require __DIR__.'/../routes/vacation.php';
    require __DIR__.'/../routes/indicators.php';
    require __DIR__.'/../routes/userHcm.php';
    require __DIR__.'/../routes/modules.php';
    require __DIR__.'/../routes/profiles.php';
    require __DIR__.'/../routes/office.php';
});

return $app;
