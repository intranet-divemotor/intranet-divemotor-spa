<body style="; background-color: #eeeeee; font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';padding: 10px;">
<table cellpadding="0" cellspacing="0" border="0" align="center" style="line-height:25px; background-color: #fff;">
    <tbody>
    <tr>
        <td colspan="3" height="30" style="padding: 20px;">
            <h2 style="color: #0a6aa1;">Respuesta a solicitud de {{ $nombresolicitud }}</h2>
        </td>
    </tr>
    <tr>
        <td width="36"></td>
        <td width="454" align="left" valign="top" style="color:#444444; border-collapse:collapse; font-size:11pt; max-width:454px">
            Estimado/a <b>{{ $nombre }}</b>:<br/>
            <br/>
            @if($tipo < 6)
                La solicitud ha sido {{ $estatus }}.
            @else
                La solicitud ha cambiado de estatus a {{ $estatus }}.
            @endif
            <br/>
            <p style="background-color:#c1e2b3;padding: 15px;">
                <b>Nota a {{ $nombre }}</b><br/>
                {{ $nota }}
            </p>
            <b>Resumen de la solicitud</b>:
            <br/>
            <table cellspacing="0" style="border: 1px solid #eeeeee;width:100%;">
                <tr>
                    <td width="30%" style="background-color:#eeeeee; text-align:right;">Solicitado por&nbsp;</td>
                    <td>&nbsp;{{ $nombre }} {{ $email }}</td>
                </tr>
                <tr>
                    <td width="30%" style="background-color:#eeeeee; text-align:right">Fecha de env&iacute;o&nbsp;</td>
                    <td>&nbsp;{{ $fecha }}</td>
                </tr>
                @if($tipo == 1)
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Fecha y Hora de cita&nbsp;</td>
                        <td>&nbsp;{{ $fechainicio }}&nbsp;{{ $horainicio }}</td>
                    </tr>
                @else
                    @if(isset($fechafin) && isset($horafin) && isset($fechainicio) && isset($horainicio))
                        <tr>
                            <td width="30%" style="background-color:#eeeeee; text-align:right">Fecha y Hora de inicio&nbsp;</td>
                            <td>&nbsp;{{ $fechainicio }}&nbsp;{{ $horainicio }}</td>
                        </tr>
                        <tr>
                            <td width="30%" style="background-color:#eeeeee; text-align:right">Fecha y Hora final&nbsp;</td>
                            <td>&nbsp;{{ $fechafin }}&nbsp;{{ $horafin }}</td>
                        </tr>
                    @elseif(isset($fechafin))
                        <tr>
                            <td width="30%" style="background-color:#eeeeee; text-align:right">Fecha de entrega&nbsp;</td>
                            <td>&nbsp;{{ $fechafin }}</td>
                        </tr>
                    @endif
                @endif
                @if(isset($descripcion))
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Descripci&oacute;n&nbsp;</td>
                        <td>&nbsp;{{ $descripcion }}</td>
                    </tr>
                @endif
                @if(isset($motivo))
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Motivo&nbsp;</td>
                        <td>&nbsp;{{ $motivo }}</td>
                    </tr>
                @endif
                @if(isset($integrantes))
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Participantes&nbsp;</td>
                        <td>
                            @foreach($integrantes as $integrante)
                                <span>{{ $integrante }}</span><br/>
                            @endforeach
                        </td>
                    </tr>
                @endif
                @if(isset($dirigidoa))
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Dirigido a&nbsp;</td>
                        <td>{{ $dirigidoa }}</td>
                    </tr>
                @endif
                @if($tipo > 1 && $tipo < 7 && $estatus == 'Aprobada')
                    <tr>
                        <td colspan="2" align="center" style="padding: 10px">
                            <button type="button" style="border-radius:3px;padding:5px">
                                <a href="https://addtocalendar.com/atc/google?utz=-240&uln=es&vjs=1.5&e[0][date_start]={{str_replace('/', '-', $fechainicio)}}%20{{str_replace(':', '%3A', $horainicio)}}&e[0][date_end]={{str_replace('/', '-', $fechafin)}}%20{{str_replace(':', '%3A', $horafin)}}&e[0][timezone]=America%2FLima&e[0][title]=Reunion%20en%20{{$nombresolicitud}}&e[0][description]={{$motivo}}&e[0][location]={{$nombresolicitud}}&e[0][organizer]={{$nombre}}&e[0][organizer_email]={{str_replace('@', '%40', $email)}}" style="text-decoration:none;">Agregar a Google Calendar</a>
                            </button>
                        </td>
                    </tr>
                @endif
            </table>
            <br/>
            Atentamente,<br/>
            {{ env('APP_NAME') }}
        </td>
        <td width="36"></td>
    </tr>
    <tr>
        <td colspan="3" height="36"></td>
    </tr>
    </tbody>
</table>
</body>