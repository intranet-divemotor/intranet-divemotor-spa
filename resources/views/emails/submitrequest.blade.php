<body style="; background-color: #eeeeee; font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';padding: 10px;">
<table cellpadding="0" cellspacing="0" border="0" align="center" style="line-height:25px; background-color: #fff;">
    <tbody>
    <tr>
        <td colspan="3" height="30" style="padding: 20px;">
            <h2 style="color: #0a6aa1;">Env&iacute;o de solicitud de {{ $nombresolicitud }}</h2>
        </td>
    </tr>
    <tr>
        <td width="36"></td>
        <td width="454" align="left" valign="top" style="color:#444444; border-collapse:collapse; font-size:11pt; max-width:454px">
            Estimado/a <b>{{ $nombre }}</b>:<br/>
            <br/>
            Su solicitud se acaba de enviar al Departamento correspondiente. Ser&aacute; notificado una vez su soliditud cambie de estado.
            <br/><br/>
            <b>Resumen de la solicitud</b>:
            <br/>
            <table cellspacing="0" style="border: 1px solid #eeeeee;width:100%;">
                <tr>
                    <td width="30%" style="background-color:#eeeeee; text-align:right;">Enviado por&nbsp;</td>
                    <td>&nbsp;{{ $nombre }} {{ $email }}</td>
                </tr>
                <tr>
                    <td width="30%" style="background-color:#eeeeee; text-align:right">Fecha de env&iacute;o&nbsp;</td>
                    <td>&nbsp;{{ $fecha }}</td>
                </tr>
                @if($tipo == 1)
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Fecha y Hora de cita&nbsp;</td>
                        <td>&nbsp;{{ $fechainicio }}&nbsp;{{ $horainicio }}</td>
                    </tr>
                @else
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Fecha y Hora de inicio&nbsp;</td>
                        <td>&nbsp;{{ $fechainicio }}&nbsp;{{ $horainicio }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Fecha y Hora final&nbsp;</td>
                        <td>&nbsp;{{ $fechafin }}&nbsp;{{ $horafin }}</td>
                    </tr>
                @endif
                @if(isset($descripcion))
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Descripci&oacute;n&nbsp;</td>
                        <td>&nbsp;{{ $descripcion }}</td>
                    </tr>
                @endif
                @if(isset($motivo))
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Motivo&nbsp;</td>
                        <td>&nbsp;{{ $motivo }}</td>
                    </tr>
                @endif
                @if(isset($integrantes))
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right">Participantes&nbsp;</td>
                        <td>
                            @foreach($integrantes as $integrante)
                                <span>{{ $integrante }}</span><br/>
                            @endforeach
                        </td>
                    </tr>
                @endif
            </table>
            <br/>
            Atentamente,<br/>
            {{ env('APP_NAME') }}
        </td>
        <td width="36"></td>
    </tr>
    <tr>
        <td colspan="3" height="36"></td>
    </tr>
    </tbody>
</table>
</body>