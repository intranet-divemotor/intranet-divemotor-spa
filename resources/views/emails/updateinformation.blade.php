<body style="; background-color: #eeeeee; font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';padding: 10px;">
<table cellpadding="0" cellspacing="0" border="0" align="center" style="line-height:25px; background-color: #fff;">
    <tbody>
    <tr>
        <td colspan="3" height="30" style="padding: 20px;">
            <h2 style="color: #0a6aa1;">Solicitud de Cambio de Datos</h2>
        </td>
    </tr>
    <tr>
        <td width="36"></td>
        <td width="454" align="left" valign="top" style="color:#444444; border-collapse:collapse; font-size:11pt; max-width:454px">
            Estimado/a <b>{{ $colaborator }}</b>:<br/>
            <br/>
            @if(!isset($accion))
                El colaborador <b>{{$nombre}} {{$apellido}}</b> con código <b>{{$hcm}}</b> ha solicitado cambiar los siguientes datos:
            @else
                La solicitud presentada por el colaborador <b>{{$nombre}}</b> ha sido <b>{{$accion}}</b>.
                <br/><br/>
                Datos solicitados:
            @endif
            <br/><br/>
            <table cellspacing="0" style="border: 1px solid #eeeeee;width:100%;">
                @foreach($data as $update)
                    <tr>
                        <td width="30%" style="background-color:#eeeeee; text-align:right;">{{$update->description}}&nbsp;</td>
                        <td>&nbsp;{{ $update->value }}</td>
                    </tr>
                @endforeach
            </table>
            <br/>
            Atentamente,<br/>
            {{ env('APP_NAME') }}
        </td>
        <td width="36"></td>
    </tr>
    <tr>
        <td colspan="3" height="36"></td>
    </tr>
    </tbody>
</table>
</body>