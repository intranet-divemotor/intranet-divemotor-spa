<body style="; background-color: #eeeeee; font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';padding: 10px;">
<table cellpadding="0" cellspacing="0" border="0" align="center" style="line-height:25px; background-color: #fff;">
    <tbody>
    <tr>
        <td colspan="3" height="30" style="padding: 20px;">
            <h2 style="color: #0a6aa1;">Env&iacute;o de solicitud de reservaci&oacute;n de men&uacute;</h2>
        </td>
    </tr>
    <tr>
        <td width="36"></td>
        <td width="454" align="left" valign="top" style="color:#444444; border-collapse:collapse; font-size:11pt; max-width:454px">
            Estimado/a <b>{{ $nombre }}</b>:<br/>
            <br/>
            Su solicitud sobre reserva de menú por prescripción médica se acaba de enviar al comedor correspondiente. Ser&aacute; notificado una vez su solicitud cambie de estado a aprobado o denegado.
            <br/><br/>
            <b>Resumen de la solicitud</b>:
            <br/>
            <table cellspacing="0" style="border: 1px solid #eeeeee;width:100%;">
                <tr>
                    <td width="30%" style="background-color:#eeeeee; text-align:right;">Enviado por&nbsp;</td>
                    <td>&nbsp;{{ $nombre }} {{ $email }}</td>
                </tr>
                <tr>
                    <td width="30%" style="background-color:#eeeeee; text-align:right">Fecha de env&iacute;o&nbsp;</td>
                    <td>&nbsp;{{ $fecha }}</td>
                </tr>
                <tr>
                    <td width="30%" style="background-color:#eeeeee; text-align:right">Descripci&oacute;n&nbsp;</td>
                    <td>&nbsp;{{ $descripcion }}</td>
                </tr>
            </table>
            <br/>
            Atentamente,<br/>
            {{ env('APP_NAME') }}
        </td>
        <td width="36"></td>
    </tr>
    <tr>
        <td colspan="3" height="36"></td>
    </tr>
    </tbody>
</table>
</body>