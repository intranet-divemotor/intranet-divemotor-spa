<body style="; background-color: #eeeeee; font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';padding: 10px;">
  <table cellpadding="0" cellspacing="0" border="0" align="center" style="line-height:25px; background-color: #fff;">
      <tbody>
      <tr>
          <td colspan="3" height="30" style="padding: 20px;">
              <h2 style="color: #0a6aa1;">Nuevo Incidente reportado</h2>
          </td>
      </tr>
      <tr>
          <td width="36"></td>
          <td width="454" align="left" valign="top" style="color:#444444; border-collapse:collapse; font-size:11pt; max-width:454px">
              Estimado/a <b>  </b>: garroyo@divemotor.com.pe <br/>
              <br/> Fecha:  {{ $date }}
              <br/> Hora: {{ $time }}
              <br/>Ubicacion: {{ $ubicacion }}
              <br/>Acciones: {{ $acciones }}
              <br/>Description: {!! $description !!}
              <br/>
              Atentamente,<br/>
              {{ env('APP_NAME') }}
          </td>
          <td width="36"></td>
      </tr>
      <tr>
          <td colspan="3" height="36"></td>
      </tr>
      </tbody>
  </table>
  </body>
