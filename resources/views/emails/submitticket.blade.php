<table cellspacing="0" style="border: 1px solid #eeeeee;width:600px;">
  <tr style="border: none">
    <td colspan="3" height="36" style="border: none">
      Mesa de Ayuda:<br/><br/>

      Se ha ingresado a través de la Intranet el siguiente caso:<br/><br/>
    </td>
  </tr>
  <tr>
    <td rowspan="6" align="center" style="width: 20%;background-color:#aaa;border-bottom: 1px solid #999;"><b>Datos de solicitante</b></td>
    <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 1px 1px 1px 0px;"><b>&nbsp;Solicitante</b></td>
    <td style="border-style: solid;border-color: #999;border-width: 1px 1px 1px 0px;">&nbsp;{{ $data[0]->name.' '.$data[0]->lastName }}</td>
  </tr>
  <tr>
    <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Cargo</b></TD>
    <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;{{ $data[0]->desPosition  }}</td>
  </tr>
  <tr>
    <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Área</b></TD>
    <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;{{ $data[0]->desArea }}</td>
  </tr>
  <tr>
    <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Ubicación</b></td>
    <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;{{ $data[0]->desCeco }}</td>
  </tr>
  <tr>
    <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Teléfono directorio</b></td>
    <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;{{ $phone }}</td>
  </tr>
  <tr>
    <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Teléfono contacto</b></td>
    <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;{{ $contactPhone }}</td>
  </tr>

  <tr>
    <td rowspan="4"  align="center" style="width: 20%;background-color:#aaa;border-bottom: 1px solid #999;"><b>Categoría</b></td>
    <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Tipo de ticket</b></td>
    <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;{{ $data[0]->ticket_type }}</td>
  </tr>
  <tr>
    <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Calificación</b></td>
    <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;{{ $data[0]->classification}}</td>
  </tr>
  <tr>
    <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Servicio</b></td>
    <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;{{ $data[0]->desComponent}}</td>
  </tr>
  <tr>
    <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Máscara del servicio</b></td>
    <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;{{ $data[0]->component}}</td>
  </tr>
  <tr>
    <td align="center" style="width: 20%;height: 30px;background-color:#aaa;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>Descripción</b></td>
    <td colspan="2" style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;{!! $data[0]->description !!}</td>
  </tr>
  <!--tr>
    <td colspan="3" height="36">
      <br>
      - El equipo de {{ env('APP_NAME') }}
    </td>
  </tr-->
  <tr>
    <td colspan="3" height="36"></td>
  </tr>
</table>