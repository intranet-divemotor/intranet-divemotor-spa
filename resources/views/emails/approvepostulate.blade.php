<table cellpadding="0" cellspacing="0" border="0" align="center" style="line-height:25px">
    <tbody>
    <tr>
        <td colspan="3" height="30"></td>
    </tr>
    <tr>
        <td width="36"></td>
        <td width="454" align="left" valign="top" style="color:#444444; border-collapse:collapse; font-size:11pt; font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif'; max-width:454px">
            Hola:<br>
            Te han {{$authorized ? 'autorizado' : 'rechazado'}} tu postulación a <b>{{$event->title}}</b><br>
            <p>
                {{$event->summary}}
            </p>
            <br>
            ¡Gracias!<br>
            - El equipo de {{ env('APP_NAME') }}</td>
        <td width="36"></td>
    </tr>
    <tr>
        <td colspan="3" height="36"></td>
    </tr>
    </tbody>
</table>