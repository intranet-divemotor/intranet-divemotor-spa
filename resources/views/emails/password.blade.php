<table cellpadding="0" cellspacing="0" border="0" align="center" style="line-height:25px">
    <tbody>
        <tr>
            <td colspan="3" height="30"></td>
        </tr>
        <tr>
            <td width="36"></td>
            <td width="454" align="left" valign="top" style="color:#444444; border-collapse:collapse; font-size:11pt; font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif'; max-width:454px">
                Hola:<br>
                <br>
                Alguien ha solicitado recientemente un cambio de contraseña para tu cuenta de {{ env('APP_NAME') }}. Si has sido tú, puedes establecer una nueva contraseña <a href="{{ url('auth/reset/'.$token) }}" target="_blank">desde aquí</a>:<br>
                <br>
    <center><a href="{{ url('auth/reset/'.$token) }}" target="_blank" style="border-radius:3px; color:white; font-size:15px; -moz-border-radius:3px; padding:14px 7px 14px 7px; max-width:210px; font-family:proxima_nova,'Open Sans','lucida grande','Segoe UI',arial,verdana,'lucida sans unicode',tahoma,sans-serif; border:1px #1373b5 solid; text-align:center; -webkit-border-radius:3px; text-decoration:none; width:210px; margin:6px auto; display:block; background-color:#007ee6">Restablecer contraseña</a></center>
    <br>
    Si no quieres cambiar tu contraseña o no lo has solicitado, haz caso omiso de este mensaje y bórralo.<br>
    <br>
    ¡Gracias!<br>
    - El equipo de {{ env('APP_NAME') }}</td>
    <td width="36"></td>
</tr>
<tr>
    <td colspan="3" height="36"></td>
</tr>
</tbody>
</table>