<?php

//codigo => [ httpstatus, mensaje , descripcion]

return [
     "error" => [
        1 => "emailRequired",
        2 => "emailInvalidFormat",
        3 => "EmailNotBelong",
        4 => "passwordRequired",
        5 => "invalidCredentials",
        6 => "couldNotCreateToken",
        7 => "tokenInvalidated",
        8 => "invalidFileType",
        9 => "currentPasswordRequired",
        10 => "newPasswordRequired",
        11 => "confirmPasswordRequired",
        12 => "passwordDoesNotMatch",
        13 => "",
        14 => "permissionDenied",


        15 =>  [
            "error" => "Modules_is_required",
            "message" => "El modulo es requerido"
        ],
        16 =>  [
            "error" => "roleName_is_required",
            "message" => "El nombre del rol de usuario es requerido"
        ],
        17 =>  [
            "error" => "getGivenName_is_required",
            "message" => "El nombre requerido"
        ],
        18 =>  [
            "error" => "getFamilyName_is_required",
            "message" => "El apellido requerido"
        ],
        19 =>  [
            "error" => "documentoid_is_unique",
            "message" => "Documento de identidad ya se encuentra registrado"
        ],
        20 =>  [
            "error" => "fecha_is_unique",
            "message" => "Fecha Cumpleaños es requerido"
        ],
        21 =>  [
            "error" => "auth",
            "message" => "Por favor, ingrese Correo y/o Contraseña y vuelva a intentar."
        ],
        22 => "emailNotSaved",
        23 => "dayNotSaved",
        24 => "requestAlreadyUpdated",
        25 => "requestNotExists",
        26 => "dateInvalid",
        27 => "timeInvalid",
        28 => "userInvalid",
        29 => "fieldsRequired",
        30 => "excelError",
        31 => "userNotFound",
        32 => "fileSizeExceeded",
        33 => "fileExtensionFail",
        34 => "indicatorNotValid",
        //Falla del sistema
        100 => "system_failure"
    ],
    "success" => [
        1 => [
            "error" => "data_save_success",
            "message" => "Datos actualizados con éxito"
        ],
        2 => [
            "error" => "data_save_success",
            "message" => "Datos guardados con éxito"
        ],
        3 => "emailSentSuccess",
        4 => "changePassword_success",
        5 =>[
            "error" => "collaborator_save_success",
            "message" => "Usuario Registrado"
        ],
        6 => "groupCreated",
        7 => "grupUpdate",
        8 => "contactupdate",
        9 => "contactnew",
        10 =>"grupdelect",
        11 =>"ballotsEliminated",
        12 => "ticketUpdated",
        13 => "ticketSaved",
        14 => "requestSaved",
        15 => "requestUpdated",
        16 => "requestDeleted",
        17 => "daySaved",
        18 => "excelSaved",
        19 => "suggestSaved",
        20 => "suggestDeleted",
        21 => "emailSaved",
        30 => "zipLoadSuccessful",
        40 => "userUpdate",
        41 => "albumSaved",
        42 => "albumDeleted",
        43 => "galleryDeleted",
        44 => "albumUpdated",
        45 => "excelLoadSuccessful",
        46 => "indicatorLoaded",
        47 => "sendIncidente"
    ],
  "email" => [
        1 => "Bienvenido a Intranet",
        2 => "Recuperación de tu cuenta Intranet"
    ]
];
