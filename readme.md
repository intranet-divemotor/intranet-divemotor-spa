# Intranet Divemotor

Es una plataforma con múltiples funciones que ofrece a los usuarios una completa flexibilidad y control de herramientas de comunicación, colaboración social y de gestión de equipos para la empresa.

## Desarrollo
Actualmente en desarrollo por Gimalca.

## Requerimientos del sistema
* Nodejs >= 4.0

## Impulsado por
* [Vuetify.js (UX framework)](https://vuetifyjs.com/)
* [Vue.js (JavaScript framework)](https://vuejs.org/)

## Configuración de construccion del proyecto

``` bash
# instalar dependencias
$ npm install
$ composer install

# developer server con hot reload localhost:3000
$ npm run dev

# build para produccion
$ npm run build

# run api server
$ php artisan serve
php -S localhost:8000 -t public/
```


