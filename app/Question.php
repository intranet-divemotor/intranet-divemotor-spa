<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
  protected $table = 'question';
  protected $primaryKey = 'idQuestion';

  protected $fillable = [
    'idQuestion', 'description', 'idTypeRequest', 'type', 'options'
  ];

  public function TypeRequest()
  {
    return $this->belongsTo('App\TypeRequest', 'idType');
  }
}
