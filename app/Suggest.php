<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggest extends Model
{
    protected $table = 'menu_suggest';
    protected $primaryKey = 'idSuggest';

    protected $fillable = [
        'idUser', 'fecha', 'description'
    ];
}
