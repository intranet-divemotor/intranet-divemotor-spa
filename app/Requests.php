<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
    protected $table = 'requests';
    protected $primaryKey = 'idRequest';

    protected $fillable = [
        'idRequest', 'idUser', 'idType', 'idStatus', 'description', 'note'
    ];

    public function Answer()
    {
        return $this->hasMany('App\Answer', 'idRequest');
    }
}
