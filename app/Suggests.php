<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggests extends Model
{
    protected $table = 'suggests';
    protected $primaryKey = 'idSuggest';

    protected $fillable = [
        'idSuggest', 'idUser', 'description', 'purpose', 'note', 'idStatus'
    ];
}
