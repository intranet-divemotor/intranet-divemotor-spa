<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPostulation extends Model
{
    protected $table = 'user_postulation';
    protected $primaryKey = 'id';

    protected $fillable = [
        'idPostulation', 'idUser', 'authorized', 'requested', 'career', 'college', 'stateCollege', 'reasons', 'cvFile', 'created_at'
    ];

    public function Postulation()
    {
        return $this->belongsTo('modules\Postulation\Entities\UserPostulation', 'idPostulation');
    }
}
