<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceComponent extends Model
{
    protected $table = 'service_component';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'description', 'created_at', 'updated_at'];
}
