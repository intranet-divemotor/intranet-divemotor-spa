<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';
    protected $primaryKey = 'idGallery';

    protected $fillable = [
        'idGallery', 'idUser', 'title', 'description', 'path', 'idType', 'idAlbum'
    ];

    public function Album() {
      return $this->belongsTo('App\Album', 'idAlbum');
    }
}
