<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceByServiceType extends Model
{
    protected $table = 'service_by_servicetype';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'idServiceType', 'idService', 'created_at', 'updated_at'];
}
