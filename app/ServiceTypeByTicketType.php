<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceTypeByTicketType extends Model
{
    protected $table = 'servicetype_by_tickettype';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'idTicketType', 'idServiceType', 'created_at', 'updated_at'];
}
