<?php
/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 16-08-2017
 * Time: 12:28 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupDivisions extends Model
{
  protected $table = 'group_divisions';
  protected $primaryKey = 'id';

  protected $fillable = ['id', 'description', 'codes', 'idStatus'];
}