<?php

namespace App\Providers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use App\Entities\User\User;
use App\Entities\User\Userdetail;
ini_set('max_execution_time', 3000);

class Google {

    protected $client;
    protected $service;

    function __construct() {
        /* Get config variables */

        $service_account_name = Config::get('google.service_account_name');
        $key = Config::get('google.api_key'); //you can use later
        $set_subject = Config::get('google.set_subject'); //you can use later
        $setAccessType = Config::get('google.setAccessType'); //you can use later
        $redirectUri = 'http://' . $_SERVER['HTTP_HOST'] . '/google/syncup';

        $this->client = new \Google_Client();
        $this->client->setApplicationName($service_account_name);
        $this->client->setDeveloperKey($key);
        $this->client->setSubject($set_subject);
        $this->client->setAccessType($setAccessType);
        $this->client->setRedirectUri($redirectUri);
        $this->client->setAuthConfig('../client_secret_auth.json');
        $this->client->setScopes([
            'https://www.googleapis.com/auth/admin.directory.user',
            'https://www.googleapis.com/auth/admin.directory.user.readonly',
            'https://www.googleapis.com/auth/admin.directory.customer'
        ]);

        $this->service = new \Google_Service_Directory($this->client); //Test with Books Service
    }

    //listar todos los email de un directorio
    public function usersall() {
        if (isset($_GET['code'])) {
            //cambiar code por token
            $this->client->authenticate($_GET['code']);
            $_SESSION['access_token'] = $this->client->getAccessToken();
        }if (isset($_SESSION['access_token'])) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $result = array();
            $pageToken = NULL;
            do {
                try {
                    $parameters = array(
                        'domain' => env('GOOGLE_APP_DOMAIN', 'divemotor.com.pe'),
                        'customer' => 'my_customer',
                        'maxResults' => 500,
                        'orderBy' => 'email',
                    );
                    if ($pageToken) {
                        $parameters['pageToken'] = $pageToken;
                    }
                    $users = $this->service->users->listUsers($parameters);
                    $result = array_merge($result, $users->getUsers());
                    $pageToken = $users->getNextPageToken();
                } catch (Exception $e) {
                    print "An error occurred: " . $e->getMessage();
                    $pageToken = NULL;
                }
            } while ($pageToken);

            foreach ($result as $user) {

                $email = strtolower($user->getPrimaryEmail());
                $verify = $this->verifydomain($email,$user->name->givenName); //verify domain intranet

                $userModel = User::where('email','=', $email)->first();
                //editar usuario que ya esta en google
                if ($userModel) {
                    $userModel->idStatus = $verify == true ? 2 : 1;
                    $userModel->avatar = $user->thumbnailPhotoUrl == "" ? null : $user->thumbnailPhotoUrl;
                    if($userModel->save()) {
                        $userdetail = Userdetail::where('idUser', '=', $userModel->idUser)->first();
                        if($userdetail){
                           $userdetail->addresses = json_encode($user->addresses);
                           $userdetail->phones = json_encode($user->phones);
                           $userdetail->save();
                        }
                    }
                } //new user
                elseif (!$userModel and $verify == false) {
                    $userModel = new User;
                    $userModel->name = $user->name->givenName;
                    $userModel->lastName = $user->name->familyName;
                    $userModel->mothersLastName = $user->name->givenName . ' ' . $user->name->familyName;
                    $userModel->email = $email;
                    $userModel->password = Hash::make(123456);
                    $userModel->idRole = 2;
                    $userModel->idStatus = 1;
                    $userModel->avatar = $user->thumbnailPhotoUrl == "" ? null : $user->thumbnailPhotoUrl;

                    if ($userModel->save()) {
                        $userdetail = new Userdetail;
                        $userdetail->idUser = $userModel->idUser;
                        $userdetail->colaborador = $user->name->givenName . $user->name->familyName;        //NOMBRE COMPLETO COLABORADOR
                        $userdetail->addresses = isset($user->addresses) ? json_encode($user->addresses) : null ;
                        $userdetail->phones = isset($user->phones) ? json_encode($user->phones) : null;
                        $userdetail->save();
                    }
                }
            }
          echo '<script>window.close();</script>';
        } else {
            $loginUrl = $this->client->createAuthUrl();
            header("Location: $loginUrl");
        }
    }

    function verifydomain($email,$name) {

        $findme = "vfe.";
        $findmeName = "VFE.";
        $pos = strpos($email, $findme);
        $posName = strpos($name, $findmeName);
        if ($pos === false || $posName === false) {
            //el email no pertenece a la intranet
            $response = false;
        } else {
            // el email pertenece a la intranet
            $response = true;
            //echo " y existe en la posición $pos";
        }
        return $response;
    }
}
