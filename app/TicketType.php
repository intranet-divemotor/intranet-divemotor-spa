<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketType extends Model
{
    protected $table = 'ticket_type';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'description', 'created_at', 'updated_at'];
}
