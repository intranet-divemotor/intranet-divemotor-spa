<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationLevels extends Model
{
    protected $table = 'organization_levels';
    protected $primaryKey = 'levelsid';

    protected $fillable = [
        'nameLevel'
    ];
}
