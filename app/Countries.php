<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'countries';
    protected $primaryKey = 'idCountries';

    protected $fillable = [
        'codeCountry', 'country'
    ];
}
