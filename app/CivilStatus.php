<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CivilStatus extends Model
{
    protected $table = 'civilStatus';
    protected $primaryKey = 'civilStatusId';

    protected $fillable = [
        'civilStatusId', 'civilStatus'
    ];
}
