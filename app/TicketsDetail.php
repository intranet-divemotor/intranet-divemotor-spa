<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketsDetail extends Model
{
    protected $table = 'tickets_detail';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'id_ticket', 'idTicketType', 'idServiceType', 'idService', 'idComponent', 'idSpecification', 'subject', 'description', 'file', 'created_at', 'updated_at'];
}
