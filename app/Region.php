<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'organization_region';
    protected $primaryKey = 'idRegion';

    protected $fillable = [
        'description'
    ];
}
