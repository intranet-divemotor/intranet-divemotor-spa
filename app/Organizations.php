<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizations extends Model
{
    protected $table = 'organization';
    protected $primaryKey = 'corporativoId';

    protected $fillable = [
        'codRelational','nameStructure','levelsid','status','idUser','codFather','zone','codeCountry', 'idRegion'
    ];
}
