<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $primaryKey = 'idMenu';

    protected $fillable = [
        'idMenu', 'created_by', 'created_at', 'idStatus', 'idSucursal', 'file'
    ];

    public function MenuDetalle(){
        return $this->hasMany('modules\Notice\Entities\MenuDetalle', 'idMenu');
    }
}
