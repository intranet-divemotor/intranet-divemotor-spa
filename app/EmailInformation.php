<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailInformation extends Model
{
    protected $table = 'email_information';
    protected $primaryKey = 'idEmail';

    protected $fillable = [
        'idEmail', 'description'
    ];
}
