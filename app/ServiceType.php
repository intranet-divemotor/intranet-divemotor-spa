<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    protected $table = 'service_type';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'description', 'created_at', 'updated_at'];
}
