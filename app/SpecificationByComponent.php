<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecificationByComponent extends Model
{
    protected $table = 'specification_by_component';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'idComponent', 'idSpecification', 'created_at', 'updated_at'];
}
