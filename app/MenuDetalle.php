<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuDetalle extends Model
{
    protected $table = 'menu_detalle';
    protected $primaryKey = 'idDetalle';

    protected $fillable = [
        'hora', 'tipo', 'comida', 'fecha'
    ];

    public function Menu(){
        return $this->belongsTo('modules\Notice\Entities\Menu', 'idMenu');
    }
}
