<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComponentByService extends Model
{
    protected $table = 'component_by_service';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'idService', 'idComponent', 'created_at', 'updated_at'];
}
