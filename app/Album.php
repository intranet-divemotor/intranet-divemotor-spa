<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'album';
    protected $primaryKey = 'idAlbum';

    protected $fillable = [
        'idAlbum', 'idUser', 'title', 'description', 'coverPhoto', 'idType', 'idStatus'
    ];

    public function Gallery() {
      return $this->hasMany('App\Gallery', 'idAlbum');
    }
}
