<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specification extends Model
{
    protected $table = 'specification';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'description', 'created_at', 'updated_at'];
}
