<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answer';
    protected $primaryKey = 'idAnswer';

    protected $fillable = [
        'idAnswer', 'description', 'idQuestion'
    ];

    public function Requests()
    {
        return $this->belongsTo('App\Requests', 'idRequest');
    }
}
