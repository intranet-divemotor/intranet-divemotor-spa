<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'menu_reservation';
    protected $primaryKey = 'idReservation';

    protected $fillable = ['idUser', 'description', 'idStatus'];
}
