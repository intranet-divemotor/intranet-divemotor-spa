<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpdateInformation extends Model
{
    protected $table = 'update_information';
    protected $primaryKey = 'idUpdate';

    protected $fillable = [
        'idUpdate', 'idUser', 'description', 'value', 'idStatus'
    ];
}
