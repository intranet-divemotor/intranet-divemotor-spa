<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';
    protected $primaryKey = 'cod';

    protected $fillable = [
        'cod', 'area'
    ];
}
