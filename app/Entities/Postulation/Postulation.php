<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postulation extends Model
{
    protected $table = 'postulation';
    protected $primaryKey = 'id';

    protected $fillable = [
       'id', 'idUser', 'areaId', 'departmentId', 'title', 'summary', 'detail', 'coverPhoto', 'startDate', 'endDate', 'created_at', 'functions', 'requirements', 'extraFile'
    ];


    public function Postulates()
    {
        return $this->hasMany('modules\Postulation\Entities\UserPostulation', 'idPostulation');
    }
}
