<?php
/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 24-08-2017
 * Time: 12:27 AM
 */

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TicketServices extends Model
{
  protected $table = 'ticket_services';
  protected $primaryKey = 'id';

  protected $fillable = [
    'id', 'description', 'mask', 'type', 'idStatus', 'codeSolman',
  ];
}
