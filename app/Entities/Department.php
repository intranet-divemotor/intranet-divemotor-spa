<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'department';
    protected $primaryKey = 'departmentId';

    protected $fillable = [
        'departmentId', 'department'
    ];
}
