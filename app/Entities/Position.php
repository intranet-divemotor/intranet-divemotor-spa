<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'position';
    protected $primaryKey = 'positionId';

    protected $fillable = [
        'positionId', 'position'
    ];
}
