<?php

/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 30-08-2017
 * Time: 01:38 PM
 */

namespace App\Entities\Indicators;

use Illuminate\Database\Eloquent\Model;

class Indicators extends Model
{
  protected $table = 'indicators';
  protected $primaryKey = 'id';

  protected $fillable = ['id', 'type', 'title', 'comment', 'idStatus'];
}