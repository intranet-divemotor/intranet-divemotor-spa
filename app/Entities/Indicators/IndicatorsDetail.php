<?php

/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 30-08-2017
 * Time: 01:38 PM
 */

namespace App\Entities\Indicators;

use Illuminate\Database\Eloquent\Model;

class IndicatorsDetail extends Model
{
  protected $table = 'indicators_detail';
  protected $primaryKey = 'id';

  protected $fillable = ['id', 'idIndicator', 'brand', 'percentage', 'color'];
}