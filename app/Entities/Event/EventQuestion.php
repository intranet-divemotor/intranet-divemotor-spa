<?php

namespace App\Entities\Event;

use Illuminate\Database\Eloquent\Model;

class EventQuestion extends Model
{
    protected $table = 'event_question';
    protected $primaryKey = 'id';

    protected $fillable = [
        'idEvent', 'description', 'type', 'options'
    ];

    public function Event()
    {
        return $this->belongsTo('App\Event\Entities\Event', 'id');
    }
}
