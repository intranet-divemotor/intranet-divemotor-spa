<?php

namespace App\Entities\Event;

use Illuminate\Database\Eloquent\Model;

class EventUser extends Model
{
    protected $table = 'event_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'idUser', 'idEvent'
    ];
}
