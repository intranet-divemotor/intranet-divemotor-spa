<?php

namespace App\Entities\Event;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'event';
    protected $primaryKey = 'id';

    protected $fillable = [
        'idUser', 'region', 'headquarters', 'importance', 'areas', 'title', 'summary', 'detail', 'coverPhoto', 'startDate', 'endDate', 'hour', 'hourend', 'created_at', 'location'
    ];

    public function Question()
    {
        return $this->hasMany('App\Entities\Event\EventQuestion', 'idEvent');
    }
}
