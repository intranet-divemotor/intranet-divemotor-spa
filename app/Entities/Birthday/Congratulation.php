<?php

namespace App\Entities\Birthday;

use Illuminate\Database\Eloquent\Model;

class Congratulation extends Model
{
    protected $table = 'congratulation';
    protected $primaryKey = 'id';

    protected $fillable = [
        'idUser', 'parent', 'comment', 'like', 'created_at'
    ];
}
