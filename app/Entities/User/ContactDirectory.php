<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\Model;

class ContactDirectory extends Model
{
    protected $table = 'directory_contact';
    protected $primaryKey = 'directoryId';

    protected $fillable = [
        'idUser', 'idUserContact','typeContact'
    ];
}
