<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\Model;

class TypeDirectory extends Model
{
    protected $table = 'type_contact';
    protected $primaryKey = 'typeContact';

    protected $fillable = [
        'typeContact', 'idUser'
    ];
}
