<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Sexo extends Model
{
    protected $table = 'sexo';
    protected $primaryKey = 'sexoId';

    protected $fillable = [
        'sexoId', 'sexo'
    ];
}
