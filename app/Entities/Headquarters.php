<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Headquarters extends Model
{
    protected $table = 'headquarters';
    protected $primaryKey = 'id';

    protected $fillable = [
        'idUser', 'title', 'summary', 'detail', 'coverPhoto', 'location', 'primaryPhone', 'regionId'
    ];
}
