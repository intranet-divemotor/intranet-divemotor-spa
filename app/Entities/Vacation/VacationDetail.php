<?php
namespace App\Entities\Vacation;

use Illuminate\Database\Eloquent\Model;

class VacationDetail extends Model
{
  protected $table = 'vacation_detail';
  protected $primaryKey = 'id';

  protected $fillable = [
    'id', 'idVacation', 'hcmCollaborator', 'hcmHigher', 'contractDate', 'startPeriod', 'endPeriod', 'vacationWon', 'vacationTaken', 'vacationPending'
  ];

  public function Userdetail()
  {
    return $this->belongsTo('App\Entities\User\Userdetail', 'PosicionHCM');
  }
}