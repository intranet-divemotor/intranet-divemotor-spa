<?php
namespace App\Entities\Vacation;

use Illuminate\Database\Eloquent\Model;

class Vacation extends Model
{
  protected $table = 'vacation';
  protected $primaryKey = 'id';

  protected $fillable = [
    'id', 'idUser', 'description', 'file', 'idStatus'
  ];
}