<?php
/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 24-08-2017
 * Time: 12:27 AM
 */

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserSolman extends Model
{
  protected $table = 'user_solman';
  protected $primaryKey = 'id';

  protected $fillable = [
    'id', 'idUserSolman', 'email', 'region', 'pais', 'idStatus'
  ];
}