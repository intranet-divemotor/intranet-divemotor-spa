<?php

namespace App\Entities\Link;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'link';
    protected $primaryKey = 'id';

    protected $fillable = [
        'areaId', 'name', 'urlPath', 'iconPath', 'type'
    ];
}
