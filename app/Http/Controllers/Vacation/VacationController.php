<?php
/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 11-08-2017
 * Time: 11:04 AM
 */

namespace App\Http\Controllers\Vacation;

use App\Entities\User\User;
use App\Entities\User\Userdetail;
use App\Entities\Vacation\Vacation;
use App\Entities\Vacation\VacationDetail;
use App\Http\Controllers\SystemLink\SystemLinkController;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\File;
use Validator;
use Excel;
use Auth;
use DB;

class VacationController extends Controller
{
  public function submit(Request $request)
  {
    try {
      $validator = Validator::make($request->all(), ["excel" => "mimes:xls,xlsx"], ["excel.mimes" => trans('api.error.33')]);
      if ($validator->fails()) return response()->json(['error' => $validator->errors()->first()], 400);

      $user = JWTAuth::parseToken()->authenticate();
      $sc = new SystemLinkController();
      DB::beginTransaction();

      if ($request->hasFile('excel')) {
        VacationDetail::truncate();
        Vacation::truncate();

        $file = $request->file('excel');
        $path = $file->getRealPath();
        $objPHPExcel = \PHPExcel_IOFactory::load($path);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highestRow = $objWorksheet->getHighestRow();

        $nameFile = preg_replace('/\s+/', '', $sc->removeAccented($file->getClientOriginalName()));

        $vacation = new Vacation;
        $vacation->idUser = $user->idUser;
        $vacation->description = strtolower($file->getClientOriginalName());
        $vacation->file = $nameFile;
        $vacation->idStatus = 1;

        if($vacation->save()) {
          for ($row = 2; $row <= $highestRow; ++$row) {
            $fCell = strpos($objWorksheet->getCell('F'.$row)->getValue(), '.') > -1 ? strtotime(str_replace('.', '-', $objWorksheet->getCell('F'.$row)->getValue())) : \PHPExcel_Shared_Date::ExcelToPHP($objWorksheet->getCell('F'.$row)->getValue());
            $jCell = strpos($objWorksheet->getCell('J'.$row)->getValue(), '.') > -1 ? strtotime(str_replace('.', '-', $objWorksheet->getCell('J'.$row)->getValue())) : \PHPExcel_Shared_Date::ExcelToPHP($objWorksheet->getCell('J'.$row)->getValue());
            $kCell = strpos($objWorksheet->getCell('K'.$row)->getValue(), '.') > -1 ? strtotime(str_replace('.', '-', $objWorksheet->getCell('K'.$row)->getValue())) : \PHPExcel_Shared_Date::ExcelToPHP($objWorksheet->getCell('K'.$row)->getValue());
            $insert[] = [
              'idVacation' => $vacation->id,
              'hcmCollaborator' => $objWorksheet->getCell('A'.$row)->getValue(),
              'hcmHigher' => $objWorksheet->getCell('G'.$row)->getValue(),
              'contractDate' => date('Y-m-d', $fCell),
              'startPeriod' => date('Y-m-d', $jCell),
              'endPeriod' => date('Y-m-d', $kCell),
              'vacationWon' => intval($objWorksheet->getCell('L'.$row)->getValue()),
              'vacationTaken' => intval($objWorksheet->getCell('M'.$row)->getValue()),
              'vacationPending' => intval($objWorksheet->getCell('N'.$row)->getValue()),
            ];
          }
        }
        $toInsert = VacationDetail::insert($insert);
        if($toInsert) {
          File::deleteDirectory(base_path('public').'/files/vacation');
          $file->move('files/vacation/', $nameFile);
        }
      }
      DB::commit();
      return response()->json(["success" => trans('api.success.18')]);
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(["error" => trans('api.error.30'), "trace" => $ex->getTrace()], 400);
    }
  }

  public function getVacations(Request $request) {
    return response()->json(Vacation::get(['description', 'created_at', 'file']));
  }

  public function getMyVacations(Request $request) {
    return response()->json(VacationDetail::where('hcmCollaborator', $request->input('hcm'))->get());
  }

  public function getColVacations(Request $request) {
    $users = Userdetail::leftJoin('user', 'user_detail.idUser', '=', 'user.idUser')
      ->where('codBoss', $request->input('hcm'))->get(['posicionHCM', 'name', 'lastName']);
    foreach ($users as $user) {
      $colvacation = [];
      $totalVacWon = $totalVacTak = $totalVacPen = 0;
      $vacations = VacationDetail::where('hcmCollaborator', $user->posicionHCM)->get();
      foreach ($vacations as $vacation) {
        $totalVacWon += $vacation->vacationWon;
        $totalVacTak += $vacation->vacationTaken;
        $totalVacPen += $vacation->vacationPending;
        array_push($colvacation, $vacation);
      }
      $user->{'vacation'} = $colvacation;
      $user->{'totalVacWon'} = $totalVacWon;
      $user->{'totalVacTak'} = $totalVacTak;
      $user->{'totalVacPen'} = $totalVacPen;
    }
    return response()->json($users);
    /*return response()->json(Userdetail::leftJoin('user', 'user_detail.idUser', '=', 'user.idUser')
      ->with(['VacationDetail' => function($query) {
        $query->select('hcmCollaborator', 'hcmHigher', 'contractDate', 'startPeriod', 'endPeriod', 'vacationWon', 'vacationTaken', 'vacationPending');
      }])
      ->where('higherPosition', $request->input('hcm'))
      ->get(['PosicionHCM', 'user.name', 'user.lastName']));*/
  }
}