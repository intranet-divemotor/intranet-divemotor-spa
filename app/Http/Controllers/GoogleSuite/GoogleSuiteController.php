<?php


namespace App\Http\Controllers\GoogleSuite;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Providers\Google;


class GoogleSuiteController extends Controller
{
  /**
   * Remove the specified resource from storage.
   * @return Response
   */
  public function syncup(Google $google)
  {
    $google->usersall();
    print_r($google);
  }


}
