<?php

namespace App\Http\Controllers\Headquarters;

use App\Divisions;
use App\GroupDivisions;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Entities\Headquarters;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\SystemLinkController;
use App\Entities\Organizations;
use Carbon\Carbon;
use File;
use Intervention\Image\Facades\Image;


class HeadquartersController extends Controller
{
  /*public function fetchSelect(Request $r)
  {
    $result = Organizations::join('organization_region', 'organization.idRegion', '=', 'organization_region.idRegion')
      ->where('nameStructure', 'ilike', '%' . $r->input('q') . '%')
      ->orderBy('codRelational', 'asc');
    if (!empty($r->input('level'))) $result->where('levelsid', $r->level);
    if (!empty($r->input('id'))) $result->where('corporativoId', $r->input('id'));
    if (!empty($r->input('region'))) $result->where('organization_region.idRegion', $r->input('region'));
    if (!empty($r->input('limit'))) $result->limit($r->input('limit'));
    return json_encode($result->get(['corporativoId as value', 'nameStructure as text', 'codRelational as code', 'organization_region.description as region']));
  }*/

  public function fetchSelect(Request $r)
  {
    $result = Divisions::where('divisions.description', 'ilike', '%' . $r->input('q') . '%')
      ->orderBy('divisions.description', 'asc');
    if (!empty($r->input('id'))) $result->where('divisions.code', $r->input('id'));
    if (!empty($r->input('limit'))) $result->limit($r->input('limit'));
    return json_encode($result->get(['code as value', 'description as text']));
  }

  public function fetchSelectGroup(Request $r)
  {
    return json_encode(GroupDivisions::orderBy('description')->get(['id as value', 'description as text']));
  }

  public function fetchForAll(Request $r)
  {
    if (!empty($r->id)) return json_encode(Organizations::where('corporativoId', $r->id)->get());
    else if (!empty($r->idRegion)) return json_encode(Organizations::where('idRegion', $r->idRegion)->get());
    else return json_encode(Organizations::all());
  }

  public function fetchHeadquarters(Request $r)
  {
    $p = Headquarters::limit($r->limit)->offset($r->offset)
      ->join('organization_region', 'organization_region.idRegion', '=', 'headquarters.regionId');

    $total = count(Headquarters::count());
    $links = $p->get();
    return array('total' => $total, 'rows' => $links);
  }

  public function submit(Request $r)
  {
    $user = JWTAuth::parseToken()->authenticate();
    $sc = new SystemLinkController();
    $pathEvent = Storage::disk('headquarters')->getDriver()->getAdapter()->getPathPrefix();
    try {
      if ($r->id == 0) {
        $fileName = preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp . $r->file('coverPhoto')->getClientOriginalName()));
        $img = Image::make(file_get_contents($r->file('coverPhoto')));
        $img->crop((int)$r->cropWidth, (int)$r->cropHeight, (int)$r->startX, (int)$r->startY);
        $img->save($pathEvent . $fileName, 60);
        $exists = Storage::disk('headquarters')->exists($fileName);
        if ($exists) {
          $p = new Headquarters();
          $p->idUser = $user->idUser;
          $p->regionId = $r->regionId;
          $p->title = $r->title;
          $p->summary = $r->summary;
          $p->detail = $r->detail;
          $p->coverPhoto = $fileName;
          $p->location = $r->location;
          $p->primaryPhone = $r->primaryPhone;
          if ($p->save()) {
            return ['success' => trans('layouts::api.success.2.message')];
          } else {
            return ['error' => trans('layouts::api.error.100.error')];
          }
        }
      } else {
        $p = Headquarters::find($r->id);
        $p->idUser = $user->idUser;
        $p->regionId = $r->regionId;
        $p->title = $r->title;
        $p->summary = $r->summary;
        $p->detail = $r->detail;
        $p->location = $r->location;
        $p->primaryPhone = $r->primaryPhone;

        if ($r->hasFile('coverPhoto')) {
          $fileName = preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp . $r->file('coverPhoto')->getClientOriginalName()));
          Storage::Delete(unlink(public_path('File/headquarters/' . $r->fileName)));
          $img = Image::make(file_get_contents($r->file('coverPhoto')));
          $img->crop((int)$r->cropWidth, (int)$r->cropHeight, (int)$r->startX, (int)$r->startY);
          $img->save($pathEvent . $fileName, 60);
          $p->coverPhoto = $fileName;
        }

        if ($p->save()) {
          return ['success' => trans('layouts::api.success.2.message')];
        } else {
          return ['error' => trans('layouts::api.error.100.error')];
        }
      }
    } catch (\Exception $e) {
      return $e->getMessage();
    }
  }

  public function deleteHeadquarters(Request $r)
  {
    $e = Headquarters::find($r->id);
    Storage::Delete(unlink(public_path('File/headquarters/' . $e->coverPhoto)));
    $e->delete();
  }
}
