<?php

namespace App\Http\Controllers;

use App\Entities\User\Userdetail;
use App\GroupDivisions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
//use League\Flysystem\Exception;
use App\Organizations;
use App\Region;
use App\Entities\User\User;
use App\Menu;
use Validator;
use Excel;
use Auth;
use DB;
use App\MenuDetalle;
use App\Reservation;
use App\Suggest;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\SystemLink\SystemLinkController;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;

class MenuController extends Controller
{
  /**
   * @param Request $request
   * @return mixed
   */
  public function importExcel(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    if ($request->hasFile('menu') && strlen($request->input('idsucursal')) > 0) {
      $path = $request->file('menu')->getRealPath();

      $objPHPExcel = \PHPExcel_IOFactory::load($path);
      $objWorksheet = $objPHPExcel->getActiveSheet();

      $locale = 'es';
      $validLocale = \PHPExcel_Settings::setLocale($locale);
      if (!$validLocale) {
        print_r('Localidad Invalida');
      }

      $array_fecha[] = [];
      $highestColumnAsLetters = $objWorksheet->getHighestColumn();
      $highestRow = $objWorksheet->getHighestRow();
      $formula = \PHPExcel_Calculation::getInstance();
      $formula->disableCalculationCache();
      $comidasXls = [];
      $mes = $objWorksheet->getCell('C3')->getValue();
      $mes = strlen($mes) == 1 ? '0' . $mes : $mes;
      $aio = $objWorksheet->getCell('C4')->getValue();
      $tdy = strtotime(date('Y-m-d'));
      $lastDay = $mes == '02' ? (date('L', $aio) == 1 ? '29' : '28') : date('t', $mes);
      $primeraFecha = strtotime(date("$aio-$mes-1"));
      $ultimaFecha = strtotime("$aio-$mes-$lastDay");
      $this->deletePrevMenu($request->input('idsucursal'), date("$aio-$mes-01"), date("$aio-$mes-$lastDay"));

      $mesCurso = ($tdy >= $primeraFecha && $tdy <= $ultimaFecha) ? 's' : 'n';
      $menu = new Menu;
      $menu->created_by = intval($user->idUser);
      //$menu->created_at = date('Y-m-d');
      $menu->idStatus = 1;
      /*$menu->idSucursal = $request->input('idsucursal');*/
      $menu->idSucursal = $request->input('idsucursal');
      $menu->file = $request->file('menu')->getClientOriginalName();
      $fechas = [];
      if ($menu->save()) {
        $insert = [];
        for ($columnAsLetters = 'D'; $columnAsLetters != $highestColumnAsLetters; $columnAsLetters++) {
          $translate = $objWorksheet->getCell($columnAsLetters . '7')->getValue(); //Obtenemos la fecha
          $array_fecha[] = [];
          $fecha = '';
          if (!empty($translate)) {
            $toTranslate = $this->translateFormula($translate, $formula);
            $result = $objWorksheet->getCell($columnAsLetters . '7')
              ->setValue($toTranslate)
              ->getOldCalculatedValue();
            $fecha = date('Y-m-d', strtotime(\PHPExcel_Style_NumberFormat::toFormattedString($result, 'Y-m-d')));

            $fechaToTime = strtotime($fecha);

            if ($fechaToTime < $primeraFecha || $fechaToTime > $ultimaFecha) {
              $fecha = '';
            }
          }
          if (!empty($fecha) && !in_array($fecha, $fechas)) {
            $array_fecha[$fecha] = [];
            array_push($fechas, $fecha);
            $hora = '';
            $array_hora[] = [];
            $horaComida = ['desayuno', 'almuerzo', 'cena'];
            for ($row = 8; $row <= $highestRow; ++$row) {
              $value = $objWorksheet->getCell($columnAsLetters . $row)->getValue();
              if (!empty($value)) {
                $tipo = $objWorksheet->getCell('C' . $row)->getValue();

                if (in_array(strtolower($value), $horaComida)) { //Es hora comida
                  $hora = strtolower($value);
                  $index = array_search($hora, $horaComida);
                  unset($horaComida[$index]);
                  $array_hora[$hora] = [];
                } elseif (!in_array(strtolower($tipo), $horaComida)
                  && strtolower($tipo) != strtolower($hora)
                ) {
                  array_push($insert, [
                    'idMenu' => $menu->idMenu,
                    'hora' => $hora,
                    'tipo' => $tipo,
                    'comida' => $value,
                    'fecha' => $fecha,
                  ]);

                  $comidasXls[$tipo] = $value;
                  array_push($array_hora[$hora], $comidasXls);
                  unset($comidasXls);
                }
              } else {
                DB::rollBack();
                return response()->json(["error" => trans('api.error.30')], 400);
              }
            }
            $array_hora = array_filter($array_hora);
            array_push($array_fecha[$fecha], $array_hora);
          }
        }
        MenuDetalle::insert($insert);
        $array_fecha = array_filter($array_fecha);
      }
    } else {
      return response()->json(["error" => trans('api.error.30')], 400);
    }
    return response()->json(["success" => trans('api.success.18'), "data" => json_encode($array_fecha), "mescurso" => $mesCurso]);
  }

  /**
   * @param $sucursal
   * @param $primeraFecha
   * @param $ultimaFecha
   */
  public function deletePrevMenu($sucursal, $primeraFecha, $ultimaFecha)
  {
    try {
      $menu = Menu::where('idSucursal', $sucursal)
        ->join('menu_detalle', 'menu.idMenu', '=', 'menu_detalle.idMenu')
        ->whereBetween('fecha', [$primeraFecha, $ultimaFecha])
        ->first(['menu.idMenu']);
      if (count($menu) > 0) {
        MenuDetalle::where('idMenu', $menu->idMenu)->delete();
        Menu::find($menu->idMenu)->delete();
      }
    } catch (\Exception $ex) {
      // throwException($ex);
      return response()->json(["error" => trans('api.error.30')], 400);
    }
  }

  /**
   * @param $formula
   * @param $excelCalculation
   * @return mixed|string
   */
  public function translateFormula($formula, $excelCalculation)
  {
    $toTranslate = '';
    $toTranslate = $excelCalculation->_translateFormulaToLocale($formula);
    $toTranslate = str_replace('WORKDAY', 'DIA.LAB', $toTranslate);
    $toTranslate = str_replace('_xlfn.', '', $toTranslate);

    return $toTranslate;
  }

  /**
   * @return mixed
   */
  public function getCurrentMenu()
  {
    $user = JWTAuth::parseToken()->authenticate();
    $sede = $this->getIdDivision($user->idUser);
    $lastDay = date('t', strtotime(date("Y-m-t")));
    $primeraFecha = date("Y-m-01 00:00:00");
    $ultimaFecha = date("Y-m-$lastDay 00:00:00");

    $menus = DB::select('select "idDetalle", menu."idMenu", hora, tipo, comida, fecha
      from menu_detalle
      inner join menu on menu_detalle."idMenu" = menu."idMenu"
      where menu."idSucursal" = \''.$sede.'\'
        and hora != \'desayuno\'
        and fecha between \''.$primeraFecha.'\' and \''.$ultimaFecha.'\';');

    $fechas = [];
    $eventMenu = [];
    $foods = [];
    $nameHora = '';
    $change = false;
    foreach ($menus as $menu) {
      $minFecha = date('Y-m-d', strtotime($menu->fecha));
      if (!in_array($minFecha, $fechas)) {
        array_push($fechas, $minFecha);
      }
    }

    foreach ($fechas as $fecha) {
      $horas = [];
      $menudd = array();

      foreach ($menus as $k => $menu) {
        $minFecha = date('Y-m-d', strtotime($menu->fecha));
        if ($fecha == $minFecha) {
          if (!in_array($menu->hora, $horas)) {
            if ($k > 0 && count($foods) > 0) {
              $menudd[] = array('foods' => $foods, 'schedule' => $nameHora);
            }
            $foods = [];
            $nameHora = $menu->hora;
            array_push($horas, $nameHora);
            $change = true;
          }
          array_push($foods, [$menu->tipo => $menu->comida]);
        }
      }
      if ($change && $nameHora == 'cena') {
        $menudd[] = array('foods' => $foods, 'schedule' => $nameHora);
        $foods = [];
        $nameHora = '';
        $change = false;
      }
      array_push($eventMenu, ['title' => 'Menu del Dia', 'start' => $fecha, 'food' => $menudd]);
    }
    return ["success" => $eventMenu];
  }

  /**
   * @param Request $request
   * @return string
   */
  public function getHistoryMenu(Request $request)
  {
    $limit = $request->input('limit');
    //$limit  = $request->limit;
    //$offSet = $request->offset;
    $offSet = $request->input('offset');

    $total = count(DB::select('select file, description as "nameStructure", "idMenu"
      from Menu
      inner join group_divisions on menu."idSucursal" = group_divisions.id::text'));
    $menu = DB::select('select file, description as "nameStructure", "idMenu"
      from Menu
      inner join group_divisions on menu."idSucursal" = group_divisions.id::text
      limit '.$limit.' offset '.$offSet);
    $arrayQuery = array('total' => $total, 'rows' => $menu);

    return json_encode($arrayQuery);
  }

  /**
   * @param Request $request
   */
  public function getMenuOrganization(Request $request)
  {
    $menus = Menu::where('menu.idMenu', $request->input('idmenu'))
      ->join('menu_detalle', 'menu.idMenu', '=', 'menu_detalle.idMenu')
      ->get(['idDetalle', 'menu.idMenu', 'hora', 'tipo', 'comida', 'fecha']);

    $fechas = [];
    $eventMenu = [];
    $foods = [];
    $nameHora = '';
    $change = false;
    foreach ($menus as $menu) {
      $minFecha = date('Y-m-d', strtotime($menu->fecha));
      if (!in_array($minFecha, $fechas)) {
        array_push($fechas, $minFecha);
      }
    }

    foreach ($fechas as $fecha) {
      $horas = [];
      $menudd = array();

      foreach ($menus as $k => $menu) {
        $minFecha = date('Y-m-d', strtotime($menu->fecha));
        if ($fecha == $minFecha) {
          if (!in_array($menu->hora, $horas)) {
            if ($k > 0 && count($foods) > 0) {
              $menudd[] = array('foods' => $foods, 'schedule' => $nameHora);
            }
            $foods = [];
            $nameHora = $menu->hora;
            array_push($horas, $nameHora);
            $change = true;
          }
          array_push($foods, [$menu->tipo => $menu->comida]);
        }
      }
      if ($change && $nameHora == 'cena') {
        $menudd[] = array('foods' => $foods, 'schedule' => $nameHora);
        $foods = [];
        $nameHora = '';
        $change = false;
      }
      array_push($eventMenu, ['title' => 'Menu del Dia', 'start' => $fecha, 'food' => $menudd]);
    }

    return ["success" => $eventMenu];
  }

  /**
   * @param Request $request
   */
  public function submitSuggest(Request $request)
  {
    if (empty($request->input('description'))) {
      return response()->json(["error" => trans('api.error.29')], 400);
    }
    $user = JWTAuth::parseToken()->authenticate();
    DB::beginTransaction();
    try {
      $suggest = new Suggest;
      $suggest->idUser = $user->idUser;
      $suggest->fecha = date('Y-m-d h:m:s');
      $suggest->description = trim($request->input('description'));
      $suggest->save();
      DB::commit();
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
    return response()->json(["success" => trans('api.success.19')]);
  }

  /**
   * @return mixed
   */
  public function getSuggestions(Request $request)
  {
    /*$limit  = $request->limit;
    $offSet = $request->offset;
    $fecha  = !empty($request->fecha) ? date('Y-m-d', strtotime($request->fecha)) : '';
    $sede   = !empty($request->sede) ? $request->sede : '';*/
    $limit = $request->input('limit');
    $offSet = $request->input('offset');
    $fecha = !empty($request->input('date')) ? date('Y-m-d', strtotime($request->input('date'))) : '';
    $sede = !empty($request->input('id')) ? $request->input('id') : '';

    $total = Suggest::join('user', 'menu_suggest.idUser', '=', 'user.idUser')
      ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser')
      ->join('divisions', 'user_detail.divPersonal', '=', 'divisions.code');
    if (!empty($fecha)) {
      $total = $total->whereBetween('fecha', [$fecha . ' 00:00:00', $fecha . ' 23:59:59']);
    }
    if (!empty($sede)) {
      $total = $total->where('divisions.code', $sede);
    }
    $total = $total->count();

    $suggestions = Suggest::join('user', 'menu_suggest.idUser', '=', 'user.idUser')
      ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser')
      ->join('divisions', 'user_detail.divPersonal', '=', 'divisions.code');
    if (!empty($fecha)) {
      $suggestions = $suggestions->whereBetween('fecha', [$fecha . ' 00:00:00', $fecha . ' 23:59:59']);
    }
    if (!empty($sede)) {
      $suggestions = $suggestions->where('divisions.code', $sede);
    }
    $suggestions = $suggestions->limit($limit)
      ->offset($offSet)
      ->get(['name', 'lastName', 'fecha', 'menu_suggest.description', 'divisions.description as nameStructure', 'divisions.code as codRelational']);
    $arrayQuery = array('total' => $total, 'rows' => $suggestions);

    return response()->json($arrayQuery);
  }

  /**
   * @return string
   */
  public function getMenuDay(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    $sede = $request->has('sede') ? $request->input('sede') : $this->getIdDivision($user->idUser);
    $fecha = Carbon::today();
    $menu = DB::select('select "idDetalle", menu."idMenu", hora, tipo, comida, fecha
      from menu_detalle
      inner join menu on menu_detalle."idMenu" = menu."idMenu"
      /*inner join group_divisions on menu."idSucursal" = ANY(regexp_split_to_array(group_divisions.codes, \',\'))*/
      /*where group_divisions.id = \''.$sede.'\'*/
      where menu."idSucursal" = \''.$sede.'\'
        and hora != \'desayuno\'
        and fecha = \''.$fecha.'\'');
    $nameHora = '';
    $horas = [];
    $menudd = array();
    $foods = [];
    if ($menu) {
      $lastItem = count($menu) - 1;
      foreach ($menu as $k => $m) {
        if (!in_array($m->hora, $horas)) {
          if ($k > 0) {
            $menudd[] = array('foods' => $foods, 'schedule' => $nameHora);
          }
          $foods = [];
          $nameHora = $m->hora;
          array_push($horas, $nameHora);
        }
        array_push($foods, [$m->tipo => $m->comida]);
        if ($lastItem == $k) {
          $menudd[] = array('foods' => $foods, 'schedule' => $nameHora);
        }
      }
    }

    return json_encode($menudd);
  }

  /**
   * @param Request $request
   * @return mixed
   */
  public function deleteMenu(Request $request)
  {
    try {
      DB::beginTransaction();
      MenuDetalle::where('idMenu', $request->menu)
        ->delete();
      Menu::where('idMenu', $request->menu)->delete();
      DB::commit();
      return response()->json(["success" => trans('api.success.20')]);
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  /**
   * @param Request $request
   * @return string
   */
  public function getSedesRegion(Request $request)
  {
    return json_encode(Organizations::where('levelsid', 4)
      ->join('organization_region', 'organization.idRegion', '=', 'organization_region.idRegion')
      ->where('nameStructure', 'ilike', '%' . $request->input('q') . '%')
      ->orderBy('codRelational', 'asc')
      /*->get(['corporativoId', 'nameStructure', 'codRelational', 'organization_region.description']));*/
      ->get(['corporativoId as value', 'nameStructure as name', 'codRelational as code', 'organization_region.description']));
  }

  /**
   * @param Request $request
   * @return mixed
   */
  public function submitPrescription(Request $request)
  {
    $validator = Validator::make($request->all(), ["coverPhoto" => "mimes:doc,docx,pdf"], ["coverPhoto.mimes" => trans('api.error.33')]);
    if ($validator->fails()) return response()->json(['error' => $validator->errors()->first()], 400);

    $user = JWTAuth::parseToken()->authenticate();
    /*if(time() > strtotime('10:15')){
        return response()->json(['error' => 'Horario no permitido']);
    }*/

    if (empty($request->input('prescription'))) {
      return response()->json(["error" => trans('api.error.29')], 400);
    }
    DB::beginTransaction();
    try {
      $name = '';
      $file = $request->file('coverPhoto');
      if($file) {
        if ($file->getSize() > 5242880) return response()->json(['error' => trans('api.error.32')], 400);
        $name = 'prescription_' . Carbon::now()->timestamp . $file->getClientOriginalName();
        $file->move('files/prescription/', $name);
      }

      $reservation = new Reservation;
      $reservation->idUser = $user->idUser;
      $reservation->idStatus = 0; //Solicitado
      $reservation->description = trim($request->input('prescription'));
      if($file) $reservation->photo = $name;
      $reservation->save();

      $fecha = date('d-m-Y', strtotime($reservation->created_at));
      $email = $user->email;
      // $email = 'sergio.carrillo.miele@gmail.com';
      $nombre = $user->name;

      $data = [
        "fecha" => $fecha,
        "nombre" => $nombre,
        "email" => $email,
        "descripcion" => trim($reservation->description)
      ];

      Mail::send('emails.submitreservation', $data, function ($message) use ($email, $nombre) {
        $message->to($email, $nombre)->subject('Prescripción Médica');
      });

      DB::commit();
      return response()->json(["success" => trans('api.success.14')]);
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100'), 'trace' => $ex->getTrace()], 400);
    }
  }

  /**
   * @param Request $request
   * @return string
   */
  public function getReservation(Request $request)
  {
    $where = '';
    $and = '';
    $s = [];
    $rSede = $request->has('sede') ? $request->input('sede') : null;
    $rStatus = $request->has('status') && $request->input('status') > -1 ? $request->input('status') : null;
    $sedes = $rSede ? $this->getDivFromGroup($rSede) : null;
    foreach (explode(',', $sedes) as $sede) {
      array_push($s, "'$sede'");
    }

    if($sedes) {
      $where .= ' user_detail."divPersonal" in ('.implode(',', $s).') ';
      $and = 'and';
    }
    if($rStatus) {
      $where .= ' '.$and.' menu_reservation."idStatus" = '.$rStatus.' ';
    }

    $reservations = DB::select('select "idReservation", "user".name, menu_reservation.description, menu_reservation.created_at,
      menu_reservation."idStatus" as estatus, response, email, photo, "user"."lastName"
      from menu_reservation
      inner join "user" on "user"."idUser" = menu_reservation."idUser"
      inner join user_detail on user_detail."idUser" = "user"."idUser"
      '.(!empty($where) ? ' where '.$where : '').'
      limit '.$request->input('limit').'
      offset '.$request->input('offset'));

    return json_encode([
      'total' => Reservation::where('idStatus', 1)->count(),
      'rows' => $reservations
    ]);
  }

  /**
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function updateReservation(Request $request)
  {
    try {
      DB::beginTransaction();
      $reservation = Reservation::where('idReservation', $request->input('id'))
        ->join('user', 'menu_reservation.idUser', '=', 'user.idUser')
        ->get();
      $estatus = $request->input('status') == 1 ? 'Aprobada' : 'Rechazada';
      $fecha = date('d/m/Y', strtotime($reservation[0]->created_at));
      $email = $reservation[0]->email;
      //$email = 'sergio.carrillo.miele@gmail.com';
      $nombre = $reservation[0]->name;

      Reservation::where('idReservation', $request->input('id'))
        ->update([
          'idStatus' => $request->status,
          'response' => trim($request->response)
        ]);

      $data = [
        "fecha" => $fecha,
        "estatus" => $estatus,
        "nombre" => $nombre,
        "descripcion" => trim($reservation[0]->description),
        "nota" => trim($request->input('response')),
        "email" => $email
      ];

      Mail::send('emails.menureservation', $data, function ($message) use ($email, $nombre) {
        $message->to($email, $nombre)->subject('Prescripción médica');
      });
      DB::commit();
      return response()->json(['success' => trans('api.success.15')]);
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function fetchSedesByMenu() {
    return json_encode(DB::select('select group_divisions.id as value, group_divisions.description as text
      from group_divisions
      inner join menu on menu."idSucursal" = group_divisions.id::text
      group by group_divisions.id, group_divisions.description'));
    /*return json_encode(DB::select('select group_divisions.id as value, group_divisions.description as text
      from group_divisions
      inner join menu on menu."idSucursal" = ANY(regexp_split_to_array(group_divisions.codes, \',\'))
      group by group_divisions.id, group_divisions.description'));*/
    /*return json_encode(Menu::join('divisions', 'divisions.code', '=', 'menu.idSucursal')
      ->get(['code as value', 'description as text']));*/
  }

  public function getIdDivision($user)
  {
    $dbInfo = DB::select('select "id"
    from group_divisions
    inner join user_detail on user_detail."divPersonal" = ANY(regexp_split_to_array(group_divisions.codes, \',\'))
    where user_detail."idUser" = '.$user.';');
    return $dbInfo ? $dbInfo[0]->id : null;
  }

  public function getCod($suc)
  {
    return (DB::select('select "id"
      from group_divisions
      where \''.$suc.'\' = ANY(regexp_split_to_array(group_divisions.codes, \',\'))')[0]->id);
  }

  public function getCodDivision($user)
  {
    return Userdetail::where('idUser', $user)->get(['divPersonal'])[0]->divPersonal;
  }

  public function getDivFromGroup ($group) {
    return GroupDivisions::where('id', $group)->get(['codes'])[0]->codes;
  }
}
