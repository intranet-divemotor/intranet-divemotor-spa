<?php

/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 30-08-2017
 * Time: 01:45 PM
 */

namespace App\Http\Controllers\Indicators;

use App\Entities\Indicators\Indicators;
use App\Entities\Indicators\IndicatorsDetail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DB;

class IndicatorController extends Controller
{
  public function submit (Request $request)
  {
    DB::beginTransaction();
    try {
      $indicator = new Indicators;
      $indicator->type = 1;
      $indicator->title = $request->title;
      $indicator->comment = $request->comment;
      $indicator->idStatus = $request->status ? 1 : 0;

      if($indicator->save()) {
        foreach ($request->items as $item) {
          if(empty($item['brand']) || empty($item['color']['hex']) || empty($item['percentage']))
            return response()->json(['error' => trans('api.error.34')], 400);
          $detail = new IndicatorsDetail;
          $detail->idIndicator = $indicator->id;
          $detail->brand = $item['brand'];
          $detail->percentage = floatval($item['percentage']);
          $detail->color = $item['color']['hex'];
          $detail->save();
        }
      }
      DB::commit();
      return response()->json(['success' => trans('api.success.46')]);
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function submitNps(Request $request)
  {
    DB::beginTransaction();
    try {
      $v = Indicators::where('type', 2)->get();
      if(count($v) > 0) {
        Indicators::where('id', $v[0]->id)->update(['comment' => $request->comment]);
        IndicatorsDetail::where('idIndicator', $v[0]->id)->update(['percentage' => intval($request->value)]);
      } else {
        $i = new Indicators;
        $i->type = 2;
        $i->title = '';
        $i->comment = $request->comment;
        $i->idStatus = 1;
        if($i->save()) {
          $id = new IndicatorsDetail;
          $id->idIndicator = $i->id;
          $id->brand = '';
          $id->percentage = $request->value;
          $id->color = '';
          $id->save();
        }
      }
      DB::commit();
      return response()->json(['success' => trans('api.success.46')]);
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function submitVoc(Request $request)
  {
    DB::beginTransaction();
    try {
      $v = Indicators::where('type', 3)->get();
      if(count($v) > 0) {
        Indicators::where('id', $v[0]->id)->update(['comment' => $request->comment]);
        IndicatorsDetail::where('idIndicator', $v[0]->id)->update(['percentage' => intval($request->value)]);
      } else {
        $i = new Indicators;
        $i->type = 3;
        $i->title = '';
        $i->comment = $request->comment;
        $i->idStatus = 1;
        if($i->save()) {
          $id = new IndicatorsDetail;
          $id->idIndicator = $i->id;
          $id->brand = '';
          $id->percentage = $request->value;
          $id->color = '';
          $id->save();
        }
      }
      DB::commit();
      return response()->json(['success' => trans('api.success.46')]);
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100'), 'trace' => $ex->getMessage()], 400);
    }
  }

  public function getIndicators(Request $request)
  {
    $items = Indicators::where('type', 1)->get();
    $n = Indicators::where('type', 2)->where('idStatus', 1)->get();
    if(count($n) > 0) {
      $nps = intval(IndicatorsDetail::where('idIndicator', $n[0]->id)->get(['percentage'])[0]->percentage);
      $commentNps = Indicators::where('id', $n[0]->id)->get(['comment'])[0]->comment;
    } else {
      $nps = 0;
      $commentNps = '';
    }
    $v = Indicators::where('type', 3)->where('idStatus', 1)->get();
    if(count($v) > 0) {
      $voc = intval(IndicatorsDetail::where('idIndicator', $v[0]->id)->get(['percentage'])[0]->percentage);
      $commentVoc = Indicators::where('id', $v[0]->id)->get(['comment'])[0]->comment;
    } else {
      $voc = 0;
      $commentVoc = '';
    }
    return response()->json(['items' => $items, 'nps' => $nps, 'commentNps' => $commentNps, 'voc' => $voc, 'commentVoc' => $commentVoc]);
  }

  public function removeIndicators(Request $request)
  {
    try {
      DB::beginTransaction();
      IndicatorsDetail::where('idIndicator', $request->id)->delete();
      Indicators::where('id', $request->id)->delete();
      DB::commit();
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function toggleIndicators(Request $request)
  {
    try {
      DB::beginTransaction();
      Indicators::where('id', $request->id)->update(['idStatus' => $request->status == 1 ? 0 : 1]);
      DB::commit();
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function getIndicatorWidget(Request $request)
  {
    $items = [];

    $i = Indicators::where('type', 1)->where('idStatus', 1)->get();
    foreach ($i as $item) {
      $percDonut = [];
      $labelDonut = [];
      $colorDonut = [];
      $dataSets = [];
      $id = IndicatorsDetail::where('idIndicator', $item->id)->get();
      foreach ($id as $itemd) {
        array_push($labelDonut, $itemd->brand);
        array_push($colorDonut, $itemd->color);
        array_push($percDonut, $itemd->percentage);
      }
      array_push($dataSets, ['backgroundColor' => $colorDonut, 'data' => $percDonut, 'label' => 'Indicadores']);
      array_push($items, [
        'data' => ['labels' => $labelDonut, 'datasets' => $dataSets],
        'title' => $item->title,
        'comment' => $item->comment
      ]);
    }

    $n = Indicators::where('type', 2)->where('idStatus', 1)->get();
    if(count($n) > 0) {
      $nps = intval(IndicatorsDetail::where('idIndicator', $n[0]->id)->get(['percentage'])[0]->percentage);
      $commentNps = Indicators::where('id', $n[0]->id)->get(['comment'])[0]->comment;
    } else {
      $nps = 0;
      $commentNps = '';
    }

    $v = Indicators::where('type', 3)->where('idStatus', 1)->get();
    if(count($v) > 0) {
      $voc = intval(IndicatorsDetail::where('idIndicator', $v[0]->id)->get(['percentage'])[0]->percentage);
      $commentVoc = Indicators::where('id', $v[0]->id)->get(['comment'])[0]->comment;
    } else {
      $voc = 0;
      $commentVoc = '';
    }

    return ['items' => $items, 'nps' => $nps, 'commentNps' => $commentNps, 'voc' => $voc, 'commentVoc' => $commentVoc];
  }
}