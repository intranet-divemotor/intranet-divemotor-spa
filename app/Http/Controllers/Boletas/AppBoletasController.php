<?php
namespace App\Http\Controllers\Boletas;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Chumper\Zipper\Zipper;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\File;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Entities\User\User;
use App\Entities\User\Userdetail;
use App\Entities\User\TypeDirectory;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use DateTime;

use App\Entities\Event\Event;
use App\Entities\Event\EventUser;


class AppBoletasController extends Controller
{
    protected $user;

    function __construct() {
       $this->user = JWTAuth::parseToken()->authenticate();
    }
    
    
    public function headerPDF($typeFile, $date){

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->removeSheetByIndex(0);
        $objPHPExcel->createSheet(0);
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
        $objPHPExcel->getActiveSheet()->getCell('A1')->setValue('DIVEMOTOR');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(34)->setBold(true)->getColor()->setRGB('233141');
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(50);
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        /////
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:D2');
        //$objPHPExcel->getActiveSheet()->getCell('A2')->setValue('CARGO ENTREGA DE '.strtoupper($typeFile) .' '. date('d/m/Y'));
        $m = [ '01' => "Enero",
            '02' => "Febrero",
            '03' => "Marzo",
            '04' => "Abril",
            '05' => "Mayo",
            '06' => "Junio",
            '07' => "Julio",
            '08' => "Agosto",
            '09' => "Septiembre",
            '10' => "Octubre",
            '11' => "Noviembre",
            '12' => "Diciembre",
        ];
        if($date != ''){
            $dateFormat = new DateTime($date);
            $datePeriod = $dateFormat->format('d-m-Y');
            $i = $dateFormat->format('m');
            $mes = $m[$i];
            $year = $dateFormat->format('Y');
        }else{
            $dateFormat = new DateTime(date('d-m-Y'));
            $datePeriod  = $dateFormat->format('d-m-Y');
            $i = $dateFormat->format('m');
            $mes = $m[$i];
            $year = $dateFormat->format('Y');
        }



        $objPHPExcel->getActiveSheet()->getCell('A2')->setValue('CARGO ENTREGA DE '.strtoupper($typeFile) .' '. $datePeriod);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(16)->setBold(true)->getColor()->setRGB('233141');
        $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        ///////
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:D3');

        if(strtoupper($typeFile) === "CTS"){
            $text = 'Mediante la presente, dejamos constancia que nuestro empleador, Diveimport S.A, Divecenter S.A.C o Inversiones Calafquen S.A. nos ha hecho entrega del certificado de CTS por el periodo '.$mes.' '.$year.', en el cual se detalla la información que establecen los Art. 18, 19 y 20 del D.S 001-98-TR, incluyendo el detalle del abono realizado en mi cuenta CTS.';
        }else{
            $text = 'Mediante la presente, dejamos constancia que nuestro empleador, Diveimport S.A, Divecenter S.A.C o Inversiones Calafquen S.A. nos ha hecho entrega de la liquidación de la Boleta de Pago del periodo '.$mes.' '.$year.' , en el cual se detalla la información que establecen los Art. 18, 19 y 20 del D.S 001-98-TR, incluyendo el detalle del abono realizado en mi cuenta de haberes.';
        }
        $objPHPExcel->getActiveSheet()->getCell('A3')->setValue($text);
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(10);
        $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        ////////
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A4:D4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(20);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A4:D4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'CÓDIGO')->setShowGridlines(true)
            ->setCellValue('B4', 'DNI')->setShowGridlines(true)
            ->setCellValue('C4', 'NOMBRE COMPLETO')->setShowGridlines(true)
            ->setCellValue('D4', 'EMPRESA')->setShowGridlines(true);
        
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(45);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
        
        return $objPHPExcel;
    }
    
    public function empresa($empresa){
        if($empresa =='PE'){
            return 'Divecenter S.A.C';
        }elseif($empresa =='PI'){
            return 'Diveimport S.A';
        }elseif ($empresa =='PC') {
            return 'Inversiones Calafquen S.A';
        }
        return $empresa;

    }

    


    //Upload zip
    public function load(Request $request) {

        try {
            //Validation
            $validator = Validator::make($request->all(), [ //Reglas
                        "fileZip" => "required|mimes:zip,x-rar-compressed",
                            ], [//Mensajes
                        "fileZip.mimes" => trans("api.error.8"),
            ]);
            $email = $this->user->email;
            if ($validator->fails()) {
                dd($validator->errors()); exit;
                return response()->json(['error' => $validator->errors()->first()], 400);
            }
            //unzip zip package
            $zipper = new Zipper();
            $typeZip = $request->typeFile !='' ? $request->typeFile : "boletas";
            $zipper->make($request->fileZip)->extractTo('File/'.$typeZip);
            
            $noti = ucwords($request->typeFile !='' ? $request->typeFile : "boleta");
            
           $filelist =  $zipper->make($request->fileZip)->listFiles('/\.pdf$/i'); 
           $data = array();
           $mytime = Carbon::now();
           $mytime->toDateTimeString();
           //create pdf
           $objPHPExcel= $this->headerPDF($noti, $request->date);
           $f = 5;
           
           foreach ($filelist as $file){
               $file = explode('/', $file)[1];
               $posicionHCM = substr(explode('.', $file)[0] , 0,-6);
               $detail = Userdetail::where("posicionHCM", $posicionHCM)
                   ->first(["idUser", "posicionHCM", "colaborador", "identityDocument", "divPersonal"]);

               if($detail){
                    $data[] = array(
                         "idUser" => $detail->idUser,
                         "data" => $posicionHCM,
                         "date" =>$mytime->toDateTimeString(),
                         "typeFile" =>$noti
                    );   
                    ///////load data pdf/////
                    $empresa = $this->empresa(substr($detail->divPersonal , 0,2));
                    
                    
                    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                    $objPHPExcel->getDefaultStyle()->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);

                    $objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$f.':'.'D'.$f)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$f, $detail->posicionHCM)->setShowGridlines(false)
                        ->setCellValue('B'.$f, $detail->identityDocument)->setShowGridlines(false)
                        ->setCellValue('C'.$f, $detail->colaborador)->setShowGridlines(false)
                        ->setCellValue('D'.$f, $empresa)->setShowGridlines(false);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(45);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
                     $f++;
               }
           }
            $objPHPExcel->getActiveSheet()->setTitle('Reporte');
            $objPHPExcel->setActiveSheetIndex(0);
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
            ob_clean();
            $objWriter->save('File/reporte'.date('d-m-Y').'.pdf');
            $attachments = base_path('public') . '/File/reporte'.date('d-m-Y').'.pdf';
            
            $info = ['type' => $request->typeFile, 'info' => $data ,'user' =>$this->user->name . ' '. $this->user->lastName];
            
            Mail::send('emails.boletas', $info, function($message) use ($email,$attachments) {
                    $message->to($email, 'Intranet')->subject('Listado '. env('APP_NAME'));
                    $message->attach($attachments);
            });
        
        
         
       return new JsonResponse([
          'success' => trans('api.success.30'),
          'user' => $data
       ]);
       
       } catch (Exception $e) {
            // Something went wrong whilst attempting to encode the token
            return response()->json(['error' => trans("api.error.6")], 400);
        }
    }

    //LIST 
    public function ListFile(Request $request) {

        $idRole = $this->user->idUser;
        $carpeta = base_path() . "/public/File/".$request->input('type').'/';
        $ruta = $carpeta;
        
        $data = array();
        if (is_dir($ruta)) {
//fijamos la ruta del directorio que se va a abrir
            if ($dir = opendir($ruta)) {
//si el directorio se puede abrir
                $detail = Userdetail::where("idUser", $this->user->idUser)
                        ->first(["posicionHCM"]);
                while (($archivo = readdir($dir)) !== false) {
//le avisamos que no lea el "." y los dos ".."
                    if ($archivo != '.' && $archivo != '..') {
                        if (is_dir($ruta . $archivo)) {
                            
                            $response = $this->readFolder($ruta.$archivo,$detail->posicionHCM);
                            //verificar que el file sea del usuario    
                         //   print_r('archivo-->'.substr($response, 0,8).'.pdf');
                          //    $posicionHCM = substr(explode('.', $file)[0] , 0,-6);
                             
                            if ($detail->posicionHCM.'.pdf' == substr($response, 0,8).'.pdf') {
                                $data[] = array(
                                    "role" => $idRole,
                                    "ruta" => $ruta,
                                    "dir" => $archivo,
                                    "file" => $response,
                                    "extension" => substr($response, -3)
                                );
                            }
                        }
                    }
                }
                closedir($dir);
            }
        }
        return json_encode($data);
    }

    public function download(Request $request){
       
        $nombre = $request->input('nameDiv');  
        $filename = explode(".", $nombre); 
        
        $auth= substr($filename[0], 0,8);
        
        $detail = Userdetail::where("idUser", $this->user->idUser)
            ->first(["posicionHCM"]);
        // or $this->user->idRole ==1
        if($detail->posicionHCM == $auth or $this->user->idRole ==1){
            $filename = base_path()."/public/File/".$request->input('typeFile').'/'.$request->input('fecha').'/'.$nombre; 
            return response()->download($filename, 'output.pdf');
        }else{
           return redirect('/');
       }
    }
    
    public function deleteFile(Request $request){
        
            $typeZip = $request->input('typeFile') !='' ? $request->input('typeFile') : "boletas";
            
            $filename = base_path()."/public/File/".$typeZip.'/'.$request->input('name');  
            $this->rrmdir($filename);
            return ["success" => trans('api.success.11')];
        
        
    }
    
    public function preview(Request $request){
        $filenameexplode = explode(".", $request->name); 
        
        $detail = Userdetail::where("idUser", Auth::user()->idUser)
            ->first(["posicionHCM"]);
        if($detail->posicionHCM == $filenameexplode[0] or Auth::user()->idRole == 1){
            $filename = base_path()."/public/File/boletas/".$request->fecha.'/'.$request->name;  
             return response()->file($filename);
        }else{
        
           return redirect('/');
        }
    }
    
    function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . "/" . $object))
                        rrmdir($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            rmdir($dir);
        }
    }
    
    
    //recogemos  la ruta para entrar en el segundo nivel
    public function readFolder($leercarpeta,$PosicionHCM) {
        $leercarpeta = $leercarpeta;
        if (is_dir($leercarpeta)) {
            if ($dir = opendir($leercarpeta)) {
                while (($archivo = readdir($dir)) !== false) {
                    if ($archivo != '.' && $archivo != '..') {
                        $verificarAuth = substr($archivo , 0,-10);
                        if ($PosicionHCM.'.pdf' == $verificarAuth.'.pdf') {
                           return $archivo;
                         }
                    }
                }
                closedir($dir);
            }
        }
    }
    
    
    
    //admin
    //LIST 
    public function ListFileAll(Request $request) {
        
        $idRole = $this->user->idUser;
        $carpeta = base_path() . "/public/File/".$request->input('type').'/'.$request->input('date').'/';
        $ruta = $carpeta;
        
            
        $data = array();
        if (is_dir($ruta)) {
//fijamos la ruta del directorio que se va a abrir
            if ($dir = opendir($ruta)) {
//si el directorio se puede abrir
                while (($archivo = readdir($dir)) !== false) {
                    if ($archivo != '.' && $archivo != '..') {
                        if (is_dir($ruta)) {
                                      
                                $data[] = array(
                                    "role" => $idRole,
                                    "ruta" => $ruta,
                                    "dir" => $archivo,
                                    "extension" => substr($archivo, -3)
                                );
                            
                        }
                    }
                }
                closedir($dir);
                
            }
        }
        return json_encode($data);
    }
    
    public function readFolderAll($leercarpeta) {
        $leercarpeta = $leercarpeta;
        if (is_dir($leercarpeta)) {
            if ($dir = opendir($leercarpeta)) {
                while (($archivo = readdir($dir)) !== false) {
                    if ($archivo != '.' && $archivo != '..') {
                        return $archivo;
                         
                    }
                }
                closedir($dir);
            }
        }
    }
    
   

}
