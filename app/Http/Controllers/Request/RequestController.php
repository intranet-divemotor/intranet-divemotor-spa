<?php

namespace App\Http\Controllers\Request;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\URL;
use App\Entities\User\User;
use Auth;
use Excel;
use DB;
use App\Answer;
use App\DaysRequest;
use App\Question;
use App\Requests;
use App\TypeRequest;
use Validator;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Facades\JWTAuth;

class RequestController extends Controller
{
  /**
   * @param Request $request
   * @return mixed
   */
  public function submit(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "timePicker" => "required",
          "date_event" => "required",
          "reason_event" => "required",
          "cod_user" => "integer|required",
        ], [//Mensajes
          "timePicker.required" => trans('api.error.27'),
          "date_event.required" => trans('api.error.26'),
          "reason_event.required" => trans('api.error.29'),
          "cod_user.required" => trans('api.error.28'),
          "cod_user.integer" => trans('api.error.28'),
        ]);
      if ($validator->fails()) {
        return response()->json(['error' => $validator->errors()->first()], 400);
      }
      $toPhpTimestamp = strtotime(str_replace('/', '-', $request->input('date_event')));
      $dateEvent = date('Y-m-d', $toPhpTimestamp);
      $day = date('N', $toPhpTimestamp);
      $daysAllowed = DaysRequest::where('corporativoId', $request->input('sede'))->get(['day']);
      if (count($daysAllowed) > 0) {
        $arr = [];
        foreach ($daysAllowed as $allowed) {
          array_push($arr, $allowed->day);
        }
        if (!in_array($day, $arr) > 0) {
          return response()->json(['error' => trans('api.error.23')]);
        }
      }

      $requests = new Requests;
      $requests->idUser = $request->input('cod_user');
      $requests->description = trim($request->input('reason_event'));
      $requests->idStatus = 0;
      $requests->idType = 1;
      $requests->dateStart = $dateEvent;
      $requests->timeStart = $request->input('timePicker');

      if ($requests->save() && $request->has('q')) {
        foreach ($request->input('q') as $index => $value) {
          if (gettype($value) == 'array') {
            if (count($value) > 0) {
              foreach ($value as $v) {
                $answer = new Answer;
                $answer->description = $v;
                $answer->idQuestion = $index;
                $answer->idRequest = $requests->idRequest;
                $answer->save();
              }
            }
          } else {
            if (!empty($value)) {
              $answer = new Answer;
              $answer->description = $value;
              $answer->idQuestion = $index;
              $answer->idRequest = $requests->idRequest;
              $answer->save();
            }
          }
        }
      }

      $fecha = date('d/m/Y', strtotime($requests->created_at));
      $nombre = $user->name;
      $email   = $user->email;
      //$email = 'sergio.carrillo.miele@gmail.com';
      $nombresolicitud = TypeRequest::where('idType', 1)->get(['description'])[0]->description;
      $subject = 'Solicitud de ' . $nombresolicitud;

      $data = [
        "fecha" => $fecha,
        "fechainicio" => $request->input('date_event'),
        "horainicio" => $request->input('timePicker'),
        "nombre" => $nombre,
        "email" => $email,
        "tipo" => 1,
        "nombresolicitud" => $nombresolicitud,
        "descripcion" => trim($requests->description)
      ];

      Mail::send('emails.submitrequest', $data, function ($message) use ($email, $nombre, $subject) {
        $message->to($email, $nombre)->subject($subject);
      });

      DB::commit();
      return response()->json(['success' => trans('api.success.14'), 'events' => $this->getCurrentRequests($request)]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  /**
   * @param Request $request
   * @return mixed
   */
  public function updateEvent(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "timePicker" => "required",
          "date_event" => "required",
          "reason_event" => "required",
          "cod_user" => "integer|required",
        ], [//Mensajes
          "timePicker.required" => trans('api.error.27'),
          "date_event.required" => trans('api.error.26'),
          "reason_event.required" => trans('api.error.29'),
          "cod_user.required" => trans('api.error.28'),
          "cod_user.integer" => trans('api.error.28'),
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }

      $requests = Requests::find($request->idRequest);
      if (count($requests) <= 0) {
        return response()->json(['error' => trans('api.error.25')], 400);
      }

      $requests->description = trim($request->reason_event);
      $requests->timeStart = $request->timePicker;
      $requests->save();

      DB::commit();
      return response()->json(['success' => trans('api.success.15'), 'events' => $this->getCurrentRequests($request)]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  /**
   * @param Request $request
   * @return string
   */
  public function getCurrentRequests(Request $request)
  {
    $arrayRequest = array();
    $user = JWTAuth::parseToken()->authenticate();
    $sede = $user->idRole == 1 ? $request->input('sede') : $user->corporativoId;
    $requests = Requests::join('user', 'requests.idUser', '=', 'user.idUser')
      ->join('type_request', 'requests.idType', '=', 'type_request.idType');
    if (!in_array(Auth::user()->idRole, [1, 3])) {
      $requests = $requests->where('requests.idUser', $user->idUser);
    }
    //if(in_array(Auth::user()->idRole, [1, 3])) {
    $requests = $requests->with(['Answer' => function ($query) {
      $query->select('idAnswer', 'description', 'idQuestion', 'idRequest');
    }]);
    //}
    if (in_array(Auth::user()->idRole, [2, 3])) {
      $requests = $requests->where('corporativoId', $user->corporativoId);
    }
    if (!empty($request->input('sede')) && $user->idRole == 1) {
      $requests = $requests->where('corporativoId', $request->input('sede'));
    }
    $requests = $requests->whereIn('requests.idStatus', [0, 1])
      ->where('requests.idType', 1)
      ->get(['idRequest', 'user.name', 'requests.idUser', 'requests.description as medicalDescription', 'type_request.description as type',
        'requests.dateStart', 'requests.timeStart', 'requests.idStatus', 'requests.note']);
    foreach ($requests as $r) {
      $arrayRequest[] = array(
        'title' => $r->name,
        'start' => $r->dateStart.' '.date('h:m:s', strtotime($r->timeStart)),
        'request' => $r
      );
    }
    return json_encode(['requests' => $arrayRequest, 'daysupdated' => empty($sede) ? [] : DaysRequest::where('corporativoId', $sede)->get()]);
  }

  /**
   * @param Request $request
   * @return mixed
   */
  public function updateStatusRequest(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "idRequest" => "integer|required",
          "idStatus" => "integer|required"
        ], [//Mensajes
          "idRequest.required" => "Evento inválido",
          "idRequest.integer" => "Evento inválido",
          "idStatus.required" => "Estatus inválido",
          "idStatus.integer" => "Estatus inválido"
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }

      $requests = Requests::where('idRequest', $request->idRequest)
        ->join('user', 'requests.idUser', '=', 'user.idUser')
        ->get(['requests.idRequest', 'user.name', 'user.email', 'requests.created_at', 'requests.dateStart',
          'requests.timeStart', 'requests.description', 'requests.idStatus', 'requests.note', 'requests.idType']);
      if (count($requests) <= 0) {
        return response()->json(['error' => trans('api.error.25')], 400);
      }

      if (intval($requests[0]->idStatus) == 0) {
        $requests[0]->update([
          'idStatus' => $request->idStatus,
          'note' => trim($request->noteEvent)
        ]);

        $estatus = $request->idStatus == 1 ? 'Aprobada' : 'Rechazada';
        $nombre = $requests[0]->name;
        $email = $requests[0]->email;
        //$email = 'sergio.carrillo.miele@gmail.com';
        $nombresolicitud = TypeRequest::where('idType', $requests[0]->idType)->get(['description'])[0]->description;
        $subject = 'Solicitud de ' . $nombresolicitud;

        $data = [
          "fecha" => date('d-m-Y', strtotime($requests[0]->created_at)),
          "fechainicio" => date('d-m-Y', strtotime($requests[0]->dateStart)),
          "horainicio" => $requests[0]->timeStart,
          "nombre" => $nombre,
          "email" => $email,
          "descripcion" => trim($requests[0]->description),
          "tipo" => $requests[0]->idType,
          "nombresolicitud" => $nombresolicitud,
          "estatus" => $estatus,
          "nota" => trim($requests[0]->note)
        ];

        Mail::send('layouts::emails.responserequest', $data, function ($message) use ($email, $nombre, $subject) {
          $message->to($email, $nombre)->subject($subject);
        });


        DB::commit();
        return response()->json(['success' => trans('api.success.15'), 'events' => $this->getCurrentRequests($request)]);
      } else {
        DB::rollBack();
        return response()->json(['error' => trans('api.error.24')], 400);
      }
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  /**
   * @param Request $request
   * @return string
   */
  public function exportReport(Request $request)
  {
    $objPHPExcel = new \PHPExcel();

    $dates = explode(' - ', $request->date);
    $dateStart = date('Y-m-d', strtotime(str_replace('/', '-', trim($dates[0]))));
    $dateEnd = date('Y-m-d', strtotime(str_replace('/', '-', trim($dates[1]))));

    $sede = Auth::user()->idRole == 1 ? $request->sede : Auth::user()->corporativoId;
    $requests = Requests::join('user', 'requests.idUser', '=', 'user.idUser')
      ->whereBetween('dateStart', [$dateStart, $dateEnd])
      ->where('idType', 1)
      ->where('corporativoId', $sede)
      ->get(['requests.idRequest', 'user.name', 'user.email', 'requests.created_at', 'requests.dateStart',
        'requests.timeStart', 'requests.description', 'requests.idStatus', 'requests.note']);

    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
    $objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Reporte semanal citas médicas ' . trim($dates[0]) . ' - ' . trim($dates[1]));
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFont()->setBold(true);

    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(30);
    $objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:E2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A2', 'Colaborador')->setShowGridlines(false)
      ->setCellValue('B2', 'Email')->setShowGridlines(false)
      ->setCellValue('C2', 'Descripcion')->setShowGridlines(false)
      ->setCellValue('D2', 'Fecha y Hora')->setShowGridlines(false)
      ->setCellValue('E2', 'Estatus')->setShowGridlines(false);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(45);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

    $f = 3;
    foreach ($requests as $req) {
      $objPHPExcel->getActiveSheet()->getRowDimension($f)->setRowHeight(20);
      $estatus = $req->idStatus == 0 ? 'En espera' : ($req->idStatus == 1 ? 'Aprobado' : 'Rechazado');

      $objPHPExcel->getActiveSheet()->getStyle('D' . $f . ':E' . $f)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $f, $req->name)->setShowGridlines(false)
        ->setCellValue('B' . $f, $req->email)->setShowGridlines(false)
        ->setCellValue('C' . $f, $req->description)->setShowGridlines(false)
        ->setCellValue('D' . $f, date('d-m-Y', strtotime($req->dateStart)) . ' ' . strtoupper($req->timeStart))->setShowGridlines(false)
        ->setCellValue('E' . $f, $estatus)->setShowGridlines(false);

      $f++;
    }

    $objPHPExcel->getActiveSheet()->setTitle('Reporte');
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
    $nameFile = 'divemotor_citas_medicas_' . str_replace('/', '-', trim($dates[0])) . '_' . str_replace('/', '-', trim($dates[1])) . '.pdf';
    $objWriter->save('File/report/' . $nameFile);
    return json_encode(['success' => URL::to('/File/report/' . $nameFile)]);
  }

  public function deleteRequest(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "idRequest" => "integer|required"
        ], [//Mensajes
          "idRequest.required" => "Evento inválido",
          "idRequest.integer" => "Evento inválido"
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }
      Requests::where('idRequest', $request->input('idRequest'))->delete();
      DB::commit();
      return response()->json(['success' => trans('api.success.16'), 'events' => $this->getCurrentRequests($request)]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function getDaysRequest(Request $request)
  {
    $sede = Auth::user()->idRole == 1 ? $request->sede : Auth::user()->corporativoId;
    return json_encode(DaysRequest::where('corporativoId', $sede)->get());
  }

  public function saveDays(Request $request)
  {
    try {
      DB::beginTransaction();
      $sede = Auth::user()->idRole == 1 ? $request->sede : Auth::user()->corporativoId;
      DaysRequest::where('corporativoId', $sede)->delete();
      foreach ($request->days as $day) {
        $days = new DaysRequest;
        $days->day = $day['id'];
        $days->corporativoId = $sede;
        $days->save();
      }
      DB::commit();
      return response()->json(['success' => trans('api.success.17'), 'daysupdated' => DaysRequest::where('corporativoId', $sede)->get()]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100'), 'daysupdated' => 'true'], 400);
    }
  }
}