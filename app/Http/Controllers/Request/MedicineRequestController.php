<?php

namespace App\Http\Controllers\Request;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use DB;
use Validator;
use App\Entities\User\User;
use App\Requests;
use App\Answer;
use App\Question;
use Tymon\JWTAuth\Facades\JWTAuth;

class MedicineRequestController extends Controller
{
  public function getRequests(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    $requests = Requests::join('user', 'requests.idUser', '=', 'user.idUser')
      ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser')
      ->join('position', 'user_detail.positionId', '=', 'position.positionId')
      ->join('department', 'user_detail.departmentId', '=', 'department.departmentId')
      ->join('type_request', 'requests.idType', '=', 'type_request.idType')
      ->with(['Answer' => function ($query) {
        $query->select('idAnswer', 'description', 'idQuestion', 'idRequest');
      }]);
    if (!in_array($user->idRole, [1, 3])/* < 0 && Auth::user()->idRole != 2*/) {
      $requests = $requests->where('requests.idUser', $user->idUser);
    }
    if (in_array($user->idRole, [1, 3])) {
      $requests = $requests->with(['Answer' => function ($query) {
        $query->select('idAnswer', 'description', 'idQuestion', 'idRequest');
      }]);
    }
    if (in_array($user->idRole, [2, 3])) {
      $requests = $requests->where('corporativoId', $user->corporativoId);
    }
    if ($request->has('sede')) {
      $requests = $requests->where('corporativoId', $request->input('sede'));
    }
    $requests = $requests->where('requests.idType', 2);
    $total = $requests->count();
    $requests = $requests->orderBy('requests.created_at', 'desc')
      ->limit($request->input('limit'))
      ->offset($request->input('offSet'))
      ->get(['idRequest', 'user.name', 'user.lastName', 'requests.idUser', 'type_request.description as type', 'requests.medicine',
        'requests.dateStart', 'requests.dateEnd', 'requests.timeStart', 'requests.idStatus', 'requests.note', 'requests.prescription',
        'user_detail.identityDocument', 'position.position', 'department.department']);
    return json_encode([
      'total' => $total,
      'rows' => $requests
    ]);
  }

  public function submit(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "medicine" => "required",
          "prescription" => "required",
        ], [//Mensajes
          "medicine.required" => trans('api.error.29'),
          "prescription.required" => trans('api.error.29'),
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }

      $requests = new Requests;
      $requests->idUser = $user->idUser;
      $requests->description = null;
      $requests->idStatus = 0;
      $requests->idType = 2;
      $requests->dateStart = date('Y-m-d');
      $requests->timeStart = date('h:ma');
      $requests->medicine = trim($request->input('medicine'));
      $requests->prescription = trim($request->input('prescription'));

      if ($requests->save() && $request->has('q')) {
        foreach ($request->q as $index => $value) {
          if (gettype($value) == 'array') {
            if (count($value) > 0) {
              foreach ($value as $v) {
                $answer = new Answer;
                $answer->description = $v;
                $answer->idQuestion = $index;
                $answer->idRequest = $requests->idRequest;
                $answer->save();
              }
            }
          } else {
            if (!empty($value)) {
              $answer = new Answer;
              $answer->description = $value;
              $answer->idQuestion = $index;
              $answer->idRequest = $requests->idRequest;
              $answer->save();
            }
          }
        }
      }

      DB::commit();
      return response()->json(['success' => trans('api.success.14')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function editEstimatedDate(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "estimatedDate" => "required",
        ], [//Mensajes
          "estimatedDate.required" => trans('api.error.27'),
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }

      $requests = Requests::findOrFail($request->idRequest);
      $requests->dateEnd = date('Y-m-d', strtotime(str_replace('/', '-', $request->estimatedDate)));
      $requests->save();

      DB::commit();
      return response()->json(['success' => 'OK']);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function updateMedicineStatus(Request $request)
  {
    try {
      DB::beginTransaction();
      $requests = Requests::findOrFail($request->idRequest);
      $requests->idStatus = $request->idStatus;
      $requests->save();

      DB::commit();
      return response()->json(['success' => trans('api.success.15')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function updateMedicineRequest(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "medicine" => "required",
          "prescription" => "required",
          "cod_user" => "integer|required",
        ], [//Mensajes
          "medicine.required" => trans('api.error.29'),
          "prescription.required" => trans('api.error.29'),
          "cod_user.required" => trans('api.error.28'),
          "cod_user.integer" => trans('api.error.28'),
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }

      $requests = Requests::findOrFail($request->idRequest);
      $requests->description = null;
      $requests->medicine = trim($request->medicine);
      $requests->prescription = trim($request->prescription);
      $requests->note = trim($request->note_event);
      $requests->save();

      DB::commit();
      return response()->json(['success' => trans('api.success.15')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }
}
