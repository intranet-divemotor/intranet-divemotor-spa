<?php

namespace App\Http\Controllers\Request;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use DB;
use App\Question;
use Validator;
use App\TypeRequest;

class TypeRequestController extends Controller
{
  public function getRequests(Request $request)
  {
    $limit = $request->input('limit');
    $offset = $request->input('offset');

    $total = TypeRequest::count();
    $requesttype = TypeRequest::limit($limit)->offset($offset)
      ->with(['Question' => function ($query) {
        $query->select('idTypeRequest', 'idQuestion', 'description', 'type', 'options');
      }])
      ->orderBy('idType')
      ->get(['idType', 'request', 'description']);
    $arrayQuery = array('total' => $total, 'rows' => $requesttype);
    return response()->json($arrayQuery);

  }

  public function SubmitTypeRequest(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "typequest" => "required",
        ], [//Mensajes
          "typequest.required" => trans('api.error.29'),

        ]);
      if ($validator->fails()) {
        return response()->json(['error' => $validator->errors()->first()], 400);
      }

      $requests = new TypeRequest;
      $requests->request = $request->typequest;
      $requests->description = $request->typequest;

      if ($requests->save()) {
        foreach ($request->questions as $jquestion) {
          $question = new Question;
          $question->description = $jquestion['description'];
          $question->idTypeRequest = $requests->idType;
          $question->type = $jquestion['type'];
          $question->options = $jquestion['options'];
          $question->save();
        }
      }

      DB::commit();
      return response()->json(['success' => 'Solicitud registrada con exito.']);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }

  public function editRequest(Request $request)
  {
    try {
      DB::beginTransaction();
      $requests = TypeRequest::findOrFail($request->input('idType'));
      $requests->request = $request->input('typequest');
      $requests->description = $request->input('typequest');

      if ($requests->save()) {
        Question::where('idTypeRequest', $request->input('idType'))->delete();

        foreach ($request->questions as $jquestion) {
          $question = new Question;
          $question->description = $jquestion['description'];
          $question->idTypeRequest = $requests->idType;
          $question->type = $jquestion['type'];
          $question->options = $jquestion['options'];
          $question->save();
        }
      }

      DB::commit();
      return response()->json(['success' => 'Datos Actualizados Con Exito']);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }

  public function deleteTypeRequest(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "idType" => "integer|required"
        ], [//Mensajes
          "idType.required" => "Solicitud inválida",
          "idType.integer" => "Solicitud inválida"
        ]);
      if ($validator->fails()) {
        return response()->json(['error' => $validator->errors()->first()], 400);
      }
      Question::where('idTypeRequest', $request->input('idType'))->delete();
      TypeRequest::where('idType', $request->input('idType'))->delete();
      DB::commit();
      return response()->json(['success' => 'Tipo de solicitud elimanda con exito']);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }

}
