<?php

namespace App\Http\Controllers\Request;

use App\Entities\User\Userdetail;
use App\Http\Controllers\SystemLink\SystemLinkController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use DB;
use App\TypeRequest;
use Validator;
use App\Entities\User\User;
use App\Requests;
use App\Answer;
use App\Question;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Facades\JWTAuth;
use Carbon\Carbon;

class ConstancesRequestController extends Controller
{
  public function getRequests(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    $requests = Requests::join('user', 'requests.idUser', '=', 'user.idUser')
      ->join('type_request', 'requests.idType', '=', 'type_request.idType')
      ->with(['Answer' => function($query){
          $query->select('idAnswer', 'description', 'idQuestion', 'idRequest');
      }])
      ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser');
    /*if (!in_array($user->idRole, [1, 3])) {*/
    if ($user->idRole == 2) {
      $requests = $requests->where('requests.idUser', $user->idUser);
    }
    if($user->idRole == 1 && $request->has('admin')) {
      $requests = $requests->where('requests.idUser', $user->idUser);
    }
    if (in_array($user->idRole, [1, 3])) {
      $requests = $requests->with(['Answer' => function ($query) {
        $query->select('idAnswer', 'description', 'idQuestion', 'idRequest');
      }]);
    }
    $sede = !empty($request->input('sede')) ? $request->input('sede') : Userdetail::where('idUser', $user->idUser)->get(['divPersonal'])[0]->divPersonal;
    /*if (in_array($user->idRole, [2, 3])) {*/
    if ($user->idRole == 2) {
      $requests = $requests->where('divPersonal', $sede);
    }
    if (!empty($request->input('sede'))) {
      $requests = $requests->where('divPersonal', $sede);
    }
    if($request->input('type') > 0) {
      $requests = $requests->where('requests.idType', $request->input('type'));
    } else {
      $requests = $requests->where('requests.idType', '>', 6);
    }
    if ($request->input('status') > -1) {
      $requests = $requests->where('requests.idStatus', $request->input('status'));
    }
    $total = $requests->count();
    $requests = $requests->orderBy('requests.created_at', 'desc')
      ->limit($request->input('limit'))
      ->offset($request->input('offset'))
      ->get(['idRequest', 'user.name', 'user.lastName', 'requests.idUser', 'type_request.request as type', 'requests.medicine',
        'requests.dateStart', 'requests.dateEnd', 'requests.timeStart', 'requests.idStatus', 'requests.note', 'requests.userTo',
        'requests.purpose', 'requests.idType', 'requests.file', 'requests.fileNote']);
    return json_encode([
      'total' => $total,
      'rows' => $requests
    ]);
  }

  public function getTypeRequests() {
    return response()->json(TypeRequest::where('idType', '>', 6)
      ->with(['Question' => function ($query) {
        $query->select('idTypeRequest', 'idQuestion',  'description', 'type', 'options');
      }])
      ->orderBy('idType')
      ->get(['idType', 'description as text']));
  }

  public function submit(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    $sc = new SystemLinkController;
    try {
      $accept = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'xls', 'xlsx', 'ppt', 'pptx'];
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "typeRequest" => "required",
          "purpose" => "required",
        ], [//Mensajes
          "typeRequest.required" => trans('api.error.29'),
          "purpose.required" => trans('api.error.29'),
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }


      $constanceFiles = [];
      $filesE = $request->file('files');
      if(count($filesE)) {
        foreach ($filesE as $file) {
          if($file->getSize() > 5242880) return response()->json(['error' => trans('api.error.32')], 400);
          $name = strtolower(preg_replace('/\s+/', '', $sc->removeAccented('constance_'.Carbon::now()->timestamp.$file->getClientOriginalName())));
          $arr = explode('.', $name);
          $ext = strtolower(end($arr));
          if(!in_array($ext, $accept)) return response()->json(['error' => trans('api.error.33')], 400);
          $file->move('files/constance/', $name);
          array_push($constanceFiles, $name);
        }
      }

      $requests = new Requests;
      $requests->idUser = $user->idUser;
      $requests->description = null;
      $requests->idStatus = 0;
      $requests->idType = $request->input('typeRequest');
      $requests->dateStart = date('Y-m-d');
      $requests->timeStart = date('h:ma');
      $requests->purpose = trim($request->input('purpose'));
      $requests->userTo = trim($request->input('toRequest'));
      $requests->file = implode(',', $constanceFiles);

      if($requests->save() && $request->has('q')){
        foreach ($request->input('q') as $index => $value){
          $va = explode(',', $value);
          $ln = count($va);
          if($ln > 1) {
            if(count($value) > 0) {
              foreach ($va as $v) {
                $answer = new Answer;
                $answer->description = $v;
                $answer->idQuestion = $index;
                $answer->idRequest = $requests->idRequest;
                $answer->save();
              }
            }
          }else{
            if(!empty($value)) {
              $answer = new Answer;
              $answer->description = $value;
              $answer->idQuestion = $index;
              $answer->idRequest = $requests->idRequest;
              $answer->save();
            }
          }
        }
      }

      DB::commit();
      return response()->json(['success' => trans('api.success.14')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function updateStatus(Request $request)
  {
    try {
      DB::beginTransaction();
      $requests = Requests::where('idRequest', $request->input('idRequest'))
        ->join('user', 'requests.idUser', '=', 'user.idUser')
        ->get(['requests.idRequest', 'user.name', 'user.email', 'requests.created_at', 'requests.dateStart',
          'requests.timeStart', 'requests.dateEnd', 'requests.description', 'requests.idStatus', 'requests.note', 'requests.idType',
          'requests.members', 'requests.userTo', 'requests.purpose', 'requests.idType']);
      $requests[0]->idStatus = $request->input('idStatus');

      if ($requests[0]->save()) {
        switch ($request->input('idStatus')) {
          case 0:
            $estatus = 'Solicitado';
            break;
          case 2:
            $estatus = 'Rechazado';
            break;
          case 3:
            $estatus = 'En proceso';
            break;
          case 4:
            $estatus = 'Listo para retirar';
            break;
        }
        $nombre = $requests[0]->name;
        $email = $requests[0]->email;
        //$email = 'sergio.carrillo.miele@gmail.com';
        $nombresolicitud = TypeRequest::where('idType', $requests[0]->idType)->get(['description'])[0]->description;
        $subject = 'Solicitud de ' . $nombresolicitud;

        $data = [
          "fecha" => date('d/m/Y', strtotime($requests[0]->created_at)),
          "fechainicio" => date('d/m/Y', strtotime($requests[0]->dateStart)),
          "horainicio" => $requests[0]->timeStart,
          "fechafin" => date('d/m/Y', strtotime($requests[0]->dateEnd)),
          "nombre" => $nombre,
          "email" => $email,
          "motivo" => trim($requests[0]->purpose),
          "tipo" => $requests[0]->idType,
          "nombresolicitud" => $nombresolicitud,
          "estatus" => $estatus,
          "nota" => trim($requests[0]->note),
          "dirigidoa" => trim($requests[0]->userTo)
        ];

        Mail::send('emails.responserequest', $data, function ($message) use ($email, $nombre, $subject) {
          $message->to($email, $nombre)->subject($subject);
        });
      }

      DB::commit();
      return response()->json(['success' => trans('api.success.15')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function updateRequest(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "typeRequest" => "required",
          "purpose" => "required",
          "delivery" => "required",
          "toRequest" => "required",
          "cod_user" => "integer|required",
        ], [//Mensajes
          "typeRequest.required" => trans('api.error.29'),
          "purpose.required" => trans('api.error.29'),
          "delivery.required" => trans('api.error.29'),
          "toRequest.required" => trans('api.error.29'),
          "cod_user.required" => trans('api.error.28'),
          "cod_user.integer" => trans('api.error.28'),
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()],400);
      }

      $requests = Requests::findOrFail($request->idRequest);
      $requests->description = null;
      $requests->note = trim($request->note);
      $requests->save();

      DB::commit();
      return response()->json(['success' => trans('api.success.15')]);
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function setNote(Request $request)
  {
    try {
      $sc = new SystemLinkController;
      DB::beginTransaction();
      $accept = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'xls', 'xlsx', 'ppt', 'pptx'];
      $validator = Validator::make($request->all(),
        [ //Reglas
          "note" => "required"
        ], [//Mensajes
          "note.required" => trans('api.error.29')
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }

      $constanceFiles = [];
      $filesE = $request->file('files');
      if(count($filesE)) {
        foreach ($filesE as $file) {
          if($file->getSize() > 5242880) return response()->json(['error' => trans('api.error.32')], 400);
          $name = strtolower(preg_replace('/\s+/', '', $sc->removeAccented('constance_'.Carbon::now()->timestamp.$file->getClientOriginalName())));
          $arr = explode('.', $name);
          $ext = strtolower(end($arr));
          if(!in_array($ext, $accept)) return response()->json(['error' => trans('api.error.33')], 400);
          $file->move('files/constance/', $name);
          array_push($constanceFiles, $name);
        }
      }

      $requests = Requests::findOrFail($request->input('idRequest'));
      $requests->description = null;
      $requests->note = trim($request->input('note'));
      $requests->fileNote = implode(',', $constanceFiles);
      $requests->save();

      DB::commit();
      return response()->json(['success' => trans('api.success.15')]);
    } catch (\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100'), 'block' => $ex->getTrace()], 400);
    }
  }
}
