<?php

namespace Modules\Request\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use League\Flysystem\Exception;
use modules\Auth\Entities\User;
use Auth;
use DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use modules\Request\Entities\Suggests;
use Validator;

class SuggestsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        Storage::disk('suggest/tmp')->deleteDirectory(Auth::user()->idUser);
        return view('request::suggests')
            ->with('user',
                User::where('user.idUser', Auth::user()->idUser)
                    ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser')
                    ->join('position', 'user_detail.positionId', '=', 'position.positionId')
                    ->join('department', 'user_detail.departmentId', '=', 'department.departmentId')
                    ->get(['user_detail.identityDocument', 'position.position', 'department.department', 'user.name', 'user.lastName', 'user.idUser']));
    }

    public function indexAdmin()
    {
        if(in_array(Auth::user()->idRole, [1, 3])) {
            Storage::disk('suggest/tmp')->deleteDirectory(Auth::user()->idUser);
            return view('request::suggests')
                ->with('user',
                    User::where('user.idUser', Auth::user()->idUser)
                        ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser')
                        ->join('position', 'user_detail.positionId', '=', 'position.positionId')
                        ->join('department', 'user_detail.departmentId', '=', 'department.departmentId')
                        ->get(['user_detail.identityDocument', 'position.position', 'department.department', 'user.name', 'user.lastName', 'user.idUser']))
                ->with('rol', 'admin');
        }else{
            return redirect("/home");
        }
    }

    public function getSuggests(Request $request){
        $suggest = Suggests::join('user', 'suggests.idUser', '=', 'user.idUser')
            ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser')
            ->join('position', 'user_detail.positionId', '=', 'position.positionId')
            ->join('department', 'user_detail.departmentId', '=', 'department.departmentId');
        if(!in_array(Auth::user()->idRole, [1, 3])/* < 0 && Auth::user()->idRole != 2*/){
            $suggest = $suggest->where('suggests.idUser', Auth::user()->idUser);
        }
        if(in_array(Auth::user()->idRole, [2, 3])){
            $suggest = $suggest->where('corporativoId', Auth::user()->corporativoId);
        }
        if($request->has('sede')){
            $suggest = $suggest->where('corporativoId', $request->sede);
        }
        $total   = $suggest->count();
        $suggest = $suggest->whereNotIn('suggests.idStatus', ['6'])
            ->orderBy('suggests.created_at', 'desc')
            ->limit($request->limit)
            ->offset($request->offSet)
            ->get(['idSuggest', 'user.name', 'user.lastName', 'suggests.idUser', 'suggests.purpose', 'suggests.code',
                'suggests.idStatus', 'suggests.note', 'suggests.description', 'user_detail.identityDocument',
                'position.position', 'department.department', 'suggests.created_at']);
        return json_encode([
            'total' => $total,
            'rows' => $suggest
        ]);
    }

    public function uploadFile(Request $request){
        try {
            $files = $request->file('file');
            $routes = [];

            foreach ($files as $file) {
                if($file->getSize() > 5242880) return response()->json(['error' => trans('api.error.32')], 400);
                $exclamation = substr($file->getMimeType(), 0, 5) == 'image' ? '!' : '';
                $name = $file->getClientOriginalName();
                $nameForRoute = 'sugerencia_' . Carbon::now()->timestamp . $name;
                Storage::disk('suggest/tmp')->makeDirectory(Auth::user()->idUser);
                Storage::disk('suggest/tmp')->put(Auth::user()->idUser . '/' . $nameForRoute, file_get_contents($file));
                $route = $exclamation . '[' . $name . '](/File/suggest/tmp/'.Auth::user()->idUser.'/' . $nameForRoute . ')';
                array_push($routes, $route);
            }

            return response()->json(['success' => $routes]);
        }catch (Exception $ex){
            //Storage::disk('suggest/tmp')->deleteDirectory(Auth::user()->idUser);
            return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
        }
    }

    public function submitSuggest(Request $request){
        try{
            DB::beginTransaction();
            $validator = Validator::make($request->all(),
                [ //Reglas
                    /*"date_suggest" => "required",*/
                    "purpose" => "required",
                    "description" => "required",
                    "cod_user" => "integer|required",
                ], [//Mensajes
                    /*"date_suggest.required" => "Fecha inválida",*/
                    "purpose.required" => "Debe ingresar un motivo para la sugerencia",
                    "description.required" => "Debe ingresar una descripción para la sugerencia",
                    "cod_user.required" => "Usuario inválido para la cita médica",
                    "cod_user.integer" => "Usuario inválido para la cita médica",
                ]);
            if ($validator->fails()) {
                DB::rollBack();
                return response()->json(['error' => $validator->errors()->first()]);
            }

            $description = trim($request->description);

            $files = Storage::disk('suggest/tmp')->allFiles(Auth::user()->idUser);
            if(count($files) > 0) {
                foreach ($files as $file) {
                    $f = explode('/', $file)[1];
                    $pos = strpos($request->description, $f);
                    if (!empty($pos)) {
                        Storage::disk('suggest')->move('tmp/' . Auth::user()->idUser . '/' . $f, $f);
                        $search = '/File/suggest/tmp/' . Auth::user()->idUser . '/' . $f;
                        $replace = '/File/suggest/' . $f;
                        $description = str_replace($search, $replace, $description);
                    }
                }
            }
            Storage::disk('suggest/tmp')->deleteDirectory(Auth::user()->idUser);

            $suggest = new Suggests;
            $suggest->idUser = $request->cod_user;
            $suggest->purpose = trim($request->purpose);
            $suggest->description = $description;
            $suggest->idStatus = 0;
            if($suggest->save()){
                $code = 'S'.Auth::user()->idUser.'-0'.$suggest->idSuggest;
                $suggest->code = $code;
                $suggest->save();
            }

            DB::commit();
            return response()->json(["success" => "Sugerencia enviada con exito"]);
        }catch(Exception $ex){
            DB::rollBack();
            return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
        }
    }

    public function deleteSuggest(Request $request){
        try{
            Suggests::where('idSuggest', $request->idSuggest)
                ->delete();
            return response()->json(["success" => "Sugerencia eliminada con exito"]);
        } catch (Exception $ex){
            return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
        }
    }

    public function storeSuggest(Request $request){
        try{
            Suggests::where('idSuggest', $request->idSuggest)
                ->update(['idStatus' => 6]);
            return response()->json(["success" => "Sugerencia archivada con exito"]);
        }catch (Exception $ex){
            return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
        }
    }

    public function setNote(Request $request){
        try{


            Suggests::where('idSuggest', $request->idSuggest)
                ->update(['note' => trim($request->note)]);
            return response()->json(["success" => "Nota guardada con exito"]);
        }catch (Exception $ex){
            return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
        }
    }
}
