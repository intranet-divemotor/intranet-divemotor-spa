<?php

namespace App\Http\Controllers\Request;

use App\Entities\User\Userdetail;
use App\Providers\GoogleServiceCalendar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Entities\User\User;
use Auth;
use DB;
use App\Requests;
use App\TypeRequest;
use App\Answer;
use App\Question;
use Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use Tymon\JWTAuth\Facades\JWTAuth;


class RoomRequestController extends Controller
{

  public function getRequests(Request $request)
  {
    $arrayRequest = array();
    $user = JWTAuth::parseToken()->authenticate();
    $requests = Requests::join('user', 'requests.idUser', '=', 'user.idUser')
      ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser')
      ->join('type_request', 'requests.idType', '=', 'type_request.idType');
    /*if(!in_array(Auth::user()->idRole, [1, 3])){
        $requests = $requests->where('requests.idUser', Auth::user()->idUser);
    }*/
    $requests = $requests->with(['Answer' => function ($query) {
      $query->select('idAnswer', 'description', 'idQuestion', 'idRequest');
    }]);
    $sede = !empty($request->input('sede')) ? $request->input('sede') : Userdetail::where('idUser', $user->idUser)->get(['divPersonal'])[0]->divPersonal;
    if (in_array($user->idRole, [2, 3])) {
      $requests = $requests->where('divPersonal', $sede);
    }
    if (!empty($request->input('sede'))) {
      $requests = $requests->where('divPersonal', $sede);
    }
    $requests = $requests->whereIn('requests.idStatus', [0, 1])
      ->where('requests.idType', $request->input('type'))
      ->get(['idRequest', 'user.name', 'requests.idUser', 'requests.description as medicalDescription', 'type_request.description as type',
        'requests.dateStart', 'requests.timeStart', 'requests.dateEnd', 'requests.timeEnd', 'requests.idStatus', 'requests.note',
        'requests.members', 'requests.purpose', 'user_detail.identityDocument']);
    if (count($requests) > 0) {
      foreach ($requests as $request) {
        $request->{'allmembers'} = explode(',', $request->members);
      }
    }
    foreach ($requests as $r) {
      $arrayRequest[] = array(
        'title' => $r->name,
        'start' => $r->dateStart.' '.date('h:m:s', strtotime($r->timeStart)),
        'request' => $r
      );
    }
    return json_encode(['requests' => $arrayRequest]);
  }

  /**
   * @param Request $request
   * @return mixed
   */
  public function submit(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "timeStart" => "required",
          "timeEnd" => "required",
          "dateEvent" => "required",
          "purpose" => "required",
          "members" => "required",
        ], [//Mensajes
          "timeStart.required" => trans('api.error.27'),
          "timeEnd.required" => trans('api.error.27'),
          "dateEvent.required" => trans('api.error.26'),
          "purpose.required" => trans('api.error.29'),
          "members.required" => trans('api.error.29'),
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }

      if (strtotime($request->input('timeEnd')) < strtotime($request->input('timeStart'))) {
        return response()->json(['error' => trans('api.error.27')], 400);
      }

      $requests = new Requests;
      $requests->idUser = $user->idUser;
      $requests->description = null;
      $requests->idStatus = 0;
      $requests->idType = $request->input('type');
      $requests->dateStart = $request->input('dateEvent');
      $requests->timeStart = $request->input('timeStart');
      $requests->dateEnd = $request->input('dateEvent');
      $requests->timeEnd = $request->input('timeEnd');
      $requests->members = $request->input('members');
      $requests->purpose = $request->input('purpose');

      if ($requests->save() && $request->has('q')) {
        foreach ($request->input('q') as $index => $value) {
          if (gettype($value) == 'array') {
            if (count($value) > 0) {
              foreach ($value as $v) {
                $answer = new Answer;
                $answer->description = $v;
                $answer->idQuestion = $index;
                $answer->idRequest = $requests->idRequest;
                $answer->save();
              }
            }
          } else {
            if (!empty($value)) {
              $answer = new Answer;
              $answer->description = $value;
              $answer->idQuestion = $index;
              $answer->idRequest = $requests->idRequest;
              $answer->save();
            }
          }
        }
      }

      $fecha = date('d/m/Y', strtotime($requests->created_at));
      $nombre = Auth::user()->name;
      $email = $user->email;
      //$email = 'sergio.carrillo.miele@gmail.com';
      $nombresolicitud = TypeRequest::where('idType', $request->input('type'))->get(['description'])[0]->description;
      $subject = 'Solicitud de ' . $nombresolicitud;

      $data = [
        "fecha" => $fecha,
        "fechainicio" => date('Y-m-d'),
        "horainicio" => $request->input('timeStart'),
        "fechafin" => date('Y-m-d'),
        "horafin" => $request->input('timeEnd'),
        "nombre" => $nombre,
        "email" => $email,
        "motivo" => trim($request->input('purpose')),
        "integrantes" => explode(',', $request->members),
        "tipo" => $request->input('type'),
        "nombresolicitud" => $nombresolicitud
      ];

      Mail::send('emails.submitrequest', $data, function ($message) use ($email, $nombre, $subject) {
        $message->to($email, $nombre)->subject($subject);
      });

      DB::commit();
      return response()->json(['success' => trans('api.success.14'), 'events' => $this->getRequests($request)]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function getMembers(Request $request)
  {
    return json_encode(User::where('name', 'ilike', '%' . $request->q . '%')
      ->get(['idUser as id', 'name', 'lastName as last', 'email']));
  }

  public function updateEvent(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "timePickerStart" => "required",
          "timePickerEnd" => "required",
          "date_event_start" => "required",
          "date_event_end" => "required",
          "reason_event" => "required",
          "members" => "required",
          "cod_user" => "integer|required",
        ], [//Mensajes
          "timePickerStart.required" => trans('api.error.27'),
          "timePickerEnd.required" => trans('api.error.27'),
          "date_event_start.required" => trans('api.error.26'),
          "date_event_end.required" => trans('api.error.26'),
          "reason_event.required" => trans('api.error.29'),
          "members.required" => trans('api.error.29'),
          "cod_user.required" => trans('api.error.28'),
          "cod_user.integer" => trans('api.error.28'),
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }
      $dateEventStart = date('Y-m-d', strtotime(str_replace('/', '-', $request->input('date_event_start'))));
      $dateEventEnd = date('Y-m-d', strtotime(str_replace('/', '-', $request->input('date_event_end'))));

      $requests = Requests::find($request->input('idRequest'));
      if (count($requests) <= 0) {
        return response()->json(['error' => trans('api.error.25')], 400);
      }

      $requests->purpose = trim($request->input('reason_event'));
      $requests->dateStart = $dateEventStart;
      $requests->timeStart = strtoupper($request->input('timePickerStart'));
      $requests->dateEnd = $dateEventEnd;
      $requests->timeEnd = strtoupper($request->input('timePickerEnd'));
      $requests->members = $request->input('members');
      $requests->save();

      DB::commit();
      return response()->json(['success' => trans('api.success.15'), 'events' => $this->getRequests($request)]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  /**
   * @param Request $request
   * @return mixed
   */
  public function updateStatusRequest(Request $request)
  {
    try {
      DB::beginTransaction();
      $requests = Requests::where('idRequest', $request->input('idRequest'))
        ->join('user', 'requests.idUser', '=', 'user.idUser')
        ->get(['requests.idRequest', 'user.name', 'user.email', 'requests.created_at', 'requests.dateStart', 'requests.timeStart',
          'requests.dateEnd', 'requests.timeEnd', 'requests.description', 'requests.idStatus', 'requests.note', 'requests.idType', 'requests.members',
          'requests.purpose']);
      if (count($requests) <= 0) {
        return response()->json(['error' => trans('api.error.25')]);
      }

      if (intval($requests[0]->idStatus) == 0) {
        $requests[0]->update([
          'idStatus' => $request->input('idStatus'),
          'note' => trim($request->input('note'))
        ]);

        $estatus = $request->input('idStatus') == 1 ? 'Aprobada' : 'Rechazada';
        $nombre = $requests[0]->name;
        $nombresolicitud = TypeRequest::where('idType', $requests[0]->idType)->get(['description'])[0]->description;
        $subject = 'Solicitud de ' . $nombresolicitud;
        $remitentes = [$requests[0]->email];
        if ($request->input('idStatus') == 1) {
          foreach (explode(',', $requests[0]->members) as $member) {
            array_push($remitentes, $member);
          }
        }

        $data = [
          "fecha" => date('d/m/Y', strtotime($requests[0]->created_at)),
          "fechainicio" => date('d/m/Y', strtotime($requests[0]->dateStart)),
          "horainicio" => $requests[0]->timeStart,
          "fechafin" => date('d/m/Y', strtotime($requests[0]->dateEnd)),
          "horafin" => $requests[0]->timeEnd,
          "nombre" => $nombre,
          "email" => $requests[0]->email,
          "motivo" => trim($requests[0]->purpose),
          "integrantes" => explode(',', $requests[0]->members),
          "tipo" => $requests[0]->idType,
          "nombresolicitud" => $nombresolicitud,
          "estatus" => $estatus,
          "nota" => trim($requests[0]->note)
        ];

        foreach ($remitentes as $email) {
          Mail::send('emails.responserequest', $data, function ($message) use ($email, $subject) {
            $message->to($email, 'Usuario Intranet')->subject($subject);
          });
        }

        DB::commit();
        return response()->json(['success' => trans('api.success.15'), 'events' => $this->getRequests($request)]);
      } else {
        DB::rollBack();
        return response()->json(['error' => trans('api.error.24')], 400);
      }
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function deleteRequest(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "idRequest" => "integer|required"
        ], [//Mensajes
          "idRequest.required" => "Evento inválido",
          "idRequest.integer" => "Evento inválido"
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }
      Requests::where('idRequest', $request->idRequest)->delete();
      DB::commit();
      return response()->json(['success' => trans('api.success.16'), 'events' => $this->getCurrentRequests($request)]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }
}
