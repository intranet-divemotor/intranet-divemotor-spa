<?php
namespace App\Http\Controllers\SystemLink;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Entities\Link\Link;
use App\Entities\Link\FavoriteLink;
use Carbon\Carbon;
use File;
use Intervention\Image\Facades\Image;
use Tymon\JWTAuth\Facades\JWTAuth;
class SystemLinkController extends Controller
{
  public function removeAccented($str){
    $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
      'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
      'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
      'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
      'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', '%' => '', ' ' => '', '/' => '', '\\' => '', '(' => '', ')' => '' );
    return strtr( $str, $unwanted_array);
  }


  public function fetch(Request $r){

    $links = Link::limit($r->limit)->offset($r->offset)
      ->leftJoin('area', 'area.cod' , '=', 'link.areaId');
    if(!empty($r->input('search'))){
      $event = $links->where('link.name', 'like' , '%'.$r->input('search').'%');
    }
    $links = $links->get(['link.id', 'link.areaId', 'link.name', 'link.urlPath', 'link.iconPath', 'link.type', 'area.area']);
    $total = count($links);
    return json_encode(array('total' => $total, 'rows' => $links));
  }

  public function fetchBanner(Request $r){

    $links = Link::limit(10)->where('areaId', 2);
    $links = $links->get(['link.id', 'link.areaId', 'link.name', 'link.urlPath', 'link.iconPath', 'link.type']);
    return json_encode(array('rows' => $links));
  }

  public function submitLink(Request $r){
    if($r->input('id') == 0) {
      $files = $r->file('files');
      $fileName = '';
      foreach ($files as $file) {
        $fileName = preg_replace('/\s+/', '',  $this->removeAccented(Carbon::now()->timestamp.$file->getClientOriginalName()));
        $img = Image::make(file_get_contents($file));
        if ($r->tipoBanner === 1) {
        $cropperData = json_decode($r->input('cropperData'));
        $img->crop((int)$cropperData->width, (int)$cropperData->height, (int)$cropperData->x, (int)$cropperData->y);
        }

        $img->save('files/link/'.$fileName, 50);
      }
      $l = new Link();
      $l->areaId = $r->tipoBanner;//$r->input('areaId');
      $l->name = $r->input('name');
      $l->urlPath = $r->input('urlPath');
      $l->iconPath = $fileName;
      $l->type = $r->input('type');
      if ($l->save()) {
        return ['success' => trans('layouts::api.success.2.message')];
      } else {
        return ['error' => trans('layouts::api.error.100.error')];
      }
    }else {
      $l = Link::find($r->input('id'));
      $l->areaId = $r->tipoBanner;
      $l->name = $r->input('name');
      $l->urlPath = $r->input('urlPath');
      $l->type = $r->input('type');

      if ($r->hasFile('files')) {
        unlink('files/link/' . $l->iconPath);
        $files = $r->file('files');
        $fileName = '';
        foreach ($files as $file) {
          $fileName = preg_replace('/\s+/', '', $this->removeAccented(Carbon::now()->timestamp . $file->getClientOriginalName()));
          $img = Image::make(file_get_contents($file));
          if ($r->tipoBanner === 1) {
          $cropperData = json_decode($r->input('cropperData'));
          $img->crop((int)$cropperData->width, (int)$cropperData->height, (int)$cropperData->x, (int)$cropperData->y);
          }
          $img->save('files/link/' . $fileName, 50);
        }
        $l->iconPath = $fileName;
      }

      if ($l->save()) {
        return ['success' => trans('layouts::api.success.2.message')];
      } else {
        return ['error' => trans('layouts::api.error.100.error')];
      }
    }



  }


  public function fetchMyLinks(Request $r){
    $data = Link::where('areaId', 'null')->orWhere('areaId', '1')
    ->get(['name', 'urlPath', 'iconPath', 'type']);
    return json_encode($data);
  }
  public function delete(Request $r){
    $e = Link::find($r->input('id'));
    //unlink('files/link/'.$e->iconPath);
    $e->delete();
  }

  /*Favoritos*/

  public function fetchFavorite(Request $r){
    $user = JWTAuth::parseToken()->authenticate();
    return json_encode(FavoriteLink::where('userId', $user->idUser)->get(['id', 'name', 'urlPath', 'iconPath', 'type', 'parent']));
  }


  public function submitFavorite(Request $r){
    $user = JWTAuth::parseToken()->authenticate();
    if($r->input('id') == 0) {
      $fileName = '';
      if($r->hasFile('files')) {
        $files = $r->file('files');
        foreach ($files as $file) {
          $fileName = preg_replace('/\s+/', '', $this->removeAccented(Carbon::now()->timestamp . $user->idUser . $file->getClientOriginalName()));
          $img = Image::make(file_get_contents($file));
          $cropperData = json_decode($r->input('cropperData'));
          $img->crop((int)$cropperData->width, (int)$cropperData->height, (int)$cropperData->x, (int)$cropperData->y);
          $img->save('files/link/' . $fileName, 50);
        }
      }
      $l = new FavoriteLink;
      $l->name = $r->input('name');
      $l->urlPath = $r->input('urlPath');
      $l->iconPath = $fileName;
      $l->type = $r->input('type');
      $l->userId = $user->idUser;
      if ($l->save()) {
        return ['success' => trans('layouts::api.success.2.message')];
      } else {
        return ['error' => trans('layouts::api.error.100.error')];
      }
    }else {
      $l = FavoriteLink::find($r->input('id'));
      $l->name = $r->input('name');
      $l->urlPath = $r->input('urlPath');
      $l->type = $r->input('type');

      if ($r->hasFile('files')) {
        unlink('files/link/' . $l->iconPath);
        $files = $r->file('files');
        $fileName = '';
        foreach ($files as $file) {
          $fileName = preg_replace('/\s+/', '', $this->removeAccented(Carbon::now()->timestamp.$user->idUser.$file->getClientOriginalName()));
          $img = Image::make(file_get_contents($file));
          $cropperData = json_decode($r->input('cropperData'));
          $img->crop((int)$cropperData->width, (int)$cropperData->height, (int)$cropperData->x, (int)$cropperData->y);
          $img->save('files/link/' . $fileName, 50);
        }
        $l->iconPath = $fileName;
      }

      if ($l->save()) {
        return ['success' => trans('layouts::api.success.2.message')];
      } else {
        return ['error' => trans('layouts::api.error.100.error')];
      }
    }

  }

  public function deleteFavorite(Request $r){
    $f = FavoriteLink::find($r->input('id'));
    try {
      $f->delete();
      //unlink('files/link/' . $f->iconPath);
    }catch (\Exception $e) {}

  }

  public function uploadImage(Request $r){
    $files = $r->file('files');
    $fileName = '';
    foreach ($files as $file) {
      $fileName = preg_replace('/\s+/', '',  $this->removeAccented(Carbon::now()->timestamp.$file->getClientOriginalName()));
      $img = Image::make(file_get_contents($file));
      $cropperData = json_decode($r->input('cropperData'));
      $img->crop((int)$cropperData->width, (int)$cropperData->height, (int)$cropperData->x, (int)$cropperData->y);
      $img->save('files/notification/'.$fileName, 50);
    }

    return '/files/notification/'.$fileName;
  }



}
