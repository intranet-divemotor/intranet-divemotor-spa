<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use modules\Admin\Entities\Notifications;
use modules\Auth\Entities\User;
use modules\Postulation\Entities\Postulation;
use modules\Postulation\Entities\UserPostulation;
use Illuminate\Support\Facades\Storage;
use modules\Admin\Http\Controllers\SystemLinkController;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use File;
use Auth;
use Intervention\Image\Facades\Image;

class PostulationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('postulation::index');
    }

    public function postulations()
    {
        return view('postulation::postulations');
    }

    public function chiefAdmin()
    {
        return view('postulation::chiefAdmin');
    }

    public function postulationeventDetail(Request $r)
    {
        return view('postulation::postulationDetail')->with('id', $r->id);
    }

    public function searchPost(Request $r){
        $p = Postulation::limit($r->limit)->offset($r->offset)
            ->join('area', 'area.areaId' , '=',  'postulation.areaId')
            ->join('department', 'department.departmentId' , '=', 'postulation.departmentId')
            ->leftJoin('user_postulation as u', 'u.idPostulation', '=', 'postulation.id')
            ->groupBy(['postulation.id', 'postulation.idUser', 'postulation.areaId', 'postulation.departmentId', 'postulation.title', 'postulation.summary',
                'postulation.detail', 'postulation.coverPhoto', 'postulation.startDate', 'postulation.endDate', 'postulation.created_at', 'postulation.functions',
                'postulation.requirements', 'postulation.extraFile', 'area.area', 'department.department']);
        $total = count(Postulation::count());
        $links = $p->get(['postulation.id', 'postulation.idUser', 'postulation.areaId', 'postulation.departmentId', 'postulation.title', 'postulation.summary',
            'postulation.detail', 'postulation.coverPhoto', 'postulation.startDate', 'postulation.endDate', 'postulation.created_at', 'postulation.functions',
            'postulation.requirements', 'postulation.extraFile', 'area.area', 'department.department', DB::raw('count(u.id) as countPots')]);
        return array('total' => $total, 'rows' => $links);
    }

    public function searchPostulates(Request $r){

        $post =  UserPostulation::limit($r->limit)->offset($r->offset)
            ->join('user as u', 'u.idUser', '=', 'user_postulation.idUser')
            ->join('user_detail', 'user_detail.idUser' , '=', 'u.idUser')
            ->join('position', 'position.positionId' , '=', 'user_detail.positionId')
            ->join('area', 'area.areaId' , '=', 'user_detail.areaId')
            ->where('user_postulation.idPostulation', $r->id)
            ->select(['u.name', 'u.avatar', 'area.area', 'position.position', 'user_postulation.*'])->get();

        return array('total' => UserPostulation::where('user_postulation.idPostulation', $r->id)->count(), 'rows' => $post);
    }

    public function submitPost(Request $r){
        $sc = new SystemLinkController();
        $pathEvent =  Storage::disk('postulation')->getDriver()->getAdapter()->getPathPrefix();
        try{
            if($r->id == 0) {
                $fileName = preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp.$r->file('coverPhoto')->getClientOriginalName()));
                $img = Image::make(file_get_contents($r->file('coverPhoto')));
                $img->crop((int)$r->cropWidth, (int)$r->cropHeight, (int)$r->startX, (int)$r->startY);
                $img->save($pathEvent.$fileName, 60);
                $exists = Storage::disk('postulation')->exists($fileName);
                if ($exists) {
                    $p = new Postulation();
                    $p->idUser = Auth::user()->idUser;
                    $p->areaId = $r->areaId;
                    $p->departmentId = $r->departmentId;
                    $p->title = $r->title;
                    $p->summary = $r->summary;
                    $p->detail = $r->detail;
                    $p->coverPhoto = $fileName;
                    $p->startDate = date('Y-m-d', strtotime($r->startDate));
                    $p->endDate = date('Y-m-d', strtotime($r->endDate));
                    $p->functions = $r->functions;
                    $p->requirements = $r->requirements;
                    if($r->hasFile('extraFile')) {
                        $fileNameExtra = preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp . $r->file('extraFile')->getClientOriginalName()));
                        Storage::disk('postulation')->put($fileNameExtra, file_get_contents($r->file('extraFile')));
                        $p->extraFile = $fileNameExtra;
                    }

                    if ($p->save()) {
                        /*$n = new Notifications();
                        $n->idUser = 3595;
                        $n->message = 'Nueva postulacion disponible';*/
                        return ['success' => trans('layouts::api.success.2.message'),
                            'notification' => ['icon' => 'fa-calendar', 'action' => '/postulation',
                                'title' =>  $p->title, 'date' => $p->created_at, 'id' => $p->id , 'body' => $p->summary, 'pathIcon' => 'File/postulation/'.$p->coverPhoto]];

                    } else {
                        return ['error' => trans('layouts::api.error.100.error')];
                    }
                }
            }else{
                $p = Postulation::find($r->id);
                $p->idUser = Auth::user()->idUser;
                $p->areaId = $r->areaId;
                $p->departmentId = $r->departmentId;
                $p->title = $r->title;
                $p->summary = $r->summary;
                $p->detail = $r->detail;
                $p->startDate = date('Y-m-d', strtotime($r->startDate));
                $p->endDate = date('Y-m-d', strtotime($r->endDate));
                $p->functions = $r->functions;
                $p->requirements = $r->requirements;

                if($r->hasFile('coverPhoto')){
                    $fileName = preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp.$r->file('coverPhoto')->getClientOriginalName()));
                    Storage::Delete(unlink(public_path('File/postulation/'.$r->fileName)));
                    $img = Image::make(file_get_contents($r->file('coverPhoto')));
                    $img->crop((int)$r->cropWidth, (int)$r->cropHeight, (int)$r->startX, (int)$r->startY);
                    $img->save($pathEvent.$fileName, 60);
                    $p->coverPhoto = $fileName;
                }

                if($r->hasFile('extraFile')){
                    $fileNameExtra = preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp.$r->file('extraFile')->getClientOriginalName()));
                    Storage::Delete(unlink(public_path('File/postulation/'.$r->fileNameExtra)));
                    Storage::disk('postulation')->put($fileNameExtra, file_get_contents($r->file('extraFile')));
                    $p->extraFile = $fileNameExtra;
                }

                if ($p->save()) {
                    return ['success' => trans('layouts::api.success.2.message')];
                } else {
                    return ['error' => trans('layouts::api.error.100.error')];
                }
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function searchPostForAll(Request $r){

        $event = Postulation::limit($r->limit)->offset($r->offset)
            ->leftJoin('area', 'area.areaId' , '=',  'postulation.areaId')
            ->leftJoin('department', 'department.departmentId' , '=', 'postulation.departmentId')
            ->leftJoin('user_postulation', 'user_postulation.idPostulation' , '=', 'postulation.id');

        if(!empty($r->area)){
            $event = $event->where('postulation.areaId', $r->area);
        }


        if(!empty($r->title)){
            $event = $event->where('postulation.title', 'LIKE', '%'.$r->title.'%');
        }

        if($r->idUser == "true"){
            $event = $event->where('user_postulation.idUser', DB::raw(Auth::user()->idUser));
        }

        $event = $event->where('postulation.endDate', '>', "'".Carbon::createFromFormat('Y-m-d', Carbon::now()->toDateString())->toDateString()."'");

        $total = (Postulation::count() / $r->limit);
        $links = $event->get(['postulation.id', 'postulation.idUser', 'postulation.areaId', 'postulation.departmentId', 'postulation.title', 'postulation.summary',
            'postulation.detail', 'postulation.coverPhoto', 'postulation.startDate', 'postulation.endDate', 'postulation.created_at', 'postulation.functions',
            'postulation.requirements', 'postulation.extraFile', 'area.area', 'department.department']);
        return json_encode(array('total' => $total, 'rows' => $links));
    }

    /**
     * Metodo para solicitar aprobacion de postulacion
     * @param Request $r
     * @return array
     */

    public function postulate(Request $r){
        $p = new UserPostulation();
        $p->idUser = Auth::user()->idUser;
        $p->idPostulation = $r->id;
        $p->requested = true;
            if ($p->save()) {
                $user = User::join('user_detail as ud', 'ud.idUser', '=', 'user.idUser')
                    ->join('user_detail as bs', 'bs.PosicionHCM', '=', 'ud.higherPosition')
                    ->where('user.idUser', Auth::user()->idUser)
                ->select('bs.idUser')->first();;

                $email = User::find($user->idUser);
                $data = [
                    "event" => Postulation::find($r->id)->firstOrFail(),
                    "boos" => $email->name
                ];
                Mail::send('layouts::emails.requestpostulate', $data, function($message) use ($email) {

                    $message->to($email->email, 'Jon Doe')->subject('Postulación');
                });
                return ['success' => trans('layouts::api.success.2.message'),
                    'notification' => ['icon' => 'fa-calendar', 'action' => '/postulation/chiefAdmin',
                        'title' =>  Auth::user()->name.' solicita aprovación', 'date' => $p->created_at, 'id' => $p->id , 'body' => $p->summary, 'pathIcon' => '', 'to' => $email->idUser]];
            } else {
                return ['error' => trans('layouts::api.error.100.error')];
            }
    }

    public function postulationDetail(Request $r){
        $p = DB::table('postulation')
            ->join('area', 'area.areaId' , '=',  'postulation.areaId')
            ->join('department', 'department.departmentId' , '=', 'postulation.departmentId')
            ->leftJoin('user_postulation', 'user_postulation.idPostulation' , '=', 'postulation.id')
            ->where('postulation.id', $r->id)
            ->first();

        $u = User::join('user_detail as u', 'u.idUser', '=', 'user.idUser')->join('position as p', 'p.positionId', '=', 'u.positionId')->first();

        return view('postulation::postulationDetail')->with('id', $r->id)->with('postulation', $p)->with('position', $u->position)->with('countPost', UserPostulation::where([['idPostulation', $r->id], ['postulate', true]])->count());
    }

    public function exportParticipants(Request $r){
        $objPHPExcel = new \PHPExcel();
        $participants = EventUser::join('user', 'event_user.idUser', '=', 'user.idUser')
            ->join('user_detail', 'user_detail.idUser' , '=', 'user.idUser')
            ->leftJoin('organization', 'organization.corporativoId' , '=', 'user.corporativoId')
            ->leftJoin('position', 'position.positionId' , '=', 'user_detail.positionId')
            ->leftJoin('area', 'area.areaId' , '=', 'user_detail.areaId')
            ->where('event_user.idEvent', $r->id)
            ->get(['user.name', 'user.email', 'user.lastName','organization.nameStructure', 'position.position', 'area.area', 'event_user.created_at']);

        $objPHPExcel->removeSheetByIndex(0);
        $objPHPExcel->createSheet(0);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
        $objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Inscritos en el evento '.$r->title);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A2:G2')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(30);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:G2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Nombre')->setShowGridlines(false)
            ->setCellValue('B2', 'Apellido')->setShowGridlines(false)
            ->setCellValue('C2', 'Email')->setShowGridlines(false)
            ->setCellValue('D2', 'Sede')->setShowGridlines(false)
            ->setCellValue('E2', 'Cargo')->setShowGridlines(false)
            ->setCellValue('F2', 'Area')->setShowGridlines(false)
            ->setCellValue('G2', 'Fecha y Hora')->setShowGridlines(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);



        $f = 3;
        foreach($participants as $p){
            $objPHPExcel->getActiveSheet()->getRowDimension($f)->setRowHeight(20);

            $objPHPExcel->getActiveSheet()->getStyle('D'.$f.':E'.$f)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $timestampCreate = strtotime($p->created_at);

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$f, $p->name)->setShowGridlines(false)
                ->setCellValue('B'.$f, $p->lastName)->setShowGridlines(false)
                ->setCellValue('C'.$f, $p->email)->setShowGridlines(false)
                ->setCellValue('D'.$f, $p->nameStructure)->setShowGridlines(false)
                ->setCellValue('E'.$f, $p->position)->setShowGridlines(false)
                ->setCellValue('F'.$f, $p->area)->setShowGridlines(false)
                ->setCellValue('G'.$f, (date('d-m-Y', $timestampCreate).' '.date('h:i:s a', $timestampCreate)))->setShowGridlines(false);

            $f++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Reporte');
        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename='".'Inscritos en el evento '.$r->title.".pdf'");
        ob_clean();
        $objWriter->save("php://output");
    }

    public function deletePost(Request $r){
        $e = Postulation::find($r->id);
        Storage::Delete(unlink(public_path('File/postulation/'.$e->coverPhoto)));
        $e->delete();
        UserPostulation::where('idPostulation', $r->id)->delete();
    }

    public function searchChief(Request $r){
        $p = Postulation::limit($r->limit)->offset($r->offset)
            ->join('area', 'area.areaId' , '=',  'postulation.areaId')
            ->join('department', 'department.departmentId' , '=', 'postulation.departmentId')
            ->join('user_postulation', 'user_postulation.idPostulation' , '=', 'postulation.id')
            ->join('user', 'user.idUser' , '=', 'user_postulation.idUser')
            ->join('user_detail', 'user_detail.idUser' , '=', 'user.idUser')
            ->join('position', 'position.positionId' , '=', 'user_detail.positionId')
            ->join('area as ua', 'ua.areaId' , '=',  'user_detail.areaId')
            ->join('user_detail as b', 'b.idUser' , '=', DB::raw(Auth::user()->idUser))
            ->where([['user_postulation.authorized', null], [DB::raw('"user_detail"."higherPosition"'), '=', DB::raw('b."PosicionHCM"')]])
            ->get(['user.name', 'user.avatar', 'position.position', 'ua.area as userArea', 'area.area', 'user_postulation.cvFile',
                'postulation.title', 'postulation.coverPhoto', 'department.department', 'user_postulation.reasons', 'user_postulation.id', 'user_postulation.authorized']);
        $total = Postulation::count();

        return array('total' => $total, 'rows' => $p);

    }

    public function approvepostulate(Request $r){
        $p = UserPostulation::find($r->id);
        $p->authorized = $r->authorized;
        $p->reasons = $r->reasons;

        if ($p->save()) {
            $u = User::find($p->idUser);
            $email = $u->email;
            $data = [
                "event" => Postulation::find($p->idPostulation)->firstOrFail(),
                "authorized" => $p->authorized
            ];
            Mail::send('layouts::emails.approvepostulate', $data, function($message) use ($email) {
                $message->to($email, 'Jon Doe')->subject('Respuesta postulación');
            });
            return ['success' => trans('layouts::api.success.2.message'),
                'notification' => ['icon' => 'fa-calendar', 'action' => '/postulation',
                    'title' =>  'Respuesta de solicitud', 'date' => $p->created_at, 'to' => $p->idUser , 'body' => 'Te han '.($p->authorized == "true" ? 'aprobado' : 'rechazado').' tu postulación' , 'pathIcon' => '']];
        } else {
            return ['error' => trans('layouts::api.error.100.error')];
        }
    }

    public function sendpostulate(Request $r){
        $sc = new SystemLinkController();
        $fileName = preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp.$r->file('cvFile')->getClientOriginalName()));
        Storage::disk('postulation')->put($fileName, file_get_contents($r->file('cvFile')));
        $p = UserPostulation::where([['idPostulation', $r->id], ['idUser', Auth::user()->idUser]])->first();
        $p->career = $r->career;
        $p->college = $r->college;
        $p->stateCollege = $r->stateCollege;
        $p->cvFile = $fileName;
        $p->reasons = $r->reasons;
        $p->postulate = true;

        if ($p->save()) {

            /*$email = Auth::user()->email;
            $data = [
                "event" => Postulation::find($r->id)->firstOrFail()
            ];
            Mail::send('layouts::emails.requestpostulate', $data, function($message) use ($email) {
                $message->to($email, 'Jon Doe')->subject('Postulación');
            });*/
            return ['success' => trans('layouts::api.success.2.message')];
        } else {
            return ['error' => trans('layouts::api.error.100.error')];
        }
    }



}
