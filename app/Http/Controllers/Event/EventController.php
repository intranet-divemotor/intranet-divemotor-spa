<?php

namespace App\Http\Controllers\Event;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\SystemLink\SystemLinkController;
use App\Entities\Event\Event;
use App\Entities\Event\EventUser;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\MenuController;

class EventController extends Controller
{
    public function fetch(Request $r){
        $event = Event::limit($r->limit)->offset($r->offset)
            ->join('user', 'user.idUser' , '=', 'event.idUser')
            ->leftJoin('event_user', 'event_user.idEvent', '=', 'event.id')
            ->with(['Question' => function($query) {
                $query->get();
            }])
            ->groupBy(['event.id', 'event.idUser', 'event.region', 'event.headquarters', 'event.importance', 'event.areas', 'event.location',
              'event.title', 'event.summary', 'event.detail', 'event.coverPhoto', 'event.startDate', 'event.endDate', 'event.hour', 'event.hourend', 'event.created_at',
              'user.name'])->orderBy('event.id', 'DESC');

      if(!empty($r->input('search'))){
        $event = $event->where('event.title', 'like' , '%'.$r->input('search').'%');
      }


        $total = Event::count();
        $links = $event->get(['event.id', 'event.idUser', 'event.region', 'event.headquarters', 'event.importance', 'event.areas', 'event.location',
            'event.title', 'event.summary', 'event.detail', 'event.coverPhoto', 'event.startDate', 'event.endDate', 'event.hour', 'event.hourend', 'event.created_at',
            'user.name', DB::raw('count(event_user."idEvent") as participans')]);
        return array('total' => $total, 'rows' => $links);
    }

    public function submit(Request $r){
        try{
            $user = JWTAuth::parseToken()->authenticate();
            $areas = json_decode($r->input('areas'));
            $isAllAreas = false;

            if(!empty($areas)){
              foreach ($areas as $area) {
                if($area === 'all') $isAllAreas = true;

              }
            }

          $sc = new SystemLinkController();
            if($r->input('id') == 0) {
                $files = $r->file('files');
                $fileName = '';
                foreach ($files as $file) {
                    $fileName = preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp.$file->getClientOriginalName()));
                    $img = Image::make(file_get_contents($file));
                    $cropperData = json_decode($r->input('cropperData'));
                    $img->crop((int)$cropperData->width, (int)$cropperData->height, (int)$cropperData->x, (int)$cropperData->y);
                    $img->save('files/event/'.$fileName, 50);
                }
                $e = new Event();
                $e->idUser = $user->idUser;
                $e->region = $r->input('region');
                //$e->headquarters = $r->input('region') == 'all' ? null : $r->input('headquarters');
                $e->headquarters = $r->input('headquarters');
                $e->importance = $r->input('importance');
                //$e->areas = $r->input('region') === 'all' ? null : $r->input('headquarters')== 'all' ? null : $r->input('areas');
                $e->areas = $r->input('areas');
                $e->title = $r->input('title');
                $e->summary = $r->input('summary');
                $e->detail = $r->input('detail');
                $e->coverPhoto = $fileName;
                $e->startDate = date('Y-m-d', strtotime($r->input('startDate')));
                $e->endDate = date('Y-m-d', strtotime($r->input('endDate')));
                $e->hour = $r->input('hour');
                $e->hourend = $r->input('hourend');
                $e->location = $r->input('location');
                if ($e->save()) {
                  return json_encode(Event::find($e->id));
                   /* $jquestions = json_decode($r->questions);
                    EventQuestion::where('idEvent', $r->id)->delete();
                    foreach ($jquestions as $jquestion){
                        $question = new EventQuestion();
                        $question->idEvent = $e->id;
                        $question->description = $jquestion->description;
                        $question->type = $jquestion->type;
                        $question->options = $jquestion->options;
                        $question->save();
                    }
                    return ['success' => trans('layouts::api.success.2.message'),
                        'notification' => ['icon' => 'fa-calendar', 'action' => '/event/detail/'.$e->id,
                            'title' =>  $e->title, 'date' => $e->created_at, 'id' => $e->id , 'body' => $e->summary, 'pathIcon' => "File/event/".$e->coverPhoto]];*/
                } else {
                    return ['error' => trans('layouts::api.error.100.error')];
                }

            }else{

                $e = Event::find($r->input('id'));
                $e->region = $r->input('region');
                //$e->headquarters = $r->input('region') == 'all' ? null : $r->input('headquarters');
                $e->importance = $r->input('importance');
                //$e->areas = $r->input('region') === 'all' ? null : $r->input('headquarters')== 'all' ? null : $r->input('areas');
              $e->headquarters = $r->input('headquarters');
              /*$e->areas = $r->input('headquarters') === 'all' ? null : ($isAllAreas ? 'all' : str_replace(['[', ']', '"'], '', $r->input('areas')));*/
                $e->areas = $r->input('headquarters') === 'all' ? null : $r->input('areas');
                $e->title = $r->input('title');
                $e->summary = $r->input('summary');
                $e->detail = $r->input('detail');
                $e->startDate = date('Y-m-d', strtotime($r->input('startDate')));
                $e->endDate = date('Y-m-d', strtotime($r->input('endDate')));
                $e->hour = $r->input('hour');
                $e->hourend = $r->input('hourend');
                $e->location = $r->input('location');
                $e->idUser = $user->idUser;

                if($r->hasFile('files')) {
                  unlink('files/event/'.$e->coverPhoto);
                  $files = $r->file('files');
                  $fileName = '';
                  foreach ($files as $file) {
                    $fileName = preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp.$file->getClientOriginalName()));
                    $img = Image::make(file_get_contents($file));
                    $cropperData = json_decode($r->input('cropperData'));
                    $img->crop((int)$cropperData->width, (int)$cropperData->height, (int)$cropperData->x, (int)$cropperData->y);
                    $img->save('files/event/'.$fileName, 50);
                  }
                  $e->coverPhoto = $fileName;
                }
                //$jquestions = json_decode($r->questions);
                if ($e->save()) {
                  return json_encode(Event::find($e->id));
                   /* EventQuestion::where('idEvent', $r->id)->delete();

                    foreach ($jquestions as $jquestion){
                        $question = new EventQuestion();
                        $question->idEvent = $e->id;
                        $question->description = $jquestion->description;
                        $question->type = $jquestion->type;
                        $question->options = $jquestion->options;
                        $question->save();
                    }*/
                } else {
                    return ['error' => trans('layouts::api.error.100.error')];
                }
            }
        } catch (\Exception $e) {
          return $e->getMessage();
        }
    }

    public function fetchForAll(Request $r){
      $user = JWTAuth::parseToken()->authenticate();
      $mc = new MenuController();
      $divisionId = $mc->getIdDivision($user->idUser);
      $event = Event::limit($r->limit)->offset($r->offset)
        ->leftJoin('divisions', DB::raw('divisions."code"::text') , '=', 'event.headquarters')
        /*->leftJoin('organization_region', DB::raw('organization_region."idRegion"::text') , '=', 'event.region')*/
        ->join('user', 'user.idUser' , '=',  DB::raw($user->idUser))
        ->join('user_detail', 'user_detail.idUser' , '=', 'user.idUser')
        ->leftJoin('event_user', function($join) use ($user){
          $join->on('event_user.idEvent', '=', 'event.id');
          $join->on('event_user.idUser','=', DB::raw($user->idUser));

        })
        ->whereRaw('\'"all"\' = any (string_to_array(replace(replace(event.headquarters, \'[\', \'\'), \']\', \'\'), \',\'))')
        ->orWhereRaw('\''.$divisionId.'\' = any (string_to_array(replace(replace(event.headquarters, \'[\', \'\'), \']\', \'\'), \',\'))');


      /*->where(DB::raw('event.headquarters::text'), '=' , $divisionId)
        ->orWhere(DB::raw('event.headquarters::text'), '=' , "'ALL'");*/

        if(!empty($r->input('month'))){
            $event = $event->where(DB::raw('DATE_PART(\'month\', event."startDate")'), '=' , $r->month);
        }

        if(!empty($r->input('year'))){
            $event = $event->where(DB::raw('DATE_PART(\'year\', event."startDate")'), '=' , $r->year);
        }

        if(!empty($r->input('title'))){
            $event = $event->where('event.title', 'like' , '%'.$r->title.'%');
        }

        if(!empty($r->input('area'))){

            $event = $event->whereRaw(($r->idUser == 'true' ? ' event.region = \'all\' or event.headquarters = \'all\' or event.headquarters IS NULL or ' : '').$r->area .' in (SELECT arr[i]::int
                                  FROM (
                                         SELECT generate_series(1, array_upper(arr, 1)) AS i, arr
                                         FROM (SELECT regexp_split_to_array("event".areas, \',\') arr) t
                                       ) t) or event.areas is null');
        }



        $total = (Event::count() / $r->limit);
        $links = $event->get(['event.id', 'event.idUser', 'event.region', 'event.headquarters', 'event.importance', 'event.areas',
            'event.title', 'event.summary', 'event.detail', 'event.coverPhoto', 'event.startDate', 'event.endDate', 'event.hour', 'event.created_at', 'event.location',
            'user.name', 'divisions.description', DB::raw('DATE_PART(\'year\', event."startDate")'), 'user_detail.codArea', 'event_user.idEvent as signedUp']);

        $res = [];

        foreach ($links as $l){
          if($l->areas === null || $l->areas === 'null') {
            $res[] = $l;
          }
          else{
            foreach (json_decode($l->areas) as $a) {
              if($a === (string)$l->codArea || $a === 'all' || $a === null) $res[] = $l;
            }
         }

        }

        return array('total' => $total, 'rows' => $res);
    }

    public function confirmEvent(Request $r){
      $user = JWTAuth::parseToken()->authenticate();
        if($r->flag == 'true'){
            $e = new EventUser();
            $e->idUser = $user->idUser;
            $e->idEvent = $r->input('idEvent');
            if ($e->save()) {
                $email = $user->email;
                $data = [
                    "event" => Event::find($r->input('idEvent'))->firstOrFail()
                ];
                /*Mail::send('layouts::emails.event', $data, function($message) use ($email) {
                    $message->to($email, 'Jon Doe')->subject('Inscripción Evento');
                });*/
                /*
                                if(isset($r->questions)){
                                    foreach ($r->questions as $index => $value){
                                        if(gettype($value) == 'array') {
                                            if(count($value) > 0) {
                                                foreach ($value as $v) {
                                                    $answer = new Answer;
                                                    $answer->description = $v;
                                                    $answer->idQuestion = $index;
                                                    $answer->idRequest = $r->idEvent;
                                                    $answer->save();
                                                }
                                            }
                                        }else{
                                            if(!empty($value)) {
                                                $answer = new Answer;
                                                $answer->description = $value;
                                                $answer->idQuestion = $index;
                                                $answer->idRequest = $r->idEvent;
                                                $answer->save();
                                            }
                                        }
                                    }
                                }*/


              return ['success' => $r->input('idEvent')];


            } else {
                return ['error' => trans('layouts::api.error.100.error')];
            }
        }else if($r->flag == 'false'){
            DB::table('event_user')->where([['event_user.idUser', '=', DB::raw($user->idUser)], ['event_user.idEvent', '=', $r->input('idEvent')]])->delete();
          return ['success' => null];
        }

    }

    public function fetchDetail(Request $r){
      $user = JWTAuth::parseToken()->authenticate();
        $event = Event::leftJoin('divisions', DB::raw('divisions."code"::text') , '=', 'event.headquarters')
            ->join('user', 'user.idUser' , '=',  DB::raw($user->idUser))
            ->join('user_detail', 'user_detail.idUser' , '=', 'user.idUser')
            ->leftJoin('event_user', function($join) use ($user)
            {
                $join->on('event_user.idEvent', '=', 'event.id');
                $join->on('event_user.idUser','=', DB::raw($user->idUser));

            })
            ->with(['Question' => function($query) {
                $query->get();
            }])
            ->where('event.id', $r->input('id'));

        return json_encode($event->get(['event.id', 'event.idUser', 'event.region', 'event.headquarters', 'event.importance', 'event.areas', 'event.location',
            'event.title', 'event.summary', 'event.detail', 'event.coverPhoto', 'event.startDate', 'event.endDate', 'event.hour', 'event.created_at',
            'user.name', 'divisions.description', 'user_detail.codArea', 'event_user.idEvent as signedUp', 'event.hourend'])->first());

    }

    public function fetchTopEvents(){
        return json_encode(Event::where('importance', 1)->leftJoin('organization', DB::raw('organization."corporativoId"::text') , '=', 'event.headquarters')
            ->leftJoin('organization_region', DB::raw('organization_region."idRegion"::text') , '=', 'event.region')
            ->join('user', 'user.idUser' , '=', 'event.idUser')->get(['event.id', 'event.idUser', 'event.region', 'event.headquarters', 'event.importance', 'event.areas',
                'event.title', 'event.location', 'event.summary', 'event.detail', 'event.coverPhoto', 'event.startDate', 'event.endDate', 'event.hour', 'event.created_at',
                'user.name', 'organization.nameStructure', 'organization_region.description']));
    }

    public function exportParticipants(Request $r){
        $objPHPExcel = new \PHPExcel();
        $participants = EventUser::join('user', 'event_user.idUser', '=', 'user.idUser')
            ->join('user_detail', 'user_detail.idUser' , '=', 'user.idUser')
            ->leftJoin('organization', 'organization.corporativoId' , '=', 'user.corporativoId')
            ->leftJoin('position', 'position.positionId' , '=', 'user_detail.positionId')
            ->leftJoin('area', 'area.areaId' , '=', 'user_detail.areaId')
            ->where('event_user.idEvent', $r->id)
            ->get(['user.name', 'user.email', 'user.lastName','organization.nameStructure', 'position.position', 'area.area', 'event_user.created_at']);

        $objPHPExcel->removeSheetByIndex(0);
        $objPHPExcel->createSheet(0);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
        $objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Inscritos en el evento '.$r->title);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A2:G2')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(30);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:G2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Nombre')->setShowGridlines(false)
            ->setCellValue('B2', 'Apellido')->setShowGridlines(false)
            ->setCellValue('C2', 'Email')->setShowGridlines(false)
            ->setCellValue('D2', 'Sede')->setShowGridlines(false)
            ->setCellValue('E2', 'Cargo')->setShowGridlines(false)
            ->setCellValue('F2', 'Area')->setShowGridlines(false)
            ->setCellValue('G2', 'Fecha y Hora')->setShowGridlines(false);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);



        $f = 3;
        foreach($participants as $p){
            $objPHPExcel->getActiveSheet()->getRowDimension($f)->setRowHeight(20);

            $objPHPExcel->getActiveSheet()->getStyle('D'.$f.':E'.$f)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $timestampCreate = strtotime($p->created_at);

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$f, $p->name)->setShowGridlines(false)
                ->setCellValue('B'.$f, $p->lastName)->setShowGridlines(false)
                ->setCellValue('C'.$f, $p->email)->setShowGridlines(false)
                ->setCellValue('D'.$f, $p->nameStructure)->setShowGridlines(false)
                ->setCellValue('E'.$f, $p->position)->setShowGridlines(false)
                ->setCellValue('F'.$f, $p->area)->setShowGridlines(false)
                ->setCellValue('G'.$f, (date('d-m-Y', $timestampCreate).' '.date('h:i:s a', $timestampCreate)))->setShowGridlines(false);

            $f++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Reporte');
        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename='".'Inscritos en el evento '.$r->title.".pdf'");
        ob_clean();
        $objWriter->save("php://output");
    }

    public function deleteEvent(Request $r){
        $e = Event::find($r->id);
        try{
          unlink('files/event/' . $e->coverPhoto);
        }catch (\Exception $e) {}
        DB::table('event')->where('id', '=', $r->id)->delete();
        /*EventUser::where('idEvent', $e->id)->delete();
        EventQuestion::where('idEvent', $e->id)->delete();*/
    }

    public function fecthInMura (Request $r) {
      try {
        $cliente = new \GuzzleHttp\Client();
       /* $parentid = $r->input('parentid');
        $itemsperpage = $r->input('itemsperpage');
        $pageindex = $r->input('pageindex');
        $fields = $r->input('fields');
        $params = json_decode($r->input('params'), true);*/
        $respuesta = $cliente->request('GET', $r->input('uri'), ['verify' => false]);


        return $respuesta->getBody();
      } catch (RequestException   $e) {
        return json_decode(array());
      }
    }

    public function fecthInCurrencies (Request $r) {
      try {
        $cliente = new \GuzzleHttp\Client();
        $res = $cliente->request('GET', 'https://api.diveparts.com/update-currencies', ['verify' => false])->getBody();
        $responseBody = json_decode($res, true);

        //if (is_numeric($responseBody['rate'])) {
          //$value = (1 / $responseBody['rate']);
          //$responseBody['rate'] = number_format((float)$value, 6, '.', '');
        //}
        return $responseBody;
      } catch (RequestException   $e) {
        return json_decode($e);
      }
    }
}
