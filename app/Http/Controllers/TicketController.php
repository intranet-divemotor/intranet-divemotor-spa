<?php

namespace App\Http\Controllers;

use App\Entities\TicketServices;
use App\Entities\User\Userdetail;
use App\Entities\UserSolman;
use function GuzzleHttp\default_ca_bundle;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
// use League\Flysystem\Exception;
use App\EmailInformation;
use App\Entities\User\User;
use Auth;
use DB;
use App\Service;
use App\ServiceComponent;
use App\ServiceType;
use App\Specification;
use App\TicketType;
use Illuminate\Support\Facades\File;
use Validator;
use App\ComponentByService;
use App\ServiceByServiceType;
use App\ServiceTypeByTicketType;
use App\SpecificationByComponent;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Tickets;
use App\TicketsDetail;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Facades\JWTAuth;
use GrahamCampbell\Markdown\Facades\Markdown;

class TicketController extends Controller
{

  public function getServices(Request $request)
  {
    /*return json_encode(
      ServiceTypeByTicketType::join('service_by_servicetype', 'servicetype_by_tickettype.idServiceType', '=', 'service_by_servicetype.idServiceType')
        ->join('component_by_service', 'component_by_service.idService', '=', 'service_by_servicetype.idService')
        ->join('service_component', 'service_component.id', '=', 'component_by_service.idComponent')
        ->distinct()
        ->where('idTicketType', $request->input('tickettype'))
        ->get(['service_component.id as value', 'service_component.description as name'])
    );*/
    return json_encode(
      TicketServices::where('type', $request->input('tickettype'))
        ->orderBy('mask')
        ->get(['id as value', 'mask as name'])
    );
  }

  public function getDependencies(Request $request)
  {
    $idservice = ComponentByService::where('idComponent', $request->input('id'))->get(['idService']);
    $idtypeservice = ServiceByServiceType::where('idService', $idservice[0]->idService)->get(['idServiceType']);
    $specification = SpecificationByComponent::join('specification', 'specification_by_component.idSpecification', '=', 'specification.id')
      ->where('idComponent', $request->input('id'))
      ->get(['specification.id as value', 'specification.description as name']);
    return json_encode(['servicetype' => $idtypeservice[0]->idServiceType, 'service' => $idservice[0]->idService, 'specification' => $specification]);
  }

  public function uploadFile(Request $request)
  {
    try {
      $file = $request->file('file');
      if($file->getSize() > 5242880) return response()->json(['error' => trans('api.error.32')], 400);
      $name = $file->getClientOriginalName();
      $nameForRoute = 'ticket_' . Carbon::now()->timestamp . $name;
      //$route = '!['.$name .']('. url('/File/ticket/'.$nameForRoute).')';
      $route = url('/File/ticket/'.$nameForRoute);
      $file->move('File/ticket/', $nameForRoute);
      return $route;
    } catch (Exception $ex) {
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function getMembers(Request $request)
  {
    return json_encode(User::whereRaw('(("user".name || \' \' || "user"."lastName") ilike \'%' . $request->input('q') . '%\' or "user".email ilike \'%' . $request->input('q') . '%\')')
      ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser')
      ->orderBy('user.name')
      ->get(['user_detail.identityDocument', DB::raw('"user".name || \' \' || "user"."lastName" as name'), 'user.idUser as value']));
  }

  public function headerPDF($data)
  {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->removeSheetByIndex(0);
        $objPHPExcel->createSheet(0);
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:B1');
        $objPHPExcel->getActiveSheet()->getCell('A1')->setValue('DIVEMOTOR');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(34)->setBold(true)->getColor()->setRGB('233141');
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(50);
        $objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

        /*Header datos solicitante */
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:B2');
        $objPHPExcel->getActiveSheet()->getCell('A2')->setValue('Datos de solicitante');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(16)->setBold(true)->getColor()->setRGB('233141');
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $objPHPExcel->getActiveSheet()->getStyle('A2:B2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A3:B3')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:B3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A3', 'Solicitante')->setShowGridlines(true)
            ->setCellValue('B3', $data['data'][0]->name.' '.$data['data'][0]->lastName)->setShowGridlines(true);
        
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A4:B4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A4:B4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Cargo')->setShowGridlines(true)
            ->setCellValue('B4', $data['data'][0]->desPosition)->setShowGridlines(true);
        
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A5:B5')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:B5')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A5', 'Area')->setShowGridlines(true)
            ->setCellValue('B5', $data['data'][0]->desArea)->setShowGridlines(true);
        
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A6:B6')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A6:B6')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A6', 'Ubicacion')->setShowGridlines(true)
            ->setCellValue('B6', $data['data'][0]->desCeco)->setShowGridlines(true);
        
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A7:B7')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A7:B7')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A7', 'Telefono directorio')->setShowGridlines(true)
            ->setCellValue('B7', $data['phone'])->setShowGridlines(true);
        
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A8:B8')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A8:B8')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A8', 'Telefono contacto')->setShowGridlines(true)
            ->setCellValue('B8', $data['contactPhone'])->setShowGridlines(true);
        
        
        /*Header datos categoria */
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A9:B9');
        $objPHPExcel->getActiveSheet()->getCell('A9')->setValue('Categoria');
        $objPHPExcel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A9')->getFont()->setSize(16)->setBold(true)->getColor()->setRGB('233141');
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $objPHPExcel->getActiveSheet()->getStyle('A9:B9')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A10:B10')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:B10')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A10', 'Tipo de ticket')->setShowGridlines(true)
            ->setCellValue('B10', $data['data'][0]->ticket_type)->setShowGridlines(true);
        
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A11:B11')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A11:B11')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A11', 'Calificacion')->setShowGridlines(true)
            ->setCellValue('B11', $data['data'][0]->classification)->setShowGridlines(true);
        
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A12:B12')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A12:B12')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A12', 'Servicio')->setShowGridlines(true)
            ->setCellValue('B12', $data['data'][0]->desComponent)->setShowGridlines(true);
        
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A13:B13')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A13:B13')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A13', 'Mascara del servicio')->setShowGridlines(true)
            ->setCellValue('B13', $data['data'][0]->component)->setShowGridlines(true);
        
        
         /*
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getStyle('A14:B14')->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A14:B14');
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A14:B14')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A14', 'Descripcion')->setShowGridlines(true)
            ->setCellValue('A14', $data['data'][0]->description)->setShowGridlines(true);
     
       
        $objDrawing = new \PHPExcel_Worksheet_Drawing();
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A15:B15');
        $objDrawing->setName('test_img');
        $objDrawing->setDescription('test_img');
        $objDrawing->setPath($evidencia);
        $objDrawing->setCoordinates('A15');                      
        //setOffsetX works properly             
        //set width, height
        $objDrawing->setWidth(800); 
        $objDrawing->setHeight(500); 
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
         * *
         */
        
        return $objPHPExcel;
    }
    
  public function submit(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    try {
      $accept = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'xls', 'xlsx', 'ppt', 'pptx'];
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "idCoorp" => "required",
          "component" => "required|integer|min:1",
          "description" => "required",
          "phone" => "max:12"
        ], [//Mensajes
          "idCoorp.required" => trans('api.error.28'),
          "component.required" => trans('api.error.29'),
          "component.integer" => trans('api.error.29'),
          "component.min" => trans('api.error.29'),
          "description.required" => trans('api.error.29'),
          "phone.max" => trans('api.error.29'),
        ]);
      if ($validator->fails()) {
        return response()->json(['error' => $validator->errors()->first()], 400);
      }
      

      $emails = EmailInformation::where('type', 2)->get();
      if (count($emails) <= 0) {
        return response()->json(['error' => trans('api.error.22')], 400);
      }
     
     $description = trim($request->input('description'));

      $ticketFiles = [];
      $filesE = $request->file('files');
      $nameFile = '';
      if(count($filesE)) {
        foreach ($filesE as $file) {
          if($file->getSize() > 5242880) return response()->json(['error' => trans('api.error.32')], 400);
          $name = 'ticket_'.Carbon::now()->timestamp.$file->getClientOriginalName();
          $arr = explode('.', $name);
          $ext = strtolower(end($arr));
          if(!in_array($ext, $accept)) return response()->json(['error' => trans('api.error.33')], 400);
          $file->move('File/ticket/', $name);
          $nameFile = $file->getClientOriginalName();
          array_push($ticketFiles, $name);
        }
      }
      File::deleteDirectory('/File/ticket/tmp/'.$user->idUser);
      $ticket = new Tickets;
      $ticket->idUser = $user->idUser;
      $ticket->idUserCoor = $request->input('idCoorp');
      $ticket->type = $request->input('tickettype');
      $ticket->idStatus = 0;

      if ($ticket->save()) {
        $code = 'T' . $user->idUser . '-0' . $ticket->id;
        $ticket->code = $code;
        $ticket->save();
        $ticketDetail = new TicketsDetail;
        $ticketDetail->idTicket = $ticket->id;
        $ticketDetail->idTicketType = $request->input('tickettype');
        $ticketDetail->idServiceType = 0;
        $ticketDetail->idService = 0;
        $ticketDetail->idComponent = $request->input('component');
        $ticketDetail->idSpecification = 0;
        $ticketDetail->subject = '';
        $ticketDetail->description = $description;
        $ticketDetail->file = implode(',', $ticketFiles);
        $ticketDetail->phone = $request->input('phone');
        $ticketDetail->save();
      }

      $t = Tickets::where('tickets.id', $ticket->id)
        ->join('user', 'tickets.idUserCoor', '=', 'user.idUser')
        ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser')
        ->join('tickets_detail', 'tickets.id', '=', 'tickets_detail.idTicket')
        ->join('ticket_type', 'tickets.type', '=', 'ticket_type.id')
        ->join('ticket_services', 'tickets_detail.idComponent', '=', 'ticket_services.id')
        ->join('service_type', 'ticket_services.classification', '=', 'service_type.id')
        ->get(['tickets.code', 'tickets_detail.subject', 'tickets.created_at', 'tickets_detail.description',
          'tickets_detail.file', 'ticket_type.description as ticket_type', 'tickets_detail.phone', 'user.name',
          'user.lastName', 'user.email', 'user_detail.phones', 'ticket_services.mask as component', 'ticket_services.description as desComponent',
          'service_type.description as classification', 'service_type.id as idClassification', 'ticket_services.id as idComponent',
          'ticket_type.id as idType', 'user_detail.desPosition', 'user_detail.desArea', 'user_detail.desCeco']);

      $phones = [];
      if($t[0]->phones != 'null' && !empty($t[0]->phones)) {
        $jPhones = json_decode($t[0]->phones);
        foreach($jPhones as $jPhone) {
          array_push($phones, $jPhone->value);
        }
      }

      $subject = strtoupper(substr($t[0]->ticket_type, 0, 3)) . ' - ' . $t[0]->code . ' - ' . $t[0]->component;
      $attachments = [];
      foreach ($ticketFiles as $file) {
        array_push($attachments, base_path('public') . '/File/ticket/' . $file);
      }
        $data = [
          'subject' => $subject,
          'data' => $t,
          'phone' => count($phones) > 0 ? implode(', ', $phones) : ' - ',
          'contactPhone' => !empty($request->input('phone')) ? $request->input('phone') : ' - ',
          'descripcion' => strip_tags($t[0]->description, '<br>'),
          'filename' => implode(',', $ticketFiles),
          'file' => $filesE,
        ];
       if ($ticketDetail->file) {
                $evidencia = base_path('public') . '/File/ticket/' . $ticketDetail->file;
                $exten = explode(".", $ticketDetail->file);

                $image_data = file_get_contents($evidencia);
                $encoded_image = base64_encode($image_data);
            } else {
                $encoded_image = null;
                $exten = null;
            }
            /* HTML CREATE */
       $archivo = fopen(base_path('public') . '/File/' ."detalle.html", "w") or die("error creando fichero!");
       $txt = $this->createhtml($data,$description);
       fwrite($archivo, $txt);
       fclose($archivo);
       $htmlevidencia = base_path('public') . '/File/'."detalle.html";
       $html_data=file_get_contents($htmlevidencia);
       $attachments= base64_encode($html_data);
       /* Consultamos el id del usuario solman */ 
       $userSolman = UserSolman::where('user_solman.email', 'ilike', '%' . $user->email . '%')
               ->join('user', 'user.email', '=', 'user_solman.email')
               ->first();
      /* Consultamos el tipo de ticket */
       $ticketSolman = TicketServices::where('id', $request->input('component'))->first();
       /* Create ticket solman */
       if ($userSolman) {
                $response = $this->createTickets($data, $userSolman, $ticketSolman, $attachments, $encoded_image, $nameFile);
                DB::commit();
                return response()->json(["success" => trans('api.success.13'), "ticket" => $ticket, "emails" => $emails, "solman" => $response]);
            } else {
                return response()->json(["error" => 'Usuario no se encuenta en solman']);
            }
        } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }
  
  public function createTickets($data,$userSolman,$ticketSolman,$attachments,$encoded_image,$nameFile)
  {
   try {
       $nameFiles = explode(".", $nameFile);
       $client = new \GuzzleHttp\Client();
            $body = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
                    <Body>
                        <AGS_SD_GDAPI_IF_WS_INSERT xmlns="urn:sap-com:document:sap:rfc:functions">
                            <IS_MESSAGE_DATA xmlns="">
                                <DESCRIPTION>'.$ticketSolman->mask.'</DESCRIPTION>
                                <PROCESS_TYPE>ZMRP</PROCESS_TYPE>
                                <PRIORITY>3</PRIORITY>
                                <IB_IBASE>'.$ticketSolman->ibIbase.'</IB_IBASE>
                                <IB_INSTANCE>'.$ticketSolman->ibInstance.'</IB_INSTANCE>
                                <CAT_ID>'.$ticketSolman->codeSolman.'</CAT_ID> <!-- id categoria -->
                                <ASP_ID>Z_SCH_KFPE_ZMRP</ASP_ID> <!-- fijo -->
                            </IS_MESSAGE_DATA>
                            <!-- Optional file -->
                            <IT_ATTACHMENTS xmlns="">
                             <!--Zero or more repetitions:-->
                                <item>
                                    <FILE_NAME>detalle.html</FILE_NAME>
                                    <FILE_CONTENT>'.$attachments.'</FILE_CONTENT>
                                </item>';
                            if($encoded_image != null){
                                $body = $body.'<item>
                                        <FILE_NAME>'.$nameFiles[0].'.'.$nameFiles[1].'</FILE_NAME>
                                        <FILE_CONTENT>'.$encoded_image.'</FILE_CONTENT>
                                </item>';
                            }
                                
                           $body = $body.'</IT_ATTACHMENTS>            
                            <!-- Optional -->
                            <IT_PARTNER xmlns="">
                                <!-- Optional -->
                                <item>
                                    <PARTNER_FCT>SLFN0002</PARTNER_FCT>
                                    <PARTNER>'.$userSolman->idUserSolman.'</PARTNER> <!--id usuario solman -->
                                </item>
                                <item>
                                    <PARTNER_FCT>00000001</PARTNER_FCT>
                                    <PARTNER>'.$userSolman->codeRegion.'</PARTNER> <!-- codigo sub -->
                                </item>
                            </IT_PARTNER>
                            <!-- Optional -->
                            <IT_TEXT xmlns="">
                                <!-- Optional -->
                                <item>
                                    <TDID>SU99</TDID>
                                    <TDSPRAS>S</TDSPRAS>
                                    <LINES>
                                        <!-- Optional -->
                                        <item>
                                            <TDFORMAT>*</TDFORMAT>
                                            <TDLINE>Solicitante:'.$data['data'][0]->name.' '.$data['data'][0]->lastName.'</TDLINE>
                                        </item>
                                        <item>
                                            <TDFORMAT>*</TDFORMAT>
                                            <TDLINE>Cargo:'.$data['data'][0]->desPosition.'</TDLINE>
                                        </item>
                                        <item>
                                            <TDFORMAT>*</TDFORMAT>
                                            <TDLINE>Area:'.$data['data'][0]->desArea.'</TDLINE>
                                        </item>
                                        <item>
                                            <TDFORMAT>*</TDFORMAT>
                                            <TDLINE>Ubicacion:'.$data['data'][0]->desCeco.'</TDLINE>
                                        </item>
                                        <item>
                                            <TDFORMAT>*</TDFORMAT>
                                            <TDLINE>Telefono Directorio:'.$data['data'][0]->phone.'</TDLINE>
                                        </item>
                                        <item>
                                            <TDFORMAT>*</TDFORMAT>
                                            <TDLINE>Telefono Contacto:'.$data['data'][0]->contactPhone.'</TDLINE>
                                        </item>
                                        
                                        <item>
                                            <TDFORMAT>*</TDFORMAT>
                                            <TDLINE>Tipo de ticket:'.$data['data'][0]->ticket_type.'</TDLINE>
                                        </item>
                                        <item>
                                            <TDFORMAT>*</TDFORMAT>
                                            <TDLINE>Servicio:'.$data['data'][0]->classification.'</TDLINE>
                                        </item>
                                        <item>
                                            <TDFORMAT>*</TDFORMAT>
                                            <TDLINE>Servicio:'.$data['data'][0]->desComponent.'</TDLINE>
                                        </item>
                                        <item>
                                            <TDFORMAT>*</TDFORMAT>
                                            <TDLINE>Mascara del servicio:'.$data['data'][0]->component.'</TDLINE>
                                        </item>                                       
                                    </LINES>
                                </item>
                            </IT_TEXT>
                        </AGS_SD_GDAPI_IF_WS_INSERT>
                    </Body>
                </Envelope>';

            $res = $client->request('POST', 'http://ticketperu.kaufmann.cl:8000/sap/bc/srt/rfc/sap/ags_sd_gdapi_ws_def/300/ags_sd_gdapi_ws_def/ags_sd_gdapi_ws_def', ['auth' => ['MDA_DIVE', 'Mesadeayuda2020$'],
                'headers' => ['Content-Type' => 'text/xml'],
                'body' => $body
            ]);

            $xml = (string) $res->getBody();
            $xmlData = $xml;

            $obj = simplexml_load_string($xmlData);
            return ['success' => $obj];
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }   
  }
  
  
  public function createhtml($data, $description) 
  {
    $body = '<table cellspacing="0" style="border: 1px solid #eeeeee;width:600px;">
            <tr style="border: none">
              <td colspan="3" height="36" style="border: none">
                Mesa de Ayuda:<br/><br/>
                Se ha ingresado a trav�s de la Intranet el siguiente caso:<br/><br/>
              </td>
            </tr>
            <tr>
              <td rowspan="6" align="center" style="width: 20%;background-color:#aaa;border-bottom: 1px solid #999;"><b>Datos de solicitante</b></td>
              <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 1px 1px 1px 0px;"><b>&nbsp;Solicitante</b></td>
              <td style="border-style: solid;border-color: #999;border-width: 1px 1px 1px 0px;">&nbsp; '.$data['data'][0]->name.' '.$data['data'][0]->lastName.' </td>
            </tr>
            <tr>
              <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Cargo</b></TD>
              <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp; '.$data['data'][0]->desPosition .'</td>
            </tr>
            <tr>
              <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;�rea</b></TD>
              <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp; '.$data['data'][0]->desArea .' </td>
            </tr>
            <tr>
              <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Ubicaci�n</b></td>
              <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp; '.$data['data'][0]->desco .'</td>
            </tr>
            <tr>
              <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Tel�fono directorio</b></td>
              <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp; '.$data['data'][0]->phone .'</td>
            </tr>
            <tr>
              <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Tel�fono contacto</b></td>
              <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp; '.$data['data'][0]->contactPhone .'</td>
            </tr>

            <tr>
              <td rowspan="4"  align="center" style="width: 20%;background-color:#aaa;border-bottom: 1px solid #999;"><b>Categor�a</b></td>
              <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Tipo de ticket</b></td>
              <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp; '.$data['data'][0]->ticket_type.' </td>
            </tr>
            <tr>
              <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Calificaci�n</b></td>
              <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp; '.$data['data'][0]->classification.' </td>
            </tr>
            <tr>
              <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;Servicio</b></td>
              <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp; '.$data['data'][0]->desComponent.'   </td>
            </tr>
            <tr>
              <td style="width: 25%;height: 30px;background-color:#ddd;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>&nbsp;M�scara del servicio</b></td>
              <td style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp;  '.$data['data'][0]->component.' </td>
            </tr>
            <tr>
              <td align="center" style="width: 20%;height: 30px;background-color:#aaa;border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;"><b>Descripci�n</b></td>
              <td colspan="2" style="border-style: solid;border-color: #999;border-width: 0px 1px 1px 0px;">&nbsp; '.$description.'</td>
            </tr>
            <tr>
              <td colspan="3" height="36"></td>
            </tr>
          </table>'; 
    return $body;
  }

  public function getTickets(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    $tickets = Tickets::join('user', 'tickets.idUser', '=', 'user.idUser')
      ->join('user as u2', 'tickets.idUserCoor', '=', 'u2.idUser')
      ->join('tickets_detail', 'tickets.id', '=', 'tickets_detail.idTicket')
      ->join('ticket_type', 'tickets.type', '=', 'ticket_type.id')
      ->join('ticket_services', 'tickets_detail.idComponent', '=', 'ticket_services.id')
      ->join('service_type', 'ticket_services.classification', '=', 'service_type.id')
      ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser');
    if (!in_array($user->idRole, [1, 3])) {
      $tickets = $tickets->where('tickets.idUser', Auth::user()->idUser);
    }
    $sede = !empty($request->input('sede')) ? $request->input('sede') : Userdetail::where('idUser', $user->idUser)->get(['divPersonal'])[0]->divPersonal;
    if (in_array($user->idRole, [2, 3])) {
      $tickets = $tickets->where('divPersonal', $sede);
    }
    if (!empty($request->input('sede'))) {
      $tickets = $tickets->where('divPersonal', $sede);
    }
    if ($request->has('status') && $request->input('status') > -1) {
      $tickets = $tickets->where('tickets.idStatus', $request->input('status'));
    }
    $total = $tickets->count();
    $tickets = $tickets->whereNotIn('tickets.idStatus', ['6'])
      ->orderBy('tickets.created_at', 'desc')
      ->limit($request->input('limit'))
      ->offset($request->input('offset'))
      ->get(['tickets.id', 'tickets.idUser', 'tickets.type', 'tickets.code', 'u2.name', 'u2.lastName',
        'tickets_detail.subject', 'tickets.idStatus', 'tickets.created_at', 'tickets_detail.idComponent',
        'tickets_detail.idServiceType', 'tickets_detail.description', 'tickets_detail.idService',
        'tickets_detail.idSpecification', 'tickets_detail.file', 'user_detail.identityDocument', 'ticket_type.description as ticketType',
        'ticket_services.mask as component', 'ticket_services.description as desComponent', 'tickets_detail.phone',
        'service_type.description as classification', 'service_type.description as classification', 'service_type.id as idClassification',
        'ticket_services.id as idComponent', 'ticket_type.id as idType', 'user_detail.phones']);

    return json_encode([
      'total' => $total,
      'rows' => $tickets
    ]);
  }

  public function updateStatus(Request $request)
  {
    try {
      DB::beginTransaction();
      $tickets = Tickets::findOrFail($request->id);
      $tickets->idStatus = $request->status;
      $tickets->save();

      DB::commit();
      return response()->json(['success' => trans('api.success.12')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function submitItem(Request $request)
  {
    $items = json_decode($request->items);
    try {
      DB::beginTransaction();
      switch ($request->type) {
        case 'tickettype':
          $tickettype = new TicketType;
          $tickettype->description = $request->desc;
          $tickettype->mask = $request->mask;
          $tickettype->save();
          return response()->json([
            'success' => 'Ticket modificado con exito',
            'id' => $tickettype->id,
            'desc' => $request->desc
          ]);
          break;
        case 'servicetype':
          $servicetype = new ServiceType;
          $servicetype->description = $request->desc;
          $servicetype->mask = $request->mask;
          if ($servicetype->save()) {
            $servicetypeByTickettype = new ServiceTypeByTicketType;
            $servicetypeByTickettype->idServiceType = $servicetype->id;
            $servicetypeByTickettype->idTicketType = $items->tickettype;
            $servicetypeByTickettype->save();
          }
          return response()->json([
            'success' => 'Ticket modificado con exito',
            'id' => $servicetype->id,
            'desc' => $request->desc
          ]);
          break;
        case 'service':
          $service = new Service;
          $service->description = $request->desc;
          $service->mask = $request->mask;
          if ($service->save()) {
            $serviceByServicetype = new ServiceByServiceType;
            $serviceByServicetype->idService = $service->id;
            $serviceByServicetype->idServiceType = $items->servicetype;
            $serviceByServicetype->save();
          }
          return response()->json([
            'success' => 'Ticket modificado con exito',
            'id' => $service->id,
            'desc' => $request->desc
          ]);
          break;
        case 'component':
          $component = new ServiceComponent;
          $component->description = $request->desc;
          $component->mask = $request->mask;
          if ($component->save()) {
            $componentByService = new ComponentByService;
            $componentByService->idComponent = $component->id;
            $componentByService->idService = $items->service;
            $componentByService->save();
          }
          return response()->json([
            'success' => 'Ticket modificado con exito',
            'id' => $component->id,
            'desc' => $request->desc
          ]);
          break;
        case 'specification':
          $specification = new Specification;
          $specification->description = $request->desc;
          $specification->mask = $request->mask;
          if ($specification->save()) {
            $specificationByComponent = new SpecificationByComponent;
            $specificationByComponent->idSpecification = $specification->id;
            $specificationByComponent->idComponent = $items->component;
            $specificationByComponent->save();
          }
          return response()->json([
            'success' => 'Ticket modificado con exito',
            'id' => $specification->id,
            'desc' => $request->desc
          ]);
          break;
        default:
          return response()->json(['error' => trans('api.error.100')], 400);
      }
      DB::commit();
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function getTicketType(Request $request)
  {
    return json_encode([
      'tickettype' => TicketType::get(),
      'userdata' => User::where('user.idUser', Auth::user()->idUser)
        ->leftJoin('user_detail', 'user.idUser', '=', 'user_detail.idUser')
        ->leftJoin('area', 'user_detail.areaId', '=', 'area.areaId')
        ->leftJoin('position', 'user_detail.positionId', '=', 'position.positionId')
        ->leftJoin('department', 'user_detail.departmentId', '=', 'department.departmentId')
        ->leftJoin('organization', 'user.corporativoId', '=', 'organization.corporativoId')
        ->get(['user_detail.identityDocument', 'position.position', 'department.department', 'user.name', 'user.lastName', 'user.idUser', 'area.area',
          'nameStructure'])
    ]);
  }


  public function sendIncidente(Request $request)
  {
    try {
      $email = 'garroyo@divemotor.com.pe';
      $info = [
        'date' => $request->date,
        'time' => $request->time,
        'ubicacion' => $request->ubicacion,
        'description' => $request->description,
        'acciones' => $request->acciones
      ];
      Mail::send('emails.incidente', $info, function($message) use ($email) {
        $message->to($email, 'Intranet')->subject('Incidente '. env('APP_NAME'));
      });
      return response()->json(['success' => trans('api.success.47')]);
    } catch (Exception $ex) {
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

}
