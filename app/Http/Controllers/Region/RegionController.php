<?php

namespace App\Http\Controllers\Region;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Entities\Region;
use App\Entities\Organizations;
use Carbon\Carbon;
use File;
use Intervention\Image\Facades\Image;


class RegionController extends Controller
{
    public function fetchSelect(Request $r){
        $result = Region::where('description', 'ilike', '%'.$r->input('q').'%');
        if(!empty($r->input('id')))$result->where('idRegion', $r->input('id'));
        if(!empty($r->input('limit')))$result->limit($r->input('limit'));
        return json_encode($result->get(['idRegion as value', 'description as text']));
    }
}
