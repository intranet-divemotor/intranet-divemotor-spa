<?php
namespace App\Http\Controllers\Modules;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Entities\Link\Link;
use App\Module;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use PhpParser\Node\Expr\AssignOp\Mod;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Entities\User\Role;
class ModulesController extends Controller
{

  public function fetch(Request $r){

    $data = new Module();

    if(!empty($r->input('limit'))) {
      $data = Module::limit($r->limit)->offset($r->offset);
    }

    if(!empty($r->input('search'))){
      $data = $data->where('module.name', 'like' , '%'.$r->input('search').'%');
    }
    $total = Module::count();
    $data = $data->get();
    return json_encode(array('total' => $total, 'rows' => $data));
  }

  public function submit(Request $r){
    if($r->input('id') == 0) {
      $m = new Module();
      $m->name = $r->input('name');
      $m->active = $r->input('active');
      $m->parent = $r->input('parent');
      $m->header = $r->input('header');
      $m->to = $r->input('to');
      $m->internal = $r->input('internal');
      $m->icon = $r->input('icon');
      $m->system = $r->input('system');
      if ($m->save()) {
        return ['success' => trans('layouts::api.success.2.message')];
      } else {
        return ['error' => trans('layouts::api.error.100.error')];
      }
    }else {
      $m = Module::find($r->input('id'));
      $m->name = $r->input('name');
      $m->active = $r->input('active');
      $m->parent = $r->input('parent');
      $m->header = $r->input('header');
      $m->to = $r->input('to');
      $m->internal = $r->input('internal');
      $m->icon = $r->input('icon');
      $m->system = $r->input('system');
      if ($m->save()) {
        return ['success' => trans('layouts::api.success.2.message')];
      } else {
        return ['error' => trans('layouts::api.error.100.error')];
      }
    }



  }

  public function delete(Request $r){
    $m = Module::find($r->input('id'));
    $m->delete();
  }


  public function fetchMyModules(Request $r){
    $user = JWTAuth::parseToken()->authenticate();
    $profile = Role::find($user->idRole);
    if ($profile) {
      $modules = json_decode($profile->modules);
      $myModules = Module::whereIn('id', $modules)
                    ->where('active', true)->get();
      $myModules = $myModules ? $myModules : [];
      return json_encode($myModules);
    }
  }


}
