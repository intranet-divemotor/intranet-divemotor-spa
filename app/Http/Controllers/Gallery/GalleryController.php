<?php

namespace App\Http\Controllers\Gallery;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Album;
use Auth;
use DB;
use App\Gallery;
use Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\SystemLink\SystemLinkController;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\File;

class GalleryController extends Controller
{
  protected $user;

  function __construct()
  {
    $this->user = JWTAuth::parseToken()->authenticate();
  }

  public function saveAlbum(Request $request)
  {
    try {
      $accept = ['png', 'jpg', 'jpeg'];
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "title" => "required",
        ], [//Mensajes
          "title.required" => trans('api.error.29'),
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }
      $cover = '';
      $album = new Album;
      $album->idUser = $this->user->idUser;
      $album->title = $request->input('title');
      $album->idDivision = empty($request->sede) ? 0 : $request->sede;
      $album->idStatus = 1;
      if($album->save()) {
        $filesE = $request->file('files');
        if(count($filesE)) {
          foreach ($filesE as $file) {
            $name = 'gallery_'.Carbon::now()->timestamp.$file->getClientOriginalName();
            $arr = explode('.', $name);
            $ext = strtolower(end($arr));
            if(!in_array($ext, $accept)) return response()->json(['error' => trans('api.error.33')], 400);
            $file->move('files/gallery/', $name);
            $cover = $name;
            $gallery = new Gallery;
            $gallery->idUser = $this->user->idUser;
            $gallery->title = '';
            $gallery->path = $name;
            $gallery->idType = 1;
            $gallery->idStatus = 1;
            $gallery->idAlbum = $album->idAlbum;
            $gallery->save();
          }
          $album->coverPhoto = $cover;
          $album->save();
        }
      }

      DB::commit();
      return response()->json(['success' => trans('api.success.41'), 'name' => $request->title, 'idAlbum' => $album->idAlbum]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function getGalleries(Request $request)
  {
    $divUser = $this->user->idRole > 1 ? $this->getIdDivision($this->user->idUser) : null;
    try {
      $galleries = Album::with(['Gallery' => function($query) {
        $query->select('idGallery', 'idAlbum', 'path');
      }]);
      if($request->input('sede') > 0) $galleries->whereRaw('\''.$request->sede.'\' = any (string_to_array("idDivision", \',\'))');
      if($this->user->idRole == 2) $galleries->whereRaw('\''.$divUser.'\' = any (string_to_array("idDivision", \',\'))');
      if($this->user->idRole == 2) $galleries->orWhere('idDivision', 0);
      $galleries->orderBy('created_at', 'desc');
      return response()->json($galleries->get(['idAlbum', 'title', 'coverPhoto']));
    } catch (\Exception $ex) {
      return response()->json(['error' => trans('api.error.100'), 'trace' => $ex->getMessage()], 400);
    }
  }

  public function addGallery(Request $request) {
    try {
      $accept = ['png', 'jpg', 'jpeg'];
      DB::beginTransaction();
      $filesE = $request->file('files');
      if(count($filesE)) {
        foreach ($filesE as $file) {
          $name = 'gallery_'.Carbon::now()->timestamp.$file->getClientOriginalName();
          $arr = explode('.', $name);
          $ext = strtolower(end($arr));
          if(!in_array($ext, $accept)) return response()->json(['error' => trans('api.error.33')], 400);
          $file->move('files/gallery/', $name);
          $gallery = new Gallery;
          $gallery->idUser = $this->user->idUser;
          $gallery->title = '';
          $gallery->path = $name;
          $gallery->idType = 1;
          $gallery->idStatus = 1;
          $gallery->idAlbum = $request->input('id');
          $gallery->save();
        }
      }
      DB::commit();
      return response()->json(['success' => trans('api.success.44')]);
    } catch(\Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function editAlbum(Request $request)
  {
    try {
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "title" => "required",
        ], [//Mensajes
          "title.required" => "El título del álbum es requerido",
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()]);
      }

      Album::where('idAlbum', $request->id)
        ->update(['title' => $request->title]);
      DB::commit();
      return response()->json(['success' => 'Álbum actualizado', 'name' => $request->title]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }

  public function deleteAlbum(Request $request)
  {
    try {
      DB::beginTransaction();
      $album = Album::where('idAlbum', $request->input('id'))->get();
      $galleries = Gallery::where('idAlbum', $album[0]->idAlbum)->get();
      foreach ($galleries as $gallery) {
        File::delete(base_path('public').'/files/gallery/'.$gallery->path);
        //Storage::disk('gallery')->delete($gallery->path);
        $gallery->delete();
      }
      $album[0]->delete();
      DB::commit();
      return response()->json(['success' => trans('api.success.42')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function submitNew(Request $request)
  {
    $sc = new SystemLinkController();
    $pathEvent = Storage::disk('gallery')->getDriver()->getAdapter()->getPathPrefix();
    try {
      DB::beginTransaction();
      if ($request->file('coverPhoto')->getSize() > 10485760) {
        return response()->json(['error' => 'El archivo no puede ser mayor a 10MB']);
      }

      $fileName = strtolower(preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp . $request->file('coverPhoto')->getClientOriginalName())));
      if ($request->type == 1) {
        $img = Image::make(file_get_contents($request->file('coverPhoto')));
        $img->crop((int)$request->cropWidth, (int)$request->cropHeight, (int)$request->startX, (int)$request->startY);
        $img->save($pathEvent . $fileName, 60);
        $type = 1;
      } else {
        $file = $request->file('coverPhoto');
        Storage::disk('gallery')->put($fileName, file_get_contents($file));
        $type = 2;
      }
      $exists = Storage::disk('gallery')->exists($fileName);
      if ($exists) {
        $gallery = new Gallery;
        $gallery->idUser = Auth::user()->idUser;
        $gallery->title = $request->titleGallery;
        $gallery->path = $fileName;
        $gallery->idType = $type;
        $gallery->idStatus = 1;
        $gallery->idAlbum = $request->album;
        $gallery->save();
      }
      DB::commit();
      return response()->json(['success' => 'Archivo guardado', 'path' => $fileName, 'idGallery' => $gallery->idGallery]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }

  public function getGallery(Request $request)
  {
    try {
      return json_encode(Gallery::where('idAlbum', $request->input('album'))->get(['idGallery', 'path as src']));
    } catch (Exception $ex) {
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }

  public function deleteItem(Request $request)
  {
    try {
      DB::beginTransaction();
      $gallery = Gallery::where('idGallery', $request->input('id'))->get();
      File::delete(base_path('public').'/files/gallery/'.$gallery[0]->path);
      $gallery[0]->delete();
      DB::commit();
      return response()->json(['success' => trans('api.success.43')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function getIdDivision($user)
  {
    return (DB::select('select "id"
      from group_divisions
      inner join user_detail on user_detail."divPersonal" = ANY(regexp_split_to_array(group_divisions.codes, \',\'))
      where user_detail."idUser" = '.$user.';')[0]->id);
  }
}
