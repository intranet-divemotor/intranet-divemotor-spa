<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use App\Entities\User\User;
use App\Entities\User\Userdetail;
use Validator;
use DB;

class AuthController extends Controller {

    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request) {
         //Validation
          $validator = Validator::make($request->all(),
            [ //Reglas
                'email' => 'required|email|max:255',
                'password' => 'required',
            ],[//Mensajes
                "email.required" => trans("api.error.1"),
                "email.email" => trans("api.error.2"),
                "password.required" => trans("api.error.4"),
            ]);
            if ($validator->fails()){
               return response()->json(['error' => $validator->errors()->first()], 400);
            }
        try {
            $restriction = env('RESTRICTION_DOMAIN');
            if($restriction == TRUE){
                $verify = $this->verifydomain($request->email); //verify domain intranet
                if($verify == false) {
                 // return response()->json(['error' => trans("api.error.3")], 400);
                }
            }
            // Attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($this->getCredentials($request))) {
                return $this->onUnauthorized();
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return $this->onJwtGenerationError();
        }

        // All good so return the token

        return $this->onAuthorized($token);
    }

    /**
     * What response should be returned on invalid credentials.
     *
     * @return JsonResponse
     */
    protected function onUnauthorized() {
        return response()->json(['error' => trans("api.error.5")], 400);
    }

    /**
     * What response should be returned on error while generate JWT.
     *
     * @return JsonResponse
     */
    protected function onJwtGenerationError() {
        return response()->json(['error' => trans("api.error.6")], 400);

    }

    /**
     * What response should be returned on authorized.
     *
     * @return JsonResponse
     */
    protected function onAuthorized($token) {
        try {
            $user = JWTAuth::toUser($token);
            $detail = Userdetail::where("user_detail.idUser", $user->idUser)
                    ->leftJoin('sexo', 'sexo.sexoId', '=', 'user_detail.sexoId')
                    ->join('user', 'user.idUser', '=', "user_detail.idUser")
                    ->first(["posicionHCM", "codPosicion", "codBoss", "colaborador",
                      "desPosition", "desManagement", "desSubManagement", "bossName", "desBoss",
                      "dateAdmission", "identityDocument", "sexo.sexoId", "sexo.sexo", "user_detail.phones",
                      "user_detail.desPosition", "user_detail.desArea", "user_detail.desDepartment",
                      "user_detail.desSubManagement", "user_detail.divCorp", "user_detail.divPersonal", "user_detail.bossName",
                      "user_detail.desBoss","user_detail.desDivisions", "user_detail.codArea", "user.birthdate"]);
          $response = array_merge($user->toArray(), $detail->toArray());

            return new JsonResponse([
                'token' => $token,
                'user' => $response
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function getCredentials(Request $request) {
        return $request->only('email', 'password');
    }

    /**
     * Invalidate a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteInvalidate() {
        $token = JWTAuth::parseToken();
        $token->invalidate();
        return response()->json(['error' => trans("api.error.7")], 400);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function patchRefresh() {
        $token = JWTAuth::parseToken();

        $newToken = $token->refresh();

        return new JsonResponse([
            'message' => 'token_refreshed',
            'data' => [
                'token' => $newToken
            ]
        ]);
    }

    /**
     * Get authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUser() {
        return new JsonResponse([
            'message' => 'authenticated_user',
            'data' => JWTAuth::parseToken()->authenticate()
        ]);
    }

    public function getUserGoogle(Request $request) {
        //Validation
        $validator = Validator::make($request->all(), [ //Reglas
                    'email' => 'required|email|max:255',
                        ], [//Mensajes
                    "email.required" => trans("api.error.1"),
                    "email.email" => trans("api.error.2"),
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        try {
            // Attempt to verify the credentials and create a token for the user
            $user = User::where('email', '=', $request->input('email'))->first();
            if (!$user) {
                return response()->json(['error' => trans("api.error.31")], 400);
            } else {
                $user->avatar = $request->input('avatar') == "" ? null : explode('=', $request->input('avatar'))[0] . '=250';
                $user->save();
            }
            if (!$token = JWTAuth::fromUser($user)) {
                return $this->onUnauthorized();
            }
            return $this->onAuthorized($token);
        } catch (JWTException $e) {
            return $this->onJwtGenerationError();
        }

        // All good so return the token
    }

    function verifydomain($email) {

        $findme = env('GOOGLE_APP_DOMAIN');
        $pos = strpos($email, $findme);
        if ($pos === false) {
            //el email no pertenece a la intranet
            $response = false;
        } else {
            // el email pertenece a la intranet
            $response = true;
            //echo " y existe en la posición $pos";
        }
        return $response;
    }

}
