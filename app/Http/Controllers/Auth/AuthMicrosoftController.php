<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use App\Entities\User\User;
use App\Entities\User\Userdetail;
use Illuminate\Support\Facades\Hash;
use Validator;
use DB;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class AuthMicrosoftController extends Controller {

    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function authMicroftCallUrl(Request $request) {
        // Initialize the OAuth client
        $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
          'clientId'                => env('OAUTH_APP_ID'),
          'clientSecret'            => env('OAUTH_APP_PASSWORD'),
          'redirectUri'             => env('OAUTH_REDIRECT_URI'),
          'urlAuthorize'            => env('OAUTH_AUTHORITY').env('OAUTH_AUTHORIZE_ENDPOINT'),
          'urlAccessToken'          => env('OAUTH_AUTHORITY').env('OAUTH_TOKEN_ENDPOINT'),
          'urlResourceOwnerDetails' => '',
          'scopes'                  => env('OAUTH_SCOPES')
        ]);
        try {
            // Generate the auth URL
            $authorizationUrl = $oauthClient->getAuthorizationUrl();
            // Save client state so we can validate in response
            $oauthClient->getState();
            // Redirect to authorization endpoint

          return json_encode(['callUrl' => $authorizationUrl]);
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return $this->onJwtGenerationError();
        }
    }

        /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function authMicroftGettoken(Request $r) {
         // Initialize the OAuth client
         $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
          'clientId'                => env('OAUTH_APP_ID'),
          'clientSecret'            => env('OAUTH_APP_PASSWORD'),
          'redirectUri'             => env('OAUTH_REDIRECT_URI'),
          'urlAuthorize'            => env('OAUTH_AUTHORITY').env('OAUTH_AUTHORIZE_ENDPOINT'),
          'urlAccessToken'          => env('OAUTH_AUTHORITY').env('OAUTH_TOKEN_ENDPOINT'),
          'urlResourceOwnerDetails' => '',
          'scopes'                  => env('OAUTH_SCOPES')
          ]);
      try {
        // Make the token request
        $accessToken = $oauthClient->getAccessToken('authorization_code', [
          'code' => $r->code
        ]);

        $tokenCache = new \App\TokenStore\TokenCache;
        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken($accessToken->getToken(), $accessToken->getRefreshToken(),
        $accessToken->getExpires()));

        $user = $graph->createRequest('GET', '/me')
                        ->setReturnType(Model\User::class)
                        ->execute();
        $data = json_encode($user);
        $prepareData = json_decode($data, true);
      // Attempt to verify the credentials and create a token for the user
      if ($user->getMail()) {
        $userdb = User::where('email', '=', $user->getMail())->first();
        if (!$userdb) {
          $userModel = new User;
          $userModel->name = $prepareData['givenName'];
          $userModel->lastName = $prepareData['surname'];
          $userModel->mothersLastName = $prepareData['givenName'] . ' ' . $prepareData['surname'];
          $userModel->email = $user->getMail();
          $userModel->password = Hash::make(123456);
          $userModel->idRole = 2;
          $userModel->idStatus = 1;
          $userModel->avatar = null;
          if ($userModel->save()) {
            $userdetail = new Userdetail;
            $userdetail->idUser = $userModel->idUser;
            $userdetail->colaborador = $prepareData['givenName'] . $prepareData['surname'];
            $userdetail->phones = $prepareData['mobilePhone'];
            $userdetail->save();
          }
        } else {
          $userModel = User::where('email', '=', $user->getMail())->first();
          $userModel->name = $prepareData['givenName'];
          $userModel->lastName = $prepareData['surname'];
          $userModel->mothersLastName = $prepareData['givenName'] . ' ' . $prepareData['surname'];
          if ($userModel->save()) {
            $userdetail = Userdetail::where('idUser', '=', $userModel->idUser)->first();
            $userdetail->colaborador = $prepareData['givenName'] . $prepareData['surname'];
            $userdetail->phones = $prepareData['mobilePhone'];
            $userdetail->save();
          }
        }
        $userdb = User::where('email', '=', $user->getMail())->first();
        $userdb->tokenOffice = $accessToken;
        if (!$token = JWTAuth::fromUser($userdb)) {
          return $this->onUnauthorized();
        }
        return $this->onAuthorized($token, $accessToken);
      }
      } catch (JWTException $e) {
        return $this->onJwtGenerationError();
      }
  }

    /**
     * What response should be returned on invalid credentials.
     *
     * @return JsonResponse
     */
    protected function onUnauthorized() {
        return response()->json(['error' => trans("api.error.5")], 400);
    }

    /**
     * What response should be returned on error while generate JWT.
     *
     * @return JsonResponse
     */
    protected function onJwtGenerationError() {
        return response()->json(['error' => trans("api.error.6")], 400);

    }

    /**
     * What response should be returned on authorized.
     *
     * @return JsonResponse
     */
    protected function onAuthorized($token, $tokenOffice) {
        try {
            $user = JWTAuth::toUser($token);
            $user->tokenOffice = $tokenOffice;
            $detail = Userdetail::where("user_detail.idUser", $user->idUser)
                    ->leftJoin('sexo', 'sexo.sexoId', '=', 'user_detail.sexoId')
                    ->join('user', 'user.idUser', '=', "user_detail.idUser")
                    ->first(["posicionHCM", "codPosicion", "codBoss", "colaborador",
                      "desPosition", "desManagement", "desSubManagement", "bossName", "desBoss",
                      "dateAdmission", "identityDocument", "sexo.sexoId", "sexo.sexo", "user_detail.phones",
                      "user_detail.desPosition", "user_detail.desArea", "user_detail.desDepartment",
                      "user_detail.desSubManagement", "user_detail.divCorp", "user_detail.divPersonal", "user_detail.bossName",
                      "user_detail.desBoss","user_detail.desDivisions", "user_detail.codArea", "user.birthdate"]);
          $response = array_merge($user->toArray(), $detail->toArray());

            return new JsonResponse([
                'token' => $token,
                'user' => $response
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function getCredentials(Request $request) {
        return $request->only('email', 'password');
    }

    /**
     * Invalidate a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteInvalidate() {
        $token = JWTAuth::parseToken();
        $token->invalidate();
        return response()->json(['error' => trans("api.error.7")], 400);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function patchRefresh() {
        $token = JWTAuth::parseToken();

        $newToken = $token->refresh();

        return new JsonResponse([
            'message' => 'token_refreshed',
            'data' => [
                'token' => $newToken
            ]
        ]);
    }

    /**
     * Get authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUser() {
        return new JsonResponse([
            'message' => 'authenticated_user',
            'data' => JWTAuth::parseToken()->authenticate()
        ]);
    }

    function verifydomain($email) {

        $findme = env('GOOGLE_APP_DOMAIN');
        $pos = strpos($email, $findme);
        if ($pos === false) {
            //el email no pertenece a la intranet
            $response = false;
        } else {
            // el email pertenece a la intranet
            $response = true;
            //echo " y existe en la posición $pos";
        }
        return $response;
    }

}
