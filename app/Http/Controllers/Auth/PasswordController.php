<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Entities\User\PasswordReset;
use App\Entities\User\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class PasswordController extends Controller {
   
    public function sendResetLinkEmail(Request $request) {
      //Validation
          $validator = Validator::make($request->all(),
            [ //Reglas
                'email' => 'required|email|max:255',
            ],[//Mensajes
                "email.required" => trans("api.error.1"),
                "email.email" => trans("api.error.2"),
            ]);
            if ($validator->fails()){
               return response()->json(['error' => $validator->errors()->first()], 400);
            }  
            
        try {
            //verificamos si el email exite
            $user = PasswordReset::where('email', $request->email)->first();
            $email =  $request->email;
            if ($user) {
                $user->token = $token = str_random(100);
                $user->save();
                //enviar email
                $data = [
                    "token" => $token,
                    "emailPlaysInfo" => $user->email
                ];
                Mail::send('emails.password', $data, function($message) use ($email) {
                    $message->to($email, 'Intranet')->subject('Restablecer contraseña de ' . env('APP_NAME'));
                });
                return ['success' => trans('api.success.3')];
            } else {
                $usernew = User::where('email', $request->email)->first();
                if ($usernew) {
                    $c = new PasswordReset;
                    $c->email = $request->email;
                    $c->token = str_random(100);
                    $c->save();
                    //enviar email
                    $data = [
                        "token" => $c->token,
                        "emailPlaysInfo" => $c->email
                    ];
                    Mail::send('emails.password', $data, function($message) use ($email) {
                        $message->to($email, 'Intranet')->subject('Restablecer contraseña de ' . env('APP_NAME'));
                    });
                    return response()->json(['success' => trans('api.success.3')]);
                } else {
                    return response()->json(['error' => trans('api.error.6.message')]);
                }
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function resetpassword(Request $request) {
        //Validation
        dd($request->password);
        $validator = Validator::make($request->all(), [ //Reglas
                    'password' => 'required|between:6,255|confirmed',
                    'password_confirmation' => 'required',
                        ], [//Mensajes
                    "password.confirmed" => trans('layouts::api.error.7.message'),
                    "password.required" => trans('layouts::api.error.8.message'),
                    "password.between" => trans('layouts::api.error.9.message'),
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()]);
        }

        try {
            //verificamos si el email exite
            $verifypassword = PasswordReset::where('token', $request->token)->first();
            if ($verifypassword) {
                $usernew = User::where('email', $request->email)->first();
                if ($usernew) {
                    $usernew->password = Hash::make($request->password);
                    $usernew->save();
                    return response()->json(['success' => '/']);
                }
            } else {
                return response()->json(['error' => trans('layouts::api.error.100.message')]);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

   

    public function changePassword(Request $request) {
        
        //Validation
        $validator = Validator::make($request->all(), [ //Reglas
                    'password' => 'required|between:6,255|confirmed',
                    'password_confirmation' => 'required',
                    'currentPassword' => 'required',
                        ], [//Mensajes
                    "password.required" =>  trans("api.error.10"),
                    "password.confirmed" =>  trans("api.error.12"),
                    "password_confirmation.required" =>  trans("api.error.11"), 
                    "password.between" => trans("api.error.11"),
                    "currentPassword.required" => trans("api.error.9"),  
                    "currentPassword.between" => trans("api.error.11"),
        ]);
        if ($validator->fails()) {
             return response()->json(['error' => $validator->errors()->first()], 400);
        }
        try {

           /// $user = User::findOrFail(Auth::user()->idUser);
            $user =JWTAuth::parseToken()->authenticate();
            $userdata = User::findOrFail($user->idUser);
            if (Hash::check($request->currentPassword, $userdata->password)) {
                $userdata->password = Hash::make($request->password);
                $userdata->save();
                return response()->json(['success' =>trans("api.success.4")]);
                 
            }else {
                return response()->json(['error' => trans('api.error.12')],400);
            }
        } catch (Exception $e) {
           return response()->json(['error' => trans('api.error.100')]);
        }
    }

}
