<?php


namespace App\Http\Controllers\Collaborator;
use Illuminate\Routing\Controller;
use SimpleXMLElement;
use App\Entities\User\User;
use App\Entities\User\Userdetail;
use App\Requests;
use App\Entities\User\PasswordReset;
use Illuminate\Support\Facades\Hash;
ini_set('max_execution_time', 900000);

class CollaboratorSoapController extends Controller
{
     public function getfecth() {
        try {
            $client = new \GuzzleHttp\Client();
            $hoy = date("d.m.Y");
            $body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                    <soapenv:Header/>
                    <soapenv:Body>
                       <urn:ZHCC_MF_DATOS_EMPLEADO>
                          <DATE>
                             <BEGDA>'.$hoy.'</BEGDA>
                             <ENDDA>'.$hoy.'</ENDDA>
                          </DATE>
                       </urn:ZHCC_MF_DATOS_EMPLEADO>
                    </soapenv:Body>
                 </soapenv:Envelope>';

            $res = $client->request('POST', 'http://10.1.60.155:8000/sap/bc/srt/rfc/sap/zws_datos_empleados3/300/zws_datos_empleados3/zb_datos_empleados3', ['auth' => ['WEBSERVICE', 'Global2014.'],
                'headers' => ['Content-Type' => 'text/xml'],
                'body' => $body
            ]);

            $xml = (string) $res->getBody();

            $xml1 = str_replace('<n0:ZHCC_MF_DATOS_EMPLEADOResponse xmlns:n0="urn:sap-com:document:sap:rfc:functions">', '<ZHCC_MF_DATOS_EMPLEADOResponse xmlns="urn:sap-com:document:sap:rfc:functions">', $xml);
            $xmlData = str_replace('</n0:ZHCC_MF_DATOS_EMPLEADOResponse>', '</ZHCC_MF_DATOS_EMPLEADOResponse>', $xml1);

            $obj = simplexml_load_string($xmlData);
            $insert = 0;
            $update = 0;
            $countUser = 0;

            $dataSoap = [];
            foreach ($obj->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children('urn:sap-com:document:sap:rfc:functions')
            ->ZHCC_MF_DATOS_EMPLEADOResponse->DATOS_EMPLEADO->item as $data) {
                $email = strtolower($data->EMAIL);
                $dataSoap[]=$email;

              if($email != "" && $data->ICNUM !="") {
                $countUser ++;
                $userModel = User::where('email', $email)->first();
                    if($userModel) {
                        $userModel->name = $data->VORNA;
                        $userModel->password = $data->ICNUM != "" ? Hash::make($data->ICNUM) : Hash::make(123456);
                        $userModel->lastName = $data->NACHN;
                        $userModel->mothersLastName = $data->NAME2;
                        $userModel->birthdate = $data->GBDAT;
                        if ($userModel->save()) {

                            $userdetail = Userdetail::where('idUser', $userModel->idUser)
                                        ->first();
                            //->where('identityDocument', $data->ICNUM)

                            if($userdetail) {

                            echo '<pre>';
                            print_r($data);
                            echo '</pre>';
                                $userdetail->posicionHCM = null;//$data->PERNR;
                                $userdetail->identityDocument = null;//$data->ICNUM;
                                $userdetail->colaborador = $data->CNAME;
                                $userdetail->codPosicion = $data->PLANS;
                                $userdetail->desPosition = $data->STEXT;
                                $userdetail->codCeco = is_numeric($data->KOSTL) ? $data->KOSTL : null;
                                $userdetail->desCeco = $data->KTEXT;
                                $userdetail->desManagement = $data->ORGEH3;
                                $userdetail->desSubManagement = $data->ORGEH4;
                                $userdetail->desDepartment = $data->ORGEH5;
                                $userdetail->desArea = $data->ORGEH6;
                                $userdetail->codFuncion = $data->STELL;
                                $userdetail->desFuncion = $data->STELL_TX;
                                $userdetail->codAreaBusiness = $data->HROBJID;
                                $userdetail->desAreaBusiness = $data->HROBJID_TX;
                                $userdetail->codBoss = $data->PLANS1;
                                $userdetail->bossName = $data->NJEFE;
                                $userdetail->desBoss = $data->PLANS1_TX;
                                $userdetail->dateAdmission = $this->clear($data->BEGDA);
                                $userdetail->divPersonal = $data->WERKS;
                                $userdetail->desDivisions = $data->WERKS_TX;
                                $userdetail->civilStatusId = $this->typeSexo($data->FATXT);
                                $userdetail->sexoId = $data->GESCH;
                                $userdetail->save();
                                $update ++;
                            }
                        }
                    } else {
                        //new
                        $userModel = new User;
                        $userModel->email = $email;
                        $userModel->name = $data->VORNA;
                        $userModel->lastName = $data->NACHN;
                        $userModel->mothersLastName = $data->NAME2;
                        $userModel->birthdate = $this->clear($data->GBDAT);
                        $userModel->password = $data->ICNUM != "" ? Hash::make($data->ICNUM) : Hash::make(123456);
                        $userModel->idRole = 2;
                        $userModel->idStatus = 1;
                        if ($userModel->save()) {
                            $userdetail = new Userdetail;
                            $userdetail->idUser = $userModel->idUser;
                            $userdetail->posicionHCM = null;//$data->PERNR;
                            $userdetail->identityDocument = null;//is_numeric($data->ICNUM) ? $data->ICNUM : null;
                            $userdetail->colaborador = $data->CNAME;
                            $userdetail->codPosicion = $data->PLANS;
                            $userdetail->desPosition = $data->STEXT;
                            $userdetail->codCeco = is_numeric($data->KOSTL) ? $data->KOSTL : null;
                            $userdetail->desCeco = $data->KTEXT;
                            $userdetail->desManagement = $data->ORGEH3;
                            $userdetail->desSubManagement = $data->ORGEH4;
                            $userdetail->desDepartment = $data->ORGEH5;
                            $userdetail->desArea = $data->ORGEH6;
                            $userdetail->codFuncion = $data->STELL;
                            $userdetail->desFuncion = $data->STELL_TX;
                            $userdetail->codAreaBusiness = $data->HROBJID;
                            $userdetail->desAreaBusiness = $data->HROBJID_TX;
                            $userdetail->codBoss = $data->PLANS1;
                            $userdetail->bossName = $data->NJEFE;
                            $userdetail->desBoss = $data->PLANS1_TX;
                            $userdetail->dateAdmission = $this->clear($data->BEGDA);
                            $userdetail->divPersonal = $data->WERKS;
                            $userdetail->desDivisions = $data->WERKS_TX;
                            $userdetail->civilStatusId = $this->typeSexo($data->FATXT);
                            $userdetail->sexoId = $data->GESCH;
                            $userdetail->save();
                            $insert ++;
                        }
                    }
                }
            }

            $arrayQuery = array('new_user' => $insert, 'user_update' => $update);
            //delete user not found
            $this->delectUserHcm($dataSoap);
            $this->userDefaulMedico();
            return ['success' => 'Sincronizacion realizada con exito', 'info' => $arrayQuery];
            echo '<script>window.close();</script>';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function clear($cadena){
	$cadena = str_replace(' ', '', $cadena);
	return $cadena;
    }
    public function delectUserHcm($dataSoap){
        try {
            $userModel = User::whereNotIn('email', ['intranet01@divemotor.com.pe', 'medico_ocupacional@divemotor.com.pe'])->get(['idUser','email']);
            $userModelDb = [];
             foreach ($userModel as $user){
                 $userModelDb[]= strtolower($user->email);
             }
            $resulPreview = array_diff($userModelDb, $dataSoap);
            //validate user defaul
            $userValidate = User::where('email', '=', 'intranet01@divemotor.com.pe')->first();
            if (!$userValidate) {
               $this->userDefaul();
               $this->userDefaulMedico();
            }
            $userConfirmation = User::whereIn('email',$resulPreview)->get(['idUser']);
            $idUserDelete = [];
            foreach ($userConfirmation as $idUser){
                $idUserDelete[]= $idUser->idUser;
            }
            if(count($resulPreview) > 0){
                $userDetailDelet = Userdetail::whereIn('idUser',$idUserDelete)->delete();
                $userequestsDelet = Requests::whereIn('idUser',$idUserDelete)->delete();
                $emailReset = PasswordReset::whereIn('email',$resulPreview)->delete();
                $userDelete = User::whereIn('idUser',$idUserDelete)->delete();
                return ['UserDelect' => count($userDelete), 'success'=> 'Delete success'];
            }
            return ['success' => 'Sincronizacion realizada con exito'];
           // echo '<script>window.close();</script>';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
     }


    public function typeSexo($sexo) {
        switch ($sexo) {
            case 'SOLTER' : return 1;
            case 'CASADO' : return 2;
            case 'CONVIV' : return 3;
            default: 1;
        }
    }

    public function userDefaul() {
         $user = new User;
         $user->name = 'Administrador';
         $user->lastName = 'Intranet';
         $user->mothersLastName = 'Super';
         $user->email = 'intranet01@divemotor.com.pe';
         $user->password = Hash::make('Intr@net01');
         $user->idStatus = 1;
         $user->idRole = 1;
         $user->corporativoId = 3014;
         if($user->save()){
             $userDetail = new Userdetail;
             $userDetail->idUser = $user->idUser;
             $userDetail->identityDocument = '101010101';
             $userDetail->civilStatusId = 1;
             $userDetail->sexoId = 1;
             $userDetail->posicionHCM = 21111643;
             $userDetail->colaborador = 'Super Usuario';
             $userDetail->desPosition =  'TECNICO DE TALLER';
             $userDetail->desManagement = 'Gerencia Región Sur';
             $userDetail->desSubManagement = 'Sucursal Alto Cural';        //SUB GERENCIA
             $userDetail->codBoss = null;    //CODE JEFE----- RELACION CON  PosicionHCM
             $userDetail->bossName = 'SIN JEFE';           //NOMBRE DEL JEFE
             $userDetail->desBoss = 'SUB GERENTE DE OPERACIONES';  //Descripcion posisicon del jefe
             $userDetail->dateAdmission = date("Y-m-d");
             $userDetail->save();
         }

    }

    public function userDefaulMedico() {
        $user = new User;
        $user->name = 'Vanessa';
        $user->lastName = 'Miranda';
        $user->mothersLastName = 'Madrid';
        $user->email = 'medico_ocupacional@divemotor.com.pe';
        $user->password = Hash::make('Intr@net01');
        $user->idStatus = 1;
        $user->idRole = 1;
        $user->corporativoId = 3014;
        if($user->save()){
            $userDetail = new Userdetail;
            $userDetail->idUser = $user->idUser;
            $userDetail->identityDocument = '1010101012';
            $userDetail->civilStatusId = 1;
            $userDetail->sexoId = 1;
            $userDetail->posicionHCM = 21111643;
            $userDetail->colaborador = 'Super Usuario';
            $userDetail->desPosition =  'Medico Ocupacional';
            $userDetail->desManagement = 'Gerencia de Desarrollo Humano';
            $userDetail->desSubManagement = 'Sucursal Alto Cural';        //SUB GERENCIA
            $userDetail->codBoss = null;    //CODE JEFE----- RELACION CON  PosicionHCM
            $userDetail->bossName = 'SIN JEFE';           //NOMBRE DEL JEFE
            $userDetail->desBoss = 'SUB GERENTE DE OPERACIONES';  //Descripcion posisicon del jefe
            $userDetail->dateAdmission = date("Y-m-d");
            $userDetail->save();
        }

   }


}
