<?php


namespace App\Http\Controllers\Collaborator;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use App\Providers\Google;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Entities\Area;
use App\Entities\Department;
use App\Entities\Position;
use App\Entities\Organizations;
use App\Entities\Sexo;
use App\EmailInformation;
use App\UpdateInformation;
use App\Entities\User\User;
use App\Entities\User\Userdetail;
use App\Entities\User\Role;
use App\CivilStatus;
use App\Status;

ini_set('max_execution_time', 900);

class CollaboratorController extends Controller
{

  /**
   * Display a listing of the resource.
   * @return Response
   */
  protected $user;

  function __construct()
  {
    $this->user = JWTAuth::parseToken()->authenticate();
  }

  public function getDataProfile () {
    return json_encode([
      'areas' => Area::get(),
      'organizations' => Organizations::where('status', 1)->get(),
      'sexos' => Sexo::get(),
      'positions' => Position::get(),
      'departments' => Department::get()
    ]);
  }

  //User activities
  public function getActivities(Request $request)
  {
    $total = count(DB::select('
          (select requests."dateStart" as start, requests."idType", "idUser", requests.created_at, requests."idRequest" as id, \'Generó una solicitud de \' || type_request.description as description, \'requests\' as table, null as comment
          from requests
          inner join type_request on requests."idType" = type_request."idType"
          where "idUser" = '.$this->user->idUser.')
          UNION ALL
          (select event."startDate" as start, null as idType, "idUser", created_at, id, \'Se inscribió en el evento \' || summary as description, \'event\', null
          from "event"
          where "idUser" = '.$this->user->idUser.')
          UNION ALL
          (select null as start, null as idType, user_postulation."idUser", user_postulation.created_at, user_postulation.id, \'Se postuló a un cargo: \' || title as description, \'postulation\', null
          from user_postulation
          inner join postulation on user_postulation."idPostulation" = postulation.id
          where user_postulation."idUser" = '.$this->user->idUser.')
          UNION ALL
          (select null as start, null as idType, "idUsrCmt" as idUser, congratulation.created_at, id, \'Felicitó al compañero \' || "user".name || \' \' || "user"."lastName" as description, \'congratulation\', "comment"
          from congratulation
          inner join "user" on congratulation."idUser" = "user"."idUser"
          where congratulation."idUsrCmt" = '.$this->user->idUser.')
          UNION ALL
          (select null as start, null as idType, "user"."idUser" as idUser, congratulation.created_at, id, \'El compañero \' || "user".name || \' \' || "user"."lastName" || \' te ha fecilitado\' as description, \'congratulation\', "comment"
          from congratulation
          inner join "user" on congratulation."idUsrCmt" = "user"."idUser"
          where congratulation."idUser" = '.$this->user->idUser.')
          order by created_at desc;'));
    $activities = DB::select('
          (select requests."dateStart" as start, requests."idType", "idUser", requests.created_at, requests."idRequest" as id, \'Generó una solicitud de \' || type_request.description as description, \'requests\' as table, null as comment
          from requests
          inner join type_request on requests."idType" = type_request."idType"
          where "idUser" = '.$this->user->idUser.')
          UNION ALL
          (select event."startDate" as start, null as idType, "idUser", created_at, id, \'Se inscribió en el evento \' || summary as description, \'event\', null
          from "event"
          where "idUser" = '.$this->user->idUser.')
          UNION ALL
          (select null as start, null as idType, user_postulation."idUser", user_postulation.created_at, user_postulation.id, \'Se postuló a un cargo: \' || title as description, \'postulation\', null
          from user_postulation
          inner join postulation on user_postulation."idPostulation" = postulation.id
          where user_postulation."idUser" = '.$this->user->idUser.')
          UNION ALL
          (select null as start, null as idType, "idUsrCmt" as idUser, congratulation.created_at, id, \'Felicitó al compañero \' || "user".name || \' \' || "user"."lastName" as description, \'congratulation\', "comment"
          from congratulation
          inner join "user" on congratulation."idUser" = "user"."idUser"
          where congratulation."idUsrCmt" = '.$this->user->idUser.')
          UNION ALL
          (select null as start, null as idType, "user"."idUser" as idUser, congratulation.created_at, id, \'El compañero \' || "user".name || \' \' || "user"."lastName" || \' te ha fecilitado\' as description, \'congratulation\', "comment"
          from congratulation
          inner join "user" on congratulation."idUsrCmt" = "user"."idUser"
          where congratulation."idUser" = '.$this->user->idUser.')
          order by created_at desc limit '.$request->limit.' offset '.$request->offset.';');
    $array = [];
    foreach ($activities as $activity) {
      $link = '';
      $start = '';
      $comment = '';
      switch ($activity->table) {
        case 'requests':
          if($activity->idType == 1){
            $link = '/request/medical';
          }else if($activity->idType == 2){
            $link = '/request/medicine';
          }else if($activity->idType > 2 && $activity->idType < 7){
            $link = '/request/room';
          }else{
            $link = '/request/constance';
          }
          $description = 'Ir a las solicitudes';
          $start = '<div class="caption"><i class="material-icons icon primary--text" style="margin-left:15px;padding:10px;">event</i>&nbsp;'.date('d/m/Y', strtotime($activity->start)).'</div>';
          break;
        case 'event':
          $link = '/events';
          $description = 'Ir a los eventos';
          break;
        case 'postulation':
          $link = null;
          $description = 'Ir a las postulaciones';
          break;
        case 'congratulation':
          $link = '/birthdays';
          $description = 'Ir a los cumpleaños';
          $comment = ($activity->comment) ? '<div class="caption"><i class="material-icons icon primary--text" style="margin-left:15px;padding:10px;">cake</i>&nbsp;'.$activity->comment.'</div>' : null;
          break;
      }
      $array[] = [
        'description' => $activity->description,
        'created_at' => $activity->created_at,
        'link' => ['to' => $link, 'description' => $description],
        'comment' => $comment,
        'start' => $start
      ];
    }
    return json_encode(['rows' => $array, 'total' => $total]);
  }

  public function getMyChanges(Request $request)
  {
    $total = UpdateInformation::where('idUser', $this->user->idUser)
      ->whereIn('idStatus', [0, 1, 2])
      ->groupBy('groupUpdate')
      ->get(['groupUpdate'])->count();
    $update = UpdateInformation::where('idUser', $this->user->idUser)
      ->whereIn('idStatus', [0, 1, 2])
      ->limit($request->input('limit'))
      ->offset($request->input('offSet'))
      ->groupBy('created_at', 'idStatus', 'idUser', 'groupUpdate')
      ->get(['created_at', 'idStatus', 'idUser', 'groupUpdate']);
    return json_encode(['total' => $total, 'rows' => $update]);
  }

  public function getChangeRequest(Request $request)
  {
    $update = UpdateInformation::join('user', 'update_information.idUser', '=', 'user.idUser')
      ->where('update_information.idStatus', 0);
    $total = $update->count();
    $update = $update->groupBy('update_information.idUser', 'user.name', 'user.lastName', 'user.email','update_information.created_at', 'groupUpdate')
      ->limit($request->input('limit'))
      ->offset($request->input('offSet'))
      ->get(['update_information.idUser', 'name', 'lastName', 'email', 'update_information.created_at', 'groupUpdate']);
    return json_encode(['total' => $total, 'rows' => $update]);
  }

  public function getUpdateDetail(Request $request)
  {
    return json_encode(UpdateInformation::where('idUser', $request->input('id'))
      ->where('groupUpdate', $request->input('group'))
      ->get(['description', 'value']));
  }

  public function index(Google $google)
  {

    $sincronizarurl = $google->usersall();

    $regiones = Region::all();
    $sedes = Organizations::where('levelsid', 4)
      ->get(['corporativoId', 'codRelational', 'nameStructure', 'idRegion']);
    return view('admin::collaborator')->with('sedes', $sedes)
      ->with('regiones', $regiones)->with('sincronizarurl', $sincronizarurl);
  }

  // all directory
    public function listUser(Request $request) {
        $limit = $request->input('limit');
        $offset = $request->input('offset');
        $total = User::count();

        $users = User::limit($limit)->offset($offset)
                ->join('user_detail', 'user_detail.idUser', '=', 'user.idUser')
                ->leftJoin('role', 'role.idRole', '=', 'user.idRole')
                ->leftJoin('status', 'status.idStatus', '=', 'user.idStatus')
                ->leftJoin('organization as x', 'x.corporativoId', '=', 'user.corporativoId')
                ->leftJoin('organization as y', 'x.codFather', '=', 'y.codRelational')
                ->leftJoin('organization_region as z', 'x.idRegion', '=', 'z.idRegion')
                ->leftJoin('countries', 'x.codeCountry', '=', 'countries.codeCountry');
        if (!empty($request->input('search'))) {
            $search = $request->input('search');
            $users = $users->where('user.email', 'ilike', '%' . $search . '%')->orWhere('user.name','ilike', '%' . $search . '%')->orWhere('user_detail.desPosition','ilike', '%' . $search . '%');
           // $total = count($users);
        }
        if (!empty($request->input('id'))) {
            $users = $users->where('directory_contact.idUser', $this->user->idUser)
                    ->where('directory_contact.typeContact', $request->input('id'));
            $total = count($users);
        }
        $users = $users->orderBy('user.name', 'ASC')->get(['user.idUser', 'name', 'lastName', 'email', 'status.statusName', 'status.idStatus', 'user.birthdate', 'role.idRole', 'role.roleName', 'avatar',
            'user.corporativoId', 'x.nameStructure as sucursal', 'y.nameStructure as sociedad', 'z.description as region', 'country',
            'user.avatar',
            'user_detail.identityDocument', 'user_detail.civilStatusId', 'user_detail.sexoId','user_detail.addresses','user_detail.phones',
            'user_detail.desPosition','user_detail.desArea','user_detail.desDepartment','user_detail.desSubManagement','user_detail.divCorp','user_detail.divPersonal','user_detail.desDivisions']);
        $arrayQuery = array('total' => $total, 'rows' => $users);
        return json_encode($arrayQuery);
    }

    public function listRole(Request $request) {
        $role = Role::get(['idRole','roleName']);
        return json_encode($role);
    }

    public function listStatus(Request $request) {
        $status = Status::get(['idStatus','statusName']);
        return json_encode($status);
    }

    public function updateUser(Request $request) {
        //Validation
        $validator = Validator::make($request->all(), [ //Reglas
                    "idUser" => "required",
                    "idStatus" => "required",
                    "idRole" => "required",
                        ], [//Mensajes
                    "idUser.required" => trans('api.error.1.error'),
                    "idStatus.required" => trans('api.error.2.error'),
                    "idRole.required" => trans('api.error.3.error'),
        ]);

        if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()->first()], 400);
        }
        try {

            $user = User::findOrFail($request->input('idUser'));
            $user->idRole = $request->input('idRole');
            $user->idStatus = $request->input('idStatus');
            $user->birthdate = $request->input('birthdate');
            $user->corporativoId = $request->input('sucursalid');
            if ($user->save()) {
                $userdetail = Userdetail::where('idUser', $request->input('idUser'))->first();
                $userdetail->identityDocument = $request->input('identityDocument');
                $userdetail->sexoId = $request->input('sexoId');
                if ($userdetail->save()){
                    return ['success' => trans('api.success.40')];
                }
            } else {
                return response()->json(['error' => trans("api.error.6")], 400);
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
        }
    }

    /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    //Validation
    $validator = Validator::make($request->all(), [ //Reglas
      "email" => "required|email",
      "getGivenName" => "required",
      "getFamilyName" => "required",
      "documentoid" => "required",
      "documentoid" => 'unique:user_detail,identityDocument',
      "birthdate" => "required",
      "idRole" => "bail|required|integer",
      "idStatus" => "bail|required|integer",
      "sucursalid" => "required",
    ], [//Mensajes
      "email.required" => trans('layouts::api.error.4.message'),
      "email.email" => trans('layouts::api.error.14.message'),
      "getGivenName.required" => trans('layouts::api.error.17.message'),
      "getFamilyName.required" => trans('layouts::api.error.18.message'),
      "documentoid.required" => trans('layouts::api.error.11.message'),
      "documentoid.unique" => trans('layouts::api.error.19.message'),
      "birthdate.required" => trans('layouts::api.error.20.message'),
      "idRole.required" => trans('layouts::api.error.3.message'),
      "idStatus.required" => trans('layouts::api.error.2.message'),
      "sucursalid.required" => trans('layouts::api.error.12.message'),
    ]);
    if ($validator->fails()) {
      return ["error" => $validator->errors()->first()];
    }
    try {
      $user = User::where('email', $request->email)->count();
      if (!$user) {
        $user = new User;
        $user->name = $request->getGivenName;
        $user->lastName = $request->getFamilyName;
        $user->email = $request->email;
        $user->password = Hash::make($request->documentoid);
        $user->avatar = null;
        $user->idStatus = $request->idStatus;
        $user->idRole = $request->idRole;
        $user->birthdate = $request->birthdate;
        $user->corporativoId = $request->sucursalid;
        if ($user->save()) {
          $userdetail = new Userdetail;
          $userdetail->idUser = $user->idUser;
          $userdetail->PosicionHCM = $request->posicion;
          $userdetail->identityDocument = $request->documentoid;
          $userdetail->civilStatusId = $request->statuscivil;
          $userdetail->sexoId = $request->sexo;
          $userdetail->areaId = $request->area;
          $userdetail->departmentId = $request->department;
          $userdetail->positionId = $request->position;
          if ($userdetail->save()) {
            return ['success' => trans('layouts::api.success.5.message')];
          }
        }
      } else {
        return ['error' => trans('layouts::api.error.13.message')];
      }
    } catch (\Exception $e) {
      return $e->getMessage();
    }
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function loadexcel(Request $request) {
        try {
            if ($request->hasFile('fileExcel')) {
                $path = $request->file('fileExcel')->getRealPath();
                $objPHPExcel = \PHPExcel_IOFactory::load($path);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $locale = 'es';
                $validLocale = \PHPExcel_Settings::setLocale($locale);
                if (!$validLocale) {
                    print_r('Localidad Invalida');
                }
                $highestRow = $objWorksheet->getHighestRow();
                for ($row = 3; $row <= $highestRow; $row++) {
              //$newDateString =  $objPHPExcel->getActiveSheet()->getStyle('G'.$row)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
              //$newDateString = $objWorksheet->getCell('G'.$row)->getFormattedValue();

             // $birthdate = $newDateString;
              $email = strtolower($objWorksheet->getCell('A' . $row)->getValue());
              $phone = $objWorksheet->getCell('C' . $row)->getValue();
              $anexo = $objWorksheet->getCell('B' . $row)->getValue();
              $info = [];
              array_push($info,
              ['value' => $anexo, 'type' => 'work'],
              ['value' => $phone, 'type' => 'mobile']);

                    if ($email != "") {
                        $userModel = User::where('email', '=', $email)->first();
                        if ($userModel) {
                            //$userModel->birthdate = ($birthdate != "") ? $birthdate : null;
                            if ($userModel->save()) {
                                $userdetail = Userdetail::where('idUser', '=', $userModel->idUser)->first();
                                $userdetail->phones = json_encode($info, true);
                                $userdetail->save();
                            }
                        }
                    }
                }
                return ['success' => trans('api.success.45')];

            }
        } catch (\Exception $e) {
          //  return ['error' => $e->getMessage()];
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

  public function exportexcel(Request $request)
  {
    $objPHPExcel = new \PHPExcel();
// Set properties
//echo date('H:i:s') . " Set properties\n";
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
    $objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
    $objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
    $objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
    $objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");

    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Doc. Identidad')->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Posición')->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Ape. Paterno')->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Ape. Materno')->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Nombres')->getColumnDimension('E')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'COLABORADOR')->getColumnDimension('F')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Desc.Posición')->getColumnDimension('H')->setWidth(20);

    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Gerencia')->getColumnDimension('M')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Subgerencia')->getColumnDimension('N')->setWidth(20);

    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Departamento')->getColumnDimension('O')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Área')->getColumnDimension('P')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Pos. Sup. Lineal')->getColumnDimension('U')->setWidth(20);

    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Fec. Ing. Grupo')->getColumnDimension('X')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Cod. Surcursal')->getColumnDimension('AA')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Fec. Nacimiento')->getColumnDimension('AC')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Est. Civil')->getColumnDimension('AD')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Sexo')->getColumnDimension('AE')->setWidth(20);
    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Email')->getColumnDimension('AF')->setWidth(20);

    /*
     * color de columna
     */
    $objPHPExcel->getActiveSheet()
      ->getStyle('A1:aR1')
      ->getFill()
      ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
      ->getStartColor()
      ->setARGB('D8E4BC');
    /*
     * columna horizontal center
     */
    $objPHPExcel->getActiveSheet()->getStyle('A1:aF1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A1:aF1')->getFont()->setBold(true);
    /*
     * data value column
     */
    $users = User::join('user_detail', 'user_detail.idUser', '=', 'user.idUser')
      ->join('sexo', 'sexo.sexoId', '=', 'user_detail.sexoId')
      ->join('civilStatus', 'civilStatus.civilStatusId', '=', 'user_detail.civilStatusId')
      ->join('position', 'position.positionId', '=', 'user_detail.positionId')
      ->join('department', 'department.departmentId', '=', 'user_detail.departmentId')
      ->join('area', 'area.areaId', '=', 'user_detail.areaId')
      ->join('organization', 'organization.corporativoId', '=', 'user.corporativoId')
      ->get(['user.idUser', 'user.name', 'user.mothersLastName', 'user.email', 'sexo.sexo', 'civilStatus.civilStatus', 'user_detail.identityDocument',
        'user_detail.dateAdmission', 'user_detail.management', 'user_detail.subManagement', 'user_detail.higherPosition', 'user_detail.PosicionHCM', 'user_detail.Colaborador', 'user.lastName', 'user.name', 'position.position', 'department.department', 'area.area',
        'organization.codRelational', 'organization.nameStructure']);
    $f = 2;
    foreach ($users as $user) {
      $objPHPExcel->getActiveSheet()->getRowDimension($f)->setRowHeight(30);

      $objPHPExcel->getActiveSheet()->getStyle('A' . $f . ':R' . $f)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $f, $user->identityDocument)->setShowGridlines(true)
        ->setCellValue('B' . $f, $user->PosicionHCM)->setShowGridlines(true)
        ->setCellValue('C' . $f, $user->lastName)->setShowGridlines(true)
        ->setCellValue('D' . $f, $user->mothersLastName)->setShowGridlines(true)
        ->setCellValue('E' . $f, $user->name)->setShowGridlines(true)
        ->setCellValue('F' . $f, $user->Colaborador)->setShowGridlines(true)
        ->setCellValue('G' . $f, $user->position)->setShowGridlines(true)
        ->setCellValue('H' . $f, $user->management)->setShowGridlines(true)
        ->setCellValue('I' . $f, $user->subManagement)->setShowGridlines(true)
        ->setCellValue('J' . $f, $user->department)->setShowGridlines(true)
        ->setCellValue('K' . $f, $user->area)->setShowGridlines(true)
        ->setCellValue('L' . $f, $user->higherPosition)->setShowGridlines(true)
        ->setCellValue('M' . $f, date('d-m-Y', strtotime($user->dateAdmission)))->setShowGridlines(true)
        ->setCellValue('N' . $f, $user->codRelational)->setShowGridlines(true)
        ->setCellValue('O' . $f, date('d-m-Y', strtotime($user->birthdate)))->setShowGridlines(true)
        ->setCellValue('P' . $f, $user->civilStatus)->setShowGridlines(true)
        ->setCellValue('Q' . $f, $user->sexo)->setShowGridlines(true)
        ->setCellValue('R' . $f, $user->email)->setShowGridlines(true);
      $f++;
    }


// Rename sheet
    $objPHPExcel->getActiveSheet()->setTitle('Simple');
// Save Excel 2007 file
    $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);

    header("Content-type:application/xlsx");
    header("Content-Disposition:attachment;filename='Collaborator.xlsx'");
    ob_clean();
    $objWriter->save("php://output");


  }

  /**
   * Remove the specified resource from storage.
   * @return Response
   */
  public function syncup(Google $google)
  {
    $google->usersall();
  }



  /**
   * index view contact
   * @return Response
   */
  public function indexcollaboratorList()
  {
    $regiones = Region::all();
    $sedes = Organizations::where('levelsid', 4)
      ->get(['corporativoId', 'codRelational', 'nameStructure', 'idRegion']);

    return view('admin::collaboratorList')->with('sedes', $sedes)
      ->with('regiones', $regiones);
  }

  /**
   * List intranet users.
   * @return Response
   */


  public function submitRequest(Request $request)
  {
    try {
      DB::beginTransaction();

      $emails = EmailInformation::where('type', 1)->get();
      if (count($emails) <= 0) {
        return response()->json(['error' => trans('api.error.22')], 400);
      }

      UpdateInformation::where('idUser', $this->user->idUser)
        ->where('idStatus', 0)
        ->update(['idStatus' => -1]);

      $group = $this->user->idUser . rand(1, 99999);
      foreach ($request->input('changes') as $key => $change) {
        $update = new UpdateInformation;
        $update->idUser = $this->user->idUser;
        $update->description = $change['key'];
        $update->idStatus = 0;
        $update->value = $change['value'];
        $update->groupUpdate = $group;
        $update->save();
      }

      $colaborador = User::where('user.idUser', $this->user->idUser)
        ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser')
        ->get(['name', 'lastName', 'posicionHCM', 'email']);
      $changes = UpdateInformation::where('idUser', $this->user->idUser)->where('idStatus', 0)->get(['description', 'value']);

      foreach ($emails as $email) {
        $emailTo = $email->description;
        $colaborator = User::where('email', $emailTo)->get(['name'])[0]->name;
        $data = [
          'nombre' => $colaborador[0]->name,
          'email' => $colaborador[0]->email,
          'hcm' => $colaborador[0]->posicionHCM,
          'apellido' => $colaborador[0]->lastName,
          'colaborator' => $colaborator,
          'data' => $changes
        ];
        Mail::send('emails.updateinformation', $data, function ($message) use ($emailTo, $colaborator) {
          $message->to($emailTo, $colaborator)->subject('Cambio de datos');
        });
      }

      DB::commit();
      return response()->json(['success' => trans('api.success.14')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }

  }

  public function updateStatus(Request $request)
  {
    try {
      DB::beginTransaction();
      $action = $request->input('to') == 1 ? 'Aprobado' : 'Rechazado';

      $emails = EmailInformation::where('type', 1)->get();
      if (count($emails) <= 0) {
        return response()->json(['error' => trans('api.error.22')]);
      }

      $changes = UpdateInformation::where('groupUpdate', $request->input('id'))
        ->where('idStatus', 0)->get();

      $nombre = User::where('idUser', $changes[0]->idUser)->get(['name'])[0]->name;

      foreach ($emails as $email) {
        $emailTo = $email->description;
        $colaborator = User::where('email', $emailTo)->get(['name'])[0]->name;
        $data = [
          'nombre' => $nombre,
          'colaborator' => $colaborator,
          'data' => $changes,
          'accion' => $action
        ];
        Mail::send('emails.updateinformation', $data, function ($message) use ($emailTo, $colaborator) {
          $message->to($emailTo, $colaborator)->subject('Cambio de datos');
        });
      }
      foreach ($changes as $change) {
        $change->update(['idStatus' => $request->to]);
      }
      DB::commit();
      return response()->json(['success' => trans('api.success.15')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function manageEmail(Request $request)
  {
    try {
      $validator = Validator::make($request->all(),
        [ //Reglas
          "mails" => "required",
        ], [//Mensajes
          "mails.required" => trans('api.error.1'),
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }
      DB::beginTransaction();
      //EmailInformation::truncate();
      EmailInformation::where('type', $request->input('type'))->delete();
      if (count($request->input('mails')) > 0) {
        foreach ($request->input('mails') as $mail) {
          $colaborator = User::where('email', $mail)->get(['name']);
          if (count($colaborator) <= 0) {
            DB::rollBack();
            return response()->json(['error' => trans('api.error.1')], 400);
          }
          $email = new EmailInformation;
          $email->description = $mail;
          $email->type = $request->type;
          $email->save();
        }
      }
      DB::commit();
      return response()->json(['success' => trans('api.success.21')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function getEmails(Request $request)
  {
    return json_encode(EmailInformation::where('type', $request->input('type'))->get());
  }

  public function getProfileEditcontact(Request $request)
  {
    try {
      $user = ContactDirectory::where('idUser', Auth::user()->idUser)
        ->where('idUserContact', $request->idUser)->first();
      if (!$user) {
        $user = new ContactDirectory;
        $user->idUser = Auth::user()->idUser;
        $user->idUserContact = $request->idUser;
        $user->typeContact = $request->newtypeContact;
        if ($user->save()) {
          return ['success' => trans('layouts::api.success.1.message')];
        }
      } else {
        $user->typeContact = $request->newtypeContact;
        if ($user->save()) {
          return ['success' => trans('layouts::api.success.1.message')];
        }
      }
    } catch (\Exception $e) {
      return $e->getMessage();
    }
  }

  public function uploadImage(Request $request)
  {
    $sc = new SystemLinkController();
    $path = Storage::disk('avatar')->getDriver()->getAdapter()->getPathPrefix();
    try {
      $user = User::where('idUser', Auth::user()->idUser)->get();
      $oldImg = $user[0]->avatar;
      if ($oldImg != null) {
        Storage::disk('avatar')->delete(explode('/', $oldImg)[2]);
      }

      $fileName = strtolower(preg_replace('/\s+/', '', $sc->removeAccented(Carbon::now()->timestamp . $request->file('coverPhoto')->getClientOriginalName())));
      $img = Image::make(file_get_contents($request->file('coverPhoto')));
      $img->crop((int)$request->w, (int)$request->h, (int)$request->x, (int)$request->y);
      $img->save($path . $fileName, 60);
      $exists = Storage::disk('avatar')->exists($fileName);
      if ($exists) {
        User::where('idUser', Auth::user()->idUser)
          ->update(['avatar' => '/File/avatar/' . $fileName]);
        return response()->json(['success' => 'Imagen cargada con exito', 'image' => '/File/avatar/' . $fileName]);
      }
    } catch (Exception $ex) {
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }

  public function clearStrng($string) {
     return (ereg_replace('[^ A-Za-z0-9_-ñÑ]', '', $string));
  }

}
