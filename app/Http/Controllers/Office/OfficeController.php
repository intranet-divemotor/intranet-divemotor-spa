<?php

namespace App\Http\Controllers\Office;

use Illuminate\Routing\Controller;
use Microsoft\Graph\Graph;
use App\TokenStore\TokenCache;
use Illuminate\Http\Request;
use App\Entities\User\User;
use App\Entities\User\Userdetail;
use Illuminate\Support\Facades\Hash;

class OfficeController extends Controller
{

  public function syncupOffice365(Request $request)
  {
    $tokenCache = new TokenCache;
    $graph = new Graph();
    $access_token = $request->access_token;
    $refresh_token = $request->refresh_token;
    $token_expires = 1574744995;
    $graph->setAccessToken($tokenCache->getAccessToken($access_token, $refresh_token, $token_expires));
    $getPeopleUrl = '/me/people/?$top=500';
    $peoples = $graph->createRequest('GET', $getPeopleUrl)
      ->addHeaders(array("Content-Type" => "application/json"))
      ->setReturnType(\Microsoft\Graph\Model\User::class)
      ->execute();
    $data = json_encode($peoples);
    $prepareData = json_decode($data, true);
    foreach ($prepareData as $key => $value) {
      if (!empty($value['userPrincipalName'])) {
        $email = strtolower($value['userPrincipalName']);
        //$verify = $this->verifydomain($email,$user->name->givenName); //verify domain intranet
        $userModel = User::where('email', '=', $email)->first();
        //editar usuario que ya esta en office
        if ($userModel) {
          $userModel->idStatus = 1;
          //$userModel->avatar = $user->thumbnailPhotoUrl == "" ? null : $user->thumbnailPhotoUrl;
          if ($userModel->save()) {
            $userdetail = Userdetail::where('idUser', '=', $userModel->idUser)->first();
            if ($userdetail) {
              //$userdetail->addresses = json_encode($user->addresses);
              $userdetail->phones = json_encode($value['phones']);
              $userdetail->save();
            }
          }
        } //new user
        elseif (!$userModel) {
          $userModel = new User;
          $userModel->name = $value['givenName'];
          $userModel->lastName = $value['surname'];
          $userModel->mothersLastName = $value['givenName'] . ' ' . $value['surname'];
          $userModel->email = $email;
          $userModel->password = Hash::make(123456);
          $userModel->idRole = 2;
          $userModel->idStatus = 1;
          $userModel->avatar = null;

          if ($userModel->save()) {
            $userdetail = new Userdetail;
            $userdetail->idUser = $userModel->idUser;
            $userdetail->colaborador = $value['givenName'] . $value['surname'];        //NOMBRE COMPLETO COLABORADOR
            //$userdetail->addresses = isset($user->addresses) ? json_encode($user->addresses) : null ;
            $userdetail->phones = json_encode($value['phones']);
            $userdetail->save();
          }
        }
      }
    }
    echo '<script>window.close();</script>';
  }
}
