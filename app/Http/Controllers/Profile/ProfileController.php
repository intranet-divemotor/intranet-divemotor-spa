<?php
namespace App\Http\Controllers\Profile;

use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Entities\Link\Link;
use App\Entities\User\Role;
use Carbon\Carbon;
use File;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProfileController extends Controller
{

  public function fetch(Request $r){

    $data = Role::limit($r->limit)->offset($r->offset);
    if(!empty($r->input('search'))){
      $data = $data->where('role.roleName', 'like' , '%'.$r->input('search').'%');
    }
    $total = Role::count();
    $data = $data->get();
    return json_encode(array('total' => $total, 'rows' => $data));
  }

  public function submit(Request $r){
    if($r->input('idRole') == 0) {
      $m = new Role();
      $m->roleName = $r->input('roleName');
      $m->modules = $r->input('modules');
      if ($m->save()) {
        return ['success' => trans('layouts::api.success.2.message')];
      } else {
        return ['error' => trans('layouts::api.error.100.error')];
      }
    }else {
      $m = Role::find($r->input('idRole'));
      $m->roleName = $r->input('roleName');
      $m->modules = $r->input('modules');
      if ($m->save()) {
        return ['success' => trans('layouts::api.success.2.message')];
      } else {
        return ['error' => trans('layouts::api.error.100.error')];
      }
    }
  }

  public function delete(Request $r){
    $m = Role::find($r->input('idRole'));
    $m->delete();
  }

}