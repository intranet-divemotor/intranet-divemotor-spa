<?php

namespace App\Http\Controllers\Birthday;

use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Entities\Birthday\Congratulation;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;
use Validator;
use App\Entities\User\User;
use App\Entities\User\Userdetail;

/**
 * Autor: Gabriel
 * Fecha: 30/1/2017
 * Detalle: Controlador de Birthday
 */
class BirthdayController extends Controller {
    /**
 * Autor: Gabriel
 * Fecha: 01/02/2017
 * Detalle: Metodo para buscar los cumpleaños del dia
 */
    public function fetch(Request $r) {
        $user = JWTAuth::parseToken()->authenticate();
        $day = date_format(Carbon::now(), "w");

        $queryOfDay = DB::table('user as u')
            ->leftJoin('congratulation as c', function($join) use ($user)
            {
                $join->on('c.idUser', '=', 'u.idUser')->where('c.idUsrCmt', '=', $user->idUser);
            })
            ->leftJoin('user as uc', 'uc.idUser', '=', 'c.idUsrCmt')
            ->leftJoin('user_detail as ud', 'ud.idUser', '=', 'u.idUser')
            ->leftJoin('congratulation as ca', function($join) use ($user)
            {
              $join->on('ca.idUser', '=', 'u.idUser')->where('ca.idUsrCmt', '=', $user->idUser);
            })
            ->join('group_divisions as gd', function($join) use ($user)
            {
                $join->on('ud.divPersonal', '=', 'ud.divPersonal')->where('ud.divPersonal', '=', DB::raw("ANY(regexp_split_to_array(gd.codes, ','))"));

            })
            ->where([[DB::raw("DATE_PART('month', u.birthdate)"), DB::raw("date_part('month', now())")], ['u.idUser', '<>', $user->idUser]
            ])
            ->select('u.idUser', 'u.name', 'u.lastName', 'u.birthdate', 'u.avatar', 'u.email', 'c.congratulated', 'ud.desPosition', 'ud.desArea', 'ca.comment', 'gd.description as divition');

        $queryAll = DB::table('user as u')
            ->leftJoin('user_detail as ud', 'ud.idUser', '=', 'u.idUser')
            ->join('group_divisions as gd', function($join) use ($user)
            {
                $join->on('ud.divPersonal', '=', 'ud.divPersonal')->where('ud.divPersonal', '=', DB::raw("ANY(regexp_split_to_array(gd.codes, ','))"));

            })
            ->orderBy(DB::raw("DATE_PART('day', u.birthdate)"))
            ->where([[DB::raw("DATE_PART('day', birthdate)"), '>' , DB::raw("date_part('day', now())")],
                [DB::raw("DATE_PART('month', u.birthdate)"), DB::raw("date_part('month', now())")],
                ['u.idUser', '<>', $user->idUser]])->select(
                DB::raw("DATE(date_part('year', now()) || '-' || DATE_PART('month', u.birthdate) || '-' || DATE_PART('day', u.birthdate)) as birthdate"),
                'u.idUser', 'u.name', 'u.lastName', 'u.birthdate', 'u.avatar', 'u.email', 'ud.desPosition', 'ud.desArea', DB::raw("now() as current"))
            ->select('u.idUser', 'u.name', 'u.lastName', 'u.birthdate', 'u.avatar', 'u.email', 'ud.desPosition', 'ud.desArea', 'gd.description as divition');

        if($day === 5 || $day === "5" ) $queryOfDay->whereIn(DB::raw("DATE_PART('day', u.birthdate)"), [Carbon::now()->day, Carbon::now()->addDay(2)->day, Carbon::now()->addDay(1)->day]);
        else $queryOfDay->where(DB::raw("DATE_PART('day', u.birthdate)"), DB::raw("date_part('day', now())"));

        $dayCumple = $queryOfDay->distinct()->get();
        if (count($dayCumple) > 0) {
            return json_encode($queryOfDay->distinct()->get());
        } else {
            return json_encode($queryAll->get());
        }

        //return json_encode($r->now == "true" ? $queryOfDay->distinct()->get() : $queryAll->get());
    }


    /**
     * Autor: Gabriel
     * Fecha: 01/02/2017
     * Detalle: Metodo para buscar los cumpleaños del dia
     */
    public function searchAllBirthday() {
        return json_encode(DB::table('user')->select(DB::raw("DATE(date_part('year', now()) || '-' || DATE_PART('month', birthdate) || '-' || DATE_PART('day', birthdate)) as birthdate"),
            "idUser", "name", "lastName", "email", "avatar", DB::raw("now() as current"))->orderBy('birthdate', 'desc')->get());
    }

    /**
     * Autor: Gabriel
     * Fecha: 01/02/2017
     * Detalle: Metodo para guardar comentario que emite el usuario
     */
    public function saveCongratulation(Request $r) {
      $user = JWTAuth::parseToken()->authenticate();
        //Validation
        $validator = Validator::make($r->all(), [ //Reglas
                    "idUser" => "bail|required|integer",
                        ], [//Mensajes
                    "idUser.required" => trans('layouts::api.error.1.error'),
        ]);
        if ($validator->fails()) return ["error" => $validator->errors()->first()];

        try {
            $c = new Congratulation();
            $c->idUser = $r->input('idUser');
            $c->idUsrCmt = $user->idUser;
            $c->created_at = Carbon::now();
            $c->comment = $r->input('comment');
            $c->congratulated = true;
            if ($c->save()) {
                return ['success' => trans('layouts::api.success.2.message')];
            } else {
                return ['error' => trans('layouts::api.error.100.error')];
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Autor: Gabriel
     * Fecha: 02/02/2017
     * Detalle: Metodo para guardar megusta a un comentario
     */
    public function sendLikeComment(Request $r) {
      $user = JWTAuth::parseToken()->authenticate();
        return Congratulation::where('id', $r->id)->update(['idUser' => $user->idUser, 'like' => $r->like === 'true']);
    }

    /**
     * Autor: Gabriel
     * Fecha: 02/02/2017
     * Detalle: Metodo para buscar las felicitaciones hechas al cumpleañero
     */
    public function searchCongratulation(Request $r) {
      $user = JWTAuth::parseToken()->authenticate();
        return json_encode(DB::table('congratulation as c')
                                ->join('user as u', 'u.idUser', '=', 'c.idUser')
                                ->leftJoin('user as uc', 'uc.idUser', '=', 'c.idUsrCmt')
                                ->leftJoin('congratulation as cu', 'cu.parent', '=', 'c.id')
                                ->where([['c.idUser', $user->idUser],['c.parent', null]])
                                ->select('u.idUser', 'u.name', 'u.lastName', 'u.email', 'u.avatar', 'uc.idUser as idUsrCmt',
                                        'cu.comment', 'cu.like', 'cu.id', 'cu.created_at',
                                    'uc.name as nameCmt', 'uc.lastName as lastNameCmt', 'uc.email as emailCmt', 'uc.avatar as avatarCmt',
                                    'c.id as idCmt', 'c.comment as commentCmt', 'c.like as likeCmt', 'cu.created_at as created_atCmt')->get());
    }

    public function sendResponse(Request $r){
      $user = JWTAuth::parseToken()->authenticate();
        $c = new Congratulation();
        $c->parent = $r->parent;
        $c->idUser = $user->idUser;
        $c->idUsrCmt = $user->idUser;
        $c->created_at = Carbon::now();
        $c->comment = $r->comment;
        $c->congratulated = true;
        if ($c->save()) {
            return ['success' => trans('layouts::api.success.2.message')];
        } else {
            return ['error' => trans('layouts::api.error.100.error')];
        }

    }



}
