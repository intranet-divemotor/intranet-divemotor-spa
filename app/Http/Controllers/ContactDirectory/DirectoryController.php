<?php

namespace App\Http\Controllers\ContactDirectory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Entities\User\User;
use App\Entities\User\TypeDirectory;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use DB;
use App\Entities\User\ContactDirectory;

class DirectoryController extends Controller {

    protected $user;

    function __construct() {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    // all directory
    public function listcontact(Request $request) {
        $limit = $request->input('limit');
        $offset = $request->input('offset');
        $total = User::where('user.idStatus',1)->count();

        $users = User::limit($limit)->offset($offset)
                ->join('user_detail', 'user_detail.idUser', '=', 'user.idUser')
                ->leftJoin('role', 'role.idRole', '=', 'user.idRole')
                ->leftJoin('status', 'status.idStatus', '=', 'user.idStatus')
                ->leftJoin('organization as x', 'x.corporativoId', '=', 'user.corporativoId')
                ->leftJoin('organization as y', 'x.codFather', '=', 'y.codRelational')
                ->leftJoin('organization_region as z', 'x.idRegion', '=', 'z.idRegion')
                ->leftJoin('countries', 'x.codeCountry', '=', 'countries.codeCountry')
                ->where('user.idStatus',1);

        if (!empty($request->input('search'))) {
            $search = $request->input('search');

            //filter email//
            $searchOption = User::limit($limit)->offset($offset)
                            ->join('user_detail', 'user_detail.idUser', '=', 'user.idUser')
                            ->where('user.email', 'ilike', '%' . $search . '%')->get(['user.idUser']);
            //filter email
            $countUser = count($searchOption);
            if ($countUser != 0) {
                $users = $users->where('user.email', 'ilike', '%' . $search . '%');
            } elseif ($countUser == 0) {
                //filter LastName
                $searchOption = User::limit($limit)->offset($offset)
                                ->join('user_detail', 'user_detail.idUser', '=', 'user.idUser')
                                ->where('user.lastName', 'ilike', '%' . $search . '%')->get(['user.idUser']);
                $countUser = count($searchOption);
                if ($countUser != 0) {
                    $users = $users->where('user.lastName', 'ilike', '%' . $search . '%');
                } elseif ($countUser == 0) {
                    $searchOption = User::limit($limit)->offset($offset)
                                    ->join('user_detail', 'user_detail.idUser', '=', 'user.idUser')
                                    ->where('user.name', 'ilike', '%' . $search . '%')->get(['user.idUser']);
                    $countUser = count($searchOption);
                    if($countUser != 0) {
                        $users = $users->where('user.name', 'ilike', '%' . $search . '%');
                    }elseif($countUser == 0){
                      $users = $users->where('user_detail.desPosition', 'ilike', '%' . $search . '%');
                    }
                }
            }
        }
        if (!empty($request->input('id'))) {
            $users = $users->leftJoin('directory_contact', 'directory_contact.idUserContact', '=', 'user.idUser')
                               ->where('user.idStatus',1)->where('directory_contact.idUser', $this->user->idUser)
                           ->where('directory_contact.typeContact', $request->input('id'));
            $total = count($users);
        }
        $users = $users->orderBy('user.name', 'ASC')->get(['user.idUser', 'name', 'lastName', 'mothersLastName','email', 'status.statusName', 'status.idStatus', 'user.birthdate', 'role.idRole', 'role.roleName', 'avatar',
            'user.corporativoId', 'x.nameStructure as sucursal', 'y.nameStructure as sociedad', 'z.description as region', 'country',
            'user.avatar',
            'user_detail.identityDocument', 'user_detail.civilStatusId', 'user_detail.sexoId',
            'user_detail.addresses','user_detail.phones','user_detail.divPersonal',
            'user_detail.desPosition','user_detail.desArea','user_detail.desDepartment','user_detail.desSubManagement','user_detail.divCorp','user_detail.desDivisions']);

        $countUser =count($users);

        $arrayQuery = array('total' => $total, 'rows' => $users, 'count'=>$countUser);
        return json_encode($arrayQuery);
    }

    // Create contact group
    public function save(Request $request) {
        try {
            $newdirectory = new TypeDirectory;
            $newdirectory->idUser = $this->user->idUser;
            $newdirectory->name = $request->nameGroup;
            if ($newdirectory->save()) {
                $response = array('success' => trans('api.success.6'), 'rows' => $newdirectory);
                //return response()->json(['success' => trans('api.success.3')]);
                return json_encode($response);
            } else {
                return response()->json(['error' => trans('api.error.6.message')]);

            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //update name group
    public function update(Request $request) {
        try {
            $editdirectory = TypeDirectory::findOrFail($request->id);
            $editdirectory->name = $request->nameGroup;
            if ($editdirectory->save()) {
                return ['success' => trans('api.success.7')];
            } else {
                return response()->json(['error' => trans('api.error.6.message')]);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //delete group contact
    public function delect(Request $request) {
        try {
            if($request->id){
            $f = TypeDirectory::find($request->id);
            }else{
            $f = ContactDirectory::where('idUser', $this->user->idUser)
                            ->where('idUserContact', $request->idUser)->first();
            }
            if ($f->delete()) {
                return ['success' => trans('api.success.10')];
            } else {
                return ['error' => trans('layouts::api.error.100.error')];
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //list user contact
    public function listDirectory()
    {
       $countfilter = DB::select('SELECT  COUNT(d."typeContact") , t.name, t."typeContact"
                                FROM type_contact t
                                LEFT JOIN directory_contact d ON D."typeContact" = t."typeContact"
                                WHERE t."idUser" = '.$this->user->idUser.'
                                GROUP BY t.name , t."typeContact"');
      return json_encode($countfilter);

    }

    //change group user
    public function getProfileEditcontact(Request $request) {
        try {

            foreach ($request->typeContact as $typecontact) {
                $user = ContactDirectory::where('idUser', $this->user->idUser)
                                ->where('idUserContact', $request->idUser)
                                ->where('typeContact', $typecontact)->first();
                if (!$user) {
                    $user = new ContactDirectory;
                    $user->idUser = $this->user->idUser;
                    $user->idUserContact = $request->idUser;
                    $user->typeContact = $typecontact;
                    if ($user->save()) {
                        return ['success' => trans('api.success.9')];
                    }
                }
            }
          $contactDelect =  ContactDirectory::whereNotIn('typeContact', $request->typeContact)->where('idUser', $this->user->idUser)
                             ->where('idUserContact', $request->idUser)->firstOrFail();
          if ($contactDelect) {
                $contactDelect->delete();
                return ['success' => trans('api.success.9')];
          }



        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

}
