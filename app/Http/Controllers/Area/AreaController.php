<?php

namespace App\Http\Controllers\Area;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Entities\Area;

class AreaController extends Controller
{
    public function fetchSelect(Request $r){
        $result = Area::where('area', 'ilike', '%'.$r->input('q').'%');
        if(!empty($r->input('cod')))$result->where('cod', $r->input('cod'));
        if(!empty($r->input('limit')))$result->limit($r->input('limit'));
        return json_encode($result->get(['cod as value', 'area as text']));
    }
}
