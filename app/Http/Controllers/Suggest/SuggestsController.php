<?php

namespace App\Http\Controllers\Suggest;

use App\Http\Controllers\SystemLink\SystemLinkController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Suggests;
use Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Entities\User\Userdetail;

class SuggestsController extends Controller
{
  protected $user;

  function __construct()
  {
    $this->user = JWTAuth::parseToken()->authenticate();
  }

  public function getsuggests(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    $suggest = Suggests::join('user', 'suggests.idUser', '=', 'user.idUser')
      ->join('user_detail', 'user.idUser', '=', 'user_detail.idUser');
    if (!in_array($this->user->idRole, [1, 3])/* < 0 && Auth::user()->idRole != 2*/) {
      $suggest = $suggest->where('suggests.idUser', $this->user->idUser);
    }
    $sede = !empty($request->input('sede')) ? $request->input('sede') : Userdetail::where('idUser', $user->idUser)->get(['divPersonal'])[0]->divPersonal;
    if (in_array($this->user->idRole, [2, 3])) {
      $suggest = $suggest->where('divPersonal', $sede);
    }
    if ($request->has('sede')) {
      $suggest = $suggest->where('divPersonal', $sede);
    }
    $total = $suggest->count();
    $suggest = $suggest->whereNotIn('suggests.idStatus', ['6'])
      ->orderBy('suggests.created_at', 'desc')
      ->limit($request->input('limit'))
      ->offset($request->input('offSet'))
      ->get(['idSuggest', 'user.name', 'user.lastName', 'suggests.idUser', 'suggests.purpose', 'suggests.code',
        'suggests.idStatus', 'suggests.note', 'suggests.description', 'user_detail.identityDocument',
        'suggests.created_at', 'suggests.files']);
    return json_encode([
      'total' => $total,
      'rows' => $suggest
    ]);
  }

  public function uploadFile(Request $request)
  {
    try {
      $accept = ['png', 'jpg', 'jpeg'];
      $sc = new SystemLinkController();
      $files = $request->file('file');
      $routes = [];

      foreach ($files as $file) {
        if($file->getSize() > 5242880) return response()->json(['error' => trans('api.error.32')], 400);
        $exclamation = substr($file->getMimeType(), 0, 5) == 'image' ? '!' : '';
        $name = $file->getClientOriginalName();
        $arr = explode('.', $name);
        $ext = strtolower(end($arr));
        if(!in_array($ext, $accept)) return response()->json(['error' => trans('api.error.33')], 400);
        $nameForRoute = preg_replace('/\s+/', '', $sc->removeAccented('sugerencia_' . Carbon::now()->timestamp . $name));
        Storage::disk('suggest/tmp')->makeDirectory(Auth::user()->idUser);
        Storage::disk('suggest/tmp')->put(Auth::user()->idUser . '/' . $nameForRoute, file_get_contents($file));
        $route = $exclamation . '[' . $name . '](/File/suggest/tmp/' . Auth::user()->idUser . '/' . $nameForRoute . ')';
        array_push($routes, $route);
      }

      return response()->json(['success' => $routes]);
    } catch (Exception $ex) {
      //Storage::disk('suggest/tmp')->deleteDirectory(Auth::user()->idUser);
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }

  public function submitsuggests(Request $request)
  {
    try {
      $accept = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg'];
      $sc = new SystemLinkController();
      DB::beginTransaction();
      $validator = Validator::make($request->all(),
        [ //Reglas
          "purpose" => "required",
          "description" => "required"
        ], [//Mensajes
          "purpose.required" => trans('api.error.29'),
          "description.required" => trans('api.error.29')
        ]);
      if ($validator->fails()) {
        DB::rollBack();
        return response()->json(['error' => $validator->errors()->first()], 400);
      }

      $description = trim($request->input('description'));

      $files =  [];
      $filesE = $request->file('files');
      if(count($filesE)) {
        foreach ($filesE as $file) {
          if($file->getSize() > 5242880) return response()->json(['error' => trans('api.error.32')], 400);
          $name = preg_replace('/\s+/', '', $sc->removeAccented('suggest_' . Carbon::now()->timestamp . $file->getClientOriginalName()));
          $arr = explode('.', $name);
          $ext = strtolower(end($arr));
          if(!in_array($ext, $accept)) return response()->json(['error' => trans('api.error.33')], 400);
          $file->move('files/suggest/', $name);
          array_push($files, $name);
        }
      }

      $suggest = new Suggests;
      $suggest->idUser = $this->user->idUser;
      $suggest->purpose = trim($request->input('purpose'));
      $suggest->description = $description;
      $suggest->files = implode(',', $files);
      $suggest->idStatus = 0;
      if ($suggest->save()) {
        $code = 'S' . Auth::user()->idUser . '-0' . $suggest->idSuggest;
        $suggest->code = $code;
        $suggest->save();
      }

      DB::commit();
      return response()->json(["success" => trans('api.success.19')]);
    } catch (Exception $ex) {
      DB::rollBack();
      return response()->json(['error' => trans('api.error.100')], 400);
    }
  }

  public function deleteSuggest(Request $request)
  {
    try {
      Suggests::where('idSuggest', $request->idSuggest)
        ->delete();
      return response()->json(["success" => "Sugerencia eliminada con exito"]);
    } catch (Exception $ex) {
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }

  public function storeSuggest(Request $request)
  {
    try {
      Suggests::where('idSuggest', $request->idSuggest)
        ->update(['idStatus' => 6]);
      return response()->json(["success" => "Sugerencia archivada con exito"]);
    } catch (Exception $ex) {
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }

  public function setNote(Request $request)
  {
    try {


      Suggests::where('idSuggest', $request->idSuggest)
        ->update(['note' => trim($request->note)]);
      return response()->json(["success" => "Nota guardada con exito"]);
    } catch (Exception $ex) {
      return response()->json(['error' => 'Error de sistema. Intentelo nuevamente']);
    }
  }
}
