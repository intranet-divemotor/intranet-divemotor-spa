<?php
/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 16-08-2017
 * Time: 12:28 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisions extends Model
{
  protected $table = 'divisions';
  protected $primaryKey = 'id';

  protected $fillable = [
    'id', 'description', 'code', 'society', 'idStatus'
  ];
}