<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeRequest extends Model
{
  protected $table = 'type_request';
  protected $primaryKey = 'idType';

  protected $fillable = [
    'idType', 'description'
  ];

  public function Question()
  {
    return $this->hasMany('App\Question', 'idTypeRequest');
  }
}
