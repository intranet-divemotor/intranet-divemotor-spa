<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteLink extends Model
{
    protected $table = 'favoritelink';
    protected $primaryKey = 'id';

    protected $fillable = [
        'parent', 'userId', 'areaId', 'name', 'urlPath', 'iconPath', 'type'
    ];
}
