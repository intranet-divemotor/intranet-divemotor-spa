<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    protected $table = 'tickets';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'idUser', 'idUserCoor', 'type', 'created_at', 'updated_at'];
}
