<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DaysRequest extends Model
{
    protected $table = 'days_request';
    protected $primaryKey = 'idDay';

    protected $fillable = [
        'idDay', 'day', 'corporativoId'
    ];
}
