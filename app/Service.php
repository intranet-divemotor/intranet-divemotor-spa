<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'service';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'description', 'created_at', 'updated_at'];
}
