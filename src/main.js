import Vue from 'vue'
import {sync} from 'vuex-router-sync'
import App from './App'
import './plugins'
import router from './router'
import store from './store/index'
import i18n from './plugins/i18n'
process.env.STYLES === 'DV' ? require('./assets/style/app.styl') : require('./assets/style/cr.styl')
process.env.STYLES === 'DV' ? require('./assets/scss/site.scss') : require('./assets/scss/site-ca.scss')
require('../node_modules/slick-carousel/slick/slick-theme.scss')
require('../node_modules/slick-carousel/slick/slick.scss')
Vue.config.productionTip = false
sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: { App }
})
