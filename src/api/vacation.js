import axios from 'axios'
import {url} from './'

export default {
  async submitExcelVacation (params) {
    return await axios.post(`${url}/vacation/submit`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async getVacations (params) {
    return await axios.get(`${url}/vacation/getvacations`, params)
  },
  async getMyVacations (params) {
    return await axios.get(`${url}/vacation/getmyvacations`, {params: params})
  },
  async getColVacations (params) {
    return await axios.get(`${url}/vacation/getcolvacations`, {params: params})
  }
}
