import axios from 'axios'
import {url} from './'

export default {
  async getcomponent (params) {
    return await axios.get(`${url}/ticket/getcomponent?tickettype=${params}`)
  },
  async getmembers (params) {
    return await axios.get(`${url}/ticket/getmembers`)
  },
  async getspecification (params) {
    return await axios.get(`${url}/ticket/getspecification?id=${params}`)
  },
  async uploadfile (params) {
    return await axios.post(`${url}/ticket/uploadfile`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async submit (params) {
    return await axios.post(`${url}/ticket/submit`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async getTickets (params) {
    return await axios.get(`${url}/ticket/gettickets`, {params: params})
  },
  async updateStatus (params) {
    return await axios.get(`${url}/ticket/updatestatus`, {params: params})
  },
  async sendIncidente (params) {
    return await axios.post(`${url}/ticket/sendincidente`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  }
}
