import axios from 'axios'
import {url} from './'

export default {
  async signin (params) {
    return await axios.post(`${url}/auth/login`, params)
  },
  async signinGoogle (params) {
    return await axios.post(`${url}/auth/login/google`, params)
  },
  async signout (params) {
    return await axios.post(`${url}/auth/logout`, params)
  },
  async forgotPassword (params) {
    return await axios.post(`${url}/auth/password/reset`, params)
  },
  async forgotPasswordReset (params) {
    return await axios.post(`${url}/auth/forgotPasswordReset/`, params)
  },
  async changePassword (params) {
    return await axios.post(`${url}/auth/changePassword`, params)
  },
  async signinMicrosoft (params) {
    return await axios.post(`${url}/auth/login/microsoft`, params)
  },
  async signinMicrosoftData (params) {
    return await axios.post(`${url}/auth/login/gettoken`, params)
  }
}
