import axios from 'axios'
import {url} from './'

export default {
  async fetchSelect (params) {
    return await axios.get(`${url}/headquarters/fetchSelect`, {params: params})
  },
  async fetchSelectGroup (params) {
    return await axios.get(`${url}/headquarters/fetchGroup`, {params: params})
  }
}
