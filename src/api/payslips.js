import axios from 'axios'
import {url} from './'

export default {
  async uploadZip (params) {
    return await axios.post(`${url}/boletas/uploadZip`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async listFile (params) {
    return await axios.post(`${url}/boletas/listFile`, params)
  }
}
