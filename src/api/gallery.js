import axios from 'axios'
import {url} from './'

export default {
  async submitGallery (params) {
    return await axios.post(`${url}/gallery/submitgallery`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async addGallery (params) {
    return await axios.post(`${url}/gallery/addgallery`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async getGalleries (params) {
    return await axios.get(`${url}/gallery/getgalleries`, {params: params})
  },
  async getGalleryDetail (params) {
    return await axios.get(`${url}/gallery/getgallerydetail`, {params: params})
  },
  async deleteAlbum (params) {
    return await axios.get(`${url}/gallery/deletealbum`, {params: params})
  },
  async deleteGallery (params) {
    return await axios.get(`${url}/gallery/deletegallery`, {params: params})
  }
}
