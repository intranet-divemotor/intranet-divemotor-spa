import axios from 'axios'
import {url} from './'

export default {
  async fetchSelect (params) {
    return await axios.get(`${url}/region/fetchSelect`, {params: params})
  }
}
