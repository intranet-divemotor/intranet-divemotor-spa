import axios from 'axios'
import {url} from './'

export default {
  async fetchContact (params) {
    return await axios.post(`${url}/contact`, params)
  },
  async newContact (params) {
    return await axios.post(`${url}/contact/save`, params)
  },
  async editContact (params) {
    return await axios.post(`${url}/contact/edit`, params)
  },
  async updateContact (params) {
    return await axios.post(`${url}/contact/update`, params)
  },
  async deleteContact (params) {
    return await axios.post(`${url}/contact/delete`, params)
  },
  async myContact (params) {
    return await axios.get(`${url}/contact/my`, params)
  }
}
