import axios from 'axios'
import {muraUrl, muraCrConf, muraDefConf, url} from './index'

const muraConf = process.env.MURA_SITE === 'default' ? muraDefConf : muraCrConf
const site = process.env.MURA_SITE

export default {
  async fetchSearch (params) {
    const dataInfo = []
    if (!params.parentid) params.parentid = muraConf.noticeContentId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    const noticias = await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})

    params.parentid = muraConf.communiqueId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var strParams = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    const comunicados = await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${strParams}`}})
    noticias.data.data.items.forEach((data) => {
      data.typeInfo = 1
      dataInfo.push(data)
    })
    comunicados.data.data.items.forEach((data) => {
      data.typeInfo = 2
      dataInfo.push(data)
    })
    return dataInfo
  },
  async fetchCurrencies () {
    return await axios.get(`${url}/admin/event/update-currencies`)
  },
  async fetchCommunicateds (params) {
    if (!params.parentid) params.parentid = muraConf.communiqueId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchNotices (params) {
    if (!params.parentid) params.parentid = muraConf.noticeContentId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchInfo (params) {
    if (!params.parentid) params.parentid = muraConf.infoContentId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchPopup () {
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?id=61D31AD5-313A-49ED-8DD9A1DBD06A85AD&filename=ventana-emergente`}})
  },
  async fetchHealthBenefits (params) {
    if (!params.parentid) params.parentid = muraConf.healthId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchEducationBenefits (params) {
    if (!params.parentid) params.parentid = muraConf.educationId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchExclusiveBenefits (params) {
    if (!params.parentid) params.parentid = muraConf.exclusiveId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchLawBenefits (params) {
    if (!params.parentid) params.parentid = muraConf.lawId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchDocs (params) {
    if (!params.parentid) params.parentid = muraConf.docsId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchDocsDesa (params) {
    if (!params.parentid) params.parentid = muraConf.docsDesaId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchDocsAutos (params) {
    if (!params.parentid) params.parentid = muraConf.docsAutosId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchDocsViajes (params) {
    if (!params.parentid) params.parentid = muraConf.docsViajesId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchDocsSeguros (params) {
    if (!params.parentid) params.parentid = muraConf.docsSegurosId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchDocsLegal (params) {
    if (!params.parentid) params.parentid = muraConf.docsLegalId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchPage (params) {
    if (!params.parentid) params.parentid = muraConf.noticeContentId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchCrumbs (params) {
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchDetail (params) {
    params.sortdirection = 'asc'
    params.sortby = 'ordeno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchIndicators (params) {
    if (!params.parentid) params.parentid = muraConf.indicators
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },
  async fetchBanners (params) {
    return await axios.get(`${url}/admin/link/fetch-banner`, {params: params})
  },
  async fetchDocsSalud (params) {
    if (!params.parentid) params.parentid = muraConf.docsSaludId
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  },

  async fetchManualGuia (params) {
    if (!params.parentid) params.parentid = 'E61B927F-521D-4596-9FB8CB34D87FD4FE'
    params.sortdirection = 'asc'
    params.sortby = 'orderno'
    params.itemsperpage = 100
    var str = Object.keys(params).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
    return await axios.get(`${url}/admin/event/fecthInMura`, {params: {uri: `${muraUrl}/_api/json/v1/${site}/content/?${str}`}})
  }

}
