import axios from 'axios'
import {url} from './'

export default {
  async fetch (params) {
    return await axios.get(`${url}/admin/modules/fetch`, {params: params})
  },
  async submit (params) {
    params.system = JSON.stringify(params.system)
    return await axios.post(`${url}/admin/modules/submit`, params)
  },
  async delete (params) {
    return await axios.post(`${url}/admin/modules/delete`, params)
  },
  async fetchRole () {
    return await axios.get(`${url}/admin/modules/fetchFavorite`)
  },
  async submitRole (params) {
    return await axios.post(`${url}/link/submitFavorite`, params)
  },
  async deleteRole (params) {
    return await axios.post(`${url}/link/deleteFavorite`, params)
  },
  async fetchMyModules () {
    return await axios.get(`${url}/modules/fetchMyModules`)
  }
}
