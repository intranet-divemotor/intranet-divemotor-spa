import axios from 'axios'
import {url} from './'

export default {
  async getSuggests (params) {
    return await axios.get(`${url}/suggests/getsuggests`, params)
  },
  async submitSuggest (params) {
    return await axios.post(`${url}/suggests/submitsuggests`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  }
}
