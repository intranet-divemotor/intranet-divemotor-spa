import axios from 'axios'
import {url} from './'

export default {
  async fetchLinks (params) {
    return await axios.get(`${url}/admin/link/fetch`, {params: params})
  },
  async submitLink (params) {
    return await axios.post(`${url}/admin/link/submit`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async fetchMyLinks (params) {
    return await axios.get(`${url}/link/fetchMyLinks`, {params: params})
  },
  async deleteLink (params) {
    return await axios.post(`${url}/admin/link/delete`, params)
  },
  async fetchFavorite () {
    return await axios.get(`${url}/link/fetchFavorite`)
  },
  async submitFavorite (params) {
    return await axios.post(`${url}/link/submitFavorite`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async deleteFavorite (params) {
    return await axios.post(`${url}/link/deleteFavorite`, params)
  },
  async uploadImage (params) {
    return await axios.post(`${url}/admin/link/uploadImage`, params)
  }
}
