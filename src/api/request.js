import axios from 'axios'
import {url} from './'

export default {
  /* MEDICAL REQUEST */
  async submitMedical (params) {
    return await axios.post(`${url}/request/medical/submit`, params)
  },
  async getMedicalRequest (params) {
    return await axios.get(`${url}/request/medical/getmedicalrequest`, params)
  },
  async deleteRequest (params) {
    return await axios.post(`${url}/request/medical/deleterequest`, params)
  },
  /* MEDICINE REQUEST */
  async getMedicineRequest (params) {
    return await axios.get(`${url}/request/medicine/getmedicinerequest`, {params: params})
  },
  async submitMedicine (params) {
    return await axios.post(`${url}/request/medicine/submit`, params)
  },
  /* CONSTANCE REQUEST */
  async submitConstance (params) {
    return await axios.post(`${url}/request/constance/submit`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async getConstanceRequest (params) {
    return await axios.get(`${url}/request/constance/getconstancerequest`, {params: params})
  },
  async getTypeRequest (params) {
    return await axios.get(`${url}/request/constance/gettyperequest`, {params: params})
  },
  async updateConstanceRequest (params) {
    return await axios.get(`${url}/request/constance/updateconstancestatus`, {params: params})
  },
  async setNoteConstance (params) {
    return await axios.post(`${url}/request/constance/setnoteconstance`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  /* ROOM REQUEST */
  async submitRoom (params) {
    return await axios.post(`${url}/request/room/submit`, params)
  },
  async getRoomRequest (params) {
    return await axios.get(`${url}/request/room/getroomrequest`, {params: params})
  },
  async updateStatusRequest (params) {
    return await axios.get(`${url}/request/room/updatestatusrequest`, {params: params})
  },
  /* ADMIN TYPE REQUESTS */
  async getAllTypeRequest (params) {
    return await axios.get(`${url}/request/type/getalltyperequest`, {params: params})
  },
  async submitTypeRequest (params) {
    return await axios.post(`${url}/request/type/submittyperequest`, params)
  },
  async updateTypeRequest (params) {
    return await axios.post(`${url}/request/type/updatetyperequest`, params)
  },
  async deleteTypeRequest (params) {
    return await axios.get(`${url}/request/type/deletetyperequest`, {params: params})
  }
}
