import axios from 'axios'
import {url} from './'

export default {
  async fetchEvents (params) {
    return await axios.get(`${url}/admin/event/fetch`, {params: params})
  },
  async submitEvent (params) {
    return await axios.post(`${url}/admin/event/submit`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async fetchEventsForAll (params) {
    return await axios.get(`${url}/admin/event/fetchForAll`, {params: params})
  },
  async fetchDetail (params) {
    return await axios.get(`${url}/admin/event/fetchDetail`, {params: params})
  },
  async deleteEvent (params) {
    return await axios.post(`${url}/admin/event/deleteEvent`, params)
  },
  async confirmEvent (params) {
    return await axios.post(`${url}/admin/event/confirmEvent`, params)
  }
}
