import axios from 'axios'
import {url} from './'

export default {
  async importExcelIndicator (params) {
    return await axios.post(`${url}/indicator/submit`, params)
  },
  async submitNps (params) {
    return await axios.post(`${url}/indicator/submitnps`, params)
  },
  async submitVoc (params) {
    return await axios.post(`${url}/indicator/submitvoc`, params)
  },
  async getIndicators (params) {
    return await axios.get(`${url}/indicator/getindicators`, params)
  },
  async removeIndicators (params) {
    return await axios.get(`${url}/indicator/removeindicators`, {params: params})
  },
  async toggleIndicators (params) {
    return await axios.get(`${url}/indicator/toggleindicators`, {params: params})
  },
  async getIndicatorWidget (params) {
    return await axios.get(`${url}/indicator/getindicatorwidget`, {params: params})
  }
}
