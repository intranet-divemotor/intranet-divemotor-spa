import axios from 'axios'
import {url} from './'

export default {
  async fetch (params) {
    return await axios.get(`${url}/admin/profile/fetch`, {params: params})
  },
  async submit (params) {
    return await axios.post(`${url}/admin/profile/submit`, params)
  },
  async delete (params) {
    return await axios.post(`${url}/admin/profile/delete`, params)
  }
}
