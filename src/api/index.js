// import axios from 'axios'
import firebase from 'firebase'

const fbConfPROD = {
  apiKey: process.env.apiKey,
  authDomain: process.env.authDomain,
  databaseURL: process.env.databaseURL,
  projectId: process.env.projectId,
  storageBucket: process.env.storageBucket,
  messagingSenderId: process.env.messagingSenderId
}
const fbConfDev = {
  apiKey: 'AIzaSyBLX5rvVGKC0tcEsoeFqgWKAyAWQtA4Fyk',
  authDomain: 'orusjobs.firebaseapp.com',
  databaseURL: 'https://orusjobs.firebaseio.com',
  projectId: 'orusjobs',
  storageBucket: 'orusjobs.appspot.com',
  messagingSenderId: '690907869500'
}
// Firebase config
if (firebase.apps.length === 0 && process.env.FIREBASE === 'PROD') {
  firebase.database.enableLogging(false)
  firebase.initializeApp(fbConfPROD)
} else {
  firebase.initializeApp(fbConfDev)
}

// Google Api
export const gapi = window.gapi
export const googleApikey = 'AIzaSyDFqfE8EVBBedsxxvMb8Dq4di5sJiSeMQA'
export const googleClientId = '533372092903-hvqt7ho07i7s7kbctr9g83f6rioudrvv.apps.googleusercontent.com'
export const googleScopes = [
  'https://www.googleapis.com/auth/gmail.readonly',
  'https://www.googleapis.com/auth/gmail.send',
  'https://www.googleapis.com/auth/userinfo.profile',
  'https://www.googleapis.com/auth/userinfo.email',
  'https://www.googleapis.com/auth/drive',
  'profile'
]

// Default ApiRestClient
export const url = process.env.API_URL
// Default Path
export const baseUrl = process.env.BASE_URL
// Database module of firebase
export const fbDataBase = firebase.database()
// Default mura cms
export const muraUrl = process.env.MURA
// Default mura configuration
export const muraDefConf = {
  site: process.env.MURA_SITE,
  noticeContentId: process.env.noticeContentId,
  infoContentId: process.env.infoContentId,
  communiqueId: process.env.communiqueId,
  healthId: process.env.healthId,
  educationId: process.env.educationId,
  exclusiveId: process.env.exclusiveId,
  lawId: process.env.lawId,
  docsId: process.env.docsId,
  docsDesaId: process.env.docsDesaId,
  docsAutosId: process.env.docsAutosId,
  docsViajesId: process.env.docsViajesId,
  docsSegurosId: process.env.docsSegurosId,
  docsLegalId: process.env.docsLegalId,
  indicators: process.env.indicators,
  docsSaludId: process.env.docsSaludId
}
// Default mura cr configuration
export const muraCrConf = {
  site: process.env.MURA_SITE,
  noticeContentId: process.env.noticeContentId,
  infoContentId: process.env.infoContentId,
  communiqueId: process.env.communiqueId,
  healthId: process.env.healthId,
  educationId: process.env.educationId,
  exclusiveId: process.env.exclusiveId,
  lawId: process.env.lawId,
  docsId: process.env.docsId,
  docsDesaId: process.env.docsDesaId,
  docsAutosId: process.env.docsAutosId,
  docsViajesId: process.env.docsViajesId,
  docsSegurosId: process.env.docsSegurosId,
  docsLegalId: process.env.docsLegalId,
  indicators: process.env.indicators,
  docsSaludId: process.env.docsSaludId
}

export const xmlHttp = (type, url) => {
  return new Promise((resolve, reject) => {
    var xmlHttp = new XMLHttpRequest()
    xmlHttp.open(type, url, true)
    xmlHttp.onload = () => {
      if (xmlHttp.readyState === 4) {
        if (xmlHttp.status === 200) {
          resolve(xmlHttp.responseText)
        } else {
          console.error(xmlHttp.statusText)
        }
      }
    }
    xmlHttp.send(null)
  })
}

export const xmlHttpDownloadFile = (type, url, headers, token) => {
  return new Promise((resolve, reject) => {
    var xmlHttp = new XMLHttpRequest()
    xmlHttp.open(type, url, true)
    xmlHttp.responseType = 'blob'
    if (headers) xmlHttp.setRequestHeader('Authorization', `Bearer ${token}`)
    xmlHttp.onreadystatechange = () => {
      if (xmlHttp.readyState === 4) {
        resolve(xmlHttp.response)
      }
    }
    xmlHttp.send(null)
  })
}

export const fullPath = process.env.FULL_PATH
