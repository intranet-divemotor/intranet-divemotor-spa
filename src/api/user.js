import axios from 'axios'
import {url} from './'

export default {
  async fetchUsers (params) {
    return await axios.post(`${url}/user/all`, params)
  },
  async uploadExcelUsers (params) {
    return await axios.post(`${url}/user/loadexcel`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async updateUsers (params) {
    return await axios.post(`${url}/user/update`, params)
  },
  async fetchRole (params) {
    return await axios.post(`${url}/user/roles`, params)
  },
  async fetchStatus (params) {
    return await axios.post(`${url}/user/status`, params)
  },
  async fetchPayslips (params) {
    return await axios.post(`${url}/boletas/list/file`, params)
  },
  async getActivities (params) {
    return await axios.get(`${url}/user/activities`, {params})
  },
  async deleteFile (params) {
    return await axios.post(`${url}/boletas/delete`, params)
  },
  async signout (params) {
    return await axios.post(`${url}/auth/logout`, params)
  },
  async forgotPassword (params) {
    return await axios.post(`${url}/auth/password/reset`, params)
  },
  async getDataProfile (params) {
    return await axios.get(`${url}/user/dataprofile`, params)
  },
  async submitChange (params) {
    return await axios.post(`${url}/user/submitchange`, params)
  },
  async submitEmail (params) {
    return await axios.post(`${url}/user/submitemail`, params)
  },
  async getMyChanges (params) {
    return await axios.get(`${url}/user/mychanges`, {params: params})
  },
  async getChangesRequest (params) {
    return await axios.get(`${url}/user/changesrequest`, {params: params})
  },
  async getEmails (params) {
    return await axios.get(`${url}/user/emails`, {params: params})
  },
  async updateChange (params) {
    return await axios.get(`${url}/user/updatechange`, {params: params})
  },
  async detailChange (params) {
    return await axios.get(`${url}/user/detailchange`, {params: params})
  }
}
