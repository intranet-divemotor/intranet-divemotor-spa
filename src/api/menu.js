/**
 * Created by Sergio on 30-06-2017.
 */
import axios from 'axios'
import {url} from './'

export default {
  async gethistorymenu (params) {
    return await axios.get(`${url}/menu/gethistorymenu`, {params: params})
  },
  async getmenusede (param) {
    return await axios.get(`${url}/menu/getmenusede?idmenu=${param}`)
  },
  async getsuggest (params) {
    return await axios.get(`${url}/menu/getsuggestions`, {params: params})
  },
  async getsedes (params) {
    return await axios.get(`${url}/menu/getsedes`, {params: params})
  },
  async getreservation (params) {
    return await axios.get(`${url}/menu/getreservation`, {params: params})
  },
  async updateStatusreservation (params) {
    return await axios.get(`${url}/menu/updatestatusreservation`, {params: params})
  },

  async getMenuDay (params) {
    return await axios.get(`${url}/menu/getmenuday`, {params: params})
  },
  async importExcel (params) {
    return await axios.post(`${url}/menu/importexcel`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async submitSuggest (params) {
    return await axios.post(`${url}/menu/submitsuggest`, params)
  },
  async submitPrescription (params) {
    return await axios.post(`${url}/menu/submitprescription`, params, {headers: {'Content-Type': 'multipart/form-data'}})
  },
  async deleteMenu (params) {
    return await axios.get(`${url}/menu/deletemenu`, {params: params})
  },
  async getCurrentMenu (params) {
    return await axios.get(`${url}/menu/getcurrentmenu`, {params: params})
  },
  async fetchSedesMenu (params) {
    return await axios.get(`${url}/menu/fetchsedes`)
  }
}
