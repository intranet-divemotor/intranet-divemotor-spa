import axios from 'axios'
import {url} from './'

export default {
  async fetchBirthday (params) {
    return await axios.get(`${url}/birthday/fetch`, {params: params})
  },
  async congratulated (params) {
    return await axios.post(`${url}/birthday/saveCongratulation`, params)
  }

}
