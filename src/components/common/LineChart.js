import { Line } from 'vue-chartjs'

export default Line.extend({
  props: ['data', 'options'],
  mounted () {
    this.renderChart(
      {
        labels: ['January', 'February', 'March'],
        datasets: [
          {
            label: 'Metricas',
            backgroundColor: '#64B5F6',
            data: [40, 20, 12]
          }
        ]
      },
      {
        title: {
          display: false,
          text: ''
        },
        legend: {
          display: false,
          labels: {
            fontColor: 'rgb(255, 99, 132)'
          }
        }
      })
  }
})
