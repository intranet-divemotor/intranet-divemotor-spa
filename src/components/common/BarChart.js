import { Bar } from 'vue-chartjs'

export default Bar.extend({
  mounted () {
    this.renderChart(
      {
        labels: ['J', 'F', 'M'],
        datasets: [
          {
            label: 'Metricas',
            backgroundColor: '#f87979',
            data: [40, 20, 12]
          }
        ]
      },
      {
        title: {
          display: false,
          text: ''
        },
        legend: {
          display: false,
          labels: {
            fontColor: 'rgb(255, 99, 132)'
          }
        }
      })
  }
})
