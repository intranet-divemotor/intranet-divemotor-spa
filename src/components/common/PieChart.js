import {Doughnut} from 'vue-chartjs'

export default Doughnut.extend({
  props: ['data', 'options'],
  mounted () {
    this.renderChart(this.data,
      {
        responsive: true,
        title: {
          display: false,
          text: ''
        },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            boxWidth: 12
          }
        },
        tooltips: {
          callbacks: {
            label (a, b) {
              return ' ' + b.labels[a.index] + ': ' + b.datasets[0].data[a.index] + '%'
            }
          }
        }
      })
  },
  watch: {
    'data': {
      handler: function (newData, oldData) {
        let chart = this._chart

        let newDataLabels = newData.datasets.map((dataset) => {
          return dataset.label
        })

        let oldDataLabels = oldData.datasets.map((dataset) => {
          return dataset.label
        })
        if (JSON.stringify(newDataLabels) === JSON.stringify(oldDataLabels)) {
          newData.datasets.forEach(function (dataset, i) {
            chart.data.datasets[i].data = dataset.data
          })
          chart.data.labels = newData.labels
          chart.update()
        } else {
          this.renderChart(this.data, this.options)
        }
      },
      deep: true
    }
  }
})
