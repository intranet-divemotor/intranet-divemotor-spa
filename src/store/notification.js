import { EventEmitter } from 'events'
import Push from 'push.js'
import {fbDataBase, baseUrl} from '../api/index'
import defaultImage from '../assets/images/notification/default-notifications.png'
import happy from '../assets/images/happy.jpg'
import PayslipsImage from '../assets/images/payslips.png'
import router from '../router'
const store = new EventEmitter()

export default store

store.state = {
  items: []
}

store.actions = {
  sendNotification: ({commit}, not) => {
    return new Promise((resolve, reject) => {
      var key = fbDataBase.ref().child('notifications').push().key
      var notification = {}
      notification[`notifications/${key}`] = {
        key: key,
        title: not.title,
        body: not.body,
        action: not.action,
        area: not.area,
        pathIcon: not.pathIcon,
        dateInsert: new Date().getTime()
      }
      fbDataBase.ref().update(notification)
      resolve()
    })
  },
  sendUserNotification: ({commit}, not) => {
    return new Promise((resolve, reject) => {
      var key = fbDataBase.ref().child('users').push().key
      var notification = {}
      var params = {...not, key, dateInsert: new Date().getTime()}
      notification[`users/${not.idUser}/notifications/${key}`] = params
      fbDataBase.ref().update(notification)
      resolve()
    })
  },
  // Amigo jose esto tiene que ser generico no testos quemados para eso usa los parametros y capturas la data que vas a enviar, todos los metodos estan asi
  // no se por que este esta todo quemado
  sendNotificationForUser: ({commit}, not) => {
    not.forEach((data) => {
      return new Promise((resolve, reject) => {
        var key = fbDataBase.ref().child('users').push().key
        var notification = {}
        notification[`users/${data.idUser}/notifications/${key}`] = {
          key: key,
          title: `${data.typeFile} de pago disponible`,
          body: 'Ingresa a tu perfil de Divenet para visualizar',
          action: '/user/profile',
          pathIcon: PayslipsImage,
          dateInsert: new Date().getTime()
        }
        fbDataBase.ref().update(notification)
        store.emit('notification-value')
        resolve()
      })
    })
  },
  fetchUserNotifications: ({commit}, params) => {
    fbDataBase.ref(`users/${params.user.idUser}/notifications`).orderByChild('dateInsert').once('value', snapUser => {
      var keys = Object.keys(snapUser.val() || {})
      var lastIdInSnapshot = keys[keys.length - 1]
      let nots = []
      if (snapUser.val()) {
        snapUser.forEach((data) => {
          let temp = data.val()
          temp.key = data.key
          if (!temp.read) {
            nots.push(temp)
          }
          if (data.key === lastIdInSnapshot && !temp.read && params.push) {
            Push.create(`${temp.title}`, {
              body: (temp.commId ? '' : temp.body),
              icon: (temp.avatar ? temp.avatar === 'none' ? happy : temp.avatar : temp.pathIcon ? temp.commId ? temp.pathIcon : `${baseUrl}/${temp.pathIcon}` : defaultImage),
              timeout: 6000,
              onClick: () => {
                router.push(temp.action)
              }
            })
          }
        })
      }
      commit('SET_NOTIFICATIONS', nots.reverse())
    })
  },
  readNotification: ({commit}, not) => {
    return fbDataBase.ref(`users/${not.userId}/notifications/${not.not.key}`).set(not.not)
  },
  sendEventNotification: ({commit}, e) => {
    fbDataBase.ref('notifications').orderByChild('eventId').equalTo(e.id).once('value', (snapshot) => {
      var notification = {}
      if (snapshot.val()) {
        var keys = Object.keys(snapshot.val() || {})
        keys.map(k => {
          fbDataBase.ref(`notifications/${k}`)
            .update({
              title: e.title,
              body: e.summary,
              action: `/event/${e.id}`,
              pathIcon: `/files/event/${e.coverPhoto}`,
              dateInsert: new Date().getTime()
            })
        })
      } else {
        var key = fbDataBase.ref().child('notifications').push().key
        notification[`notifications/${key}`] = {eventId: e.id, title: e.title, body: e.summary, action: `/event/${e.id}`, pathIcon: `/files/event/${e.coverPhoto}`}
        fbDataBase.ref().update(notification)
      }
    })
  },
  sendCommunicateNot: ({commit}, arr) => {
    arr.reverse().map(e => {
      fbDataBase.ref('notifications').orderByChild('commId').equalTo(e.id).once('value', (snapshot) => {
        if (!snapshot.val()) {
          var key = fbDataBase.ref().child('notifications').push().key
          fbDataBase.ref(`notifications/${key}`).set({commId: e.id, title: e.title, body: e.summary, action: `/general/communicated/${e.id}`, pathIcon: `${e.images.small}`, dateInsert: new Date().getTime()})
        }
      })
    })
  },
  removeEventNotification: ({commit}, id) => {
    fbDataBase.ref('notifications').orderByChild('eventId').equalTo(id).once('value', (snapshot) => {
      if (snapshot.val()) {
        var keys = Object.keys(snapshot.val() || {})
        keys.map(k => {
          fbDataBase.ref('notifications').child(k).remove()
          fbDataBase.ref('notifications').child('notifications').remove()
          fbDataBase.ref('users').once('value', user => {
            if (user.val()) {
              var userKeys = Object.keys(snapshot.val() || {})
              userKeys.map(uk => {
                fbDataBase.ref(`users/${uk}/notifications`).child(k).remove()
                fbDataBase.ref(`users/${uk}/notifications`).child('notifications').remove()
              })
            }
          })
        })
      }
    })
  },
  deleteNode: ({commit}, type) => {
    fbDataBase.ref(`${type === 1 ? 'users' : 'notifications'}`).remove()
  }
}

store.mutations = {
  'SET_NOTIFICATIONS': (state, data) => {
    state.items = data
  }
}

store.fetchNotifications = (user) => {
  fbDataBase.ref('notifications').orderByChild('dateInsert').on('value', snap => {
    if (snap.val()) {
      snap.forEach((data) => {
        fbDataBase.ref(`users/${user.idUser}/notifications/${data.key}`).once('value', userNot => {
          let row = userNot.val()
          if (!row) {
            row = data.val()
            row.read = false
            row.dateInsert = new Date().getTime()
            if (row.area) {
              if (row.area === 'all' || row.area.toString() === user.codArea.toString()) fbDataBase.ref(`users/${user.idUser}/notifications/${userNot.key}`).set(row)
            } else {
              fbDataBase.ref(`users/${user.idUser}/notifications/${userNot.key}`).set(row)
            }
          }
          store.emit('notification-value')
        })
      })
    }
  })

  fbDataBase.ref(`users/${user.idUser}/notifications`).on('child_added', snap => {
    store.emit('notification-value')
  })
}
