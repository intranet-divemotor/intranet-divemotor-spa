import { EventEmitter } from 'events'
import api from '../api/module'
const store = new EventEmitter()

export default store

store.actions = {
  fetchModules: ({commit}, params) => {
    return api.fetch(params)
  },
  submitModules: ({commit}, params) => {
    return api.submit(params)
  },
  deleteModules: ({commit}, params) => {
    return api.delete(params)
  },
  fetchMyModules: ({commit}, params) => {
    return api.fetchMyModules(params)
  }
}
