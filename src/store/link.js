import { EventEmitter } from 'events'
// import { Promise } from 'es6-promise'
// import isEmpty from 'lodash/isEmpty'
import api from '../api/link'
const store = new EventEmitter()

export default store

store.actions = {
  // Fetch Event by id, search query and all
  fetchLink: ({commit}, params) => {
    return api.fetchLinks(params)
  },
  deleteLink: ({commit}, id) => {
    return api.deleteLink({id})
  },
  submitLink: ({commit}, event) => {
    let keys = Object.keys(event)
    let formData = new FormData()
    keys.map(k => {
      if (k === 'files') event[k].map(f => { formData.append('files[]', f.file) })
      else formData.append(k, (typeof event[k] === 'object' ? JSON.stringify(event[k]) : event[k]))
    })
    return api.submitLink(formData)
  },
  fetchMyLinks: ({commit}, params) => {
    return api.fetchMyLinks(params)
  },
  fetchFavorite: ({commit}) => {
    return api.fetchFavorite()
  },
  submitFavorite: ({commit}, params) => {
    let keys = Object.keys(params)
    let formData = new FormData()
    keys.map(k => {
      if (k === 'files') params[k].map(f => { formData.append('files[]', f.file) })
      else formData.append(k, (typeof params[k] === 'object' ? JSON.stringify(params[k]) : params[k]))
    })
    return api.submitFavorite(formData)
  },
  deleteFavorite: ({commit}, params) => {
    return api.deleteFavorite(params)
  },
  uploadImage: ({commit}, params) => {
    let keys = Object.keys(params)
    let formData = new FormData()
    keys.map(k => {
      if (k === 'files') params[k].map(f => { formData.append('files[]', f.file) })
      else formData.append(k, (typeof params[k] === 'object' ? JSON.stringify(params[k]) : params[k]))
    })
    return api.uploadImage(formData)
  }
}

