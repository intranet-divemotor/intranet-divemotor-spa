import { EventEmitter } from 'events'
import {gapi} from '../api'
const store = new EventEmitter()

export default store

store.state = {
  mails: []
}

store.actions = {
  // Fetch User by id, search query and all
  fetchInbox: ({commit}, params) => {
    let iData = []
    return new Promise((resolve, reject) => {
      // Get list
      const getList = new Promise((resolve, reject) => {
        gapi.client.gmail.users.messages.list({
          'userId': 'me',
//          'labelIds': 'INBOX',
          'q': 'in:inbox is:unread -category:(promotions OR social OR updates)',
          'includeSpamTrash': false
        }).execute(res => { resolve(res) })
      })

      // Get message
      const message = new Promise((resolve, reject) => {
        getList.then(res => {
          res.messages.map(r => {
            gapi.client.gmail.users.messages.get({
              'userId': 'me',
              'id': r.id
            }).execute((mail) => {
              let msg = {}
              // Get headers
              mail.payload.headers.map(f => {
                switch (f.name.toLowerCase()) {
                  case 'from':
                    msg.from = f.value
                    break
                  case 'subject':
                    msg.subject = f.value
                    break
                  case 'date':
                    msg.date = f.value
                    break
                  case 'reply-to':
                    msg.replyTo = f.value
                    break
                }
              })

              // Set messageId
              msg.messageId = r.id

              // Get body
              if (mail.payload.parts) {
                mail.payload.parts.map(b => {
                  if (b.mimeType === 'text/html') msg.body = decodeURIComponent(escape(window.atob(b.body.data.replace(/-/g, '+').replace(/_/g, '/').replace(/\s/g, ''))))
                })
              }
              iData.push(msg)
              resolve(iData)
            })
          })
        })
      })

      message.then(mails => {
        commit('SET_MAILS', mails)
        resolve(mails)
      })
    })
  }
}

store.mutations = {
  'SET_MAILS' (state, mails) {
    state.mails = mails
  }
}
