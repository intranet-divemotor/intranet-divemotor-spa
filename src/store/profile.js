import { EventEmitter } from 'events'
import api from '../api/profile'
const store = new EventEmitter()

export default store

store.actions = {
  fetchProfiles: ({commit}, params) => {
    return api.fetch(params)
  },
  submitProfile: ({commit}, params) => {
    params.modules = JSON.stringify(params.modules)
    return api.submit(params)
  },
  deleteProfile: ({commit}, params) => {
    return api.delete(params)
  }
}
