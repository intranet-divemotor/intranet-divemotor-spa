import { EventEmitter } from 'events'
import api from '../api/area'
const store = new EventEmitter()
export default store

store.actions = {
  // Fetch Event by id, search query and all
  fetchAreaSelect: ({commit}, params) => {
    return api.fetchSelect(params)
  }
}
