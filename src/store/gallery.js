import { EventEmitter } from 'events'
import api from '../api/gallery'
const store = new EventEmitter()

export default store

store.actions = {
  submitGallery: ({commit}, params) => {
    return api.submitGallery(params)
  },
  addGallery: ({commit}, params) => {
    return api.addGallery(params)
  },
  getGalleries: ({commit}, params) => {
    return api.getGalleries(params)
  },
  getGalleryDetail: ({commit}, params) => {
    return api.getGalleryDetail(params)
  },
  deleteAlbum: ({commit}, params) => {
    return api.deleteAlbum(params)
  },
  deleteGallery: ({commit}, params) => {
    return api.deleteGallery(params)
  }
}
