import { EventEmitter } from 'events'
import api from '../api/birthday'
const store = new EventEmitter()
export default store

store.actions = {
  fetchBirthday: ({commit}, params) => {
    return api.fetchBirthday(params)
  },
  congratulated: ({commit}, params) => {
    return api.congratulated(params)
  }
}
