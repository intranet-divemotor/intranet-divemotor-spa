import { EventEmitter } from 'events'
// import { Promise } from 'es6-promise'
// import isEmpty from 'lodash/isEmpty'
import api from '../api/region'
const store = new EventEmitter()

export default store

store.actions = {
  // Fetch Event by id, search query and all
  fetchRegionSelect: ({commit}, params) => {
    return api.fetchSelect(params)
  }
}

store.mutations = {
}
