import { EventEmitter } from 'events'
import api from '../api/indicators'
const store = new EventEmitter()

export default store

store.actions = {
  importExcelIndicator: ({commit}, params) => {
    return api.importExcelIndicator(params)
  },
  submitNps: ({commit}, params) => {
    return api.submitNps(params)
  },
  submitVoc: ({commit}, params) => {
    return api.submitVoc(params)
  },
  getIndicators: ({commit}, params) => {
    return api.getIndicators(params)
  },
  removeIndicators: ({commit}, params) => {
    return api.removeIndicators(params)
  },
  toggleIndicators: ({commit}, params) => {
    return api.toggleIndicators(params)
  },
  getIndicatorWidget: ({commit}, params) => {
    return api.getIndicatorWidget(params)
  }
}
