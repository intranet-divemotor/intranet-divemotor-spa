import { EventEmitter } from 'events'
import api from '../api/vacation'
const store = new EventEmitter()

export default store

store.actions = {
  submitExcelVacation: ({commit}, params) => {
    return api.submitExcelVacation(params)
  },
  getVacations: ({commit}, params) => {
    return api.getVacations(params)
  },
  getMyVacations: ({commit}, params) => {
    return api.getMyVacations(params)
  },
  getColVacations: ({commit}, params) => {
    return api.getColVacations(params)
  }
}
