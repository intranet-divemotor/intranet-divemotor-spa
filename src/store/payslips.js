import { EventEmitter } from 'events'
// import { Promise } from 'es6-promise'
// import isEmpty from 'lodash/isEmpty'
import api from '../api/payslips'
const store = new EventEmitter()

export default store

store.actions = {
  // Fetch User by id, search query and all
  uploadZip: ({commit}, params) => {
    return api.uploadZip(params)
  },
  listFile: ({commit}, params) => {
    return api.listFile(params)
  }
}

store.mutations = {
}
