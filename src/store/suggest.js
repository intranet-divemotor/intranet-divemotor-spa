import { EventEmitter } from 'events'
import api from '../api/suggest'
const store = new EventEmitter()

export default store

store.actions = {
  getSuggests: ({commit}, params) => {
    return api.getSuggests(params)
  },
  submitSuggestGeneral: ({commit}, params) => {
    return api.submitSuggest(params)
  }
}
