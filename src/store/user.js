import { EventEmitter } from 'events'
// import { Promise } from 'es6-promise'
// import isEmpty from 'lodash/isEmpty'
import api from '../api/user'
const store = new EventEmitter()

export default store

store.actions = {
  // Fetch User by id, search query and all
  fetchUsers: ({commit}, params) => {
    return api.fetchUsers(params)
  },
  uploadExcelUsers: ({commit}, params) => {
    return api.uploadExcelUsers(params)
  },
  updateUsers: ({commit}, params) => {
    return api.updateUsers(params)
  },
  fetchRole: ({commit}, params) => {
    return api.fetchRole(params)
  },
  fetchStatus: ({commit}, params) => {
    return api.fetchStatus(params)
  },
  getActivities: ({commit}, params) => {
    return api.getActivities(params)
  },
  deleteUser: ({commit}, id) => {
  },
  fetchPayslips: ({commit}, params) => {
    return api.fetchPayslips(params)
  },
  deleteFile: ({commit}, params) => {
    return api.deleteFile(params)
  },
  getDataProfile: ({commit}, params) => {
    return api.getDataProfile(params)
  },
  submitChange: ({commit}, params) => {
    return api.submitChange(params)
  },
  submitEmail: ({commit}, params) => {
    return api.submitEmail(params)
  },
  getMyChanges: ({commit}, params) => {
    return api.getMyChanges(params)
  },
  getChangesRequest: ({commit}, params) => {
    return api.getChangesRequest(params)
  },
  getEmails: ({commit}, params) => {
    return api.getEmails(params)
  },
  updateChange: ({commit}, params) => {
    return api.updateChange(params)
  },
  detailChange: ({commit}, params) => {
    return api.detailChange(params)
  }
}

store.mutations = {
}
