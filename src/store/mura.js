import { EventEmitter } from 'events'
import api from '../api/mura'
const store = new EventEmitter()

export default store

store.actions = {
  fetchSearch: ({commit}, params) => {
    return api.fetchSearch(params)
  },
  fetchCurrencies: ({commit}, params) => {
    return api.fetchCurrencies(params)
  },
  fetchCommunicated: ({commit}, params) => {
    return api.fetchCommunicateds(params)
  },
  fetchNotices: ({commit}, params) => {
    return api.fetchNotices(params)
  },
  fetchPopup: ({commit}, params) => {
    return api.fetchPopup()
  },
  fetchInfo: ({commit}, params) => {
    return api.fetchInfo(params)
  },
  fetchHealthBenefits: ({commit}, params) => {
    return api.fetchHealthBenefits(params)
  },
  fetchEducationBenefits: ({commit}, params) => {
    return api.fetchEducationBenefits(params)
  },
  fetchExclusiveBenefits: ({commit}, params) => {
    return api.fetchExclusiveBenefits(params)
  },
  fetchLawBenefits: ({commit}, params) => {
    return api.fetchLawBenefits(params)
  },
  fetchDocs: ({commit}, params) => {
    return api.fetchDocs(params)
  },
  fetchDocsDesa: ({commit}, params) => {
    return api.fetchDocsDesa(params)
  },
  fetchDocsAutos: ({commit}, params) => {
    return api.fetchDocsAutos(params)
  },
  fetchDocsViajes: ({commit}, params) => {
    return api.fetchDocsViajes(params)
  },
  fetchDocsSeguros: ({commit}, params) => {
    return api.fetchDocsSeguros(params)
  },
  fetchDocsLegal: ({commit}, params) => {
    return api.fetchDocsLegal(params)
  },
  fetchManualGuia: ({commit}, params) => {
    return api.fetchManualGuia(params)
  },
  fetchPage: ({commit}, params) => {
    return api.fetchInfo(params)
  },
  fetchCrumbs: ({commit}, params) => {
    return api.fetchCrumbs(params)
  },
  fetchDetailMura: ({commit}, params) => {
    return api.fetchDetail(params)
  },
  fetchDocsSalud: ({commit}, params) => {
    return api.fetchDocsSalud(params)
  }
}
