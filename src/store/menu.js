/**
 * Created by Sergio on 30-06-2017.
 */
import { EventEmitter } from 'events'
import api from '../api/menu'
const store = new EventEmitter()

export default store

store.actions = {
  getHistoryMenu: ({commit}, params) => {
    return api.gethistorymenu(params)
  },
  getMenuSede: ({commit}, params) => {
    return api.getmenusede(params)
  },
  getSuggest: ({commit}, params) => {
    return api.getsuggest(params)
  },
  getSedes: ({commit}, params) => {
    return api.getsedes(params)
  },
  getReservation: ({commit}, params) => {
    return api.getreservation(params)
  },
  updateStatusReservation: ({commit}, params) => {
    return api.updateStatusreservation(params)
  },
  getMenuDay: ({commit}, params) => {
    return api.getMenuDay(params)
  },
  importExcel: ({commit}, params) => {
    return api.importExcel(params)
  },
  submitSuggest: ({commit}, params) => {
    return api.submitSuggest(params)
  },
  submitPrescription: ({commit}, params) => {
    return api.submitPrescription(params)
  },
  deleteMenu: ({commit}, params) => {
    return api.deleteMenu(params)
  },
  getCurrentMenu: ({commit}, params) => {
    return api.getCurrentMenu(params)
  },
  fetchSedesMenu: ({commit}, params) => {
    return api.fetchSedesMenu(params)
  }
}
