import { EventEmitter } from 'events'
import api from '../api/ticket'
const store = new EventEmitter()

export default store

store.actions = {
  getComponent: ({commit}, params) => {
    return api.getcomponent(params)
  },
  getSpecification: ({commit}, params) => {
    return api.getspecification(params)
  },
  getMembers: ({commit}, params) => {
    return api.getmembers(params)
  },
  uploadFile: ({commit}, params) => {
    return api.uploadfile(params)
  },
  submit: ({commit}, params) => {
    return api.submit(params)
  },
  getTickets: ({commit}, params) => {
    return api.getTickets(params)
  },
  updateStatus: ({commit}, params) => {
    return api.updateStatus(params)
  },
  sendIncidente: ({commit}, params) => {
    return api.sendIncidente(params)
  }
}
