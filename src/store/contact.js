import { EventEmitter } from 'events'
// import { Promise } from 'es6-promise'
// import isEmpty from 'lodash/isEmpty'
import api from '../api/contact'
const store = new EventEmitter()

export default store

store.actions = {
  // Fetch User by id, search query and all
  fetchContact: ({commit}, params) => {
    return api.fetchContact(params)
  },
  newContact: ({commit}, params) => {
    return api.newContact(params)
  },
  editContact: ({commit}, params) => {
    return api.editContact(params)
  },
  updateContact: ({commit}, params) => {
    return api.updateContact(params)
  },
  deleteContact: ({commit}, params) => {
    return api.deleteContact(params)
  },
  myContact: ({commit}, params) => {
    return api.myContact(params)
  }
}

