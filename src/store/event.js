import { EventEmitter } from 'events'
// import { Promise } from 'es6-promise'
// import isEmpty from 'lodash/isEmpty'
import api from '../api/event'
const store = new EventEmitter()

export default store

store.actions = {
  // Fetch Event by id, search query and all
  fetchEvents: ({commit}, params) => {
    return api.fetchEvents(params)
  },
  deleteEvent: ({commit}, id) => {
    return api.deleteEvent({id})
  },
  submitEvent: ({commit}, event) => {
    let keys = Object.keys(event)
    let formData = new FormData()
    keys.map(k => {
      if (k === 'files') event[k].map(f => { formData.append('files[]', f.file) })
      else formData.append(k, (typeof event[k] === 'object' ? JSON.stringify(event[k]) : event[k]))
    })
    return api.submitEvent(formData)
  },
  fetchEventsForAll: ({commit}, params) => {
    return api.fetchEventsForAll(params)
  },
  fetchDetail: ({commit}, params) => {
    return api.fetchDetail(params)
  },
  confirmEvent: ({commit}, params) => {
    return api.confirmEvent(params)
  }
}

