import { EventEmitter } from 'events'
// import { Promise } from 'es6-promise'
// import isEmpty from 'lodash/isEmpty'
import api from '../api/headquarters'
const store = new EventEmitter()

export default store

store.actions = {
  // Fetch Event by id, search query and all
  fetchHeadquartersSelect: ({commit}, params) => {
    return api.fetchSelect(params)
  },
  fetchHeadquartersGroup: ({commit}, params) => {
    return api.fetchSelectGroup(params)
  }
}

store.mutations = {
}
