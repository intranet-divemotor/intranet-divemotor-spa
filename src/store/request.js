import { EventEmitter } from 'events'
import api from '../api/request'
const store = new EventEmitter()

export default store

store.actions = {
  /* MEDICAL REQUEST */
  submitMedical: ({commit}, params) => {
    return api.submitMedical(params)
  },
  getMedicalRequest: ({commit}, params) => {
    return api.getMedicalRequest(params)
  },
  deleteRequest: ({commit}, params) => {
    return api.deleteRequest(params)
  },
  /* MEDICINE REQUEST */
  submitMedicine: ({commit}, params) => {
    return api.submitMedicine(params)
  },
  getMedicineRequest: ({commit}, params) => {
    return api.getMedicineRequest(params)
  },
  /* CONSTANCE REQUEST */
  submitConstance: ({commit}, params) => {
    return api.submitConstance(params)
  },
  getConstanceRequest: ({commit}, params) => {
    return api.getConstanceRequest(params)
  },
  getTypeRequest: ({commit}, params) => {
    return api.getTypeRequest(params)
  },
  updateConstanceRequest: ({commit}, params) => {
    return api.updateConstanceRequest(params)
  },
  setNoteConstance: ({commit}, params) => {
    return api.setNoteConstance(params)
  },
  /* ROOM REQUEST */
  submitRoom: ({commit}, params) => {
    return api.submitRoom(params)
  },
  getRoomRequest: ({commit}, params) => {
    return api.getRoomRequest(params)
  },
  updateStatusRequest: ({commit}, params) => {
    return api.updateStatusRequest(params)
  },
  /* ADMIN TYPE REQUESTS */
  getAllTypeRequest: ({commit}, params) => {
    return api.getAllTypeRequest(params)
  },
  submitTypeRequest: ({commit}, params) => {
    return api.submitTypeRequest(params)
  },
  updateTypeRequest: ({commit}, params) => {
    return api.updateTypeRequest(params)
  },
  deleteTypeRequest: ({commit}, params) => {
    return api.deleteTypeRequest(params)
  }
}
