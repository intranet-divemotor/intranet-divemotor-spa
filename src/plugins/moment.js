import moment from 'moment'
moment.locale('es')
export const dateFormat = (val, format) => {
  return moment(val).format((format || 'DD/MM/YYYY'))
}
