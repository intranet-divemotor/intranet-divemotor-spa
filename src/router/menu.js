import Layout from '../layouts/Layout.vue'
import Menu from '../pages/admin/Menu.vue'
import MenuCollaborator from '../pages/menu/Menu.vue'
import Reservation from '../pages/admin/Reservation.vue'

export default [
  {
    path: '/admin',
    component: Layout,
    children: [
      {
        path: 'menu',
        name: 'menuAdmin',
        component: Menu,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'reservation',
        name: 'reservation',
        component: Reservation,
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'menu',
        name: 'menu',
        component: MenuCollaborator,
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]
