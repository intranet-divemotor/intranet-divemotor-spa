import Layout from '../layouts/Layout.vue'
import Module from '../pages/admin/Module.vue'

export default [{
  path: '/admin',
  component: Layout,
  children: [
    {
      path: 'modules',
      name: 'modules',
      component: Module,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
