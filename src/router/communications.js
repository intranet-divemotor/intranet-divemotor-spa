import Index from '../pages/Index'
import Layout from '../layouts/Layout.vue'

export default [{
  path: '/comunications',
  component: Layout,
  children: [
    {
      path: '/comunications',
      name: 'comunications',
      component: Index,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
