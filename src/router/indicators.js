import Layout from '../layouts/Layout.vue'
import Indicator from '../pages/admin/Indicators.vue'

export default [
  {
    path: '/admin',
    component: Layout,
    children: [
      {
        path: 'indicators',
        name: 'adminIndicators',
        component: Indicator,
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]
