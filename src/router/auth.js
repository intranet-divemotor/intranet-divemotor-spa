import LayoutEmpty from '../layouts/LayoutEmpty.vue'
import Signin from '../pages/auth/Signin'
import ForgotPassword from '../pages/auth/ForgotPassword.vue'
import ForgotPasswordReset from '../pages/auth/ForgotPasswordReset.vue'
export default [{
  path: '/auth',
  component: LayoutEmpty,
  children: [
    {
      path: 'signin',
      name: 'signin',
      component: Signin
    },
    {
      path: 'forgotPassword',
      name: 'forgotPassword',
      component: ForgotPassword
    },
    {
      path: 'forgotPasswordReset',
      name: 'ForgotPasswordReset',
      component: ForgotPasswordReset
    }
  ]
}]
