import Layout from '../layouts/Layout.vue'
import Events from '../pages/admin/Events.vue'
import AllEvents from '../pages/event/AllEvents.vue'
import EventDetail from '../pages/event/EventDetail.vue'

export default [
  {
    path: '/admin',
    component: Layout,
    children: [
      {
        path: 'events',
        name: 'adminEvent',
        component: Events,
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/events',
    component: Layout,
    children: [
      {
        path: '/',
        name: 'AllEvent',
        component: AllEvents,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: '/event/:id',
        name: 'EventDetail',
        component: EventDetail,
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]
