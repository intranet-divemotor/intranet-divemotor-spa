import Layout from '../layouts/Layout.vue'
import Link from '../pages/admin/Link.vue'

export default [
  {
    path: '/admin',
    component: Layout,
    children: [
      {
        path: 'link',
        name: 'adminLink',
        component: Link,
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]
