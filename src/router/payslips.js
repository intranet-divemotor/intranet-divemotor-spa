import Layout from '../layouts/Layout.vue'
import Payslips from '../pages/admin/Payslips.vue'

export default [{
  path: '/admin',
  component: Layout,
  children: [
    {
      path: 'payslips',
      name: 'payslips',
      component: Payslips,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
