import Index from '../pages/Index'
import Layout from '../layouts/Layout.vue'

export default [{
  path: '/',
  component: Layout,
  children: [
    {
      path: '/',
      name: 'home',
      component: Index,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
