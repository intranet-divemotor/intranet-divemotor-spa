import Layout from '../layouts/Layout.vue'
import TicketAdmin from '../pages/admin/Ticket.vue'
import Ticket from '../pages/ticket/Ticket.vue'

export default [
  {
    path: '/admin',
    component: Layout,
    children: [
      {
        path: 'ticket',
        name: 'ticketAdmin',
        component: TicketAdmin,
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/ticket',
    component: Layout,
    children: [
      {
        path: '/',
        name: 'ticket',
        component: Ticket,
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]
