import Layout from '../layouts/Layout.vue'
import Contact from '../pages/contact/Contact.vue'

export default [{
  path: '/directory',
  component: Layout,
  children: [
    {
      path: 'contact',
      name: '',
      component: Contact,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
