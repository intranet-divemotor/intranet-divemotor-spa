import AllBirthdays from '../pages/birthday/AllBirthdays.vue'
import Layout from '../layouts/Layout.vue'

export default [{
  path: '/birthdays',
  component: Layout,
  children: [
    {
      path: '/',
      name: 'birthdays',
      component: AllBirthdays,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
