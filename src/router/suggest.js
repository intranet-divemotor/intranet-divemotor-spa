import Layout from '../layouts/Layout.vue'
import Suggest from '../pages/suggest/Suggest.vue'

export default [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'suggest',
        name: 'suggest',
        component: Suggest,
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]
