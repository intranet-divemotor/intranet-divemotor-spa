import Layout from '../layouts/Layout.vue'
import GalleryAdmin from '../pages/admin/Gallery.vue'
import Album from '../pages/gallery/Album.vue'
import Gallery from '../pages/gallery/Gallery.vue'

export default [
  {
    path: '/admin',
    component: Layout,
    children: [
      {
        path: 'gallery',
        name: 'galleryAdmin',
        component: GalleryAdmin,
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/gallery',
    component: Layout,
    children: [
      {
        path: 'album/:id/:name',
        name: 'album',
        component: Album,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: '/',
        name: 'gallery',
        component: Gallery,
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]
