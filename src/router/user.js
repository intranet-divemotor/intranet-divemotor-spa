import Layout from '../layouts/Layout.vue'
import Users from '../pages/admin/users.vue'
import Profile from '../pages/user/Profile.vue'

export default [
  {
    path: '/admin',
    component: Layout,
    children: [
      {
        path: 'users',
        name: 'adminUser',
        component: Users,
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    children: [
      {
        path: 'profile',
        name: '',
        component: Profile,
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]
