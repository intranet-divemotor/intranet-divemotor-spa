import Layout from '../layouts/Layout.vue'
import MedicalRequest from '../pages/request/MedicalRequest.vue'
import MedicineRequest from '../pages/request/MedicineRequest.vue'
import ConstanceRequest from '../pages/request/ConstanceRequest.vue'
import RoomRequest from '../pages/request/RoomRequest.vue'
import Change from '../pages/admin/Change.vue'
import AdminRoom from '../pages/admin/RoomRequest.vue'
import AdminConstance from '../pages/admin/ConstanceRequest.vue'
import Types from '../pages/admin/TypeRequest.vue'

export default [
  {
    path: '/request',
    component: Layout,
    children: [
      {
        path: 'medical',
        name: 'medical',
        component: MedicalRequest,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'medicine',
        name: 'medicine',
        component: MedicineRequest,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'constance',
        name: 'constance',
        component: ConstanceRequest,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'room',
        name: 'room',
        component: RoomRequest,
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/admin',
    component: Layout,
    children: [
      {
        path: 'dataExchange',
        name: 'dataExchange',
        component: Change,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'room',
        name: 'roomAdmin',
        component: AdminRoom,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'constance',
        name: 'constanceAdmin',
        component: AdminConstance,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'types',
        name: 'typeRequest',
        component: Types,
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]
