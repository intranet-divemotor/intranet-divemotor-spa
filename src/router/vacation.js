import Layout from '../layouts/Layout.vue'
import Vacation from '../pages/admin/Vacation.vue'

export default [{
  path: '/admin',
  component: Layout,
  children: [
    {
      path: 'vacations',
      name: 'vacations',
      component: Vacation,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
