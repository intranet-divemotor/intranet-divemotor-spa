import Layout from '../layouts/Layout.vue'
import Institution from '../pages/general/Institution.vue'
import notice from '../pages/general/Notice.vue'
import Communicated from '../pages/general/Communicated.vue'
import CommunicatedDetail from '../pages/general/CommunicatedDetail.vue'
import NoticeDetail from '../pages/general/NoticeDetail.vue'
import Health from '../pages/general/HealthBenefit.vue'
import Education from '../pages/general/EducationBenefit.vue'
import Exclusive from '../pages/general/ExclusiveBenefit.vue'
import Law from '../pages/general/LawBenefit.vue'
import Documents from '../pages/general/Documents.vue'
import DocDesa from '../pages/general/DocsDesa.vue'
import DocAutos from '../pages/general/DocsAutos.vue'
import DocViajes from '../pages/general/DocsViajes.vue'
import DocSeguros from '../pages/general/DocsSeguros.vue'
import DocLegal from '../pages/general/DocsLegal.vue'
import Areas from '../pages/general/Areas.vue'
import DocsSalud from '../pages/general/DocsSalud.vue'
import ManualGuia from '../pages/general/ManualGuia.vue'

export default [{
  path: '/general',
  component: Layout,
  children: [
    {
      path: 'institution',
      name: 'institution',
      component: Institution,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'notice',
      name: 'notice',
      component: notice,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'notice/:id',
      name: 'noticeDetail',
      component: NoticeDetail,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'communicated',
      name: 'communicated',
      component: Communicated,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'communicated/:id',
      name: 'communicatedDetail',
      component: CommunicatedDetail,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'health',
      name: 'health',
      component: Health,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'education',
      name: 'education',
      component: Education,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'exclusive',
      name: 'exclusive',
      component: Exclusive,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'law',
      name: 'law',
      component: Law,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'docs',
      name: 'docs',
      component: Documents,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'docsDesa',
      name: 'docsDesa',
      component: DocDesa,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'docsAutos',
      name: 'docsAutos',
      component: DocAutos,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'docsViajes',
      name: 'docsViajes',
      component: DocViajes,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'docsSeguros',
      name: 'docsSeguros',
      component: DocSeguros,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'docsLegal',
      name: 'docsLegal',
      component: DocLegal,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/manual/guias',
      name: 'manualGuia',
      component: ManualGuia,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'docsSalud',
      name: 'docsSalud',
      component: DocsSalud,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'areas',
      name: 'areas',
      component: Areas,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
