import Layout from '../layouts/Layout.vue'
import document from '../pages/info/Document.vue'
import DocumentDetails from '../pages/info/DocumentDetails.vue'
import FolderDetail from '../pages/info/FolderDetail.vue'
import Page from '../pages/info/Page.vue'

export default [{
  path: '/info',
  component: Layout,
  children: [
    {
      path: 'documents',
      name: 'document',
      component: document,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'documents/:id',
      name: 'documentDetails',
      component: DocumentDetails,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'documents/folder/:id',
      name: 'foltherDetail',
      component: FolderDetail,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: 'page/:id',
      name: 'page',
      component: Page,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
