import Layout from '../layouts/Layout.vue'
import Profile from '../pages/admin/Profile.vue'

export default [{
  path: '/admin',
  component: Layout,
  children: [
    {
      path: 'profile',
      name: 'profile',
      component: Profile,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
