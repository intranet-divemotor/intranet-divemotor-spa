import Router from 'vue-router'
import auth from '../store/auth'
import isEmpty from 'lodash/isEmpty'
const files = require.context('.', false, /\.js$/)
const modules = []
files.keys().forEach((key) => {
  if (key === './index.js') return
  files(key).default.map(r => modules.push(r))
})

const router = new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: modules
})

router.beforeEach((to, from, next) => {
  if (!auth.state.isAuth) auth.state.isAuth = !isEmpty(auth.getLocalUser())

  if (auth.state.isAuth && to.path === '/auth/signin') {
    next({
      path: '/'
    })
  } else if (to.matched.some(record => record.meta.requiresAuth)) {
    if (auth.state.isAuth) {
      next()
    } else {
      next({
        path: '/auth/signin'
      })
    }
  } else {
    next()
  }
})
export default router
