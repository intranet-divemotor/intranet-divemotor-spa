import Distribution from '../pages/notification/Distribution.vue'
import Layout from '../layouts/Layout.vue'

export default [{
  path: '/admin',
  component: Layout,
  children: [
    {
      path: 'notification',
      name: 'notification',
      component: Distribution,
      meta: {
        requiresAuth: true
      }
    }
  ]
}]
