export default (context) => {
  if (context.store.state.auth.isAuth && context.route.path === '/auth/signin') return context.redirect('/')
}
