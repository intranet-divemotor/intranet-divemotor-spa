<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    private $email = 'intranet01@divemotor.com.pe';
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginWithWrongValidation()
    {
        //$this->artisan('db:seed');

        $this->json('post', 'api/auth/login', [
            // 'email'     => $this->faker->email,
            'email'     => $this->email,
            'password'  => 'asdasd'
        ])->seeJson([
            'error'   => 'invalidCredentials'
        ])->assertResponseStatus(400 );
    }

    /**
     *
     */
    public function testCorrectLogin()
    {
        $password = '123456';

//        factory(\App\User::class)->create([
//            'email' => $email,
//            'password' => app('hash')->make($password)
//        ]);

        $this->json('post', 'api/auth/login', [
            'email'     => $this->email,
            'password'  => $password
        ])->seeJson([
            'email'   => $this->email
        ])
            ->assertResponseOk();
    }

}
