<?php

$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', ['namespace' => 'App\Http\Controllers\Profile'], function ($api) {

  $api->group(['middleware' => 'api.auth', 'prefix' => 'profile'], function () use ($api) {
  });

  $api->group(['middleware' => 'api.auth', 'prefix' => 'admin/profile'], function () use ($api) {
    $api->get('/fetch', 'ProfileController@fetch');
    $api->post('/submit', 'ProfileController@submit');
    $api->post('/delete', 'ProfileController@delete');
  });
});


