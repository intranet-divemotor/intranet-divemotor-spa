<?php

$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', function($api) {
  $api->post('/vacation/submit', [
    'as' => 'api.vacation.submit',
    'uses' => 'App\Http\Controllers\Vacation\VacationController@submit'
  ]);
  $api->get('/vacation/getvacations', [
    'as' => 'api.vacation.getvacations',
    'uses' => 'App\Http\Controllers\Vacation\VacationController@getVacations'
  ]);
  $api->get('/vacation/getmyvacations', [
    'as' => 'api.vacation.getmyvacations',
    'uses' => 'App\Http\Controllers\Vacation\VacationController@getMyVacations'
  ]);
  $api->get('/vacation/getcolvacations', [
    'as' => 'api.vacation.getcolvacations',
    'uses' => 'App\Http\Controllers\Vacation\VacationController@getColVacations'
  ]);
});