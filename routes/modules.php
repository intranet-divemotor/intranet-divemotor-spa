<?php

$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', ['namespace' => 'App\Http\Controllers\Modules'], function ($api) {

  $api->group(['middleware' => 'api.auth', 'prefix' => 'modules'], function () use ($api) {
    $api->get('/fetchMyModules', 'ModulesController@fetchMyModules');
  });

  $api->group(['middleware' => 'api.auth', 'prefix' => 'admin/modules'], function () use ($api) {
    $api->get('/fetch', 'ModulesController@fetch');
    $api->post('/submit', 'ModulesController@submit');
    $api->post('/delete', 'ModulesController@delete');
  });
});


