<?php

$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', function($api) {
  $api->post('/gallery/submitgallery', [
    'as' => 'api.gallery.submitgallery',
    'uses' => 'App\Http\Controllers\Gallery\GalleryController@saveAlbum'
  ]);
  $api->post('/gallery/addgallery', [
    'as' => 'api.gallery.addgallery',
    'uses' => 'App\Http\Controllers\Gallery\GalleryController@addGallery'
  ]);
  $api->get('/gallery/getgalleries', [
    'as' => 'api.gallery.getgalleries',
    'uses' => 'App\Http\Controllers\Gallery\GalleryController@getGalleries'
  ]);
  $api->get('/gallery/getgallerydetail', [
    'as' => 'api.gallery.getgallerydetail',
    'uses' => 'App\Http\Controllers\Gallery\GalleryController@getGallery'
  ]);
  $api->get('/gallery/deletealbum', [
    'as' => 'api.gallery.deletealbum',
    'uses' => 'App\Http\Controllers\Gallery\GalleryController@deleteAlbum'
  ]);
  $api->get('/gallery/deletegallery', [
    'as' => 'api.gallery.deletegallery',
    'uses' => 'App\Http\Controllers\Gallery\GalleryController@deleteItem'
  ]);
});
