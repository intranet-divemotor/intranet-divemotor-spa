<?php

$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', ['namespace' => 'App\Http\Controllers\SystemLink'], function ($api) {

  $api->group(['middleware' => 'api.auth', 'prefix' => 'link'], function () use ($api) {
    $api->get('/fetchMyLinks', 'SystemLinkController@fetchMyLinks');
    $api->get('/fetchFavorite', 'SystemLinkController@fetchFavorite');
    $api->post('/submitFavorite', 'SystemLinkController@submitFavorite');
    $api->post('/deleteFavorite', 'SystemLinkController@deleteFavorite');
  });

  $api->group(['middleware' => 'api.auth', 'prefix' => 'admin/link'], function () use ($api) {
    $api->get('/fetch', 'SystemLinkController@fetch');
    $api->get('/fetch-banner', 'SystemLinkController@fetchBanner');
    $api->post('/submit', 'SystemLinkController@submitLink');
    $api->post('/delete', 'SystemLinkController@delete');
    $api->post('/uploadImage', 'SystemLinkController@uploadImage');
  });
});


