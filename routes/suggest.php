<?php

$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', function($api) {
  $api->get('/suggests/getsuggests', [
    'as' => 'api.suggest.getsuggests',
    'uses' => 'App\Http\Controllers\Suggest\SuggestsController@getsuggests'
  ]);
  $api->post('/suggests/submitsuggests', [
    'as' => 'api.suggest.getsuggests',
    'uses' => 'App\Http\Controllers\Suggest\SuggestsController@submitsuggests'
  ]);
});