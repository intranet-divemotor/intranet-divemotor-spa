<?php

/*
|--------------------------------------------------------------------------
| Birthday Routes
|--------------------------------------------------------------------------
|
| Api for users birthdays
|
*/

$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1', ['namespace' => 'App\Http\Controllers\Birthday'], function ($api) {
  $api->group(['middleware' => 'api.auth', 'prefix' => 'birthday'], function () use ($api) {
    $api->get('/fetch', 'BirthdayController@fetch');
    $api->post('/saveCongratulation', 'BirthdayController@saveCongratulation');
  });
});
