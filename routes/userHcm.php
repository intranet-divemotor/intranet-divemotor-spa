<?php

/**
 * Created by PhpStorm.
 * User: Jose Hernandez
 * Date: 18-07-2017
 * Time: 11:43 PM
 *
 */
$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1', ['namespace' => 'App\Http\Controllers\Collaborator'], function ($api) {
  $api->group(['prefix' => 'hcm'], function () use ($api) {
    $api->get('/getfecth', 'CollaboratorSoapController@getfecth');
    $api->get('/delectfecth', 'CollaboratorSoapController@delectUserHcm');
  });


});












