<?php
/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 30-06-2017
 * Time: 12:43 PM
 */
$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', function($api) {
  $api->get('/menu/gethistorymenu', [
    'as' => 'api.menu.gethistorymenu',
    'uses' => 'App\Http\Controllers\MenuController@getHistoryMenu'
  ]);
  $api->get('/menu/getmenusede', [
    'as' => 'api.menu.getmenusede',
    'uses' => 'App\Http\Controllers\MenuController@getMenuOrganization'
  ]);
  $api->get('/menu/getcurrentmenu', [
    'as' => 'api.menu.getcurrentmenu',
    'uses' => 'App\Http\Controllers\MenuController@getCurrentMenu'
  ]);
  $api->get('/menu/getsuggestions', [
    'as' => 'api.menu.getsuggestions',
    'uses' => 'App\Http\Controllers\MenuController@getSuggestions'
  ]);
  $api->get('/menu/getsedes', [
    'as' => 'api.menu.getsedes',
    'uses' => 'App\Http\Controllers\MenuController@getSedesRegion'
  ]);
  $api->get('/menu/getreservation', [
    'as' => 'api.menu.getreservation',
    'uses' => 'App\Http\Controllers\MenuController@getReservation'
  ]);
  $api->get('/menu/updatestatusreservation', [
    'as' => 'api.menu.updatestatusreservation',
    'uses' => 'App\Http\Controllers\MenuController@updateReservation'
  ]);
  $api->get('/menu/getmenuday', [
    'as' => 'api.menu.getmenuday',
    'uses' => 'App\Http\Controllers\MenuController@getMenuDay'
  ]);
  $api->post('/menu/importexcel', [
    'as' => 'api.menu.importexcel',
    'uses' => 'App\Http\Controllers\MenuController@importExcel'
  ]);
  $api->post('/menu/submitsuggest', [
    'as' => 'api.menu.submitsuggest',
    'uses' => 'App\Http\Controllers\MenuController@submitSuggest'
  ]);
  $api->post('/menu/submitprescription', [
    'as' => 'api.menu.submitprescription',
    'uses' => 'App\Http\Controllers\MenuController@submitPrescription'
  ]);
  $api->get('/menu/deletemenu', [
    'as' => 'api.menu.deletemenu',
    'uses' => 'App\Http\Controllers\MenuController@deleteMenu'
  ]);
  $api->get('/menu/fetchsedes', [
    'as' => 'api.menu.fetchsedes',
    'uses' => 'App\Http\Controllers\MenuController@fetchSedesByMenu'
  ]);
});