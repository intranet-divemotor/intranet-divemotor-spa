<?php
/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 13-07-2017
 * Time: 10:29 PM
 */
$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', function($api) {
  /* MEDICAL REQUEST */
  $api->post('/request/medical/submit', [
    'as' => 'api.request.medical.submit',
    'uses' => 'App\Http\Controllers\Request\RequestController@submit'
  ]);
  $api->get('/request/medical/getmedicalrequest', [
    'as' => 'api.request.medical.getrequest',
    'uses' => 'App\Http\Controllers\Request\RequestController@getCurrentRequests'
  ]);
  $api->post('/request/medical/deleterequest', [
    'as' => 'api.request.medical.deleterequest',
    'uses' => 'App\Http\Controllers\Request\RequestController@deleteRequest'
  ]);
  /* MEDICINE REQUEST */
  $api->get('/request/medicine/getmedicinerequest', [
    'as' => 'api.request.medicine.getmedicinerequest',
    'uses' => 'App\Http\Controllers\Request\MedicineRequestController@getRequests'
  ]);
  $api->post('/request/medicine/submit', [
    'as' => 'api.request.medicine.submit',
    'uses' => 'App\Http\Controllers\Request\MedicineRequestController@submit'
  ]);
  /* CONSTANCE REQUEST */
  $api->post('/request/constance/submit', [
    'as' => 'api.request.constance.submit',
    'uses' => 'App\Http\Controllers\Request\ConstancesRequestController@submit'
  ]);
  $api->get('/request/constance/getconstancerequest', [
    'as' => 'api.request.constance.getconstancerequest',
    'uses' => 'App\Http\Controllers\Request\ConstancesRequestController@getRequests'
  ]);
  $api->get('/request/constance/gettyperequest', [
    'as' => 'api.request.constance.gettyperequest',
    'uses' => 'App\Http\Controllers\Request\ConstancesRequestController@getTypeRequests'
  ]);
  $api->get('/request/constance/updateconstancestatus', [
    'as' => 'api.request.constance.updateconstancestatus',
    'uses' => 'App\Http\Controllers\Request\ConstancesRequestController@updateStatus'
  ]);
  $api->post('/request/constance/setnoteconstance', [
    'as' => 'api.request.constance.setnoteconstance',
    'uses' => 'App\Http\Controllers\Request\ConstancesRequestController@setNote'
  ]);
  /* ROOM REQUEST */
  $api->post('/request/room/submit', [
    'as' => 'api.request.room.submit',
    'uses' => 'App\Http\Controllers\Request\RoomRequestController@submit'
  ]);
  $api->get('/request/room/getroomrequest', [
    'as' => 'api.request.room.getroomrequest',
    'uses' => 'App\Http\Controllers\Request\RoomRequestController@getRequests'
  ]);
  $api->get('/request/room/updatestatusrequest', [
    'as' => 'api.request.room.updatestatusrequest',
    'uses' => 'App\Http\Controllers\Request\RoomRequestController@updateStatusRequest'
  ]);
  /* ADMIN TYPES REQUESTS */
  $api->get('/request/type/getalltyperequest', [
    'as' => 'api.request.type.getall',
    'uses' => 'App\Http\Controllers\Request\TypeRequestController@getRequests'
  ]);
  $api->post('/request/type/submittyperequest', [
    'as' => 'api.request.type.submittyperequest',
    'uses' => 'App\Http\Controllers\Request\TypeRequestController@SubmitTypeRequest'
  ]);
  $api->post('/request/type/updatetyperequest', [
    'as' => 'api.request.type.updatetyperequest',
    'uses' => 'App\Http\Controllers\Request\TypeRequestController@editRequest'
  ]);
  $api->get('/request/type/deletetyperequest', [
    'as' => 'api.request.type.deletetyperequest',
    'uses' => 'App\Http\Controllers\Request\TypeRequestController@deleteTypeRequest'
  ]);
});