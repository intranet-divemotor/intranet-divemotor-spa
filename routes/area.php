<?php

/*
|--------------------------------------------------------------------------
| Event Routes
|--------------------------------------------------------------------------
|
| Api for users Events
|
*/
$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1', ['namespace' => 'App\Http\Controllers\Area'], function ($api) {
    $api->group(['middleware' => 'api.auth', 'prefix' => 'admin/area'], function () use ($api) {
    });

    $api->group(['middleware' => 'api.auth', 'prefix' => 'area'], function () use ($api) {
        $api->get('/fetchSelect', 'AreaController@fetchSelect');
    });
});









