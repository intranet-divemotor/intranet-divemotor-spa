<?php

/**
 * Created by PhpStorm.
 * User: Jose Hernandez
 * Date: 03-07-2017
 * Time: 11:43 PM
 *  
 */
$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1', ['namespace' => 'App\Http\Controllers\ContactDirectory'], function ($api) {
    //['api.auth','jwt.refres']
    $api->group(['middleware' => ['api.auth'], 'prefix' => 'contact'], function () use ($api) {
        $api->post('/',  'DirectoryController@listcontact'); //all contact
        $api->get('/my',  'DirectoryController@listDirectory');//my contact
        $api->post('/save',  'DirectoryController@save'); //all contact
        $api->post('/edit',  'DirectoryController@update'); //edit name group
        $api->post('/update',  'DirectoryController@getProfileEditcontact'); //update change gruop
        $api->post('/delete',  'DirectoryController@delect'); //update change gruop
    });
});









