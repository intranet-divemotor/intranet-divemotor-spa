  <?php

/*
|--------------------------------------------------------------------------
| Event Routes
|--------------------------------------------------------------------------
|
| Api for users Events
|
*/
$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1', ['namespace' => 'App\Http\Controllers\Event'], function ($api) {
    $api->group(['middleware' => 'api.auth', 'prefix' => 'admin/event'], function () use ($api) {
        $api->get('/fetch', 'EventController@fetch');
        $api->get('/fetchForAll', 'EventController@fetchForAll');
        $api->get('/fetchDetail', 'EventController@fetchDetail');
        $api->post('/submit', 'EventController@submit');
        $api->post('/deleteEvent', 'EventController@deleteEvent');
        $api->post('/confirmEvent', 'EventController@confirmEvent');
       $api->get('/fecthInMura', 'EventController@fecthInMura');
       $api->get('/update-currencies', 'EventController@fecthInCurrencies');
    });
});









