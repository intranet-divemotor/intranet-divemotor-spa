<?php

$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', ['namespace' => 'App\Http\Controllers\Indicators'], function ($api) {
  $api->group(['middleware' => 'api.auth', 'prefix' => 'indicator'], function () use ($api) {
    $api->post('/submit', 'IndicatorController@submit');
    $api->post('/submitnps', 'IndicatorController@submitNps');
    $api->post('/submitvoc', 'IndicatorController@submitVoc');
    $api->get('/getindicators', 'IndicatorController@getIndicators');
    $api->get('/removeindicators', 'IndicatorController@removeIndicators');
    $api->get('/toggleindicators', 'IndicatorController@toggleIndicators');
    $api->get('/getindicatorwidget', 'IndicatorController@getIndicatorWidget');
  });
});