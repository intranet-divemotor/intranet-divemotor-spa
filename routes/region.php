<?php

/*
|--------------------------------------------------------------------------
| Event Routes
|--------------------------------------------------------------------------
|
| Api for users Events
|
*/
$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1', ['namespace' => 'App\Http\Controllers\Region'], function ($api) {
    $api->group(['middleware' => 'api.auth', 'prefix' => 'admin/Region'], function () use ($api) {
    });

    $api->group(['middleware' => 'api.auth', 'prefix' => 'region'], function () use ($api) {
        $api->get('/fetchSelect', 'RegionController@fetchSelect');
    });
});









