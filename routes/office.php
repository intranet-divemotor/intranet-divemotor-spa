<?php

/**
 * Created by PhpStorm.
 * User: Jose Hernandez
 * Date: 18-07-2017
 * Time: 11:43 PM
 *
 */
$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1', ['namespace' => 'App\Http\Controllers\Office'], function ($api) {
  $api->group(['prefix' => 'office'], function () use ($api) {
    $api->get('syncup',  'OfficeController@syncupOffice365'); //all contact
  });
});









