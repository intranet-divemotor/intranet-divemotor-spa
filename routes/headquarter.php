<?php

/*
|--------------------------------------------------------------------------
| Event Routes
|--------------------------------------------------------------------------
|
| Api for users Events
|
*/
$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1', ['namespace' => 'App\Http\Controllers\Headquarters'], function ($api) {
    $api->group(['middleware' => 'api.auth', 'prefix' => 'admin/Headquarters'], function () use ($api) {
        $api->get('/fetch', 'EventController@fetch');
        $api->get('/submit', 'EventController@submitEvent');
    });

    $api->group(['middleware' => 'api.auth', 'prefix' => 'headquarters'], function () use ($api) {
        $api->get('/fetchSelect', 'HeadquartersController@fetchSelect');
        $api->get('/fetchGroup', 'HeadquartersController@fetchSelectGroup');
    });
});









