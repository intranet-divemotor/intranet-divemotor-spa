  <?php

/**
 * Created by PhpStorm.
 * User: Jose Hernandez
 * Date: 18-07-2017
 * Time: 11:43 PM
 *
 */
$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1', ['namespace' => 'App\Http\Controllers\Collaborator'], function ($api) {
  $api->group(['middleware' => 'api.auth', 'prefix' => 'user'], function () use ($api) {
    $api->get('/activities', 'CollaboratorController@getActivities');
    $api->get('/dataprofile', 'CollaboratorController@getDataProfile');
    $api->get('/mychanges', 'CollaboratorController@getMyChanges');
    $api->get('/changesrequest', 'CollaboratorController@getChangeRequest');
    $api->get('/emails', 'CollaboratorController@getEmails');
    $api->post('/submitchange', 'CollaboratorController@submitRequest');
    $api->post('/submitemail', 'CollaboratorController@manageEmail');
    $api->get('/updatechange', 'CollaboratorController@updateStatus');
    $api->get('/detailchange', 'CollaboratorController@getUpdateDetail');
    
    //admin
    $api->post('/all',  'CollaboratorController@listUser'); //all contact
    $api->post('/roles',  'CollaboratorController@listRole'); //all contact
    $api->post('/status',  'CollaboratorController@listStatus'); //all contact
    $api->post('/update',  'CollaboratorController@updateUser'); //all contact
    
    $api->post('/loadexcel',  'CollaboratorController@loadexcel'); //all contact
   
  });
  
  
});









