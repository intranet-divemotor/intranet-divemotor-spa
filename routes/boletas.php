<?php

/**
 * Created by PhpStorm.
 * User: Jose Hernandez
 * Date: 03-07-2017
 * Time: 11:43 PM
 *  
 */
$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1', ['namespace' => 'App\Http\Controllers\Boletas'], function ($api) {
    $api->group(['middleware' => 'api.auth', 'prefix' => 'boletas'], function () use ($api) {
        $api->get('/', ['uses' => 'AppBoletasController@index']);
        
       // $api->get('/pdf', ['uses' => 'AppBoletasController@']);
        
        $api->post('/list/file', 'AppBoletasController@ListFile');
        $api->post('/uploadZip', 'AppBoletasController@load');
        $api->post('/delete', 'AppBoletasController@deleteFile');
        $api->get('/download', 'AppBoletasController@download');
        $api->get('/preview', 'AppBoletasController@preview');
        
        $api->post('/listFile', 'AppBoletasController@ListFileAll');
       
    });
});









