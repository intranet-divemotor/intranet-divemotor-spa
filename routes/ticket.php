<?php
/**
 * Created by PhpStorm.
 * User: Sergio
 * Date: 03-07-2017
 * Time: 02:22 PM
 */
$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', function($api) {
  $api->get('/ticket/getmembers', [
    'as' => 'api.ticket.getmembers',
    'uses' => 'App\Http\Controllers\TicketController@getMembers'
  ]);

  $api->get('/ticket/getcomponent', [
    'as' => 'api.ticket.getcomponent',
    'uses' => 'App\Http\Controllers\TicketController@getServices'
  ]);

  $api->get('/ticket/getspecification', [
    'as' => 'api.ticket.getspecification',
    'uses' => 'App\Http\Controllers\TicketController@getDependencies'
  ]);

  $api->post('/ticket/uploadfile', [
    'as' => 'api.ticket.uploadfile',
    'uses' => 'App\Http\Controllers\TicketController@uploadFile'
  ]);

  $api->post('/ticket/submit', [
    'as' => 'api.ticket.submit',
    'uses' => 'App\Http\Controllers\TicketController@submit'
  ]);

  $api->get('/ticket/gettickets', [
    'as' => 'api.ticket.gettickets',
    'uses' => 'App\Http\Controllers\TicketController@getTickets'
  ]);

  $api->get('/ticket/updatestatus', [
    'as' => 'api.ticket.updatestatus',
    'uses' => 'App\Http\Controllers\TicketController@updateStatus'
  ]);

  $api->post('/ticket/sendincidente', [
    'as' => 'api.ticket.sendincidente',
    'uses' => 'App\Http\Controllers\TicketController@sendIncidente'
  ]);
});
