<?php

/**
 * Created by PhpStorm.
 * User: Jose Hernandez
 * Date: 03-07-2017
 * Time: 11:43 PM
 *
 */

$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', ['namespace' => 'App\Http\Controllers\Auth'], function ($api) {
    $api->post('/auth/login', 'AuthController@postLogin');
    $api->post('/auth/login/google', 'AuthController@getUserGoogle');
    $api->post('/auth/password/reset', 'PasswordController@sendResetLinkEmail');
    $api->post('/auth/forgotPasswordReset/{token}', 'PasswordController@resetpassword');

    // Api microsoft
    $api->post('/auth/login/microsoft', 'AuthMicrosoftController@authMicroftCallUrl');
    $api->post('/auth/login/gettoken', 'AuthMicrosoftController@authMicroftGettoken');

    $api->group(['middleware' => 'api.auth', 'prefix' => 'auth'], function () use ($api) {
        $api->get('/user', 'AuthController@getUser');
        $api->patch('/refresh', 'AuthController@patchRefresh');
        $api->delete('/invalidate', 'AuthController@deleteInvalidate');
        $api->post('/changePassword', 'PasswordController@changePassword');
    });
});


