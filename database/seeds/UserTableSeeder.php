<?php

use Illuminate\Database\Seeder;
use App\Entities\User\User;
use App\Entities\User\Userdetail;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
         $user = [
            [
               // 'idUser' => 1,
                'name' => 'Administrador',
                'lastName'=> 'Intranet',
                'mothersLastName' => 'Super',
               // 'email'=>'intranet01@divemotor.com.pe',
                'email'=> 'divemotor@intranetdev365.onmicrosoft.com',
                'password'=> Hash::make(123456),
                'idStatus'=>1,
                'idRole'=>1,
                'corporativoId'=>3014,

            ]
        ];

                $userdetail = [
            [
                'idUser' => 1,
                'identityDocument' => '1',
                'civilStatusId'=>1,
                'sexoId'=>1,
                'posicionHCM'=> 21111643,
                'colaborador' => 'Super Usuario',
                'desPosition'=> 'TECNICO DE TALLER',
                'desManagement'=>'Gerencia Región Sur',
                'desSubManagement'=>'Sucursal Alto Cural',        //SUB GERENCIA
                'codBoss'=>null,    //CODE JEFE----- RELACION CON  PosicionHCM
                'bossName'=>'SIN JEFE',           //NOMBRE DEL JEFE
                'desBoss'=>'SUB GERENTE DE OPERACIONES',  //Descripcion posisicon del jefe
                'dateAdmission'=> date("Y-m-d"),
            ]
        ];



        foreach ($user as $user) {
            User::create($user);
        }
        foreach ($userdetail as $userdetail) {
            Userdetail::create($userdetail);
        }
    }

}
