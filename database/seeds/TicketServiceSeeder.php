<?php

use Illuminate\Database\Seeder;
use App\Entities\TicketServices;

class TicketServiceSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $services = [
      [
        'description' => 'Credenciales de red',
        'mask' => 'Usuario de red bloqueado',
        'code' => 1,
        'classification' => 3,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_02'
      ],
      [
        'description' => 'Software Base',
        'mask' => 'Software de Escritorio',
        'code' => 3,
        'classification' => 4,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04'
      ],
      [
        'description' => 'Software No Estandar',
        'mask' => 'Ofimática (Word, Excel, etc.)',
        'code' => 4,
        'classification' => 4,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04_18'
      ],
      [
        'description' => 'Software No Estandar',
        'mask' => 'Software Varios',
        'code' => 5,
        'classification' => 4,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04_39'
      ],
      [
        'description' => 'SAP',
        'mask' => 'SAP',
        'code' => 3,
        'classification' => 1,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04_25'
      ],
      [
        'description' => 'SAP CRM',
        'mask' => 'SAP CRM',
        'code' => 5,
        'classification' => 1,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'SAP BI/BO',
        'mask' => 'SAP BI/BO',
        'code' => 4,
        'classification' => 1,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'SID',
        'mask' => 'SID',
        'code' => 8,
        'classification' => 1,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04_33'
      ],
      [
        'description' => 'Aplicaciones Web',
        'mask' => 'Aplicaciones Web',
        'code' => 1,
        'classification' => 1,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_02'
      ],
      [
        'description' => 'Aplicaciones de fábrica',
        'mask' => 'Aplicaciones de fábrica',
        'code' => 1,
        'classification' => 4,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_02'
      ],
      [
        'description' => 'Go-Socket',
        'mask' => 'Facturación Electrónica',
        'code' => 2,
        'classification' => 1,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'No aplica'
      ],
      [
        'description' => 'SAP HCM',
        'mask' => 'SAP HCM',
        'code' => 6,
        'classification' => 1,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'SAV2000',
        'mask' => 'SAV2000',
        'code' => 7,
        'classification' => 1,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'SMP - Solman Productivo',
        'mask' => 'Sistema de Gestión Tickets',
        'code' => 9,
        'classification' => 1,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'Equipamiento de escritorio',
        'mask' => 'Equipos de Escritorio',
        'code' => 5,
        'classification' => 2,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02'
      ],
      [
        'description' => 'Redes',
        'mask' => 'Redes (Conexión de red, Wifi)',
        'code' => 6,
        'classification' => 2,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02_34'
      ],
      [
        'description' => 'Telefonía',
        'mask' => 'Telefonía',
        'code' => 7,
        'classification' => 2,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02_27'
      ],
      [
        'description' => 'Equipos de diagnóstico',
        'mask' => 'Equipos de diagnóstico',
        'code' => 1,
        'classification' => 2,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02'
      ],
      [
        'description' => 'Equipos de impresión',
        'mask' => 'Equipos de impresión',
        'code' => 2,
        'classification' => 2,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02'
      ],
      [
        'description' => 'Equipos Móviles',
        'mask' => 'Equipos Móviles (Tablet, Ipad)',
        'code' => 3,
        'classification' => 2,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02_11'
      ],
      [
        'description' => 'Equipos de multimedia',
        'mask' => 'Proyector',
        'code' => 4,
        'classification' => 2,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02_16'
      ],
      [
        'description' => 'Servidores',
        'mask' => 'Conexión Remota (VPN, GoGLobal)',
        'code' => 2,
        'classification' => 4,
        'type' => 1,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01_05'
      ],
      [
        'description' => 'Credenciales de red',
        'mask' => 'Usuario de red',
        'code' => 1,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_02'
      ],
      [
        'description' => 'SID',
        'mask' => 'SID',
        'code' => 2,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04_33'
      ],
      [
        'description' => 'Software Base',
        'mask' => 'Software de Escritorio',
        'code' => 1,
        'classification' => 4,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04'
      ],
      [
        'description' => 'Software No Estandar',
        'mask' => 'Ofimática (Word, Excel, etc.)',
        'code' => 2,
        'classification' => 4,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04_18'
      ],
      [
        'description' => 'Software No Estandar',
        'mask' => 'Software Varios',
        'code' => 3,
        'classification' => 4,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04_39'
      ],
      [
        'description' => 'Software No Estandar',
        'mask' => 'Acceso Remoto (VPN, GoGLobal)',
        'code' => 3,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04_42'
      ],
      [
        'description' => 'Solicitud de Información',
        'mask' => 'Otras solicitudes',
        'code' => 1,
        'classification' => 5,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01_16'
      ],
      [
        'description' => 'Asistencia al usuario',
        'mask' => 'Asistencia al usuario (Webex)',
        'code' => 4,
        'classification' => 4,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04_37_01'
      ],
      [
        'description' => 'SAP',
        'mask' => 'SAP',
        'code' => 4,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_04_25'
      ],
      [
        'description' => 'SAP CRM',
        'mask' => 'SAP CRM',
        'code' => 5,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'SAP BI/BO',
        'mask' => 'SAP BI/BO',
        'code' => 6,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'Go-Socket',
        'mask' => 'Facturación Electrónica',
        'code' => 1,
        'classification' => 2,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'No aplica'
      ],
      [
        'description' => 'SMP - Solman Productivo',
        'mask' => 'Sistema de Gestión Tickets',
        'code' => 2,
        'classification' => 2,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'Equipamiento de escritorio',
        'mask' => 'Equipos de Escritorio',
        'code' => 1,
        'classification' => 3,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02'
      ],
      [
        'description' => 'Redes',
        'mask' => 'Redes (Conexión de red, Wifi)',
        'code' => 2,
        'classification' => 3,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02_34'
      ],
      [
        'description' => 'Equipos de Diagnóstico',
        'mask' => 'Equipos de Diagnóstico',
        'code' => 3,
        'classification' => 3,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02'
      ],
      [
        'description' => 'Equipos de impresión',
        'mask' => 'Equipos de impresión',
        'code' => 4,
        'classification' => 3,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02'
      ],
      [
        'description' => 'Equipos de multimedia',
        'mask' => 'Proyector',
        'code' => 5,
        'classification' => 3,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02_16'
      ],
      [
        'description' => 'Equipos móviles',
        'mask' => 'Equipos Móviles (Tablet, Ipad)',
        'code' => 6,
        'classification' => 3,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02_11'
      ],
      [
        'description' => 'Telefonía',
        'mask' => 'Telefonía',
        'code' => 7,
        'classification' => 3,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02_27'
      ],
      [
        'description' => 'Gestión de Usuario',
        'mask' => 'Altas y Bajas de Usuario',
        'code' => 7,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_02_27'
      ],
      [
        'description' => 'Google Apps',
        'mask' => 'Correo',
        'code' => 8,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'Internet',
        'mask' => 'Internet',
        'code' => 9,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'SAP HCM',
        'mask' => 'SAP HCM',
        'code' => 10,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01'
      ],
      [
        'description' => 'Servidores',
        'mask' => 'Acceso Unidad Compartida (G:)',
        'code' => 11,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_01_05'
      ],
      [
        'description' => 'Aplicaciones de fábrica',
        'mask' => 'Aplicaciones de fábrica',
        'code' => 12,
        'classification' => 1,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_02'
      ],
      [
        'description' => 'Aplicaciones Web',
        'mask' => 'Aplicaciones Web',
        'code' => 5,
        'classification' => 4,
        'type' => 2,
        'idStatus' => 1,
        'codeSolman' => 'GKFPE02_03_02'
      ]
    ];

    foreach ($services as $service) {
      TicketServices::create($service);
    }
  }
}
