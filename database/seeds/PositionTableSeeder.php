<?php

use Illuminate\Database\Seeder;
use App\Entities\Position;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * 
*/
    public function run()
    {
        $position = [
            [
                'positionId' => 1,
                'position' => 'ANALISTA DE DESARROLLO HUMANO'
            ],
            [
                'positionId' => 2,
                'position' => 'TECNICO DE TALLER'
            ],
            [
                'positionId' => 3,
                'position' => 'SUPERVISOR DE CONTRATOS DE MANTENIMIENTO'
            ],
            [
                'positionId' => 4,
                'position' => 'ANALISTA DE MARKETING'
            ],
            [
                'positionId' => 5,
                'position' => 'SUPERVISOR DE TALLER'
            ],
            [
                'positionId' => 6,
                'position' => 'ASISTENTE ADMINISTRATIVO'
            ],
            [
                'positionId' => 7,
                'position' => 'PLANIFICADOR DE TALLER'
            ],
            [
                'positionId' => 8,
                'position' => 'ASESOR DE SERVICIO'
            ],
            [
                'positionId' => 9,
                'position' => 'GESTOR DE CUENTAS CLAVES'
            ],
            [
                'positionId' => 10,
                'position' => 'JEFE DE SOPORTE TECNICO'
            ],
            [
                'positionId' => 11,
                'position' => 'ASESOR TECNICO'
            ],
            [
                'positionId' => 12,
                'position' => 'COORDINADOR DE PROYECTOS'
            ],
            [
                'positionId' => 13,
                'position' => 'TECNICO DIAGNOSTICADOR'
            ],
            [
                'positionId' => 14,
                'position' => 'PRACTICANTE'
            ],
            [
                'positionId' => 15,
                'position' => 'ASESOR TECNICO'
            ],
            [
                'positionId' => 16,
                'position' => 'ANALISTA PROGRAMADOR WEB'
            ],

        ];

    //    foreach($position as $position){
   //         Position::create($position);
   //     }
    }
}
