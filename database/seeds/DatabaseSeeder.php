<?php

use Illuminate\Database\Seeder;
use App\Entities\User\Role;
use App\Status;
use App\Entities\Sexo;
use App\CivilStatus;
class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $roles = [
            [
                'roleName' => 'Super Administrador',
                'modules'=> '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,20,21,22,25,26,27,28,29,30,31,32'
            ],
            [
                'roleName' => 'Intranet',
                'modules'=> '1,2,3,4,10,11,12,13,14,15,27,32'
            ],
            [
                'roleName' => 'Administrador',
                'modules'=> '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,25,32'
            ]
        ];

        $status = [
            [
                'idStatus' => 1,
                'statusName' => 'Activo'
            ],
            [
                'idStatus' => 2,
                'statusName' => 'Suspendido'
            ]
        ];
        
        $sexo = [
            [
                'sexoId' => 1,
                'sexo' => 'Masculino'
            ],
            [
                'sexoId' => 2,
                'sexo' => 'Femenino'
            ],
            [
                'sexoId' => 3,
                'sexo' => 'N/D'
            ]
        ];
        
       $statuscivil = [
            [
                'civilStatus' => 'SOLTERO'
            ],
            [
                'civilStatus' => 'SOLTERA'
            ],
            [
                'civilStatus' => 'VIUDO'
            ],
            [
                'civilStatus' => 'VIUDA'
            ],
            [
                'civilStatus' => 'CASADO'
            ],
            [
                'civilStatus' => 'CASADA'
            ],
            [
                'civilStatus' => 'DIVORCIADO'
            ],
            [
                'civilStatus' => 'DIVORCIADA'
            ],
            [
                'civilStatus' => 'CONVIVIENTE'
            ],
            [
                'civilStatus' => 'N/D'
            ]
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }
        foreach ($status as $statu) {
            Status::create($statu);
        }
        foreach ($sexo as $sexo){
            Sexo::create($sexo);
        }
        foreach ($statuscivil as $statuscivil){
            CivilStatus::create($statuscivil);
        }

        $this->call(OrganizationTableSeeder::class);
        $this->call(RegionTableSeeder::class);
        $this->call(PositionTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(AreaTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(RequestTypeTableSeeder::class);
        $this->call(ModuleTableSeeder::class);
        $this->call(TicketsTableSeeder::class);
        $this->call(DivisionTableSeeder::class);
        $this->call(TicketServiceSeeder::class);
        $this->call(GroupDivisionTableSeeder::class);
    }

}
