<?php

use Illuminate\Database\Seeder;
use App\Entities\Department;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * 
*/
    public function run()
    {
        $department = [
            [
                'departmentId' => 1,
                'department' => 'Gerencia Desarrollo Humano'
            ],
            [
                'departmentId' => 2,
                'department' => 'Contratos de Mantenimiento'
            ],
            [
                'departmentId' => 3,
                'department' => 'Taller Comercial'
            ],
            [
                'departmentId' => 4,
                'department' => 'Gerencia de Repuestos'
            ],
            [
                'departmentId' => 5,
                'department' => 'Gerencia Administracion y Contabilidad'
            ],
            [
                'departmentId' => 6,
                'department' => 'Sucursal Cusco'
            ],
            [
                'departmentId' => 7,
                'department' => 'Soporte de Operaciones'
            ],
            [
                'departmentId' => 8,
                'department' => 'Gerencia Informatica'
            ],
            [
                'departmentId' => 9,
                'department' => 'Gerencia División Post Venta'
            ],
            [
                'departmentId' => 10,
                'department' => 'Taller Camiones'
            ],
            [
                'departmentId' => 11,
                'department' => 'Gerencia de Desarrollo'
            ],
            [
                'departmentId' => 12,
                'department' => 'Sucursal Trujillo'
            ],
            
        ];

    //    foreach($department as $department){
    //        Department::create($department);
    //    }
    }
}
