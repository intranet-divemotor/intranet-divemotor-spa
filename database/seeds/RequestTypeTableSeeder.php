<?php

use Illuminate\Database\Seeder;
use App\TypeRequest;

class RequestTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'request' => 'Cita médica',
                'description' => 'Cita médica',
            ],
            [
                'request' => 'Medicamentos',
                'description' => 'Medicamentos',
            ],
            [
                'request' => 'Sala de computo',
                'description' => 'Sala de computo',
            ],
            [
                'request' => 'Sala de reuniones',
                'description' => 'Sala de reuniones',
            ],
            [
                'request' => 'Sala de capacitación',
                'description' => 'Sala de capacitación',
            ],
            [
                'request' => 'Sala de administración',
                'description' => 'Sala de administración',
            ],
            [
                'request' => 'Constancia laboral',
                'description' => 'Constancia laboral',
            ],
            [
                'request' => 'Constancia salarial',
                'description' => 'Constancia salarial',
            ],
            [
                'request' => 'Licencia por paternidad',
                'description' => 'Licencia por paternidad',
            ],
            [
                'request' => 'Licencia por luto',
                'description' => 'Licencia por luto',
            ],
            [
                'request' => 'Licencia por familiar grave',
                'description' => 'Licencia por familiar grave',
            ],

        ];

        foreach ($types as $type) {
            TypeRequest::create($type);
        }
    }
}
