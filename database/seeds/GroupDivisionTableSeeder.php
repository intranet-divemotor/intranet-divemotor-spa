<?php

use Illuminate\Database\Seeder;
use App\GroupDivisions;

class GroupDivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $divisions = [
        ['description' => 'Piura', 'codes' => 'PE01,PI08', 'idStatus' => 1],
        ['description' => 'Trujillo', 'codes' => 'PE02,PI04', 'idStatus' => 1],
        ['description' => 'Tienda Chiclayo', 'codes' => 'PE03,PI09', 'idStatus' => 1],
        ['description' => 'Cajamarca', 'codes' => 'PE04,PI10', 'idStatus' => 1],
        ['description' => 'Panamericana Sur', 'codes' => 'PE05,PI06', 'idStatus' => 1],
        ['description' => 'Canada', 'codes' => 'PE06,PI07', 'idStatus' => 1],
        ['description' => 'Faucett', 'codes' => 'PE07,PI03', 'idStatus' => 1],
        ['description' => 'Tienda Arriola', 'codes' => 'PE08,PI11', 'idStatus' => 1],
        ['description' => 'Tienda Independencia', 'codes' => 'PE09,PI12', 'idStatus' => 1],
        ['description' => 'Arambubu', 'codes' => 'PE10,PI02', 'idStatus' => 1],
        ['description' => 'Encalada', 'codes' => 'PE11,PI13', 'idStatus' => 1],
        ['description' => 'Huaraz', 'codes' => 'PE12,PI14', 'idStatus' => 1],
        ['description' => 'Arequipa Pampas', 'codes' => 'PE13,PI05,PE15,PI16,PE37,PI37', 'idStatus' => 1],
        ['description' => 'Uchumayo', 'codes' => 'PE14,PI15', 'idStatus' => 1],
        ['description' => 'Cusco', 'codes' => 'PE16,PI24', 'idStatus' => 1],
        ['description' => 'Arriola', 'codes' => 'PE17,PI01,PC01', 'idStatus' => 1],
        ['description' => 'Juliaca', 'codes' => 'PE18,PI17', 'idStatus' => 1],
        ['description' => 'San Luis', 'codes' => 'PE19,PI18', 'idStatus' => 1],
        ['description' => 'Lurin', 'codes' => 'PE20,PI19', 'idStatus' => 1],
        ['description' => 'Berlin', 'codes' => 'PE22,PI20', 'idStatus' => 1],
        ['description' => 'Camacho', 'codes' => 'PE23,PI21', 'idStatus' => 1],
        ['description' => 'Plaza Lima Norte', 'codes' => 'PE24,PI22', 'idStatus' => 1],
        ['description' => 'Ayllon', 'codes' => 'PE25,PI23', 'idStatus' => 1],
        ['description' => 'Lambayeque', 'codes' => 'PE26,PI25', 'idStatus' => 1],
        ['description' => 'Domingo Orue', 'codes' => 'PE27,PI26', 'idStatus' => 1],
        ['description' => 'Huancayo', 'codes' => 'PE28,PI27', 'idStatus' => 1],
        ['description' => 'Cerro de Pasco', 'codes' => 'PE29,PI28', 'idStatus' => 1],
        ['description' => 'Jicamarca', 'codes' => 'PE30,PI29', 'idStatus' => 1],
        ['description' => 'Independencia', 'codes' => 'PE31,PI30', 'idStatus' => 1],
        ['description' => 'Cusco 2', 'codes' => 'PE32,PI31', 'idStatus' => 1],
        ['description' => 'Los Olivos', 'codes' => 'PE33,PI32', 'idStatus' => 1],
        ['description' => 'Arriola 2', 'codes' => 'PE34,PI33', 'idStatus' => 1],
        ['description' => 'Espinar', 'codes' => 'PE35,PI34', 'idStatus' => 1],
        ['description' => 'Juliaca 2', 'codes' => 'PE36,PI35', 'idStatus' => 1]
      ];

      foreach ($divisions as $division) {
        GroupDivisions::create($division);
      }
    }
}
