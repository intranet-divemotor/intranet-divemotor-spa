<?php

use Illuminate\Database\Seeder;
use App\Region;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            [
                'idRegion' => 1,
                'description' => 'Norte'
            ],
            [
                'idRegion' => 2,
                'description' => 'Centro'
            ],
            [
                'idRegion' => 3,
                'description' => 'Sur'
            ],

        ];

        foreach($regions as $region){
            Region::create($region);
        }
    }
}
