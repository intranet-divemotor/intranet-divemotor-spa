<?php

use Illuminate\Database\Seeder;
use App\Divisions;

class DivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  public function run()
  {
    $divisions = [
      ['description' => 'Arriola', 'code' => 'PC01', 'society' => '3300', 'idStatus' => 1],
      ['description' => 'Piura', 'code' => 'PE01', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Trujillo', 'code' => 'PE02', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Tienda Chiclayo', 'code' => 'PE03', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Cajamarca', 'code' => 'PE04', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Panamericana Sur', 'code' => 'PE05', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Canada', 'code' => 'PE06', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Faucett', 'code' => 'PE07', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Tienda Arriola', 'code' => 'PE08', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Tienda Independencia', 'code' => 'PE09', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Aramburu', 'code' => 'PE10', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Encalada', 'code' => 'PE11', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Huaraz', 'code' => 'PE12', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Arequipa', 'code' => 'PE13', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Var. Uchumayo K3.8-Arequipa', 'code' => 'PE14', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Alto Cural', 'code' => 'PE15', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Cusco', 'code' => 'PE16', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Arriola', 'code' => 'PE17', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Juliaca', 'code' => 'PE18', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'San Luis', 'code' => 'PE19', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Lurin', 'code' => 'PE20', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'N/A', 'code' => 'PE21', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Berlin', 'code' => 'PE22', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Camacho', 'code' => 'PE23', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Plaza Lima Norte', 'code' => 'PE24', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Ayllon', 'code' => 'PE25', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Lambayeque', 'code' => 'PE26', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Domingo Orue', 'code' => 'PE27', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Huancayo', 'code' => 'PE28', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Cerro de Pasco', 'code' => 'PE29', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Jicamarca', 'code' => 'PE30', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Independencia', 'code' => 'PE31', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Cusco 2', 'code' => 'PE32', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Los Olivos', 'code' => 'PE33', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Arriola 2', 'code' => 'PE34', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Espinar', 'code' => 'PE35', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Juliaca 2', 'code' => 'PE36', 'society' => '3100', 'idStatus' => 1],
      ['description' => 'Arriola', 'code' => 'PI01', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Aramburu', 'code' => 'PI02', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Faucett', 'code' => 'PI03', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Trujillo', 'code' => 'PI04', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Arequipa', 'code' => 'PI05', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Panamericana Sur', 'code' => 'PI06', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Canada', 'code' => 'PI07', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Piura', 'code' => 'PI08', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Tienda Chiclayo', 'code' => 'PI09', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Cajamarca', 'code' => 'PI10', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Tienda Arriola', 'code' => 'PI11', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Tienda Independencia', 'code' => 'PI12', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Encalada', 'code' => 'PI13', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Huaraz', 'code' => 'PI14', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Var. Uchumayo K3.8-Arequipa', 'code' => 'PI15', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Alto Cural', 'code' => 'PI16', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Juliaca', 'code' => 'PI17', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'San Luis', 'code' => 'PI18', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Lurin', 'code' => 'PI19', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Berlin', 'code' => 'PI20', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Camacho', 'code' => 'PI21', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Plaza Lima Norte', 'code' => 'PI22', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Ayllon', 'code' => 'PI23', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Cusco', 'code' => 'PI24', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Lambayeque', 'code' => 'PI25', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Domingo Orue', 'code' => 'PI26', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Huancayo', 'code' => 'PI27', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Cerro de Pasco', 'code' => 'PI28', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Jicamarca', 'code' => 'PI29', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Independencia', 'code' => 'PI30', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Cusco 2', 'code' => 'PI31', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Los Olivos', 'code' => 'PI32', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Arriola 2', 'code' => 'PI33', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Espinar', 'code' => 'PI34', 'society' => '3000', 'idStatus' => 1],
      ['description' => 'Juliaca 2', 'code' => 'PI35', 'society' => '3000', 'idStatus' => 1]
    ];

    foreach ($divisions as $division) {
      Divisions::create($division);
    }
  }
}
