<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPostulationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_postulation', function (Blueprint $table) {
            $table->increments('id')->first();
            $table->integer('idPostulation');
            $table->integer('idUser');
            $table->boolean('authorized')->nullable();
            $table->boolean('requested')->nullable();
            $table->boolean('postulate')->nullable();
            $table->text('career')->nullable();
            $table->text('college')->nullable();
            $table->text('stateCollege')->nullable();
            $table->text('reasons')->nullable();
            $table->text('cvFile')->nullable();
            $table->timestamps('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_postulation');
    }
}
