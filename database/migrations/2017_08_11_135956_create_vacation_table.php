<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacationTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('vacation', function (Blueprint $table) {
      $table->increments('id')->unique();
      $table->integer('idUser');
      $table->text('description');
      $table->text('file');
      $table->integer('idStatus');
      $table->timestamps();
    });

    Schema::create('vacation_detail', function (Blueprint $table) {
      $table->increments('id')->unique();
      $table->integer('idVacation');
      $table->bigInteger('hcmCollaborator');
      $table->bigInteger('hcmHigher')->nullable();
      $table->date('contractDate');
      $table->date('startPeriod');
      $table->date('endPeriod');
      $table->float('vacationWon');
      $table->float('vacationTaken');
      $table->float('vacationPending');
      $table->timestamps();
    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
  {
    Schema::drop('vacation_detail');
    Schema::drop('vacation');
  }
}
