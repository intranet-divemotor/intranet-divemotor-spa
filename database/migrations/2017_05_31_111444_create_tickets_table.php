<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_type', function(Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('description');
            $table->string('mask');
            $table->timestamps();
        });

        Schema::create('service_type', function(Blueprint $table){
            $table->increments('id')->unique();
            $table->string('description');
            $table->string('mask');
            $table->timestamps();
        });

        Schema::create('service', function(Blueprint $table){
            $table->increments('id')->unique();
            $table->string('description');
            $table->string('mask');
            $table->timestamps();
        });

        Schema::create('service_component', function(Blueprint $table){
            $table->increments('id')->unique();
            $table->string('description');
            $table->string('mask');
            $table->timestamps();
        });

        Schema::create('specification', function(Blueprint $table){
            $table->increments('id')->unique();
            $table->string('description');
            $table->string('mask');
            $table->timestamps();
        });

        Schema::create('servicetype_by_tickettype', function(Blueprint $table){
            $table->increments('id')->unique();
            $table->integer('idTicketType');
            $table->integer('idServiceType');
            $table->timestamps();
            $table->foreign('idTicketType')->references('id')->on('ticket_type');
            $table->foreign('idServiceType')->references('id')->on('service_type');
        });

        Schema::create('service_by_servicetype', function(Blueprint $table){
            $table->increments('id')->unique();
            $table->integer('idServiceType');
            $table->integer('idService');
            $table->timestamps();
            $table->foreign('idServiceType')->references('id')->on('service_type');
            $table->foreign('idService')->references('id')->on('service');
        });

        Schema::create('component_by_service', function(Blueprint $table){
            $table->increments('id')->unique();
            $table->integer('idService');
            $table->integer('idComponent');
            $table->timestamps();
            $table->foreign('idService')->references('id')->on('service');
            $table->foreign('idComponent')->references('id')->on('service_component');
        });

        Schema::create('specification_by_component', function(Blueprint $table){
            $table->increments('id')->unique();
            $table->integer('idComponent');
            $table->integer('idSpecification');
            $table->timestamps();
            $table->foreign('idComponent')->references('id')->on('service_component');
            $table->foreign('idSpecification')->references('id')->on('specification');
        });

        Schema::create('tickets', function (Blueprint $table){
            $table->increments('id')->unique();
            $table->integer('idUser');
            $table->integer('idUserCoor');
            $table->integer('type');
            $table->integer('idStatus');
            $table->text('code')->nullable();
            $table->timestamps();
        });

        Schema::create('tickets_detail', function (Blueprint $table){
            $table->increments('id')->unique();
            $table->integer('idTicket');
            $table->integer('idTicketType');
            $table->integer('idServiceType');
            $table->integer('idService');
            $table->integer('idComponent')->nullable();
            $table->integer('idSpecification')->nullable();
            $table->text('subject');
            $table->text('description');
            $table->text('file')->nullable();
            $table->text('phone')->nullable();
            $table->timestamps();
            $table->foreign('idTicket')->references('id')->on('tickets');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specification_by_component');
        Schema::dropIfExists('component_by_service');
        Schema::dropIfExists('service_by_servicetype');
        Schema::dropIfExists('servicetype_by_tickettype');
        Schema::dropIfExists('specification');
        Schema::dropIfExists('service_component');
        Schema::dropIfExists('service');
        Schema::dropIfExists('service_type');
        Schema::dropIfExists('ticket_type');
        Schema::dropIfExists('tickets');
        Schema::dropIfExists('tickets_detail');
    }
}
