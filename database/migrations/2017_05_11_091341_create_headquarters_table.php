<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadquartersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headquarters', function (Blueprint $table) {
            $table->increments('id')->first();
            $table->integer('idUser');
            $table->integer('regionId');
            $table->text('title')->nullable();
            $table->text('summary')->nullable();
            $table->text('detail')->nullable();
            $table->text('coverPhoto')->nullable();
            $table->text('location')->nullable();
            $table->text('primaryPhone')->nullable();
            $table->timestamps('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('headquarters');
    }
}
