<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostulationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulation', function (Blueprint $table) {
            $table->increments('id')->first();
            $table->integer('idUser');
            $table->integer('areaId');
            $table->integer('departmentId');
            $table->text('title');
            $table->text('summary');
            $table->text('detail');
            $table->text('functions');
            $table->text('requirements');
            $table->text('coverPhoto');
            $table->text('extraFile')->nullable();
            $table->date('startDate');
            $table->date('endDate');
            $table->timestamps('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('postulation');
    }
}
