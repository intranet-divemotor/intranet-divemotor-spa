<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album', function (Blueprint $table) {
            $table->increments('idAlbum');
            $table->integer('idUser');
            $table->text('idDivision');
            $table->text('title');
            $table->text('description')->nullable();
            $table->text('coverPhoto')->nullable();
            $table->integer('idType')->nullable();
            $table->integer('idStatus');
            $table->timestamps();
        });

        Schema::create('gallery', function (Blueprint $table) {
            $table->increments('idGallery');
            $table->integer('idUser');
            $table->text('title');
            $table->text('description')->nullable();
            $table->text('path');
            $table->integer('idType');
            $table->integer('idStatus');
            $table->integer('idAlbum');
            $table->timestamps();
            $table->foreign('idAlbum')->references('idAlbum')->on('album');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery');
        Schema::dropIfExists('album');
    }
}
