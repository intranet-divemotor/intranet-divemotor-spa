<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ticket_services', function (Blueprint $table) {
        $table->increments('id')->unique();
        $table->integer('code');
        $table->integer('classification');
        $table->text('description');
        $table->text('mask');
        $table->integer('type');
        $table->integer('idStatus');
        $table->text('codeSolman')->nullable();
        $table->text('ibIbase')->nullable();
        $table->text('ibInstance')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('ticket_services');
    }
}
