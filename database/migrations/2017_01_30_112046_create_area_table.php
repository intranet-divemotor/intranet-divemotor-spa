<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //Area a la que pertenece un colaborador
        Schema::create('area', function (Blueprint $table) {
            $table->string('cod')->unique();
            $table->string('area');
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });
      /*
      //departamento a la que pertenece un colaborador
      Schema::create('department', function (Blueprint $table) {
          $table->increments('departmentId')->unique();
          $table->string('department');
          $table->timestamps();
          $table->dateTimeTz('deleted_at')->nullable();
      });

       //departamento a la que pertenece un colaborador
      Schema::create('position', function (Blueprint $table) {
          $table->increments('positionId')->unique();
          $table->string('position');
          $table->integer('cod')->nullable();
          $table->timestamps();
          $table->dateTimeTz('deleted_at')->nullable();
      });
       */
        
       
        
        
        //sexo
        Schema::create('sexo', function (Blueprint $table) {
            $table->increments('sexoId')->unique();
            $table->string('sexo');
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });
        
         //sexo
        Schema::create('civilStatus', function (Blueprint $table) {
            $table->increments('civilStatusId')->unique();
            $table->string('civilStatus');
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //  Schema::dropIfExists('area');
      //  Schema::dropIfExists('department');
       // Schema::dropIfExists('position');
    }
}
