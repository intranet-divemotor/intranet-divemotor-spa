<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoritelinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favoritelink', function (Blueprint $table) {
            $table->increments('id')->first();
            $table->integer('parent')->nullable();
            $table->integer('userId');
            $table->text('name');
            $table->text('urlPath');
            $table->text('iconPath');
            $table->integer('type');
            $table->timestamps('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('favoritelink');
    }
}
