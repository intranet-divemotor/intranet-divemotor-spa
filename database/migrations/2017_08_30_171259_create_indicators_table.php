<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('indicators', function (Blueprint $table) {
        $table->increments('id')->unique();
        $table->integer('type');
        $table->text('title');
        $table->text('comment');
        $table->integer('idStatus');
        $table->timestamps();
      });

      Schema::create('indicators_detail', function (Blueprint $table) {
        $table->increments('id')->unique();
        $table->integer('idIndicator');
        $table->text('brand');
        $table->float('percentage');
        $table->text('color');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('indicators_detail');
      Schema::drop('indicators');
    }
}
