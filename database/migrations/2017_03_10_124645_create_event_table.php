<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->increments('id')->first();
            $table->integer('idUser');
            $table->string('region', 250);
            $table->text('headquarters', 250)->nullable();
            $table->integer('importance');
            $table->text('areas', 250)->nullable();
            $table->text('title');
            $table->text('summary');
            $table->text('detail');
            $table->text('coverPhoto');
            $table->date('startDate');
            $table->date('endDate');
            $table->string('hour', 10);
            $table->string('hourend', 10);
            $table->text('location');
            $table->timestamps('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event');
    }
}
