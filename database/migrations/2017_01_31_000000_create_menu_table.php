<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('menu', function(Blueprint $table) {
            // Error generate : INTEGER NOT NULL
            $table->increments('idMenu')->unique();
            $table->integer('idStatus');
            $table->integer('created_by');
            $table->text('idSucursal');
            $table->text('file');
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });

        Schema::create('menu_detalle', function(Blueprint $table) {
            // Error generate : INTEGER NOT NULL
            $table->increments('idDetalle');
            $table->integer('idMenu');
            $table->string('hora', 10);
            $table->string('tipo', 255);
            $table->string('comida', 255);
            $table->timestamp('fecha');
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
            $table->foreign('idMenu')->references('idMenu')->on('menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('menu_detalle');
        Schema::drop('menu');
    }

}
