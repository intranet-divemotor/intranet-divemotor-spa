<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedOrganizationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('countries', function(Blueprint $table) {
            $table->increments('idCountries')->unique();
            $table->string('codeCountry')->unique();
            $table->string('country'); //Grupo Kaufmann
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });

        Schema::create('organization_levels', function(Blueprint $table) {
            $table->increments('levelsid')->unique();
            $table->string('nameLevel');
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });

        Schema::create('organization', function(Blueprint $table) {
            $table->increments('corporativoId')->unique();
            $table->char('codRelational', 10);
            $table->string('nameStructure'); //Grupo Kaufmann
            $table->integer('levelsid');  //Corporativo
            $table->integer('status');  //1
            $table->integer('idUser'); //
            $table->char('codFather', 10);
            $table->integer('idRegion')->nullable();
            $table->string('codeCountry')->nullable();
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
            $table->foreign('levelsid')->references('levelsid')->on('organization_levels');
            //$table->foreign('codeCountry')->references('codeCountry')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('organization');
    }

}
