<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_request', function(Blueprint $table) {
            $table->increments('idType')->unique();
            $table->string('request');
            $table->string('description');
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });

        Schema::create('requests', function(Blueprint $table) {
            $table->increments('idRequest')->unique();
            $table->integer('idUser');
            $table->string('description')->nullable();
            $table->string('note')->nullable();
            $table->integer('idStatus');
            $table->integer('idType');
            $table->date('dateStart');
            $table->date('dateEnd')->nullable();
            $table->string('timeStart', 10);
            $table->string('timeEnd', 10)->nullable();
            $table->string('medicine')->nullable();
            $table->string('prescription')->nullable();
            $table->string('members')->nullable();
            $table->string('purpose')->nullable();
            $table->string('userTo')->nullable();
            $table->text('file')->nullable();
            $table->text('fileNote')->nullable();
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
            $table->foreign('idUser')->references('idUser')->on('user');
            $table->foreign('idType')->references('idType')->on('type_request');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
