<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSolmanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_solman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUserSolman')->nullable();
            $table->string('email');
            $table->string('region')->nullable();
            $table->integer('codeRegion')->nullable();
            $table->string('pais')->nullable();
            $table->integer('idStatus')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_solman');
    }
}
