<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer', function (Blueprint $table) {
            $table->increments('idAnswer');
            $table->string('description');
            $table->integer('idQuestion');
            $table->integer('idRequest');
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
            $table->foreign('idQuestion')->references('idQuestion')->on('question');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer');
    }
}
