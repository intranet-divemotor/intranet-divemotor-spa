<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCongratulationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congratulation', function (Blueprint $table) {
            $table->increments('id')->first();
            $table->integer('idUser');
            $table->integer('idUsrCmt');
            $table->integer('parent')->nullable();
            $table->text('comment')->nullable();
            $table->boolean('like')->nullable();
            $table->boolean('congratulated')->nullable();
            $table->timestamps('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('congratulation');
    }
}
