<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('role', function (Blueprint $table) {
            $table->increments('idRole');
            $table->string('roleName');
            $table->string('modules')->nullable();
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });

        Schema::create('module', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('name');
            $table->boolean('active');
            $table->integer('parent');
            $table->integer('header');
            $table->dateTimeTz('created_at')->nullable();
            $table->dateTimeTz('updated_at')->nullable();
            $table->dateTimeTz('deleted_at')->nullable();
            $table->string('to');
            $table->boolean('internal');
            $table->string('icon');
            $table->string('system');
         //   $table->timestamps();
        });

        Schema::create('status', function (Blueprint $table) {
            $table->increments('Statusid');
            $table->integer('idStatus')->unique();
            $table->string('statusName');
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });

        Schema::create('user', function (Blueprint $table) {
            $table->increments('idUser');
            $table->string('name');
            $table->string('lastName');
            $table->string('mothersLastName')->nullable();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->integer('idStatus')->default(1);
            $table->integer('idRole')->nullable();
            $table->integer('corporativoId')->nullable();
            $table->string('avatar', 1000)->nullable();
            $table->date('birthdate')->nullable();
            $table->integer('intent')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });

        Schema::create('user_detail', function (Blueprint $table) {
            $table->integer('idUser');
            $table->integer('posicionHCM')->unique()->nullable();
            $table->integer('identityDocument')->unique()->nullable();
            $table->string('colaborador')->nullable();          //NOMBRE COMPLETO COLABORADOR
            $table->integer('codPosicion')->nullable();
            $table->string('desPosition')->nullable();         //Desc.Posición
            $table->integer('codCeco')->nullable();
            $table->string('desCeco')->nullable();

            $table->integer('codDivCorp')->nullable();
            $table->string('divCorp')->nullable();
            $table->integer('codDivCorpCountry')->nullable();
            $table->string('divCorpCountry')->nullable();

            $table->integer('codManagement')->nullable();
            $table->string('desManagement')->nullable();
            $table->integer('codSubManagement')->nullable();
            $table->string('desSubManagement')->nullable();
            $table->integer('codDepartment')->nullable();
            $table->string('desDepartment')->nullable();
            $table->integer('codArea')->nullable();
            $table->string('desArea')->nullable();
            $table->integer('codFuncion')->nullable();
            $table->string('desFuncion')->nullable();
            $table->integer('codAreaBusiness')->nullable();
            $table->string('desAreaBusiness')->nullable();
            $table->string('bossName')->nullable();
            $table->integer('codBoss')->nullable();
            $table->string('desBoss')->nullable();
            $table->date('dateAdmission')->nullable();
            $table->json('addresses')->nullable();
            $table->json('phones')->nullable();
            $table->string('divPersonal')->nullable();
            $table->string('desDivisions')->nullable();
            $table->integer('civilStatusId')->nullable();
            $table->integer('sexoId')->nullable();
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
            $table->foreign('idUser')->references('idUser')->on('user');
            $table->foreign('sexoId')->references('sexoId')->on('sexo');
            $table->foreign('civilStatusId')->references('civilStatusId')->on('civilStatus');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('user');
        Schema::drop('user_detail');
        Schema::drop('role');
        Schema::drop('status');
        schema::drop('module');

    }

}
