<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectoryContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //directorio 
        Schema::create('directory_contact', function (Blueprint $table) {
            $table->increments('directoryId')->unique();
            $table->integer('idUser')->nullable();
            $table->integer('idUserContact')->nullable();
            $table->integer('typeContact')->nullable();
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });
        
        //directorio 
        Schema::create('type_contact', function (Blueprint $table) {
            $table->increments('typeContact')->unique();
            $table->integer('idUser')->nullable();
            $table->string('name');
            $table->timestamps();
            $table->dateTimeTz('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('directory_contact');
        Schema::drop('type_contact');
    }
}
